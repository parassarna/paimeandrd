package app.pairme;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final int NOTIFICATION_ID = 1;
    String channelname = "Pairme";
    NotificationCompat.Builder notification;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.d("tag", "onMessageReceived: " + new Gson().toJson(remoteMessage.getData()));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            makeNotificationChannel(channelname, channelname, NotificationManager.IMPORTANCE_DEFAULT);
        }
        setNotificationMessage(remoteMessage.getData().get("text"), remoteMessage.getData().get("title"), remoteMessage.getData().get("type"));
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<JsonObject> call = service.updatetoken("Bearer " + Helper.getToken(this), s);
        call.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (!response.isSuccessful()) {
                    Log.d("error", "else: " + response.body());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d("error", "Failure; " + t);
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    void makeNotificationChannel(String id, String name, int importance) {
        NotificationChannel channel = new NotificationChannel(id, name, importance);
        channel.setShowBadge(true); // set false to disable badges, Oreo exclusive

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        assert notificationManager != null;
        notificationManager.createNotificationChannel(channel);
    }

    void setNotificationMessage(String message, String title, String type) {

        // make the channel. The method has been discussed before.

        // the check ensures that the channel will only be made
        // if the device is running Android 8+
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("type", type);
        PendingIntent pendingIntent=PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_ONE_SHOT);

        notification =
                new NotificationCompat.Builder(this, channelname);
        // the second parameter is the channel id.
        // it should be the same as passed to the makeNotificationChannel() method

        notification
                // can use any other icon
                .setContentTitle(title)
                .setContentIntent(pendingIntent)
                .setContentText(message); // this shows a number in the notification dots
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notification.setSmallIcon(R.drawable.app_icon);
        } else {
            notification.setSmallIcon(R.drawable.app_icon);
        }
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        assert notificationManager != null;
        notificationManager.notify(NOTIFICATION_ID, notification.build());
        // it is better to not use 0 as notification id, so used 1.
    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }
}