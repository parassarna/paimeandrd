package app.pairme.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.pairme.ClassesPojo.UserInfoPojo.Data;
import app.pairme.R;
import app.pairme.adapters.SearchAdapter;

public class MatchesFragment extends Fragment {
    View view;
    List<Data> data;
    private RecyclerView recyclerView;
    private SearchAdapter searchAdapter;
    private GridLayoutManager _sGridLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_matches, container, false);
        initControl();
        getAllList();
        return view;
    }

    private void initControl() {
        recyclerView = view.findViewById(R.id.recycler_search);
    }


    private void getAllList() {
        Type collectionType = new TypeToken<List<Data>>() {
        }.getType();
        data = new Gson().fromJson(getArguments().getString("data"), collectionType);
        _sGridLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(_sGridLayoutManager);
        searchAdapter = new SearchAdapter(getContext(), data);
        recyclerView.setAdapter(searchAdapter);
    }
}
