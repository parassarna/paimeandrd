package app.pairme.fragments;


import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.UserInfoPojo.UserInfo;
import app.pairme.R;
import app.pairme.activities.Activity_Explore;
import app.pairme.activities.EditProfile;
import app.pairme.activities.MediaActivity;
import app.pairme.activities.PairmePlusActivity;
import app.pairme.activities.ProgressProfile;
import app.pairme.activities.RequestAgent;
import app.pairme.activities.SettingScreen;
import app.pairme.activities.SupportActivity;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.pairme.Utils.imageloader;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFragment extends Fragment implements View.OnClickListener {
    private ImageView iv_banner;
    private String imageUrl, email, gender, name;
    private RecyclerView explore_rv;
    private ImageView iv_setting, iv_edit;
    private CircleImageView iv_profile;
    private TextView tv_name, tv_description;
    private RelativeLayout relative_pair_plus, relative_activity, relative_profile, relative_progress, relative_editProfile, relative_media,
            relative_support, relative_setting;
    private View view;

    public MenuFragment() {
        // Required empty public constructor
    }


    public static void setStatusBarGradiant(Activity activity, Drawable background) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_menu, container, false);
        return view;
    }

    private void initData() {
        initControl();
        email = Helper.getUserData(getContext()).getEmail();
        gender = Helper.getUserData(getContext()).getGender();
        name = Helper.getUserData(getContext()).getName();
        imageUrl = Helper.getUserData(getContext()).getProfileImage();
        if (imageUrl != null) {
            Glide.with(getActivity()).load(Helper.getUserData(getActivity()).getType() != null ? imageUrl : "http://pairme.co/img/userProfile/" + imageUrl).placeholder(imageloader(getActivity())).error(R.drawable.place_holder).into(iv_profile);
        } else {
            Glide.with(getActivity()).load(R.drawable.user_default).placeholder(imageloader(getActivity())).error(R.drawable.place_holder).into(iv_profile);
        }
        tv_name.setText(name);
        tv_description.setText(email);
    }

    @Override
    public void onResume() {
        super.onResume();
        setStatusBarGradiant(getActivity(), getResources().getDrawable(R.drawable.background_screen));
        initData();
    }

    private void initControl() {
        iv_setting = view.findViewById(R.id.iv_setting);
        iv_edit = view.findViewById(R.id.iv_edit);
        iv_profile = view.findViewById(R.id.iv_profile);
        tv_name = view.findViewById(R.id.tv_name);
        tv_description = view.findViewById(R.id.tv_description);
        relative_pair_plus = view.findViewById(R.id.relative_pair_plus);
        relative_activity = view.findViewById(R.id.relative_activity);
        relative_profile = view.findViewById(R.id.relative_profile);
        relative_progress = view.findViewById(R.id.relative_progress);
        relative_editProfile = view.findViewById(R.id.relative_editProfile);
        relative_media = view.findViewById(R.id.relative_media);
        relative_support = view.findViewById(R.id.relative_support);
        //  relative_setting = view.findViewById(R.id.relative_setting);
        iv_banner = view.findViewById(R.id.iv_banner);
        iv_setting.setOnClickListener(this);
        iv_edit.setOnClickListener(this);
        relative_pair_plus.setOnClickListener(this);
        relative_activity.setOnClickListener(this);
        relative_profile.setOnClickListener(this);
        relative_progress.setOnClickListener(this);
        relative_editProfile.setOnClickListener(this);
        relative_media.setOnClickListener(this);
        relative_support.setOnClickListener(this);
        //   relative_setting.setOnClickListener(this);
        iv_banner.setOnClickListener(this);
        iv_profile.setOnClickListener(this);
        Log.d("TAG", "initControlStatus: "+Helper.getUserData(getActivity()).getSubscriptionStatus());
        iv_banner.setVisibility(Helper.getUserData(getActivity()).getGender().equalsIgnoreCase("male")?View.VISIBLE:View.GONE);
        relative_pair_plus.setVisibility(Helper.getUserData(getActivity()).getGender().equalsIgnoreCase("male")?View.VISIBLE:View.GONE);
        if (Helper.getUserData(getActivity()).getSubscriptionStatus().equalsIgnoreCase("1")) {
            relative_pair_plus.setVisibility(View.GONE);
            iv_banner.setVisibility(View.GONE);
        }


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.iv_setting:
                // case R.id.relative_setting:
                startActivity(new Intent(getContext(), SettingScreen.class));
                break;
            case R.id.iv_edit:
                // case R.id.relative_editProfile:
                startActivity(new Intent(getContext(), EditProfile.class));
                break;
            case R.id.relative_editProfile:
                // case R.id.relative_editProfile:
                startActivity(new Intent(getContext(), RequestAgent.class));
                break;
            case R.id.relative_pair_plus:
            case R.id.iv_banner:
                PairmePlusActivity plusActivity = new PairmePlusActivity();
                plusActivity.show(((AppCompatActivity) (getContext())).getSupportFragmentManager(), PairmePlusActivity.TAG);
                break;
            case R.id.relative_activity:
                startActivity(new Intent(getContext(), Activity_Explore.class));
                break;
            case R.id.relative_profile:
            case R.id.iv_profile:
                UserInfoFragment myProfileActivity = new UserInfoFragment(Helper.getUserData(getActivity()), 6);
                myProfileActivity.show(((AppCompatActivity) getActivity()).getSupportFragmentManager(), UserInfoFragment.TAG);
                break;
            case R.id.relative_progress:
                startActivity(new Intent(getContext(), ProgressProfile.class));
                break;
            case R.id.relative_media:
                startActivity(new Intent(getContext(), MediaActivity.class));
                break;
            case R.id.relative_support:
                startActivity(new Intent(getContext(), SupportActivity.class));
                break;
        }

    }




    @Override
    public void onPause() {
        setStatusBarGradiant(getActivity(), null);
        super.onPause();
    }
}
