package app.pairme.fragments;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.Matches;
import app.pairme.MainActivity;
import app.pairme.MyApplication;
import app.pairme.R;
import app.pairme.adapters.ChatingAdapter;
import app.pairme.adapters.MatcheschatAdapter;
import app.pairme.adapters.SwipeToDeleteCallback;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends Fragment implements SwipeToDeleteCallback.RecyclerItemTouchHelperListener {


    public Emitter.Listener handleIncomingMessages = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Log.d("TAG", "run notification: " + data);

                }
            });
        }
    };
    RecyclerView match_rv, chats_rv;
    View view;
    TextView more_tv, tv_go, tv_noRequest;
    LinearLayout empty_ll;
    boolean isConnected;
    MyApplication app;
    Socket socket;
    String id;
    ChatingAdapter chatBoxAdapter;
    JSONArray userarray;
    Emitter.Listener getUsers = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            final JSONObject jsonObject = (JSONObject) args[0];
            if (getActivity() != null)
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            userarray = jsonObject.getJSONArray("chatList");
                            Log.d("TAG", "run: " + userarray);
                            if (userarray.length() > 0) {
                                empty_ll.setVisibility(View.GONE);
                                chatBoxAdapter = new ChatingAdapter(getActivity(), userarray, socket);
                                chats_rv.setAdapter(chatBoxAdapter);
                            } else {
                                empty_ll.setVisibility(View.VISIBLE);
                                chats_rv.setVisibility(View.GONE);
                            }
                            ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new SwipeToDeleteCallback(0, ItemTouchHelper.LEFT, ChatFragment.this);
                            new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(chats_rv);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        }
    };

    public ChatFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_chat, container, false);
        match_rv = view.findViewById(R.id.match_rv);
        chats_rv = view.findViewById(R.id.chats_rv);
        more_tv = view.findViewById(R.id.more_tv);
        tv_go = view.findViewById(R.id.tv_go);
        tv_noRequest = view.findViewById(R.id.tv_noRequest);
        empty_ll = view.findViewById(R.id.empty_ll);
        ((MainActivity) getActivity()).badgechat.setVisibility(View.GONE);
        ((MainActivity) getActivity()).badgechat_active.setVisibility(View.GONE);
        app = (MyApplication) getActivity().getApplication();

        socket = app.getSocket();
        socket.connect();
        socket.on("chatListRes", getUsers);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("fromUserId", Helper.getUserData(getActivity()).getId());
            jsonObject.put("toUserId", Helper.getUserData(getActivity()).getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        socket.emit("getMessages", jsonObject);
        socket.emit("chatList", Helper.getUserData(getActivity()).getId());
        socket.emit("getnotification", Helper.getUserData(getActivity()).getId());
        match_rv.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
        chats_rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        tv_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).linear_discover_inactive.performClick();
            }
        });
        getMatches();
        return view;
    }

    private void getMatches() {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<Matches> call = service.getmatchesList("Bearer " + Helper.getToken(getActivity()));
        call.enqueue(new Callback<Matches>() {
            @Override
            public void onResponse(Call<Matches> call, final Response<Matches> response) {
                if (response.body().getSuccess())
                    more_tv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Bundle bundle = new Bundle();
                            bundle.putString("data", new Gson().toJson(response.body().data));
                            MatchesFragment chatFragment = new MatchesFragment();
                            chatFragment.setArguments(bundle);
                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame, chatFragment).commit();
                        }
                    });
                match_rv.setAdapter(new MatcheschatAdapter(getActivity(), response.body().data));
            }

            @Override
            public void onFailure(Call<Matches> call, Throwable t) {

            }
        });

    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof ChatingAdapter.MyViewHolder) {
            try {
                removeItem(userarray.getJSONObject(position).getString("id"), position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            chatBoxAdapter.removeItem(viewHolder.getAdapterPosition());
        }

    }

    private void removeItem(String id, final int pos) {
        Helper.showLoader(getActivity(), "Please wait..");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<JsonObject> call = service.deletechat("Bearer " + Helper.getToken(getActivity()), id);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, final Response<JsonObject> response) {
                chatBoxAdapter.notifyDataSetChanged();
                Helper.hideLoader();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Helper.hideLoader();
            }
        });
    }
}
