package app.pairme.fragments;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.RequestPojo;
import app.pairme.R;
import app.pairme.adapters.RequetsDataAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class LikedFragment extends Fragment {


    View view;
    TextView title_tv;
    RelativeLayout null_rl;
    RecyclerView requests_rv;

    int type;

    public LikedFragment(int adapterPosition) {
        type = adapterPosition;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_liked, container, false);
        initViews();
        return view;
    }

    private void initViews() {
        null_rl = view.findViewById(R.id.null_rl);
        title_tv = view.findViewById(R.id.title_tv);
        requests_rv = view.findViewById(R.id.requests_rv);
        title_tv.setText(type == 1 ? "Liked You" : "Checked You");

        requests_rv.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        inflateData();
    }

    private void inflateData() {

        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<RequestPojo> call = service.requestsData("Bearer " + Helper.getToken(getContext()));
        call.enqueue(new Callback<RequestPojo>() {

            @Override
            public void onResponse(Call<RequestPojo> call, Response<RequestPojo> response) {
                if (response.isSuccessful()) {
                    Log.d("tag", "onResponse: " + response.body().likedYou.size());
                    if (type == 1 && response.body().likedYou.size() >0 ) {
                        requests_rv.setVisibility(View.VISIBLE);
                        null_rl.setVisibility(View.GONE);
                    } else if (response.body().checked.size() >0 ) {
                        requests_rv.setVisibility(View.VISIBLE);
                        null_rl.setVisibility(View.GONE);
                    } else {
                        requests_rv.setVisibility(View.GONE);
                        null_rl.setVisibility(View.VISIBLE);
                    }
                    requests_rv.setAdapter(new RequetsDataAdapter(getActivity(), type == 1 ? response.body().likedYou : response.body().checked, 3));
                } else {
                    Log.d("error", "else: " + response.body());
                }
            }

            @Override
            public void onFailure(Call<RequestPojo> call, Throwable t) {
                Log.d("error", "Failure; " + t);
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
