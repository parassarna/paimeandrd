package app.pairme.fragments;


import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.BlockPojo;
import app.pairme.ClassesPojo.Infodata;
import app.pairme.ClassesPojo.LikedislikePojo;
import app.pairme.ClassesPojo.UserInfoPojo.Data;
import app.pairme.ClassesPojo.UserInfoPojo.PhotoAlbum;
import app.pairme.MainActivity;
import app.pairme.MyApplication;
import app.pairme.R;
import app.pairme.activities.ChatActivity;
import app.pairme.activities.ContactUs;
import app.pairme.activities.EditProfile;
import app.pairme.adapters.AlbumAdapter;
import app.pairme.adapters.UpdatHeightAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.pairme.Utils.getArabic;
import static app.pairme.Utils.imageloader;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserInfoFragment extends BottomSheetDialogFragment implements View.OnClickListener {
    public static final String TAG = "UserInfoFragment";
    private final long[] durations = new long[]{
            500L, 1000L, 1500L, 4000L, 5000L, 1000,
    };
    public List<String> getPhotoAlbum = new ArrayList<>();
    public List<Infodata.Data> report = new ArrayList<>();
    ViewPager view_pager;
    RecyclerView photo_rv;
    Data userdata;
    ProgressBar progressBar_cyclic;
    boolean name, request, lifestyle, interests, appearance, photos, about, basics, ss;
    int type = -1;
    String id;
    FrameLayout match_fl;
    ScrollView scollview;
    RecyclerView RecyclerView;
    boolean updated;
    LinearLayout layoutDots, bottom_sheet;
    HashMap<String, String> named = new HashMap<>();
    List<Data> data = new ArrayList<>();
    private TextView tv_smoking, tv_eating_habits, tv_photocounts,
            tv_exercise_habits, tv_sleep_habits, tv_pets, tv_family_values, tv_polygamy_opinion, tv_personality1, tv_personality2, tv_personality3, tv_languages, tv_languages2, tv_languages3, tv_languages4, tv_languages5,
            tv_hobbies, tv_hobbies2, tv_hobbies3, tv_cuisine, tv_cuisine2, tv_cuisine3, tv_music, tv_music2, tv_music3, tv_movies, tv_movies2, tv_movies3, tv_sports, tv_sports2, tv_sports3;
    private GridLayoutManager _sGridLayoutManager;
    private long pressTime = 0L;
    private long limit = 500L;
    private RelativeLayout age_rl, look_rl, city_rl, origin_rl, education_rl, occupation_rl, sector_rl, status_rl, kids_rl, hvkids_rl, ls_rl, relocate_rl, appearance_rl, body_rl, complextion_rl, hstatus_rl, hair_rl, hairlength_rl, type_rl, eye_rl, eyewear_rl, makeup_rl, weapon_rl,
            style_rl, smoking_rl, eating_rl, exc_rl, sleep_rl, pets_rl, family_rl, polygamy_rl, person_rl, lang_rl, hobies_rl, cuisine_rl, music_rl, movies_rl, sports_rl;
    private LinearLayout ll_block, hi_ll, ll_edit, ll_cancel;
    private View view, reverse, skip;
    private CardView name_cv, request_cv, lifestyle_cv, interests_cv, appearance_cv, about_cv, basics_cv, ss_cv, photo_cv;
    private Dialog dialog;
    private ImageView down_iv, menu_iv, cross_btn, heart_btn, iv_profile, self_iv,otheruser_iv;
    private LinearLayout ll_report, buttons_ll;
    private TextView request_tv, requestnote_tv, name_tv, kids_tv, adress_tv, body_tv, living_tv, relocate_tv, appearance_tv, wantkids_tv, about_tv, status_tv, sector_tv, occupation_tv, education_tv, age_tv, look_tv, city_tv, town_tv, login_tv, complextion_tv, hstatus_tv, hair_tv, length_tv, hairtype_tv, eye_tv, eyewear_tv, makeup_tv, weapon_tv, style_tv;
    private int counter = 0;


    public UserInfoFragment(Data id, int type) {
        this.type = type;
        this.userdata = id;
        Log.d(TAG, "UserInfoFragment: " + new Gson().toJson(userdata));
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void setWhiteNavigationBar(@NonNull Dialog dialog) {
        Window window = dialog.getWindow();
        if (window != null) {
            DisplayMetrics metrics = new DisplayMetrics();
            window.getWindowManager().getDefaultDisplay().getMetrics(metrics);

            GradientDrawable dimDrawable = new GradientDrawable();
            // ...customize your dim effect here

            GradientDrawable navigationBarDrawable = new GradientDrawable();
            navigationBarDrawable.setShape(GradientDrawable.RECTANGLE);
            navigationBarDrawable.setColor(Color.WHITE);

            Drawable[] layers = {dimDrawable, navigationBarDrawable};

            LayerDrawable windowBackground = new LayerDrawable(layers);
            windowBackground.setLayerInsetTop(1, metrics.heightPixels);

            window.setBackgroundDrawable(windowBackground);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_user_info, container, false);
        checkedProfile();
        initViews();
        return view;
    }


    private void checkedProfile() {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<JsonObject> call = service.checkedProfile("Bearer " + Helper.getToken(getActivity()), userdata.getId().toString());
        call.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {

                } else {

                    Log.d("error", "else: " + response.body());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                Log.d("error", "Failure; " + t);
            }
        });
    }

    private void inflateReport() {
        Infodata.Data data = new Infodata.Data();
        data.name = getResources().getString(R.string.Fake_User);
        report.add(data);
        data = new Infodata.Data();
        data.name = getResources().getString(R.string.Harassment);
        report.add(data);
        data = new Infodata.Data();
        data.name = getResources().getString(R.string.Inappropriate);
        report.add(data);
        data = new Infodata.Data();
        data.name = getResources().getString(R.string.Scam);
        report.add(data);
        data = new Infodata.Data();
        data.name = getResources().getString(R.string.Underage);
        report.add(data);
        data = new Infodata.Data();
        data.name = getResources().getString(R.string.other);
        report.add(data);
    }


    private void initViews() {
        request_cv = view.findViewById(R.id.request_cv);
        if (type == 1 || type == 2 || type == 7)
            request_cv.setVisibility(View.VISIBLE);
        ll_block = view.findViewById(R.id.ll_block);
        ll_report = view.findViewById(R.id.ll_report);
        hi_ll = view.findViewById(R.id.hi_ll);
        ll_edit = view.findViewById(R.id.ll_edit);
        ll_cancel = view.findViewById(R.id.ll_cancel);
        view_pager = view.findViewById(R.id.view_pager);
        hobies_rl = view.findViewById(R.id.hobies_rl);
        tv_photocounts = view.findViewById(R.id.tv_photocounts);
        photo_cv = view.findViewById(R.id.photo_cv);
        photo_rv = view.findViewById(R.id.photo_rv);
        request_tv = view.findViewById(R.id.request_tv);
        self_iv = view.findViewById(R.id.self_iv);
        match_fl = view.findViewById(R.id.match_fl);
        scollview = view.findViewById(R.id.scollview);
        requestnote_tv = view.findViewById(R.id.requestnote_tv);
        cuisine_rl = view.findViewById(R.id.cuisine_rl);
        music_rl = view.findViewById(R.id.music_rl);
        bottom_sheet = view.findViewById(R.id.bottom_sheet);
        movies_rl = view.findViewById(R.id.movies_rl);
        reverse = view.findViewById(R.id.reverse);
        skip = view.findViewById(R.id.skip);
        sports_rl = view.findViewById(R.id.sports_rl);
        appearance_rl = view.findViewById(R.id.appearance_rl);
        body_rl = view.findViewById(R.id.body_rl);
        lifestyle_cv = view.findViewById(R.id.lifestyle_cv);
        complextion_rl = view.findViewById(R.id.complextion_rl);
        hstatus_rl = view.findViewById(R.id.hstatus_rl);
        hair_rl = view.findViewById(R.id.hair_rl);
        hairlength_rl = view.findViewById(R.id.hairlength_rl);
        type_rl = view.findViewById(R.id.type_rl);
        eye_rl = view.findViewById(R.id.eye_rl);
        eyewear_rl = view.findViewById(R.id.eyewear_rl);
        makeup_rl = view.findViewById(R.id.makeup_rl);
        otheruser_iv = view.findViewById(R.id.otheruser_iv);
        interests_cv = view.findViewById(R.id.interests_cv);
        weapon_rl = view.findViewById(R.id.weapon_rl);
        style_rl = view.findViewById(R.id.style_rl);
        down_iv = view.findViewById(R.id.down_iv);
        name_cv = view.findViewById(R.id.name_cv);
        menu_iv = view.findViewById(R.id.menu_iv);
        basics_cv = view.findViewById(R.id.basics_cv);
        ss_cv = view.findViewById(R.id.ss_cv);
        name_tv = view.findViewById(R.id.name_tv);
        kids_tv = view.findViewById(R.id.kids_tv);
        smoking_rl = view.findViewById(R.id.smoking_rl);
        eating_rl = view.findViewById(R.id.eating_rl);
        exc_rl = view.findViewById(R.id.exc_rl);
        sleep_rl = view.findViewById(R.id.sleep_rl);
        pets_rl = view.findViewById(R.id.pets_rl);
        family_rl = view.findViewById(R.id.family_rl);
        polygamy_rl = view.findViewById(R.id.polygamy_rl);
        person_rl = view.findViewById(R.id.person_rl);
        lang_rl = view.findViewById(R.id.lang_rl);
        about_cv = view.findViewById(R.id.about_cv);
        education_rl = view.findViewById(R.id.education_rl);
        occupation_rl = view.findViewById(R.id.occupation_rl);
        sector_rl = view.findViewById(R.id.sector_rl);
        status_rl = view.findViewById(R.id.status_rl);
        kids_rl = view.findViewById(R.id.kids_rl);
        appearance_cv = view.findViewById(R.id.appearance_cv);
        hvkids_rl = view.findViewById(R.id.hvkids_rl);
        ls_rl = view.findViewById(R.id.ls_rl);
        relocate_rl = view.findViewById(R.id.relocate_rl);
        iv_profile = view.findViewById(R.id.user_iv);
        wantkids_tv = view.findViewById(R.id.wantkids_tv);
        adress_tv = view.findViewById(R.id.adress_tv);
        cross_btn = view.findViewById(R.id.cross_btn);
        heart_btn = view.findViewById(R.id.heart_btn);
        buttons_ll = view.findViewById(R.id.buttons_ll);
        age_rl = view.findViewById(R.id.age_rl);
        look_rl = view.findViewById(R.id.look_rl);
        city_rl = view.findViewById(R.id.city_rl);
        origin_rl = view.findViewById(R.id.origin_rl);
        about_tv = view.findViewById(R.id.about_tv);
        age_tv = view.findViewById(R.id.age_tv);
        look_tv = view.findViewById(R.id.look_tv);
        body_tv = view.findViewById(R.id.body_tv);
        appearance_tv = view.findViewById(R.id.appearance_tv);
        city_tv = view.findViewById(R.id.city_tv);
        town_tv = view.findViewById(R.id.town_tv);
        occupation_tv = view.findViewById(R.id.occupation_tv);
        sector_tv = view.findViewById(R.id.sector_tv);
        login_tv = view.findViewById(R.id.login_tv);
        education_tv = view.findViewById(R.id.education_tv);
        status_tv = view.findViewById(R.id.status_tv);
        living_tv = view.findViewById(R.id.living_tv);
        relocate_tv = view.findViewById(R.id.relocate_tv);
        complextion_tv = view.findViewById(R.id.complextion_tv);
        hstatus_tv = view.findViewById(R.id.hstatus_tv);
        hair_tv = view.findViewById(R.id.hair_tv);
        length_tv = view.findViewById(R.id.length_tv);
        hairtype_tv = view.findViewById(R.id.hairtype_tv);
        eye_tv = view.findViewById(R.id.eye_tv);
        eyewear_tv = view.findViewById(R.id.eyewear_tv);
        makeup_tv = view.findViewById(R.id.makeup_tv);
        weapon_tv = view.findViewById(R.id.weapon_tv);
        style_tv = view.findViewById(R.id.style_tv);
        layoutDots = view.findViewById(R.id.layoutDots);
        tv_smoking = view.findViewById(R.id.tv_smoking);
        tv_eating_habits = view.findViewById(R.id.tv_eating_habits);
        tv_exercise_habits = view.findViewById(R.id.tv_exercise_habits);
        tv_sleep_habits = view.findViewById(R.id.tv_sleep_habits);
        tv_pets = view.findViewById(R.id.tv_pets);
        tv_family_values = view.findViewById(R.id.tv_family_values);
        tv_polygamy_opinion = view.findViewById(R.id.tv_polygamy_opinion);
        tv_personality1 = view.findViewById(R.id.tv_personality1);
        tv_personality2 = view.findViewById(R.id.tv_personality2);
        tv_personality3 = view.findViewById(R.id.tv_personality3);
        tv_languages = view.findViewById(R.id.tv_languages1);
        tv_languages2 = view.findViewById(R.id.tv_languages2);
        tv_languages3 = view.findViewById(R.id.tv_languages3);
        tv_languages4 = view.findViewById(R.id.tv_languages4);
        tv_languages5 = view.findViewById(R.id.tv_languages5);
        tv_hobbies = view.findViewById(R.id.tv_hobbies1);
        tv_hobbies2 = view.findViewById(R.id.tv_hobbies2);
        tv_hobbies3 = view.findViewById(R.id.tv_hobbies3);
        tv_cuisine = view.findViewById(R.id.tv_cuisine1);
        tv_cuisine2 = view.findViewById(R.id.tv_cuisine2);
        tv_cuisine3 = view.findViewById(R.id.tv_cuisine3);
        tv_music = view.findViewById(R.id.tv_music1);
        tv_music2 = view.findViewById(R.id.tv_music2);
        tv_music3 = view.findViewById(R.id.tv_music3);
        tv_movies = view.findViewById(R.id.tv_movies1);
        tv_movies2 = view.findViewById(R.id.tv_movies2);
        tv_movies3 = view.findViewById(R.id.tv_movies3);
        tv_sports = view.findViewById(R.id.tv_sports1);
        tv_sports2 = view.findViewById(R.id.tv_sports2);
        tv_sports3 = view.findViewById(R.id.tv_sports3);
        if (Helper.getLanguage(getActivity()).equalsIgnoreCase("ar")) {
            request_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            requestnote_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            name_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            adress_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            age_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            look_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            city_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            town_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            education_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            occupation_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            sector_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            status_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            kids_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            wantkids_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            living_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            relocate_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            appearance_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            body_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            complextion_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            hstatus_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            hair_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            length_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            hairtype_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            eye_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            eyewear_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            makeup_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            weapon_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            style_tv.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            tv_smoking.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            tv_eating_habits.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            tv_exercise_habits.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            tv_sleep_habits.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            tv_pets.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            tv_family_values.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            tv_polygamy_opinion.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
        }
        inflateReport();
        if (type == 5 || type == 1 || type == 3 || type == 0 || type == 7 || type == 2) {
            cross_btn.setImageResource(R.drawable.chat_grad);
        } else if (type == 6) {
            ll_block.setVisibility(View.GONE);
            ll_report.setVisibility(View.GONE);
            ll_edit.setVisibility(View.VISIBLE);
            buttons_ll.setVisibility(View.GONE);
        }
        initClicks();
        getUserdata();
        scollview.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                if (scollview != null) {
                    Rect mReact = new Rect();
                    scollview.getHitRect(mReact);
                    if (!photo_cv.getLocalVisibleRect(mReact) && !photos) {
                        slideUp(photo_cv);
                    } else if (!basics_cv.getLocalVisibleRect(mReact) && !basics) {
                        slideUp(basics_cv);
                    } else if (!ss_cv.getLocalVisibleRect(mReact) && !ss) {
                        slideUp(ss_cv);
                    } else if (!appearance_cv.getLocalVisibleRect(mReact) && !appearance) {
                        slideUp(appearance_cv);
                    } else if (!lifestyle_cv.getLocalVisibleRect(mReact) && !lifestyle) {
                        slideUp(lifestyle_cv);
                    } else if (!interests_cv.getLocalVisibleRect(mReact) && !interests) {
                        slideUp(interests_cv);
                    }
                }
            }
        });
    }

    private void getUserdata() {

        if (type != 4 && userdata.is_like) {
            sayhi();
        }

        if (type == 1 ) {
            request_tv.setText(userdata.getName() + " " + getResources().getString(R.string.liked_you));
            requestnote_tv.setText(getResources().getString(R.string.interested_like_her_back_to_match));
            sayhi();
        } else if (type == 2 || type == 7|| type == 0) {

            match_fl.setVisibility(View.VISIBLE);
            Glide.with(getActivity()).load("http://pairme.co/img/userProfile/" + Helper.getUserData(getActivity()).getProfileImage()).error(userdata.getGender().equalsIgnoreCase("male") ? R.drawable.place_holder_m : R.drawable.place_holder_f).into(self_iv);
            Glide.with(getActivity()).load("http://pairme.co/img/userProfile/" + userdata.getProfileImage()).error(userdata.getGender().equalsIgnoreCase("male") ? R.drawable.place_holder_m : R.drawable.place_holder_f).into(otheruser_iv);

            request_tv.setText(userdata.getName() + " " + getResources().getString(R.string.checked_you_out));
            requestnote_tv.setText(getResources().getString(R.string.interested_like_her_like));
            if (type == 7) {
                request_tv.setText(getResources().getString(R.string.matched) + " " + userdata.getName());
                requestnote_tv.setText(getResources().getString(R.string.say_hi));
                sayhi();
            } else if (type == 0) {
                request_tv.setText(userdata.getName() + " " + getString(R.string.messaged));
                requestnote_tv.setText(getResources().getString(R.string.say_hi));
            }
        }
        if (userdata.getProfileImage() != null) {
            getPhotoAlbum.add("http://pairme.co/img/userProfile/" + userdata.getProfileImage());
        }

        if (userdata.getPhotoAlbum() == null || userdata.getPhotoAlbum().size() == 0) {
            photos = true;
            photo_cv.setVisibility(View.GONE);
        } else {
            for (PhotoAlbum getPhotoAlbums : userdata.getPhotoAlbum()) {
                getPhotoAlbum.add("http://pairme.co/img/userAlbum/" + getPhotoAlbums.getImage());
            }
            String count = String.valueOf(getPhotoAlbum.size());
            tv_photocounts.setText(getResources().getString(R.string.photo_album) + " " + count);
            _sGridLayoutManager = new GridLayoutManager(getActivity(), 2);
            photo_rv.setLayoutManager(_sGridLayoutManager);
            photo_rv.setAdapter(new AlbumAdapter(getActivity(), getPhotoAlbum, 1, view_pager, scollview));
        }
        addbottomdots(counter);
        if (userdata.getName() != null) {
            name_tv.setText(userdata.getName());
            adress_tv.setText(userdata.getLivesIn());
        } else {
            name = true;
        }
        if (userdata.getAboutMeContent() == null) {
            about = true;
            about_cv.setVisibility(View.GONE);
        } else {
            about_tv.setText(userdata.getAboutMeContent());
        }
        if (userdata.getAge() == null && userdata.getLookingFor() == null && userdata.getLivesIn() == null && userdata.getOrginallyFrom() == null) {
            basics = true;
            basics_cv.setVisibility(View.GONE);
        } else {
            if (userdata.getAge() == null)
                age_rl.setVisibility(View.GONE);
            else {
                age_tv.setText(userdata.getAge());
            }
            if (userdata.getLookingFor() == null)
                look_rl.setVisibility(View.GONE);
            else
                look_tv.setText(getArabic(getActivity(), userdata.getLookingFor().trim()));
            if (userdata.getLivesIn() == null)
                city_rl.setVisibility(View.GONE);
            else city_tv.setText(userdata.getLivesIn());
            if (userdata.getOrginallyFrom() == null)
                origin_rl.setVisibility(View.GONE);
            else town_tv.setText(getArabic(getActivity(), userdata.getOrginallyFrom().trim()));
            // else town_tv.setText(getArabic(getActivity(), userdata.getOrginallyFrom().trim()));
        }
        if (userdata.getEducation() == null && userdata.getOccupation() == null && userdata.getJobSector() == null && userdata.getRelationship() == null && userdata.getHaveKids() == null && userdata.getWantKids() == null && userdata.getLivingSituation() == null && userdata.getWillingToRelocate() == null) {
            ss = true;
            ss_cv.setVisibility(View.GONE);
        } else {
            if (userdata.getEducation() == null)
                education_rl.setVisibility(View.GONE);
            else
                education_tv.setText(getArabic(getActivity(), userdata.getEducation()));
            ;
            if (userdata.getOccupation() == null)
                occupation_rl.setVisibility(View.GONE);
            else
                occupation_tv.setText(getArabic(getActivity(), userdata.getOccupation()));
            if (userdata.getJobSector() == null)
                sector_rl.setVisibility(View.GONE);
            else
                sector_tv.setText(getArabic(getActivity(), userdata.getJobSector()));
            ;
            if (userdata.getRelationship() == null)
                status_rl.setVisibility(View.GONE);
            else
                status_tv.setText(getArabic(getActivity(), userdata.getRelationship()));
            ;
            if (userdata.getHaveKids() == null)
                kids_rl.setVisibility(View.GONE);
            else
                kids_tv.setText(getArabic(getActivity(), userdata.getHaveKids()));
            if (userdata.getWantKids() == null)
                hvkids_rl.setVisibility(View.GONE);
            else
                wantkids_tv.setText(getArabic(getActivity(), userdata.getWantKids()));
            if (userdata.getLivingSituation() == null)
                ls_rl.setVisibility(View.GONE);
            else
                living_tv.setText(getArabic(getActivity(), userdata.getLivingSituation()));
            if (userdata.getWillingToRelocate() == null)
                relocate_rl.setVisibility(View.GONE);
            else
                relocate_tv.setText(getArabic(getActivity(), userdata.getWillingToRelocate()));
        }
        if (userdata.getAppearance() == null && userdata.getBodyType() == null && userdata.getComplexion() == null && userdata.getHealthStatus() == null && userdata.getHairColor() == null && userdata.getHairLength() == null && userdata.getHairType() == null && userdata.getEyeColor() == null && userdata.getEyeWear() == null && userdata.getMakeup() == null && userdata.getSecretWeapon() == null && userdata.getStyle() == null) {
            appearance = true;
            appearance_cv.setVisibility(View.GONE);
        } else {
            if (userdata.getAppearance() != null)
                appearance_tv.setText(getArabic(getActivity(), userdata.getAppearance()));
            else appearance_rl.setVisibility(View.GONE);
            if (userdata.getBodyType() != null)
                body_tv.setText(getArabic(getActivity(), userdata.getBodyType()));
            else body_rl.setVisibility(View.GONE);
            if (userdata.getComplexion() != null)
                complextion_tv.setText(getArabic(getActivity(), userdata.getComplexion()));
            else complextion_rl.setVisibility(View.GONE);
            if (userdata.getHealthStatus() != null)
                hstatus_tv.setText(getArabic(getActivity(), userdata.getHealthStatus()));
            else hstatus_rl.setVisibility(View.GONE);
            if (userdata.getHairColor() != null)
                hair_tv.setText(getArabic(getActivity(), userdata.getHairColor()));
            else hair_rl.setVisibility(View.GONE);
            if (userdata.getHairLength() != null)
                length_tv.setText(getArabic(getActivity(), userdata.getHairLength()));
            else hairlength_rl.setVisibility(View.GONE);
            if (userdata.getHairType() != null)
                hairtype_tv.setText(getArabic(getActivity(), userdata.getHairType()));
            else type_rl.setVisibility(View.GONE);
            if (userdata.getEyeColor() != null)
                eye_tv.setText(getArabic(getActivity(), userdata.getEyeColor()));
            else eye_rl.setVisibility(View.GONE);
            if (userdata.getEyeWear() != null)
                eyewear_tv.setText(getArabic(getActivity(), userdata.getEyeWear()));
            else eyewear_rl.setVisibility(View.GONE);
            if (userdata.getMakeup() != null)
                makeup_tv.setText(getArabic(getActivity(), userdata.getMakeup()));
            else makeup_rl.setVisibility(View.GONE);
            if (userdata.getSecretWeapon() != null)
                weapon_tv.setText(getArabic(getActivity(), userdata.getSecretWeapon()));
            else weapon_rl.setVisibility(View.GONE);
            if (userdata.getStyle() != null)
                style_tv.setText(getArabic(getActivity(), userdata.getStyle()));
            else style_rl.setVisibility(View.GONE);

        }
        if (userdata.getSmoking() == null && userdata.getEatingHabits() == null && userdata.getExerciseHabits() == null && userdata.getSleepHabits() == null && userdata.getPets() == null && userdata.getFamilyValues() == null && userdata.getPolygamyOpinion() == null && userdata.getPersonality() == null && userdata.getLanguages() == null) {
            lifestyle = true;
            lifestyle_cv.setVisibility(View.GONE);
        } else {
            if (userdata.getSmoking() == null)
                smoking_rl.setVisibility(View.GONE);
            else tv_smoking.setText(getArabic(getActivity(), userdata.getSmoking()));
            if (userdata.getEatingHabits() == null)
                eating_rl.setVisibility(View.GONE);
            else
                tv_eating_habits.setText(getArabic(getActivity(), userdata.getEatingHabits()));
            if (userdata.getExerciseHabits() == null)
                exc_rl.setVisibility(View.GONE);
            else
                tv_exercise_habits.setText(getArabic(getActivity(), userdata.getExerciseHabits()));
            if (userdata.getSleepHabits() == null)
                sleep_rl.setVisibility(View.GONE);
            else tv_sleep_habits.setText(getArabic(getActivity(), userdata.getSleepHabits()));
            if (userdata.getPets() == null)
                pets_rl.setVisibility(View.GONE);
            else tv_pets.setText(getArabic(getActivity(), userdata.getPets()));
            if (userdata.getFamilyValues() == null)
                family_rl.setVisibility(View.GONE);
            else tv_family_values.setText(getArabic(getActivity(), userdata.getFamilyValues()));
            if (userdata.getPolygamyOpinion() == null)
                polygamy_rl.setVisibility(View.GONE);
            else
                tv_polygamy_opinion.setText(getArabic(getActivity(), userdata.getPolygamyOpinion()));
            if (userdata.getPersonality() == null)
                person_rl.setVisibility(View.GONE);
            else setData("tv_personality", userdata.getPersonality());
            if (userdata.getLanguages() == null)
                lang_rl.setVisibility(View.GONE);
            else setData("tv_languages", userdata.getLanguages());
        }
        if (userdata.getHobbies() == null && userdata.getCuisine() == null && userdata.getMovies() == null && userdata.getMusic() == null && userdata.getSports() == null) {
            interests = true;
            interests_cv.setVisibility(View.GONE);
        } else {
            if (userdata.getHobbies() == null)
                hobies_rl.setVisibility(View.GONE);
            else setData("tv_hobbies", userdata.getHobbies());
            if (userdata.getCuisine() == null)
                cuisine_rl.setVisibility(View.GONE);
            else setData("tv_cuisine", userdata.getCuisine());
            if (userdata.getMusic() == null)
                music_rl.setVisibility(View.GONE);
            else setData("tv_music", userdata.getMusic());
            if (userdata.getMovies() == null)
                movies_rl.setVisibility(View.GONE);
            else setData("tv_movies", userdata.getMovies());
            if (userdata.getSports() == null)
                sports_rl.setVisibility(View.GONE);
            else setData("tv_sports", userdata.getSports());
        }

    }

    private void sayhi() {
        heart_btn.setVisibility(View.GONE);
        cross_btn.setVisibility(View.GONE);
        hi_ll.setVisibility(View.VISIBLE);
    }

    private void addbottomdots(int pos) {
        if (getPhotoAlbum.size() > 0) {
            Glide.with(getActivity()).load(getPhotoAlbum.get(counter)).placeholder(imageloader(getActivity())).error(userdata.getGender().equalsIgnoreCase("male") ? R.drawable.place_holder_m : R.drawable.place_holder_f).into(iv_profile);
            layoutDots.removeAllViews();
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1.0f);
            param.setMargins(5, 0, 5, 0);
            View[] dots = new View[getPhotoAlbum.size()];
            for (int i = 0; i < getPhotoAlbum.size(); i++) {
                dots[i] = new View(getActivity());
                dots[i].setBackgroundColor(Color.GRAY);
                dots[i].setLayoutParams(param);
                layoutDots.addView(dots[i]);
            }
            dots[pos].setBackgroundColor(Color.WHITE);
        } else {
            Glide.with(getActivity()).load(userdata.getGender().equalsIgnoreCase("male") ? R.drawable.place_holder_m : R.drawable.place_holder_f).error(userdata.getGender().equalsIgnoreCase("male") ? R.drawable.place_holder_m : R.drawable.place_holder_f).into(iv_profile);
        }
    }

    private void initClicks() {
        down_iv.setOnClickListener(this);
        cross_btn.setOnClickListener(this);
        heart_btn.setOnClickListener(this);
        reverse.setOnClickListener(this);
        ll_cancel.setOnClickListener(this);
        skip.setOnClickListener(this);
        menu_iv.setOnClickListener(this);
        ll_edit.setOnClickListener(this);
        ll_block.setOnClickListener(this);
        ll_report.setOnClickListener(this);
        hi_ll.setOnClickListener(this);
    }

    private void setData(String tv_personality, String personality) {
        try {
            String[] data = personality.split(",");
            for (int i = 0; i < data.length; i++) {
                String id = tv_personality + (i + 1);
                int resID = getResources().getIdentifier(id, "id", getActivity().getPackageName());
                ((TextView) view.findViewById(resID)).setText(getArabic(getActivity(), data[i]));
                ((TextView) view.findViewById(resID)).setVisibility(View.VISIBLE);
                if (Helper.getLanguage(getActivity()).equalsIgnoreCase("ar"))
                    ((TextView) view.findViewById(resID)).setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (scollview.getVisibility() != View.VISIBLE && keyCode == KeyEvent.KEYCODE_BACK) {
                    scollview.setVisibility(View.VISIBLE);
                    view_pager.setVisibility(View.GONE);
                    return true;
                }

                return false;
            }
        });

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) dialogInterface;

                setupFullHeight(bottomSheetDialog);
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setWhiteNavigationBar(dialog);
        }
        return dialog;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        slideUp(name_cv);
        slideUp(about_cv);
        if (type == 1 || type == 2 || type==0)
            slideUp(request_cv);
        if (type != 6)
            slideUp(buttons_ll);
        // slideUp(name_cv);
        //  slideUp(about_cv);
        // slideUp(basics_cv);

    }

    public void slideUp(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.slide_uphalf));
        view.setVisibility(View.VISIBLE);
    }

    private void setupFullHeight(BottomSheetDialog bottomSheetDialog) {
        FrameLayout bottomSheet = bottomSheetDialog.findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();

        int windowHeight = getWindowHeight();
        if (layoutParams != null) {
            layoutParams.height = windowHeight;
        }
        bottomSheet.setLayoutParams(layoutParams);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    private int getWindowHeight() {
        // Calculate window height for fullscreen use
        DisplayMetrics displayMetrics = new DisplayMetrics();
        (getActivity()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.down_iv:
                dialog.dismiss();
                break;
            case R.id.ll_edit:
                startActivity(new Intent(getActivity(), EditProfile.class));
                getActivity().finish();
                break;
            case R.id.ll_block:
                openDialog();
                break;
            case R.id.menu_iv:
                slideUp(bottom_sheet);
                break;
            case R.id.ll_cancel:
                bottom_sheet.setVisibility(View.GONE);
                break;
            case R.id.ll_report:
                showHeightDialog(getResources().getString(R.string.report), report);
                break;
            case R.id.hi_ll:
            case R.id.cross_btn:
                dialog.dismiss();
                if (type == 5 || type == 1 || type == 3 || type == 0 || type == 7 || type == 2) {
                    MyApplication app = (MyApplication) getActivity().getApplication();
                    try {
                        getActivity().startActivity(ChatActivity.Start(app.getSocket(), getActivity(), new JSONObject(new Gson().toJson(userdata))));
                    } catch (JSONException e) {
                        Log.d(TAG, "onClick: " + e.getMessage());
                        e.printStackTrace();
                    }
                } else {
                    ((DiscoverFragment) getParentFragment()).swipeLeft();
                }
                break;
            case R.id.heart_btn:
                if (type == 4) {
                    ((DiscoverFragment) getParentFragment()).swipeRight();
                    dialog.dismiss();
                } else likedislike();
                break;
            case R.id.skip:
                counter++;
                if (counter < getPhotoAlbum.size())
                    addbottomdots(counter);
                else counter = getPhotoAlbum.size() - 1;

                break;
            case R.id.reverse:
                --counter;
                if (counter >= 0)
                    addbottomdots(counter);
                else counter = 0;
                break;
        }
    }

    private void openDialog() {
        dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_logout);
        dialog.show();
        dialog.setCancelable(true);
        Button declineButton = dialog.findViewById(R.id.btn_logout);
        TextView tv_all = dialog.findViewById(R.id.tv_all);
        TextView tv_logout = dialog.findViewById(R.id.tv_logout);
        tv_all.setText(getResources().getString(R.string.this_action_prevents_you_from_any_further_contact));
        tv_logout.setText(getResources().getString(R.string.block));
        declineButton.setText(getResources().getString(R.string.confirm));
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userBlocked();
                dialog.dismiss();

            }
        });
        TextView cancel = dialog.findViewById(R.id.tv_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


    }


    private void userBlocked() {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<BlockPojo> call = service.blockUser("Bearer " + Helper.getToken(getActivity()), userdata.getId().toString());
        call.enqueue(new Callback<BlockPojo>() {
            @Override
            public void onResponse(Call<BlockPojo> call, Response<BlockPojo> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getActivity(), MainActivity.class));
                    getActivity().finish();
                } else {
                    Log.d("error", "else: " + response.body());
                }
            }

            @Override
            public void onFailure(Call<BlockPojo> call, Throwable t) {
                Log.d("error", "Failure; " + t);
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void likedislike() {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<LikedislikePojo> call = service.likedislike("Bearer " + Helper.getToken(getActivity()), userdata.getId().toString(), 0);
        call.enqueue(new Callback<LikedislikePojo>() {

            @Override
            public void onResponse(Call<LikedislikePojo> call, Response<LikedislikePojo> response) {
                if (response.isSuccessful()) {
                    heart_btn.setVisibility(View.GONE);
                    if (type != 4) {
                        hi_ll.setVisibility(View.VISIBLE);
                        cross_btn.setVisibility(View.GONE);

                        SearchFragment fragm = (SearchFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.frame);
                        fragm.getAllList();
                        hi_ll.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                MyApplication app = (MyApplication) getActivity().getApplication();
                                try {
                                    getActivity().startActivity(ChatActivity.Start(app.getSocket(), getActivity(), new JSONObject(new Gson().toJson(userdata))));
                                } catch (JSONException e) {
                                    Log.d(TAG, "onClick: " + e.getMessage());
                                    e.printStackTrace();
                                }

                            }
                        });
                    }
                } else {
                    /// Helper.hideLoader();
                    Log.d("error", "else: " + response.body());
                }
            }

            @Override
            public void onFailure(Call<LikedislikePojo> call, Throwable t) {
                Log.d("error", "Failure; " + t);
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showHeightDialog(final String header, final List<Infodata.Data> datas) {
        dialog = new Dialog(getActivity());
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_listview);
        TextView tv_header = dialog.findViewById(R.id.tv_header);
        tv_header.setText(header);
        Button btndialog = (Button) dialog.findViewById(R.id.btndialog);
        progressBar_cyclic = dialog.findViewById(R.id.progressBar_cyclic);
        progressBar_cyclic.setVisibility(View.GONE);
        btndialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        RecyclerView = dialog.findViewById(R.id.listview);
        UpdatHeightAdapter interestsAdapter = new UpdatHeightAdapter(datas, new UpdatHeightAdapter.OnStatusListner() {
            @Override
            public void OnStatusClick(String p, String name) {
                updated = true;
                Intent intent = new Intent(getActivity(), ContactUs.class);
                intent.putExtra("report", p);
                startActivity(intent);
                dialog.dismiss();
            }

            @Override
            public void OnStatusmultClick(List<String> p, List<String> name) {

            }

        });
        RecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        RecyclerView.setAdapter(interestsAdapter);
        dialog.show();
    }

}
