package app.pairme.fragments;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.RequestPojo;
import app.pairme.MainActivity;
import app.pairme.R;
import app.pairme.adapters.RequetsAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RequestFragment extends Fragment {


    View view;
    RelativeLayout null_rl;
    RecyclerView requests_rv;
    SwipeRefreshLayout swiperefresh;

    public RequestFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_request, container, false);
        initViews();
        return view;
    }

    private void initViews() {
        ((MainActivity) getActivity()).badge.setVisibility(View.GONE);
        ((MainActivity) getActivity()).badge_active.setVisibility(View.GONE);
        null_rl = view.findViewById(R.id.null_rl);
        requests_rv = view.findViewById(R.id.requests_rv);
        swiperefresh = view.findViewById(R.id.swiperefresh);
        swiperefresh.setColorSchemeResources(
                R.color.refresh_progress_1,
                R.color.refresh_progress_2,
                R.color.refresh_progress_3);
        requests_rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        swiperefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        inflateData();
                    }
                }
        );
        inflateData();
    }

    private void inflateData() {
        Helper.showLoader(getActivity(), "");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<RequestPojo> call = service.requestsData("Bearer " + Helper.getToken(getContext()));
        call.enqueue(new Callback<RequestPojo>() {

            @Override
            public void onResponse(Call<RequestPojo> call, Response<RequestPojo> response) {
                Helper.hideLoader();
                swiperefresh.setRefreshing(false);
                if (response.isSuccessful()) {
                    if (response.body().likedYou.size() < 0 && response.body().checked.size() < 0) {
                        requests_rv.setVisibility(View.GONE);
                        null_rl.setVisibility(View.VISIBLE);
                    } else {
                        requests_rv.setVisibility(View.VISIBLE);
                        null_rl.setVisibility(View.GONE);
                    }
                    requests_rv.setAdapter(new RequetsAdapter(getActivity(), response.body()));
                } else {
                    Log.d("error", "else: " + response.body());
                }
            }
            @Override
            public void onFailure(Call<RequestPojo> call, Throwable t) {
                Log.d("error", "Failure; " + t);
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
