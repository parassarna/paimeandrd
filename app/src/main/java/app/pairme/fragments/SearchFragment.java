package app.pairme.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.AllUserPojo.AllUserPojo;
import app.pairme.ClassesPojo.UserInfoPojo.Data;
import app.pairme.R;
import app.pairme.activities.SearchActivity;
import app.pairme.adapters.SearchAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFragment extends Fragment implements View.OnClickListener {
    View view;
    List<Data> data = new ArrayList<>();
    AVLoadingIndicatorView avi;
    SwipeRefreshLayout swiperefresh;
    private RecyclerView recyclerView;
    private ImageView iv_sort;
    private SearchAdapter searchAdapter;
    private GridLayoutManager _sGridLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_search, container, false);

        initControl();
        return view;
    }

    private void initControl() {
        recyclerView = view.findViewById(R.id.recycler_search);
        swiperefresh = view.findViewById(R.id.swiperefresh);
        iv_sort = view.findViewById(R.id.iv_sort);
        avi = view.findViewById(R.id.avi);
        iv_sort.setOnClickListener(this);
        swiperefresh.setColorSchemeResources(
                R.color.refresh_progress_1,
                R.color.refresh_progress_2,
                R.color.refresh_progress_3);
        swiperefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getAllList();
                    }
                }
        );
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_sort:
                startActivity(new Intent(getContext(), SearchActivity.class));
                break;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        getAllList();

    }

    public void getAllList() {
        data.clear();
        avi.show();
        avi.setIndicator("Please wait...");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<AllUserPojo> call = service.searchList("Bearer " + Helper.getToken(getActivity()));
        call.enqueue(new Callback<AllUserPojo>() {
            @Override
            public void onResponse(Call<AllUserPojo> call, Response<AllUserPojo> response) {
                swiperefresh.setRefreshing(false);
                if (response.isSuccessful()) {
                    avi.hide();
                    data = response.body().getData();
                    Collections.reverse(data);
                    response.body().setData(data);
                    _sGridLayoutManager = new GridLayoutManager(getActivity(), 2);
                    recyclerView.setLayoutManager(_sGridLayoutManager);
                    recyclerView.setHasFixedSize(true);
                    searchAdapter = new SearchAdapter(getContext(), response.body().getData());
                    recyclerView.setAdapter(searchAdapter);


                } else {
                    avi.hide();
                    Log.d("error", "else: " + response.body());
                }

            }

            @Override
            public void onFailure(Call<AllUserPojo> call, Throwable t) {
                swiperefresh.setRefreshing(false);
                avi.hide();
                Log.d("error", "Failure; " + t);
            }
        });
    }
}
