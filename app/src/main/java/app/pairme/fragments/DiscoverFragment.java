package app.pairme.fragments;


import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.wang.avi.AVLoadingIndicatorView;
import com.yuyakaido.android.cardstackview.CardStackView;
import com.yuyakaido.android.cardstackview.SwipeDirection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.AllUserPojo.AllUserPojo;
import app.pairme.ClassesPojo.CardItem;
import app.pairme.ClassesPojo.LikedislikePojo;
import app.pairme.ClassesPojo.UserInfoPojo.Data;
import app.pairme.MainActivity;
import app.pairme.R;
import app.pairme.adapters.SwipeAdapter;
import app.pairme.fragments.RelateToFragment_OnBack.RootFragment;
import pl.bclogic.pulsator4droid.library.PulsatorLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class DiscoverFragment extends RootFragment implements View.OnClickListener {
    public static boolean is_reload_users = false;
    View view;
    AVLoadingIndicatorView avi;
    private List<CardItem> datumList;
    private List<Data> userdata = new ArrayList<>();
    private CardStackView card_viewstack;
    private SwipeAdapter adapter;
    private ImageView refresh_btn, detail_btn, cross_btn, heart_btn;
    private RelativeLayout user_list_layout, find_nearby_User;
    private int currentpos = 0;


    public DiscoverFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_discover, container, false);
        card_viewstack = view.findViewById(R.id.card_viewstack);
        user_list_layout = view.findViewById(R.id.user_list_layout);
        find_nearby_User = view.findViewById(R.id.find_nearby_User);
        avi = view.findViewById(R.id.avi);
        datumList = new ArrayList<>();
        getAllList();

        card_viewstack.setCardEventListener(new CardStackView.CardEventListener() {
            @Override
            public void onCardDragging(float percentX, float percentY) {

            }

            @Override
            public void onCardSwiped(SwipeDirection direction) {
                int positon = card_viewstack.getTopIndex() - 1;
                if (positon < adapter.getCount()) {
                    if (direction.equals(SwipeDirection.Left)) {
                        // likedislike(0);
                    } else if (direction.equals(SwipeDirection.Right)) {
                        likedislike(1);
                    } else if (direction.equals(SwipeDirection.Top)) {
                        getInfo();
                    }
                    // find if the swipes card is last or not
                    if (card_viewstack.getTopIndex() == adapter.getCount()) {
                        ShowfindingView();
                    }
                }
            }

            @Override
            public void onCardReversed() {
                int positon = card_viewstack.getTopIndex();
                if (positon < adapter.getCount()) {
                    Toast.makeText(getContext(), "Card Reversed", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCardMovedToOrigin() {

            }

            @Override
            public void onCardClicked(int index) {

            }
        });
        init_bottom_view();
        return view;

    }

    private void getInfo() {
        UserInfoFragment userInfoFragment = new UserInfoFragment(userdata.get(card_viewstack.getTopIndex()), 4);
        userInfoFragment.show(getChildFragmentManager(), UserInfoFragment.TAG);
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    private void ShowfindingView() {
        is_reload_users = true;
        user_list_layout.setVisibility(View.GONE);
        find_nearby_User.setVisibility(View.VISIBLE);
        final PulsatorLayout pulsator = view.findViewById(R.id.pulsator);
        pulsator.start();
    }


    private void init_bottom_view() {
        refresh_btn = view.findViewById(R.id.refresh_btn);
        cross_btn = view.findViewById(R.id.cross_btn);
        heart_btn = view.findViewById(R.id.heart_btn);
        detail_btn = view.findViewById(R.id.detail_btn);
        refresh_btn.setOnClickListener(this);
        detail_btn.setOnClickListener(this);
        cross_btn.setOnClickListener(this);
        heart_btn.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.cross_btn:
                swipeLeft();
                break;

            case R.id.heart_btn:
                swipeRight();
                break;

            case R.id.refresh_btn:
                card_viewstack.reverse();
                break;
            case R.id.detail_btn:
                getInfo();
                break;
        }
    }


    private void getAllList() {
        avi.show();
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<AllUserPojo> call = service.getAllList("Bearer " + Helper.getToken(getActivity()));
        call.enqueue(new Callback<AllUserPojo>() {
            @Override
            public void onResponse(Call<AllUserPojo> call, Response<AllUserPojo> response) {
                if (response.isSuccessful()) {
                    avi.hide();
                    if (response.body().getData().size() > 0) {
                        user_list_layout.setVisibility(View.VISIBLE);
                        find_nearby_User.setVisibility(View.GONE);
                        userdata = response.body().getData();
                        Collections.reverse(userdata);
                        try {
                            if (response.body().requestcount > 0) {
                                ((MainActivity) getActivity()).badge.setVisibility(View.VISIBLE);
                                ((MainActivity) getActivity()).badge_active.setVisibility(View.VISIBLE);
                                ((MainActivity) getActivity()).badge.setText(String.valueOf(response.body().requestcount));
                                ((MainActivity) getActivity()).badge_active.setText(String.valueOf(response.body().requestcount));
                            } else {
                                ((MainActivity) getActivity()).badge.setVisibility(View.GONE);
                                ((MainActivity) getActivity()).badge_active.setVisibility(View.GONE);
                            }
                            if (response.body().chatcount > 0) {
                                ((MainActivity) getActivity()).badgechat.setVisibility(View.VISIBLE);
                                ((MainActivity) getActivity()).badgechat_active.setVisibility(View.VISIBLE);
                                ((MainActivity) getActivity()).badgechat.setText(String.valueOf(response.body().chatcount));
                                ((MainActivity) getActivity()).badgechat_active.setText(String.valueOf(response.body().chatcount));
                            } else {
                                ((MainActivity) getActivity()).badgechat.setVisibility(View.GONE);
                                ((MainActivity) getActivity()).badgechat_active.setVisibility(View.GONE);
                            }
                        }catch (Exception e){

                        }
                        adapter = new SwipeAdapter(getActivity(), userdata);
                        card_viewstack.setAdapter(adapter);
                    } else {
                        ShowfindingView();
                    }
                } else {
                    avi.hide();
                    Log.d("error", "else: " + response.body());
                }
            }

            @Override
            public void onFailure(Call<AllUserPojo> call, Throwable t) {
                avi.hide();
                Log.d("error", "Failure; " + t);
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void swipeLeft() {
        View target = card_viewstack.getTopView();
        View targetOverlay = card_viewstack.getTopView().getOverlayContainer();
        ValueAnimator rotation = ObjectAnimator.ofPropertyValuesHolder(
                target, PropertyValuesHolder.ofFloat("rotation", -10f));
        rotation.setDuration(200);
        ValueAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(
                target, PropertyValuesHolder.ofFloat("translationX", 0f, -2000f));
        ValueAnimator translateY = ObjectAnimator.ofPropertyValuesHolder(
                target, PropertyValuesHolder.ofFloat("translationY", 0f, 500f));
        translateX.setStartDelay(400);
        translateY.setStartDelay(400);
        translateX.setDuration(500);
        translateY.setDuration(500);
        AnimatorSet cardAnimationSet = new AnimatorSet();
        cardAnimationSet.playTogether(rotation, translateX, translateY);
        ObjectAnimator overlayAnimator = ObjectAnimator.ofFloat(targetOverlay, "alpha", 0f, 1f);
        overlayAnimator.setDuration(200);
        AnimatorSet overlayAnimationSet = new AnimatorSet();
        overlayAnimationSet.playTogether(overlayAnimator);
        card_viewstack.swipe(SwipeDirection.Left, cardAnimationSet, overlayAnimationSet);
    }

    private void likedislike(int status) {
        Log.d("tag", "likedislike: " + currentpos);
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<LikedislikePojo> call = service.likedislike("Bearer " + Helper.getToken(getContext()), userdata.get(currentpos).getId().toString(), status);
        call.enqueue(new Callback<LikedislikePojo>() {
            @Override
            public void onResponse(Call<LikedislikePojo> call, Response<LikedislikePojo> response) {
                if (response.isSuccessful()) {
                    currentpos++;

                } else {
                    /// Helper.hideLoader();
                    Log.d("error", "else: " + response.body());
                }
            }

            @Override
            public void onFailure(Call<LikedislikePojo> call, Throwable t) {
                //   Helper.hideLoader();
                Log.d("error", "Failure; " + t);
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void swipeRight() {
        View target = card_viewstack.getTopView();
        View targetOverlay = card_viewstack.getTopView().getOverlayContainer();
        ValueAnimator rotation = ObjectAnimator.ofPropertyValuesHolder(
                target, PropertyValuesHolder.ofFloat("rotation", 10f));
        rotation.setDuration(200);
        ValueAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(
                target, PropertyValuesHolder.ofFloat("translationX", 0f, 2000f));
        ValueAnimator translateY = ObjectAnimator.ofPropertyValuesHolder(
                target, PropertyValuesHolder.ofFloat("translationY", 0f, 500f));
        translateX.setStartDelay(400);
        translateY.setStartDelay(400);
        translateX.setDuration(500);
        translateY.setDuration(500);
        AnimatorSet cardAnimationSet = new AnimatorSet();
        cardAnimationSet.playTogether(rotation, translateX, translateY);

        ObjectAnimator overlayAnimator = ObjectAnimator.ofFloat(targetOverlay, "alpha", 0f, 1f);
        overlayAnimator.setDuration(200);
        AnimatorSet overlayAnimationSet = new AnimatorSet();
        overlayAnimationSet.playTogether(overlayAnimator);
        card_viewstack.swipe(SwipeDirection.Right, cardAnimationSet, overlayAnimationSet);
        likedislike(1);
    }

}