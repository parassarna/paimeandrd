package app.pairme;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.FirebaseApp;
import com.stripe.android.PaymentConfiguration;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;

public class MyApplication extends Application {
    private Socket mSocket;


    public Socket getSocket() {
        return mSocket;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
        try {
            mSocket = IO.socket("https://pairme.co:6001/");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))//enable logging when app is in debug mode
                .twitterAuthConfig(new TwitterAuthConfig(getResources().getString(R.string.twitter_CONSUMER_KEY), getResources().getString(R.string.twitter_CONSUMER_SECRET)))//pass the created app Consumer KEY and Secret also called API Key and Secret
                .debug(true)//enable debug mode
                .build();
        PaymentConfiguration.init(getApplicationContext(),
                "pk_test_3NP1NH5SsQk8Mv8hkuEfP3Rj00cf0eZ9ZF"
        );
        //finally initialize twitter with created configs
        Twitter.initialize(config);
    }

}