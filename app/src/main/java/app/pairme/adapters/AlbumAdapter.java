package app.pairme.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.List;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.UserInfoPojo.PhotoAlbum;
import app.pairme.ClassesPojo.UserInfoPojo.UserInfo;
import app.pairme.R;
import app.pairme.activities.EditProfile;
import app.pairme.activities.PhotoAlbumActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.pairme.Utils.imageloader;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.GridItemViewHolder> {

    int type;
    ViewPager viewPager;
    ScrollView scrollView;
    List<PhotoAlbum> photoAlbums;
    private List<String> imageList;
    private Context c;

    public AlbumAdapter(Context c, List<String> imageList, int type, List<PhotoAlbum> photoAlbums) {
        this.c = c;
        this.imageList = imageList;
        this.type = type;
        this.photoAlbums = photoAlbums;
    }

    public AlbumAdapter(Context c, List<String> imageList, int type, ViewPager viewPager, ScrollView scrollView) {
        this.c = c;
        this.imageList = imageList;
        this.type = type;
        this.viewPager = viewPager;
        this.scrollView = scrollView;
    }

    @Override
    public GridItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.photo_item, parent, false);
        return new GridItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(GridItemViewHolder holder, final int position) {
        Glide.with(c).load(imageList.get(position)).placeholder(imageloader(c)).error(R.drawable.place_holder).into(holder.siv);

        holder.iv_delete.setVisibility(type == 0 ? View.VISIBLE : View.GONE);
        holder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                deleteimage(position);
            }
        });
        if (type != 0)
            holder.siv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewPager.setVisibility(View.VISIBLE);
                    scrollView.setVisibility(View.GONE);
                    viewPager.setAdapter(new MyViewPagerAdapter());
                    viewPager.setCurrentItem(position);
                }
            });
    }

    private void deleteimage(final int pos) {
        Helper.showLoader(c,"Please wait...");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        service.delAlbumImage(photoAlbums.get(pos).getId(), "Bearer " + Helper.getToken(c)).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Helper.hideLoader();
                if (response.isSuccessful()) {

                    if (response.body().get("success").getAsBoolean()) {
                        getUserdata();
                        imageList.remove(pos);
                        notifyDataSetChanged();
                    }

                } else {

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Helper.hideLoader();

                Toast.makeText(c, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    private void getUserdata() {
        // Helper.showLoader(this, "Please Wait....");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<UserInfo> call = service.getUserInfo("Bearer " + Helper.getToken(c), Helper.getId(c));
        call.enqueue(new Callback<UserInfo>() {

            @Override
            public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                if (response.isSuccessful()) {
                    //  Helper.hideLoader();
                    Helper.setUserData(new Gson().toJson(response.body().getData()), c);


                } else {
                    //   Helper.hideLoader();
                    Log.d("error", "else: " + response.body());
                }
            }

            @Override
            public void onFailure(Call<UserInfo> call, Throwable t) {
                //  Helper.hideLoader();
                Log.d("error", "Failure; " + t);
                Toast.makeText(c, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }

    public class GridItemViewHolder extends RecyclerView.ViewHolder {
        ImageView siv, iv_delete;

        public GridItemViewHolder(View view) {
            super(view);
            // c.startActivity(PhotoSlider.Start(c, imageList, position));
            siv = view.findViewById(R.id.iv_photo);
            iv_delete = view.findViewById(R.id.iv_delete);
        }
    }

    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.photo_preview, container, false);
            ImageView iv = view.findViewById(R.id.iv);
            Glide.with(c).load(imageList.get(position)).placeholder(imageloader(c)).error(R.drawable.place_holder).into(iv);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return imageList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }

    }
}
