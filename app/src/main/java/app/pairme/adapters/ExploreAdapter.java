package app.pairme.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.text.TextPaint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import app.pairme.R;
import app.pairme.fragments.SearchFragment;

public class ExploreAdapter extends RecyclerView.Adapter<ExploreAdapter.GridItemViewHolder> {

    TextView title_tv;
    RecyclerView recyclerView;
    private JsonArray datas;
    private Context c;

    public ExploreAdapter(Context c, TextView title_tv, JsonArray data) {
        this.c = c;
        this.datas = data;
        this.title_tv = title_tv;
        hasStableIds();
    }

    @Override
    public GridItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.explore_item, parent, false);
        return new GridItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final GridItemViewHolder holder, int position) {
        TextPaint paint = holder.more_tv.getPaint();
        float width = paint.measureText(c.getResources().getString(R.string.see_more_potential_matches));
        Shader textShader = new LinearGradient(0, 0, width, holder.more_tv.getTextSize(),
                new int[]{
                        Color.parseColor("#b697c4"),
                        Color.parseColor("#7db7e5"),
                        Color.parseColor("#e77098"),
                }, null, Shader.TileMode.CLAMP);
        holder.more_tv.getPaint().setShader(textShader);
        if (datas.get(position).getAsJsonObject().get("type").getAsString().equalsIgnoreCase("matching_data")) {
            JsonObject data = datas.get(position).getAsJsonObject();
            holder.title_tv.setText(c.getResources().getString(R.string.potential_matches));
            holder.grid_ll.setVisibility(View.VISIBLE);
            holder.request_cv.setVisibility(View.GONE);
            holder.singleitem_ll.setVisibility(View.GONE);
            holder.note_cv.setVisibility(View.GONE);
            holder.note_cv1.setVisibility(View.GONE);
            holder.note_cv2.setVisibility(View.GONE);
            holder.singleitem_ll.setVisibility(View.GONE);
            holder.check_ll.setVisibility(View.GONE);
            holder.check_title.setVisibility(View.GONE);
            if (!data.get("matching_data").getAsJsonArray().get(0).getAsJsonObject().get("profile_image").isJsonNull())
                Glide.with(c).load("https://pairme.co/img/userProfile/" + data.get("matching_data").getAsJsonArray().get(0).getAsJsonObject().get("profile_image").getAsString()).error(R.drawable.user_default).into(holder.image1);
            holder.username1.setText(data.get("matching_data").getAsJsonArray().get(0).getAsJsonObject().get("name").getAsString());
            holder.age1.setText(" . " + data.get("matching_data").getAsJsonArray().get(0).getAsJsonObject().get("age").getAsString());
            if (!data.get("matching_data").getAsJsonArray().get(0).getAsJsonObject().get("lives_in").isJsonNull())
                holder.distance_txt1.setText(data.get("matching_data").getAsJsonArray().get(0).getAsJsonObject().get("lives_in").getAsString());
            if (data.get("matching_data").getAsJsonArray().size() > 1) {
                holder.cv2.setVisibility(View.VISIBLE);
                if (!data.get("matching_data").getAsJsonArray().get(1).getAsJsonObject().get("profile_image").isJsonNull())
                    Glide.with(c).load("https://pairme.co/img/userProfile/" + data.get("matching_data").getAsJsonArray().get(1).getAsJsonObject().get("profile_image").getAsString()).error(R.drawable.user_default).into(holder.image2);
                holder.username2.setText(data.get("matching_data").getAsJsonArray().get(1).getAsJsonObject().get("name").getAsString());
                holder.age2.setText(" . " + data.get("matching_data").getAsJsonArray().get(1).getAsJsonObject().get("age").getAsString());
                if (!data.get("matching_data").getAsJsonArray().get(1).getAsJsonObject().get("lives_in").isJsonNull())
                    holder.distance_txt2.setText(data.get("matching_data").getAsJsonArray().get(1).getAsJsonObject().get("lives_in").getAsString());
            } else {
                holder.cv2.setVisibility(View.GONE);
            }
            if (data.get("matching_data").getAsJsonArray().size() > 2) {
                holder.cv3.setVisibility(View.VISIBLE);
                if (!data.get("matching_data").getAsJsonArray().get(2).getAsJsonObject().get("profile_image").isJsonNull())
                    Glide.with(c).load("https://pairme.co/img/userProfile/" + data.get("matching_data").getAsJsonArray().get(2).getAsJsonObject().get("profile_image").getAsString()).error(R.drawable.user_default).into(holder.image3);
                if (!data.get("matching_data").getAsJsonArray().get(2).getAsJsonObject().get("name").isJsonNull())
                    holder.username3.setText(data.get("matching_data").getAsJsonArray().get(2).getAsJsonObject().get("name").getAsString());
                if (!data.get("matching_data").getAsJsonArray().get(2).getAsJsonObject().get("age").isJsonNull())
                    holder.age3.setText(" . " + data.get("matching_data").getAsJsonArray().get(2).getAsJsonObject().get("age").getAsString());
                if (!data.get("matching_data").getAsJsonArray().get(2).getAsJsonObject().get("lives_in").isJsonNull())
                    holder.distance_txt3.setText(data.get("matching_data").getAsJsonArray().get(2).getAsJsonObject().get("lives_in").getAsString());
            } else {
                holder.cv3.setVisibility(View.GONE);
            }
            if (data.get("matching_data").getAsJsonArray().size() > 3) {
                holder.cv4.setVisibility(View.VISIBLE);
                if (!data.get("matching_data").getAsJsonArray().get(3).getAsJsonObject().get("profile_image").isJsonNull())
                    Glide.with(c).load("https://pairme.co/img/userProfile/" + data.get("matching_data").getAsJsonArray().get(3).getAsJsonObject().get("profile_image").getAsString()).error(R.drawable.user_default).into(holder.image4);
                if (!data.get("matching_data").getAsJsonArray().get(3).getAsJsonObject().get("name").isJsonNull())
                    holder.username4.setText(data.get("matching_data").getAsJsonArray().get(3).getAsJsonObject().get("name").getAsString());
                if (!data.get("matching_data").getAsJsonArray().get(3).getAsJsonObject().get("age").isJsonNull())
                    holder.age4.setText(" . " + data.get("matching_data").getAsJsonArray().get(3).getAsJsonObject().get("age").getAsString());
                if (!data.get("matching_data").getAsJsonArray().get(3).getAsJsonObject().get("lives_in").isJsonNull())
                    holder.distance_txt4.setText(data.get("matching_data").getAsJsonArray().get(3).getAsJsonObject().get("lives_in").getAsString());
            } else {
                holder.cv4.setVisibility(View.GONE);
            }
            holder.more_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    title_tv.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);
                    ((AppCompatActivity) c).getSupportFragmentManager().beginTransaction().replace(R.id.frame, new SearchFragment()).commit();
                }
            });

        } else if (datas.get(position).getAsJsonObject().get("type").getAsString().equalsIgnoreCase("matching_data")) {
            JsonObject data = datas.get(position).getAsJsonObject();
            holder.title_tv.setText(c.getResources().getString(R.string.matches));
            holder.request_cv.setVisibility(View.VISIBLE);
            holder.singleitem_ll.setVisibility(View.GONE);
            holder.note_cv.setVisibility(View.GONE);
            holder.note_cv1.setVisibility(View.GONE);
            holder.note_cv2.setVisibility(View.GONE);
            holder.singleitem_ll.setVisibility(View.GONE);
            holder.grid_ll.setVisibility(View.GONE);
            holder.check_ll.setVisibility(View.GONE);
            holder.check_title.setVisibility(View.GONE);
            if (!data.get("matching_data").getAsJsonArray().get(0).getAsJsonObject().get("profile_image").isJsonNull())
                Glide.with(c).load("https://pairme.co/img/userProfile/" + data.get("matching_data").getAsJsonArray().get(0).getAsJsonObject().get("profile_image").getAsString()).error(R.drawable.user_default).into(holder.user_iv);
            if (!datas.get(0).getAsJsonObject().get("userData").getAsJsonObject().get("profile_image").isJsonNull())
                Glide.with(c).load("https://pairme.co/img/userProfile/" + datas.get(0).getAsJsonObject().get("userData").getAsJsonObject().get("profile_image").getAsString()).error(R.drawable.user_default).into(holder.self_iv);
            holder.request_tv.setText(c.getResources().getString(R.string.matched_with) + " " + data.get("matching_data").getAsJsonArray().get(0).getAsJsonObject().get("name").getAsString());
        } else if (datas.get(position).getAsJsonObject().get("type").getAsString().equalsIgnoreCase("newest_member")) {
            JsonObject data = datas.get(position).getAsJsonObject();
            holder.title_tv.setText(c.getResources().getString(R.string.Newest_Member));
            holder.request_cv.setVisibility(View.GONE);
            holder.singleitem_ll.setVisibility(View.VISIBLE);
            holder.note_cv.setVisibility(View.GONE);
            holder.note_cv1.setVisibility(View.GONE);
            holder.check_ll.setVisibility(View.GONE);
            holder.note_cv2.setVisibility(View.GONE);
            holder.grid_ll.setVisibility(View.GONE);
            holder.check_title.setVisibility(View.GONE);
            if (!data.get("newest_member").getAsJsonArray().get(0).getAsJsonObject().get("name").isJsonNull())
                holder.username.setText(data.get("newest_member").getAsJsonArray().get(0).getAsJsonObject().get("name").getAsString());
            if (!data.get("newest_member").getAsJsonArray().get(0).getAsJsonObject().get("age").isJsonNull())
                holder.age.setText(" . " + data.get("newest_member").getAsJsonArray().get(0).getAsJsonObject().get("age").getAsString());
            if (!data.get("newest_member").getAsJsonArray().get(0).getAsJsonObject().get("lives_in").isJsonNull())
                holder.distance_txt.setText(data.get("newest_member").getAsJsonArray().get(0).getAsJsonObject().get("lives_in").getAsString());
            if (!data.get("newest_member").getAsJsonArray().get(0).getAsJsonObject().get("profile_image").isJsonNull())
                Glide.with(c).load("https://pairme.co/img/userProfile/" + data.get("newest_member").getAsJsonArray().get(0).getAsJsonObject().get("profile_image").getAsString()).error(R.drawable.user_default).error(R.drawable.user_default).into(holder.item_image);
        } else if (datas.get(position).getAsJsonObject().get("type").getAsString().equalsIgnoreCase("news")) {
            JsonObject data = datas.get(position).getAsJsonObject();
            holder.grid_ll.setVisibility(View.GONE);
            holder.request_cv.setVisibility(View.GONE);
            holder.singleitem_ll.setVisibility(View.GONE);
            holder.check_ll.setVisibility(View.GONE);
            holder.note_cv.setVisibility(View.VISIBLE);
            holder.note_cv1.setVisibility(View.GONE);
            holder.note_cv2.setVisibility(View.GONE);
            holder.singleitem_ll.setVisibility(View.GONE);
            holder.check_title.setVisibility(View.GONE);
            holder.title_tv.setText(c.getResources().getString(R.string.News));
            if (!data.get("news").getAsJsonArray().get(0).getAsJsonObject().get("full_news").isJsonNull())
                holder.note.setText(data.get("news").getAsJsonArray().get(0).getAsJsonObject().get("full_news").getAsString());
            //Glide.with(c).load(data.get("news").getAsJsonObject().get("image")).into(holder.note_image);
        } else if (datas.get(position).getAsJsonObject().get("type").getAsString().equalsIgnoreCase("meetData")) {
            JsonObject data = datas.get(position).getAsJsonObject();
            holder.title_tv.setText(c.getResources().getString(R.string.Meet_the_member));
            holder.request_cv.setVisibility(View.GONE);
            holder.singleitem_ll.setVisibility(View.VISIBLE);
            holder.note_cv.setVisibility(View.GONE);
            holder.check_ll.setVisibility(View.GONE);
            holder.note_cv1.setVisibility(View.GONE);
            holder.note_cv2.setVisibility(View.GONE);
            holder.grid_ll.setVisibility(View.GONE);
            holder.check_title.setVisibility(View.GONE);
            if (!data.get("meetData").getAsJsonArray().get(0).getAsJsonObject().get("name").isJsonNull())
                holder.username.setText(data.get("meetData").getAsJsonArray().get(0).getAsJsonObject().get("name").getAsString());
            if (!data.get("meetData").getAsJsonArray().get(0).getAsJsonObject().get("age").isJsonNull())
                holder.age.setText(" . " + data.get("meetData").getAsJsonArray().get(0).getAsJsonObject().get("age").getAsString());
            if (!data.get("meetData").getAsJsonArray().get(0).getAsJsonObject().get("lives_in").isJsonNull())
                holder.distance_txt.setText(data.get("meetData").getAsJsonArray().get(0).getAsJsonObject().get("lives_in").getAsString());
            if (!data.get("meetData").getAsJsonArray().get(0).getAsJsonObject().get("profile_image").isJsonNull())
                Glide.with(c).load("https://pairme.co/img/userProfile/" + data.get("meetData").getAsJsonArray().get(0).getAsJsonObject().get("profile_image").getAsString()).error(R.drawable.user_default).error(R.drawable.user_default).into(holder.item_image);
        } else if (datas.get(position).getAsJsonObject().get("type").getAsString().equalsIgnoreCase("blog")) {
            JsonObject data = datas.get(position).getAsJsonObject();
            holder.grid_ll.setVisibility(View.GONE);
            holder.request_cv.setVisibility(View.GONE);
            holder.singleitem_ll.setVisibility(View.GONE);
            holder.check_ll.setVisibility(View.GONE);
            holder.note_cv.setVisibility(View.GONE);
            holder.note_cv1.setVisibility(View.VISIBLE);
            holder.note_cv2.setVisibility(View.GONE);
            holder.singleitem_ll.setVisibility(View.GONE);
            holder.check_title.setVisibility(View.GONE);
            holder.title_tv.setText(c.getResources().getString(R.string.blog));
//            holder.note_text1.setText(data.get("blog").getAsJsonArray().get(0).getAsJsonObject().get("author_name").getAsString());
            holder.note1.setText(data.get("blog").getAsJsonArray().get(0).getAsJsonObject().get("all_text").getAsString());
            Glide.with(c).load("https://pairme.co/img/blog/" + data.get("blog").getAsJsonArray().get(0).getAsJsonObject().get("blog_image").getAsString()).error(R.drawable.user_default).into(holder.note_image1);
        } else if (datas.get(position).getAsJsonObject().get("type").getAsString().equalsIgnoreCase("story")) {
            JsonObject data = datas.get(position).getAsJsonObject();
            holder.grid_ll.setVisibility(View.GONE);
            holder.request_cv.setVisibility(View.GONE);
            holder.singleitem_ll.setVisibility(View.GONE);
            holder.check_ll.setVisibility(View.GONE);
            holder.note_cv.setVisibility(View.GONE);
            holder.note_cv1.setVisibility(View.GONE);
            holder.note_cv2.setVisibility(View.VISIBLE);
            holder.singleitem_ll.setVisibility(View.GONE);
            holder.check_title.setVisibility(View.GONE);
            holder.title_tv.setText(c.getResources().getString(R.string.Story));
            holder.note_text2.setText(data.get("story").getAsJsonArray().get(0).getAsJsonObject().get("story_title").getAsString());
            holder.note2.setText(data.get("story").getAsJsonArray().get(0).getAsJsonObject().get("description").getAsString());
            Glide.with(c).load("https://pairme.co/img/story/" + data.get("story").getAsJsonArray().get(0).getAsJsonObject().get("image").getAsString()).error(R.drawable.user_default).into(holder.note_image2);
        } else if (datas.get(position).getAsJsonObject().get("type").getAsString().equalsIgnoreCase("likedata")) {
            JsonObject data = datas.get(position).getAsJsonObject();
            holder.title_tv.setText(c.getResources().getString(R.string.Liked));
            holder.request_cv.setVisibility(View.GONE);
            holder.singleitem_ll.setVisibility(View.VISIBLE);
            holder.note_cv.setVisibility(View.GONE);
            holder.check_ll.setVisibility(View.GONE);
            holder.note_cv1.setVisibility(View.GONE);
            holder.note_cv2.setVisibility(View.GONE);
            holder.grid_ll.setVisibility(View.GONE);
            holder.check_title.setVisibility(View.GONE);
            holder.username.setText(data.get("likedata").getAsJsonArray().get(0).getAsJsonObject().get("name").getAsString());
            holder.age.setText(" . " + data.get("likedata").getAsJsonArray().get(0).getAsJsonObject().get("age").getAsString());
            if (!data.get("likedata").getAsJsonArray().get(0).getAsJsonObject().get("lives_in").isJsonNull())
                holder.distance_txt.setText(data.get("likedata").getAsJsonArray().get(0).getAsJsonObject().get("lives_in").getAsString());
            if (!data.get("likedata").getAsJsonArray().get(0).getAsJsonObject().get("profile_image").isJsonNull())
                Glide.with(c).load("https://pairme.co/img/userProfile/" + data.get("likedata").getAsJsonArray().get(0).getAsJsonObject().get("profile_image").getAsString()).error(R.drawable.user_default).error(R.drawable.user_default).into(holder.item_image);
        } else if (datas.get(position).getAsJsonObject().get("type").getAsString().equalsIgnoreCase("featured_members")) {
            JsonObject data = datas.get(position).getAsJsonObject();
            holder.title_tv.setText(c.getResources().getString(R.string.featured_Members));
            holder.request_cv.setVisibility(View.GONE);
            holder.singleitem_ll.setVisibility(View.GONE);
            holder.note_cv.setVisibility(View.GONE);
            holder.check_ll.setVisibility(View.GONE);
            holder.note_cv1.setVisibility(View.GONE);
            holder.note_cv2.setVisibility(View.GONE);
            holder.grid_ll.setVisibility(View.VISIBLE);
            holder.check_title.setVisibility(View.GONE);
            holder.more_tv.setVisibility(View.GONE);
            if (!data.get("featured_members").getAsJsonArray().get(0).getAsJsonObject().get("profile_image").isJsonNull())
                Glide.with(c).load("https://pairme.co/img/userProfile/" + data.get("featured_members").getAsJsonArray().get(0).getAsJsonObject().get("profile_image").getAsString()).error(R.drawable.user_default).into(holder.image1);
            holder.username1.setText(data.get("featured_members").getAsJsonArray().get(0).getAsJsonObject().get("name").getAsString());
            holder.age1.setText(" . " + data.get("featured_members").getAsJsonArray().get(0).getAsJsonObject().get("age").getAsString());
            if (!data.get("featured_members").getAsJsonArray().get(0).getAsJsonObject().get("lives_in").isJsonNull())
                holder.distance_txt1.setText(data.get("featured_members").getAsJsonArray().get(0).getAsJsonObject().get("lives_in").getAsString());
            if (data.get("featured_members").getAsJsonArray().size() > 1) {
                holder.cv2.setVisibility(View.VISIBLE);
                if (!data.get("featured_members").getAsJsonArray().get(1).getAsJsonObject().get("profile_image").isJsonNull())
                    Glide.with(c).load("https://pairme.co/img/userProfile/" + data.get("featured_members").getAsJsonArray().get(1).getAsJsonObject().get("profile_image").getAsString()).error(R.drawable.user_default).into(holder.image2);
                if (!data.get("featured_members").getAsJsonArray().get(1).getAsJsonObject().get("name").isJsonNull())
                    holder.username2.setText(data.get("featured_members").getAsJsonArray().get(1).getAsJsonObject().get("name").getAsString());
                if (!data.get("featured_members").getAsJsonArray().get(1).getAsJsonObject().get("age").isJsonNull())
                    holder.age2.setText(" . " + data.get("featured_members").getAsJsonArray().get(1).getAsJsonObject().get("age").getAsString());
                if (!data.get("featured_members").getAsJsonArray().get(1).getAsJsonObject().get("lives_in").isJsonNull())
                    holder.distance_txt2.setText(data.get("featured_members").getAsJsonArray().get(1).getAsJsonObject().get("lives_in").getAsString());
            } else {
                holder.cv2.setVisibility(View.GONE);
            }
            if (data.get("featured_members").getAsJsonArray().size() > 2) {
                holder.cv3.setVisibility(View.VISIBLE);
                if (!data.get("featured_members").getAsJsonArray().get(2).getAsJsonObject().get("profile_image").isJsonNull())
                    Glide.with(c).load("https://pairme.co/img/userProfile/" + data.get("featured_members").getAsJsonArray().get(2).getAsJsonObject().get("profile_image").getAsString()).error(R.drawable.user_default).into(holder.image3);
                if (!data.get("featured_members").getAsJsonArray().get(2).getAsJsonObject().get("name").isJsonNull())
                    holder.username3.setText(data.get("featured_members").getAsJsonArray().get(2).getAsJsonObject().get("name").getAsString());
                if (!data.get("featured_members").getAsJsonArray().get(2).getAsJsonObject().get("age").isJsonNull())
                    holder.age3.setText(" . " + data.get("featured_members").getAsJsonArray().get(2).getAsJsonObject().get("age").getAsString());
                if (!data.get("featured_members").getAsJsonArray().get(2).getAsJsonObject().get("lives_in").isJsonNull())
                    holder.distance_txt3.setText(data.get("featured_members").getAsJsonArray().get(2).getAsJsonObject().get("lives_in").getAsString());
            } else {
                holder.cv3.setVisibility(View.GONE);
            }
            if (data.get("featured_members").getAsJsonArray().size() > 3) {
                holder.cv4.setVisibility(View.VISIBLE);
                if (!data.get("featured_members").getAsJsonArray().get(3).getAsJsonObject().get("profile_image").isJsonNull())
                    Glide.with(c).load("https://pairme.co/img/userProfile/" + data.get("featured_members").getAsJsonArray().get(3).getAsJsonObject().get("profile_image").getAsString()).error(R.drawable.user_default).into(holder.image4);
                if (!data.get("featured_members").getAsJsonArray().get(3).getAsJsonObject().get("name").isJsonNull())
                    holder.username4.setText(data.get("featured_members").getAsJsonArray().get(3).getAsJsonObject().get("name").getAsString());
                if (!data.get("featured_members").getAsJsonArray().get(3).getAsJsonObject().get("age").isJsonNull())
                    holder.age4.setText(" . " + data.get("featured_members").getAsJsonArray().get(3).getAsJsonObject().get("age").getAsString());
                if (!data.get("featured_members").getAsJsonArray().get(3).getAsJsonObject().get("lives_in").isJsonNull())
                    holder.distance_txt4.setText(data.get("featured_members").getAsJsonArray().get(3).getAsJsonObject().get("lives_in").getAsString());
            } else {
                holder.cv4.setVisibility(View.GONE);
            }

        } else if (datas.get(position).getAsJsonObject().get("type").getAsString().equalsIgnoreCase("checkdata")) {
            JsonObject data = datas.get(position).getAsJsonObject();
            holder.title_tv.setText(c.getResources().getString(R.string.checked_you_out));
            holder.check_title.setVisibility(View.VISIBLE);
            holder.check_title.setText(c.getResources().getString(R.string.Something_about_you_caught) + data.get("checkdata").getAsJsonArray().get(0).getAsJsonObject().get("name").getAsString() + c.getResources().getString(R.string.how_about_making_move_or_two));
            holder.singleitem_ll.setVisibility(View.GONE);
            holder.grid_ll.setVisibility(View.GONE);
            holder.request_cv.setVisibility(View.GONE);
            holder.check_ll.setVisibility(View.VISIBLE);
            holder.note_cv.setVisibility(View.GONE);
            holder.note_cv1.setVisibility(View.GONE);
            holder.note_cv2.setVisibility(View.GONE);
            holder.check_username.setText(data.get("checkdata").getAsJsonArray().get(0).getAsJsonObject().get("name").getAsString());
            holder.check_age.setText(" . " + data.get("checkdata").getAsJsonArray().get(0).getAsJsonObject().get("age").getAsString());
            if (!data.get("checkdata").getAsJsonArray().get(0).getAsJsonObject().get("lives_in").isJsonNull())
                holder.check_distance_txt.setText(data.get("checkdata").getAsJsonArray().get(0).getAsJsonObject().get("profile_image").getAsString());
            if (!data.get("checkdata").getAsJsonArray().get(0).getAsJsonObject().get("profile_image").isJsonNull())
                Glide.with(c).load("https://pairme.co/img/userProfile/" + data.get("checkdata").getAsJsonArray().get(0).getAsJsonObject().get("profile_image").getAsString()).error(R.drawable.user_default).error(R.drawable.user_default).into(holder.item_image);
        }
    }


    @Override
    public int getItemCount() {
        return datas.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class GridItemViewHolder extends RecyclerView.ViewHolder {
        TextView more_tv, title_tv, request_tv, requestnote_tv, username, age, distance_txt, note_text, note_text1, note_text2, note, note1, note2, username1, age1, distance_txt1, username2, age2, distance_txt2, username3, age3, check_age, distance_txt3, username4, check_title, age4, check_username, distance_txt4, check_distance_txt;
        ImageView self_iv, user_iv, item_image, note_image, note_image1, note_image2, image1, image2, image3, image4, check_image;
        CardView request_cv, note_cv, note_cv1, note_cv2, cv2, cv3, cv4;
        LinearLayout singleitem_ll, grid_ll, check_ll;

        public GridItemViewHolder(View view) {
            super(view);
            check_ll = view.findViewById(R.id.check_ll);
            check_title = view.findViewById(R.id.check_title);
            check_image = view.findViewById(R.id.check_image);
            check_age = view.findViewById(R.id.check_age);
            check_distance_txt = view.findViewById(R.id.check_distance_txt);
            cv2 = view.findViewById(R.id.cv2);
            cv3 = view.findViewById(R.id.cv3);
            cv4 = view.findViewById(R.id.cv4);
            username = view.findViewById(R.id.username);
            note_text = view.findViewById(R.id.note_text);
            check_username = view.findViewById(R.id.check_username);
            note = view.findViewById(R.id.note);
            note_image = view.findViewById(R.id.note_image);
            note_cv = view.findViewById(R.id.note_cv);
            note_text1 = view.findViewById(R.id.note_text1);
            note1 = view.findViewById(R.id.note1);
            note_image1 = view.findViewById(R.id.note_image1);
            note_cv1 = view.findViewById(R.id.note_cv1);
            note_text2 = view.findViewById(R.id.note_text2);
            grid_ll = view.findViewById(R.id.grid_ll);
            note2 = view.findViewById(R.id.note2);
            note_image2 = view.findViewById(R.id.note_image2);
            note_cv2 = view.findViewById(R.id.note_cv2);
            age = view.findViewById(R.id.age);
            distance_txt = view.findViewById(R.id.distance_txt);
            item_image = view.findViewById(R.id.item_image);
            more_tv = view.findViewById(R.id.more_tv);
            title_tv = view.findViewById(R.id.title_tv);
            request_tv = view.findViewById(R.id.request_tv);
            requestnote_tv = view.findViewById(R.id.requestnote_tv);
            request_cv = view.findViewById(R.id.request_cv);
            singleitem_ll = view.findViewById(R.id.singleitem_ll);
            self_iv = view.findViewById(R.id.self_iv);
            user_iv = view.findViewById(R.id.user_iv);
            username1 = view.findViewById(R.id.username1);
            age1 = view.findViewById(R.id.age1);
            distance_txt1 = view.findViewById(R.id.distance_txt1);
            username2 = view.findViewById(R.id.username2);
            age2 = view.findViewById(R.id.age2);
            distance_txt2 = view.findViewById(R.id.distance_txt2);
            username3 = view.findViewById(R.id.username3);
            age3 = view.findViewById(R.id.age3);
            distance_txt3 = view.findViewById(R.id.distance_txt3);
            username4 = view.findViewById(R.id.username4);
            age4 = view.findViewById(R.id.age4);
            distance_txt4 = view.findViewById(R.id.distance_txt4);
            image2 = view.findViewById(R.id.image2);
            image1 = view.findViewById(R.id.image1);
            image3 = view.findViewById(R.id.image3);
            image4 = view.findViewById(R.id.image4);
            Shader textShader = new LinearGradient(0, 0, 0, 20,
                    new int[]{Color.parseColor("#b697c4"), Color.parseColor("#e77098")},
                    new float[]{0, 1}, Shader.TileMode.CLAMP);
            more_tv.getPaint().setShader(textShader);
        }
    }
}
