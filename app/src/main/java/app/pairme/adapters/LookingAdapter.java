package app.pairme.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.Infodata;
import app.pairme.R;
import app.pairme.activities.AgeActivity;

public class LookingAdapter extends RecyclerView.Adapter<LookingAdapter.HomeViewHolder> {
    Context context;
    List<String> lookingName;
    List<Infodata.Data> lookingFor;
    public LookingAdapter(Context context, List<String> lookingName,List<Infodata.Data> lookingFor) {
        this.context = context;
        this.lookingName = lookingName;
        this.lookingFor = lookingFor;
    }


    @NonNull
    @Override
    public LookingAdapter.HomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RelativeLayout layout = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.looking_recycler_item, parent, false);
        return new HomeViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull final HomeViewHolder holder, final int position) {
        holder.itemView.animate().scaleY(1f).scaleX(1f).setInterpolator(new AccelerateDecelerateInterpolator())
                .setDuration(300).start();
        holder.textView_inactive.setText(lookingName.get(position));
        holder.textView_inactive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.textView_active.setVisibility(View.VISIBLE);
                holder.textView_inactive.setVisibility(View.GONE);
                holder.textView_active.setText(lookingName.get(position));
                Helper.clearLooking(context);
                Helper.setLooking(lookingFor.get(position).name, context);
                Intent intent = new Intent(context, AgeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }
    @Override
    public int getItemCount() {
        return lookingName.size();
    }

    public class HomeViewHolder extends RecyclerView.ViewHolder {
        TextView textView_inactive, textView_active;

        public HomeViewHolder(View itemView) {
            super(itemView);

            textView_inactive = itemView.findViewById(R.id.btn_marriage_inactive);
            textView_active = itemView.findViewById(R.id.btn_marriage_active);


        }
    }


}
