package app.pairme.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.bumptech.glide.Glide;

import java.util.List;

import app.pairme.ClassesPojo.UserInfoPojo.Data;
import app.pairme.R;
import app.pairme.Utils;
import app.pairme.fragments.UserInfoFragment;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    Context context;
    List<Data> data;


    public SearchAdapter(Context context, List<Data> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.search_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.itemView.animate().scaleY(1f).scaleX(1f).setInterpolator(new AccelerateDecelerateInterpolator())
                .setDuration(300).start();
        holder.tv_name.setText(data.get(position).getName());
        holder.tv_age.setText(" . " + data.get(position).getAge());
        holder.tv_distance.setText(data.get(position).getLivesIn());
        Glide.with(context).load("http://pairme.co/img/userProfile/" + data.get(position).getProfileImage()).placeholder(Utils.imageloader(context)).error(data.get(position).getGender().equalsIgnoreCase("male") ? R.drawable.place_holder_m : R.drawable.place_holder_f).into(holder.iv_image);


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name, tv_age, tv_distance;
        ImageView iv_image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.username);
            tv_distance = itemView.findViewById(R.id.distance_txt);
            tv_age = itemView.findViewById(R.id.age);
            iv_image = itemView.findViewById(R.id.image);
           itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    UserInfoFragment myProfileActivity = new UserInfoFragment(data.get(getAdapterPosition()), 5);
                    myProfileActivity.show(((AppCompatActivity) context).getSupportFragmentManager(), UserInfoFragment.TAG);
                }
            });
        }
    }


}