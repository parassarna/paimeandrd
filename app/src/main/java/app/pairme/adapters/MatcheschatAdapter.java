package app.pairme.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import app.pairme.ClassesPojo.UserInfoPojo.Data;
import app.pairme.MainActivity;
import app.pairme.R;
import app.pairme.fragments.UserInfoFragment;
import de.hdodenhof.circleimageview.CircleImageView;

public class MatcheschatAdapter extends RecyclerView.Adapter<MatcheschatAdapter.RequestViewholder> {
    Activity activity;
    List<Data> data;

    public MatcheschatAdapter(Activity activity, List<Data> data) {
        this.activity = activity;
        this.data = data;
    }

    @NonNull
    @Override
    public RequestViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.matchchat_rv, parent, false);
        return new RequestViewholder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull RequestViewholder holder, final int position) {
        if (position >= data.size()) {
            holder.fl.setVisibility(View.VISIBLE);
            holder.match.setVisibility(View.GONE);
            holder.fl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((MainActivity) activity).linear_discover_inactive.performClick();

                }
            });
        } else {
            holder.fl.setVisibility(View.GONE);
            holder.match.setVisibility(View.VISIBLE);
            holder.name.setText(data.get(position).getName());
            Glide.with(activity).load("https://pairme.co/img/userProfile/" + data.get(position).getProfileImage())
                    .error(R.drawable.place_holder).into(holder.image);
            holder.match.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserInfoFragment userInfoFragment = new UserInfoFragment(data.get(position), 7);
                    userInfoFragment.show((((AppCompatActivity) activity).getSupportFragmentManager()), UserInfoFragment.TAG);
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return data.size() + 1;
    }

    class RequestViewholder extends RecyclerView.ViewHolder {
        TextView name;
        CircleImageView image;
        FrameLayout fl;
        LinearLayout match;

        RequestViewholder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
            fl = itemView.findViewById(R.id.fl);
            match = itemView.findViewById(R.id.match);

        }
    }
}
