package app.pairme.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.Infodata;
import app.pairme.R;
import app.pairme.Utils;

import static app.pairme.Utils.isArabic;
import static app.pairme.Utils.language;
import static app.pairme.Utils.multiple;
import static app.pairme.Utils.selected;

public class UpdatinfoAdapter extends RecyclerView.Adapter<UpdatinfoAdapter.HomeViewHolder> {
    OnStatusListner onStatusListner;
    List<String> ids = new ArrayList<>();
    List<String> names = new ArrayList<>();
    Activity activity;
    private List<Infodata.Data> datas;

    public UpdatinfoAdapter(List<Infodata.Data> datas, OnStatusListner onStatusListner) {
        this.datas = datas;
        this.onStatusListner = onStatusListner;
    }

    public UpdatinfoAdapter(Activity activity, List<Infodata.Data> datas, OnStatusListner onStatusListner) {
        this.activity = activity;
        this.datas = datas;
        this.onStatusListner = onStatusListner;
    }

    @NonNull
    @Override
    public UpdatinfoAdapter.HomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new HomeViewHolder(layout, onStatusListner);
    }

    @Override
    public void onBindViewHolder(@NonNull final HomeViewHolder holder, final int position) {
        if (isArabic) {
            holder.textView_inactive.setText(datas.get(position).name);
        } else if (Utils.localisation.has(datas.get(position).name))
           // Log.d("TAG", "onBindViewHolder_rpeort: " + Utils.localisation.get(datas.get(position).name).getAsString().trim());
        holder.textView_inactive.setText(Utils.localisation.get(datas.get(position).name).getAsString().trim());
        if (multiple) {
            String[] selectedstring = selected.split(",");
            for (String data : selectedstring) {
                if (selected != null && data.trim().equalsIgnoreCase(holder.textView_inactive.getText().toString().trim())) {
                    holder.textView_inactive.setBackgroundResource(R.drawable.btn_gradiant);
                    holder.textView_inactive.setTextColor(Color.WHITE);
                    ids.add(holder.textView_inactive.getText().toString());
                    names.add(data.trim());
                }
            }
        } else {
            if (selected != null && selected.trim().equalsIgnoreCase(holder.textView_inactive.getText().toString().trim())) {
                holder.textView_inactive.setBackgroundResource(R.drawable.btn_gradiant);
                holder.textView_inactive.setTextColor(Color.WHITE);
            }
        }
        if (activity != null)
            if (Helper.getGender(activity).equalsIgnoreCase("female") && datas.get(position).name.equalsIgnoreCase("Married"))
                holder.textView_inactive.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public interface OnStatusListner {
        void OnStatusClick(String p, String trim);

        void OnStatusmultClick(List<String> p, List<String> names);
    }

    public class HomeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textView_inactive, textView_active;
        OnStatusListner onStatusListner;

        public HomeViewHolder(View itemView, OnStatusListner onStatusListner) {
            super(itemView);
            textView_inactive = itemView.findViewById(R.id.tv_inactive);
            textView_active = itemView.findViewById(R.id.tv_active);
            this.onStatusListner = onStatusListner;
            itemView.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            if (Utils.multiple) {
                if (names.indexOf(datas.get(getAdapterPosition()).name.trim()) == -1) {
                    if (ids.size() < (language ? 5 : 3)) {
                        textView_inactive.setBackgroundResource(R.drawable.btn_gradiant);
                        textView_inactive.setTextColor(Color.WHITE);
                        ids.add(textView_inactive.getText().toString());
                        names.add(datas.get(getAdapterPosition()).name.trim());
                    }
                } else {
                    textView_inactive.setBackgroundResource(R.drawable.round_tv_bg);
                    textView_inactive.setTextColor(Color.BLACK);
                    ids.remove(textView_inactive.getText().toString());
                    names.remove(datas.get(getAdapterPosition()).name.trim());
                }
                onStatusListner.OnStatusmultClick(ids, names);

            } else {
                onStatusListner.OnStatusClick(textView_inactive.getText().toString(), datas.get(getAdapterPosition()).name.trim());

            }
        }
    }


}
