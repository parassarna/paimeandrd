package app.pairme.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import app.pairme.R;
import app.pairme.activities.BankTransfer;

public class BankAccountsAdapter extends RecyclerView.Adapter<BankAccountsAdapter.HomeViewHolder> {
    Context context;
    List<String> name, iban, image, account;


    public BankAccountsAdapter(Context context, List<String> name, List<String> iban, List<String> image, List<String> account) {
        this.context = context;
        this.name = name;
        this.iban = iban;
        this.image = image;
        this.account = account;
    }

    @NonNull
    @Override
    public BankAccountsAdapter.HomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LinearLayout layout = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bank_account_item, parent, false);
        return new HomeViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull final HomeViewHolder holder, final int position) {
        Glide.with(context).load("https://pairme.co/img/bank/" + image.get(position)).error(R.drawable.place_holder).into(holder.iv_profile);
        holder.tv_name.setText(name.get(position));
        holder.tv_account.setText("A/c:" + account.get(position));
        holder.tv_iban.setText("IBAN" + iban.get(position));
        holder.pay_sbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, BankTransfer.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return name.size();
    }

    public class HomeViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name, tv_account, tv_iban, pay_sbi;
        ImageView iv_profile;

        public HomeViewHolder(View itemView) {
            super(itemView);
            iv_profile = itemView.findViewById(R.id.iv_profile);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_account = itemView.findViewById(R.id.tv_account);
            tv_iban = itemView.findViewById(R.id.tv_iban);
            pay_sbi = itemView.findViewById(R.id.pay_sbi);


        }
    }


}
