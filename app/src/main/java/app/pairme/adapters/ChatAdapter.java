package app.pairme.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;

import app.pairme.ApiServices.Helper;
import app.pairme.R;
import app.pairme.activities.ChatActivity;
import de.hdodenhof.circleimageview.CircleImageView;

import static app.pairme.Utils.imageloader;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.RequestViewholder> {
    Context activity;
    JSONArray name;


    public ChatAdapter(Context activity, JSONArray name) {
        this.activity = activity;
        this.name = name;
        hasStableIds();
    }

    @NonNull
    @Override
    public RequestViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout;
        layout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chatrv, parent, false);
        return new RequestViewholder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull RequestViewholder holder, int position) {
        Log.d("tag", "onBindViewHolder: " + name);
        if (Helper.getLanguage(activity).equalsIgnoreCase("ar")) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.own.getLayoutParams();
            params.addRule(RelativeLayout.ALIGN_PARENT_START);
            params.removeRule(RelativeLayout.ALIGN_PARENT_END);
            holder.own.setLayoutParams(params);
            params = (RelativeLayout.LayoutParams) holder.match_another.getLayoutParams();
            params.addRule(RelativeLayout.ALIGN_PARENT_END);
            holder.match_another.setLayoutParams(params);
        }

        try {
            if (name.getJSONObject(position).has("fromUserId") && !ChatAdapter.this.name.getJSONObject(position).getString("fromUserId").equalsIgnoreCase(((ChatActivity) activity).mUsername)) {
                holder.own.setVisibility(View.GONE);
                holder.time.setText(name.getJSONObject(position).getString("time"));
                holder.match_another.setVisibility(View.VISIBLE);
              /*  holder.name.setText(((ChatActivity) activity).otherusername);
                holder.name.setVisibility(View.GONE);*/

                holder.msg.setText(name.getJSONObject(position).getString("message"));
                Glide.with(activity).load("https://pairme.co/img/userProfile/" + ((ChatActivity) activity).image_chat).placeholder(imageloader(activity)).into(holder.image);

            } else {
                holder.match_another.setVisibility(View.GONE);
                holder.own.setVisibility(View.VISIBLE);
                holder.msganother.setText(name.getJSONObject(position).getString("message"));
                holder.time_own.setText(name.getJSONObject(position).getString("time"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        return name.length();
    }

    class RequestViewholder extends RecyclerView.ViewHolder {
        TextView name, msg, time, time_own, msganother;
        CircleImageView image;
        RelativeLayout match_another;
        LinearLayout own;
        RelativeLayout pl;

        RequestViewholder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
           // name = itemView.findViewById(R.id.name);
            time = itemView.findViewById(R.id.time);
            msg = itemView.findViewById(R.id.msg);
            own = itemView.findViewById(R.id.own);
            pl = itemView.findViewById(R.id.pl);
            time_own = itemView.findViewById(R.id.time_own);
            match_another = itemView.findViewById(R.id.match_another);
            msganother = itemView.findViewById(R.id.msganother);
        }
    }
}
