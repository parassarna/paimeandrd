package app.pairme.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import app.pairme.ClassesPojo.RequestPojo;
import app.pairme.R;
import app.pairme.fragments.LikedFragment;

public class RequetsAdapter extends RecyclerView.Adapter<RequetsAdapter.RequestViewholder> {
    Activity activity;
    RequestPojo requestPojos;

    public RequetsAdapter(Activity activity, RequestPojo requestPojos) {
        this.activity = activity;
        this.requestPojos = requestPojos;
    }

    @NonNull
    @Override
    public RequestViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.request_rv, parent, false);
        return new RequestViewholder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull RequestViewholder holder, int position) {
        holder.data_rv.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false));
        if (position == 0) {
            holder.title_tv.setText(activity.getResources().getString(R.string.Messaged_You));
            holder.more_tv.setVisibility(View.GONE);
            holder.data_rv.setAdapter(new RequetsDataAdapter(activity, requestPojos.messaged_you, 0));
        } else if (position == 1) {
            holder.title_tv.setText(activity.getResources().getString(R.string.liked_you));
            holder.data_rv.setAdapter(new RequetsDataAdapter(activity, requestPojos.likedYou, 1));
        } else if (position == 2) {
            holder.title_tv.setText(activity.getResources().getString(R.string.checked_you_out));
            holder.data_rv.setAdapter(new RequetsDataAdapter(activity, requestPojos.checked, 2));
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    class RequestViewholder extends RecyclerView.ViewHolder {
        TextView title_tv, more_tv;
        RecyclerView data_rv;

        RequestViewholder(@NonNull View itemView) {
            super(itemView);
            title_tv = itemView.findViewById(R.id.title_tv);
            more_tv = itemView.findViewById(R.id.more_tv);
            data_rv = itemView.findViewById(R.id.data_rv);
            data_rv = itemView.findViewById(R.id.data_rv);
            more_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((AppCompatActivity) activity).getSupportFragmentManager().beginTransaction().replace(R.id.frame, new LikedFragment(getAdapterPosition())).addToBackStack(null).commit();
                }
            });
        }
    }
}
