package app.pairme.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.BlockListPojo.Datum;
import app.pairme.ClassesPojo.BlockPojo;
import app.pairme.R;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.pairme.Utils.imageloader;

public class BlockListAdapter extends RecyclerView.Adapter<BlockListAdapter.BlockViewHolder> {
    Context context;
    List<Datum> data;

    public BlockListAdapter(Context context, List<Datum> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public BlockViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.block_list_item, parent, false);
        return new BlockViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final BlockViewHolder holder, final int position) {

        Glide.with(context).load("https://pairme.co/img/userProfile/" + data.get(position).getUserimage()).placeholder(imageloader(context)).placeholder(R.drawable.place_holder_m).into(holder.iv_profile);
        holder.tv_name.setText(data.get(position).getUsername());
        holder.tv_detail.setText(data.get(position).getUserage() + "." + data.get(position).getUserlivein());
        holder.tv_unblock.setVisibility(View.VISIBLE);
        holder.tv_unblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.tv_block.setVisibility(View.VISIBLE);
                holder.tv_unblock.setVisibility(View.GONE);
                unblockUser(data.get(position).getWhomBlocked());
            }
        });
        holder.tv_block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.tv_block.setVisibility(View.VISIBLE);
                holder.tv_unblock.setVisibility(View.GONE);
                blockUser(data.get(position).getId());
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private void blockUser(String id) {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<BlockPojo> call = service.blockUser("Bearer " + Helper.getToken(context), id);
        call.enqueue(new Callback<BlockPojo>() {
            @Override
            public void onResponse(Call<BlockPojo> call, Response<BlockPojo> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                } else {

                    Log.d("error", "else: " + response.body());
                }
            }

            @Override
            public void onFailure(Call<BlockPojo> call, Throwable t) {

                Log.d("error", "Failure; " + t);
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void unblockUser(String id) {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<BlockPojo> call = service.unblockUser("Bearer " + Helper.getToken(context), id);

        call.enqueue(new Callback<BlockPojo>() {
            @Override
            public void onResponse(Call<BlockPojo> call, Response<BlockPojo> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                } else {

                    Log.d("error", "else: " + response.body());
                }
            }
            @Override
            public void onFailure(Call<BlockPojo> call, Throwable t) {
                Log.d("error", "Failure; " + t);
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public class BlockViewHolder extends RecyclerView.ViewHolder {
        CircleImageView iv_profile;
        TextView tv_name, tv_detail, tv_unblock, tv_block;

        public BlockViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_profile = itemView.findViewById(R.id.iv_profile);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_detail = itemView.findViewById(R.id.tv_detail);
            tv_unblock = itemView.findViewById(R.id.tv_unblock);
            tv_block = itemView.findViewById(R.id.tv_block);
        }
    }


}
