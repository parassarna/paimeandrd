package app.pairme.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;

import app.pairme.R;
import app.pairme.activities.ChatActivity;
import de.hdodenhof.circleimageview.CircleImageView;
import io.socket.client.Socket;

public class ChatingAdapter extends RecyclerView.Adapter<ChatingAdapter.MyViewHolder> {
    Socket socket;
    private JSONArray modelList;
    private Activity mcontext;

    public ChatingAdapter(Activity mcontext, JSONArray modelList, Socket socket) {
        this.modelList = modelList;
        this.mcontext = mcontext;
        this.socket = socket;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mcontext).inflate(R.layout.chatlist_rv, parent, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Log.d("tag", "onBindViewHolder: "+modelList);
        try {
            holder.usrname.setText(modelList.getJSONObject(position).getString("name"));
            holder.lastmsg.setText(modelList.getJSONObject(position).getString("last_message"));
            //holder.weekname.setText(modelList.getJSONObject(position).getString(""));
            Glide.with(mcontext).load("https://pairme.co/img/userProfile/" + modelList.getJSONObject(position).getString("profile_image")).into(holder.usr_pic);
            if (modelList.getJSONObject(position).getString("online").equalsIgnoreCase("y"))
                holder.online.setBackground(mcontext.getResources().getDrawable(R.drawable.online));

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return modelList.length();
    }

    public void removeItem(int position) {
        modelList.remove(position);
        notifyItemRemoved(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView usrname, lastmsg, weekname;
        CircleImageView usr_pic, online;
        RelativeLayout view_background, view_foreground;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            usrname = itemView.findViewById(R.id.usrname);
            lastmsg = itemView.findViewById(R.id.lastmsg);
            weekname = itemView.findViewById(R.id.weekname);
            online = itemView.findViewById(R.id.online);
            usr_pic = itemView.findViewById(R.id.usr_pic);
            online = itemView.findViewById(R.id.online);
            view_background = itemView.findViewById(R.id.view_background);
            view_foreground = itemView.findViewById(R.id.view_foreground);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        mcontext.startActivity(ChatActivity.Start(socket, mcontext, modelList.getJSONObject(getAdapterPosition())));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }
}
