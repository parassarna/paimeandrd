package app.pairme.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import app.pairme.ClassesPojo.UserInfoPojo.Data;
import app.pairme.ClassesPojo.UserInfoPojo.PhotoAlbum;
import app.pairme.R;
import app.pairme.Utils;

import static app.pairme.Utils.imageloader;

public class SwipeAdapter extends BaseAdapter {
    Context context;
    Vibrator v;
    private List<Data> userdata = new ArrayList<>();
    private float xValue, yValue, leftPersentage;
    private int height, width;

    public SwipeAdapter(Context context, List<Data> userdata) {
        this.context = context;
        this.userdata = userdata;
    }

    @Override
    public int getCount() {
        return userdata.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {
            final ViewHolder holder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            // If holder not exist then locate all view from UI file.

            if (userdata.get(position).getProfileImage() != null)
                userdata.get(position).photos.add("https://pairme.co/img/userProfile/" + userdata.get(position).getProfileImage());
            for (PhotoAlbum photoAlbum : userdata.get(position).getPhotoAlbum())
                userdata.get(position).photos.add("https://pairme.co/img/userAlbum/" + photoAlbum.getImage());
            if (convertView == null) {
                // inflate UI from XML file
                convertView = inflater.inflate(R.layout.item_user_layout, parent, false);
                // get all UI view
                holder = new ViewHolder(convertView);
                // set tag for holder
                convertView.setTag(holder);
            } else {
                // if holder created, get tag from view
                holder = (ViewHolder) convertView.getTag();
            }
            DisplayMetrics displaymetrics = new DisplayMetrics();
            ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            height = displaymetrics.heightPixels;
            width = displaymetrics.widthPixels;
            leftPersentage = (width) * 50 / 100;
            convertView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    xValue = event.getX();
                    yValue = event.getY();
                    if (xValue <= leftPersentage) {
                        // perform your task
                        --userdata.get(position).counter;
                        Vibrator vib = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            vib.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));
                        } else {
                            vib.vibrate(50);
                        }
                        if (userdata.get(position).counter >= 0)
                            addbottomdots(holder.layoutDots, userdata.get(position).counter, position, holder.image);
                        else userdata.get(position).counter = 0;
                    } else {

                        userdata.get(position).counter++;
                        Vibrator vib = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            vib.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));
                        } else {
                            vib.vibrate(50);
                        }
                        if (userdata.get(position).counter < userdata.get(position).photos.size()) {
                            addbottomdots(holder.layoutDots, userdata.get(position).counter, position, holder.image);
                        } else --userdata.get(position).counter;
                    }
                    return false;
                }

            });
            addbottomdots(holder.layoutDots, userdata.get(position).counter, position, holder.image);

            //setting data to views
            holder.name.setText(userdata.get(position).getName() + " . " + userdata.get(position).getAge());
            holder.distance_txt.setText(userdata.get(position).getLivesIn());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    private void addbottomdots(LinearLayout layoutDots, int pos, int position, ImageView iv_profile) {
        if (userdata.get(position).photos.size() > 0) {
            Glide.with(context).load(userdata.get(position).photos.get(pos))
                    .placeholder(imageloader(context)).error(R.drawable.place_holder).into(iv_profile);
            layoutDots.removeAllViews();
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1.0f);
            param.setMargins(5, 0, 5, 0);
            View[] dots = new View[userdata.get(position).photos.size()];
            for (int i = 0; i < userdata.get(position).photos.size(); i++) {
                dots[i] = new View(context);
                dots[i].setBackgroundColor(Color.GRAY);
                dots[i].setLayoutParams(param);
                layoutDots.addView(dots[i]);
            }

            dots[pos].setBackgroundColor(Color.WHITE);
        } else iv_profile.setImageResource(R.drawable.place_holder);
    }


    private class ViewHolder {
        TextView name, distance_txt;
        ImageView image;
        LinearLayout layoutDots;
        CardView cv;


        private ViewHolder(View view) {
            cv = view.findViewById(R.id.cv);
            name = view.findViewById(R.id.item_name);
            layoutDots = view.findViewById(R.id.layoutDots);
            distance_txt = view.findViewById(R.id.item_city);
            image = view.findViewById(R.id.item_image);
        }

    }
}

