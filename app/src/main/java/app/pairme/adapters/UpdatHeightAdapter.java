package app.pairme.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import app.pairme.ClassesPojo.Infodata;
import app.pairme.R;

import static app.pairme.Utils.selected;

public class UpdatHeightAdapter extends RecyclerView.Adapter<UpdatHeightAdapter.HomeViewHolder> {
    OnStatusListner onStatusListner;
    private List<Infodata.Data> datas;

    public UpdatHeightAdapter(List<Infodata.Data> datas, OnStatusListner onStatusListner) {
        this.datas = datas;
        this.onStatusListner = onStatusListner;
    }

    @NonNull
    @Override
    public UpdatHeightAdapter.HomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new HomeViewHolder(layout, onStatusListner);
    }

    @Override
    public void onBindViewHolder(@NonNull final HomeViewHolder holder, final int position) {
        holder.textView_inactive.setText(datas.get(position).name);
        if (selected != null && selected.trim().equalsIgnoreCase(holder.textView_inactive.getText().toString().trim())) {
            holder.textView_inactive.setBackgroundResource(R.drawable.btn_gradiant);
            holder.textView_inactive.setTextColor(Color.WHITE);

        }

    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public interface OnStatusListner {
        void OnStatusClick(String p, String name);

        void OnStatusmultClick(List<String> p, List<String> name);
    }

    public class HomeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textView_inactive, textView_active;
        OnStatusListner onStatusListner;

        public HomeViewHolder(View itemView, OnStatusListner onStatusListner) {
            super(itemView);
            textView_inactive = itemView.findViewById(R.id.tv_inactive);
            textView_active = itemView.findViewById(R.id.tv_active);
            this.onStatusListner = onStatusListner;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onStatusListner.OnStatusClick(textView_inactive.getText().toString(), datas.get(getAdapterPosition()).name.trim());
        }
    }


}
