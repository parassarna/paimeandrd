package app.pairme.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import app.pairme.ClassesPojo.UserInfoPojo.Data;
import app.pairme.MainActivity;
import app.pairme.R;
import app.pairme.fragments.UserInfoFragment;
import de.hdodenhof.circleimageview.CircleImageView;

import static app.pairme.Utils.imageloader;

public class RequetsDataAdapter extends RecyclerView.Adapter<RequetsDataAdapter.RequestViewholder> {
    Activity activity;
    List<Data> liked;
    int type;

    public RequetsDataAdapter(Activity activity, List<Data> liked, int type) {
        this.activity = activity;
        this.liked = liked;
        this.type = type;
    }


    @NonNull
    @Override
    public RequestViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = null;
        if (type != 3)
            layout = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.requestdata_rv, parent, false);
        else layout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.likeddata_rv, parent, false);
        return new RequestViewholder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull RequestViewholder holder, int position) {
        try{
            if (position >= liked.size()) {
                if (type != 0) {
                    holder.last_cv.setVisibility(View.VISIBLE);
                    holder.note_tv.setText(type == 1 ? activity.getResources().getString(R.string.more_likes) : activity.getResources().getString(R.string.lift_profile));

                    holder.last_cv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (type == 1)
                                ((MainActivity) activity).linear_discover_inactive.performClick();
                            else activity.startActivity(new Intent(activity, MainActivity.class));

                        }
                    });
                }
            } else {
                holder.data_civ.setVisibility(type == 0 ? View.VISIBLE : View.GONE);
                holder.data_cv.setVisibility(type == 0 ? View.GONE : View.VISIBLE);

                Glide.with(activity).load("https://pairme.co/img/userProfile/" + liked.get(position).getProfileImage())
                        .placeholder(imageloader(activity)).error(liked.get(position).getGender().equalsIgnoreCase("male") ? R.drawable.place_holder_m : R.drawable.place_holder_f).into(holder.iv);
                Glide.with(activity).load("https://pairme.co/img/userProfile/" + liked.get(position).getProfileImage())
                        .placeholder(imageloader(activity)).error(liked.get(position).getGender().equalsIgnoreCase("male") ? R.drawable.place_holder_m : R.drawable.place_holder_f).into(holder.data_civ);
                holder.age_tv.setText(" . " + liked.get(position).getAge());
                holder.name_tv.setText(liked.get(position).getName());
                holder.address_tv.setText(liked.get(position).getLivesIn());
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return type != 3 ? liked.size() + 1 : liked.size();
    }

    class RequestViewholder extends RecyclerView.ViewHolder {
        CircleImageView data_civ;
        CardView data_cv, last_cv;
        ImageView iv;
        TextView name_tv, age_tv, address_tv, note_tv;

        RequestViewholder(@NonNull View itemView) {
            super(itemView);
            data_civ = itemView.findViewById(R.id.data_civ);
            data_cv = itemView.findViewById(R.id.data_cv);
            iv = itemView.findViewById(R.id.iv);
            name_tv = itemView.findViewById(R.id.name_tv);
            age_tv = itemView.findViewById(R.id.age_tv);
            address_tv = itemView.findViewById(R.id.address_tv);
            last_cv = itemView.findViewById(R.id.last_cv);
            note_tv = itemView.findViewById(R.id.note_tv);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserInfoFragment userInfoFragment = new UserInfoFragment(liked.get(getAdapterPosition()), type);
                    userInfoFragment.show((((AppCompatActivity) activity).getSupportFragmentManager()), UserInfoFragment.TAG);
                }
            });
        }
    }
}
