package app.pairme.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import app.pairme.ApiServices.Helper;
import app.pairme.R;

public class MediaActivity extends AppCompatActivity implements View.OnClickListener {
    RelativeLayout relative_story, relative_blog, relative_follow_us, relative_latest_news;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media);
        changeStatusBarColor();
        initScreen();
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
        }
    }

    public void initScreen() {
        relative_story = findViewById(R.id.relative_story);
        relative_blog = findViewById(R.id.relative_blog);
        relative_follow_us = findViewById(R.id.relative_follow_us);
       // relative_latest_news = findViewById(R.id.relative_latest_news);
        relative_story.setOnClickListener(this);
        relative_blog.setOnClickListener(this);
        relative_follow_us.setOnClickListener(this);
        if (Helper.getLanguage(this).equalsIgnoreCase("ar")) {
            for (int i = 0; i <= 2; i++) {
                String id = "icon" + (i);
                int resID = getResources().getIdentifier(id, "id",this.getPackageName());
                ((ImageView) findViewById(resID)).setRotation(180);
            }
        }
     //   relative_latest_news.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relative_story:
                startActivity(new Intent(getApplicationContext(), SuccessStory.class));
                break;
            case R.id.relative_blog:
                startActivity(new Intent(getApplicationContext(), BlogScreen.class));
                break;
            case R.id.relative_follow_us:
                startActivity(new Intent(getApplicationContext(), FollowScreen.class));
                break;
           /* case R.id.relative_latest_news:
                startActivity(new Intent(getApplicationContext(), LatestNewsScreen.class));
                break;*/
        }
    }
}
