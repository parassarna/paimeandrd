package app.pairme.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.DescriptionPojo.DescriptionPojo;
import app.pairme.ClassesPojo.Infodata;
import app.pairme.R;
import app.pairme.Utils;
import app.pairme.adapters.UpdatHeightAdapter;
import app.pairme.adapters.UpdatinfoAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.pairme.Utils.getArabic;

public class ContactUs extends AppCompatActivity implements View.OnClickListener {

    Button btn_faq, btn_contact;
    EditText et_name, et_email, et_subject, et_message;
    String id;
    boolean updated;
    Dialog dialog;
    List<Infodata.Data> report = new ArrayList<>();
    RecyclerView RecyclerView;
    HashMap<String, String> named = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        changeStatusBarColor();
        inflateReport();
        initScreen();
    }

    private void inflateReport() {
        Infodata.Data data = new Infodata.Data();
        data.name = getResources().getString(R.string.Fake_User);
        report.add(data);

        data = new Infodata.Data();
        data.name = getResources().getString(R.string.Harassment);
        report.add(data);

        data = new Infodata.Data();
        data.name = getResources().getString(R.string.Inappropriate);
        report.add(data);

        data = new Infodata.Data();
        data.name = getResources().getString(R.string.Scam);
        report.add(data);

        data = new Infodata.Data();
        data.name = getResources().getString(R.string.Underage);
        report.add(data);

        data = new Infodata.Data();
        data.name = getResources().getString(R.string.other);
        report.add(data);

    }

    private void initScreen() {
        btn_faq = findViewById(R.id.btn_faq);
        btn_contact = findViewById(R.id.btn_contact);
        et_name = findViewById(R.id.et_name);
        et_email = findViewById(R.id.et_email);
        et_subject = findViewById(R.id.et_subject);
        et_message = findViewById(R.id.et_message);
        et_name.setText(Helper.getUserData(getApplicationContext()).getName());
        et_email.setText(Helper.getUserData(getApplicationContext()).getEmail());
        id = String.valueOf(Helper.getUserData(getApplicationContext()).getId());
        btn_faq.setOnClickListener(this);
        btn_contact.setOnClickListener(this);
        et_subject.setOnClickListener(this);
        String report = getIntent().getStringExtra("report");
        et_subject.setText(getArabic(ContactUs.this, report));
        ContactUs.this.named.put(getResources().getString(R.string.report), report);

    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_faq:
                startActivity(new Intent(getApplicationContext(), FaqScreen.class));
                break;
            case R.id.et_subject:
                showDialog(getResources().getString(R.string.report), report, et_subject);
                break;
            case R.id.btn_contact:
                if (et_subject.getText().toString().trim().isEmpty()) {
                    et_subject.setError(getResources().getString(R.string.Enter_Your_Subject));
                } else if (et_message.getText().toString().trim().isEmpty()) {
                    et_message.setError(getResources().getString(R.string.Enter_the_message));
                } else {
                    postContactData();
                    startActivity(new Intent(getApplicationContext(), SupportActivity.class));
                }
        }

    }

    private void postContactData() {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<DescriptionPojo> call = service.contactUS("Bearer " + Helper.getToken(getApplicationContext()), id, et_name.getText().toString(), et_email.getText().toString(),
                named.get(getResources().getString(R.string.report)), et_message.getText().toString());
        call.enqueue(new Callback<DescriptionPojo>() {
            @Override
            public void onResponse(Call<DescriptionPojo> call, Response<DescriptionPojo> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(ContactUs.this, String.valueOf(response.body().getSuccess()), Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<DescriptionPojo> call, Throwable t) {
                Log.d("error", "Failure; " + t);
            }
        });


    }

    private void setData(EditText editText, String s) {
        editText.setText(s);
        editText.setBackgroundResource(R.drawable.green_background);
    }

    private void showDialog(final String header, final List<Infodata.Data> data, final EditText editText) {
        Utils.selected = editText.getText().toString().trim();
        dialog = new Dialog(ContactUs.this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_listview);
        TextView tv_header = dialog.findViewById(R.id.tv_header);
        tv_header.setText(header);
        final TextView tv_count = dialog.findViewById(R.id.tv_count);
        Button btndialog = (Button) dialog.findViewById(R.id.btndialog);
        Button btndok = (Button) dialog.findViewById(R.id.btndok);
        ProgressBar progressBar_cyclic = dialog.findViewById(R.id.progressBar_cyclic);
        progressBar_cyclic.setVisibility(View.GONE);
        tv_count.setText(getResources().getString(R.string.tell_us_why) + Helper.getUserData(ContactUs.this).getName());
        btndialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btndok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        RecyclerView = dialog.findViewById(R.id.listview);
        UpdatHeightAdapter interestsAdapter = new UpdatHeightAdapter(data, new UpdatHeightAdapter.OnStatusListner() {
            @Override
            public void OnStatusClick(String p, String trim) {
                ContactUs.this.named.put(header, trim);
                updated = true;
                setData(editText, p);
                dialog.dismiss();
            }

            @Override
            public void OnStatusmultClick(List<String> p, List<String> names) {
                updated = true;

            }
        });
        RecyclerView.setLayoutManager(new LinearLayoutManager(ContactUs.this));
        RecyclerView.setAdapter(interestsAdapter);
        dialog.show();
    }


}
