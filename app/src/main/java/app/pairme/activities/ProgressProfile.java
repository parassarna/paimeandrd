package app.pairme.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.gson.Gson;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.UserInfoPojo.UserInfo;
import app.pairme.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProgressProfile extends AppCompatActivity implements View.OnClickListener {
    float pStatus = 0, status = 0;
    ProgressBar mProgress;
    TextView tv;
    ImageView iv_email_one, iv_email_two, iv_status_pp, iv_status_Appearance, iv_status_lifestyle, iv_status_interest, iv_status_photos, iv_status_plus, iv_verify, iv_verify_one, iv_verify_two, iv_Appearance, iv_lifestyle, iv_interest, iv_photos, iv_status;
    TextView tv_email_one, tv_email_two, tv_status_pp, tv_status_Appearance, tv_status_lifestyle, tv_status_interest, tv_status_photos, tv_status_plus;
    RelativeLayout appearance_rl, ss_rl, lifestyle_rl, desc_rl, profile_rl, interest_rl, album_rl, subscription_rl;
    CardView card_profile_photo,
            card_description,
            card_social_status,
            card_appearance,
            card_lifestyle,
            card_share_interest,
            card_add_photo,
            card_members,
            card_likes,
            card_pairme_plus;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_profile);

        changeStatusBarColor();


    }

    @Override
    protected void onResume() {
        super.onResume();
        getUserdata();
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void initControl() {
        Resources res = getResources();
        Drawable drawable = res.getDrawable(R.drawable.circular);
        mProgress = findViewById(R.id.circularProgressbar);
        mProgress.setProgressDrawable(drawable);
        mProgress.setProgress(0);   // Main Progress
        mProgress.setSecondaryProgress(100); // Secondary Progress
        mProgress.setMax(100); // Maximum Progress
        tv = findViewById(R.id.tv);
        //  card_email = findViewById(R.id.card_email);
        profile_rl = findViewById(R.id.profile_rl);
        interest_rl = findViewById(R.id.interest_rl);
        album_rl = findViewById(R.id.album_rl);
        // iv_email = findViewById(R.id.iv_email);
        iv_email_one = findViewById(R.id.iv_email_one);
        iv_email_two = findViewById(R.id.iv_email_two);
        iv_status_pp = findViewById(R.id.iv_status_pp);
        iv_status_Appearance = findViewById(R.id.iv_status_Appearance);
        iv_status_lifestyle = findViewById(R.id.iv_status_lifestyle);
        iv_status_interest = findViewById(R.id.iv_status_interest);
        iv_status_photos = findViewById(R.id.iv_status_photos);
        //  tv_email = findViewById(R.id.tv_email);
        tv_email_one = findViewById(R.id.tv_email_one);
        tv_email_two = findViewById(R.id.tv_email_two);
        tv_status_pp = findViewById(R.id.tv_status);
        tv_status_Appearance = findViewById(R.id.tv_Appearance);
        tv_status_lifestyle = findViewById(R.id.tv_lifestyle);
        tv_status_interest = findViewById(R.id.tv_interest);
        tv_status_photos = findViewById(R.id.tv_photos);

        //   iv_verify = findViewById(R.id.iv_verify);
        iv_verify_one = findViewById(R.id.iv_verify_one);
        iv_verify_two = findViewById(R.id.iv_verify_two);
        iv_status = findViewById(R.id.iv_status);
        iv_Appearance = findViewById(R.id.iv_Appearance);
        iv_lifestyle = findViewById(R.id.iv_lifestyle);
        iv_interest = findViewById(R.id.iv_interest);
        iv_photos = findViewById(R.id.iv_photos);
        // iv_status_plus = findViewById(R.id.iv_status_plus);
        //  subscription_rl = findViewById(R.id.subscription_rl);
        desc_rl = findViewById(R.id.desc_rl);
        //   email_rl = findViewById(R.id.email_rl);
        lifestyle_rl = findViewById(R.id.lifestyle_rl);
        appearance_rl = findViewById(R.id.appearance_rl);
        ss_rl = findViewById(R.id.ss_rl);
        card_profile_photo = findViewById(R.id.card_profile_photo);
        card_description = findViewById(R.id.card_description);
        card_social_status = findViewById(R.id.card_social_status);
        card_appearance = findViewById(R.id.card_appearance);
        card_lifestyle = findViewById(R.id.card_lifestyle);
        card_share_interest = findViewById(R.id.card_share_interest);
        card_add_photo = findViewById(R.id.card_add_photo);
        // card_pairme_plus = findViewById(R.id.card_pairme_plus);
        //  card_email.setOnClickListener(this);
        card_profile_photo.setOnClickListener(this);
        card_description.setOnClickListener(this);
        card_social_status.setOnClickListener(this);
        card_appearance.setOnClickListener(this);
        card_lifestyle.setOnClickListener(this);
        card_share_interest.setOnClickListener(this);
        card_add_photo.setOnClickListener(this);
        //  card_pairme_plus.setOnClickListener(this);
       /* if (Helper.getUserData(this).getProfileStatus().equalsIgnoreCase("1")) {
            status += 12.5;
            email_rl.setBackground(getResources().getDrawable(R.drawable.background_screen));
            tv_email.setTextColor(getResources().getColor(R.color.white));
            iv_verify.setImageResource(R.drawable.white_email_128);
            seticon(iv_email);
        }*/
        if (Helper.getUserData(this).getDpImageStatus().equalsIgnoreCase("1")) {
            status += 14.29;
            profile_rl.setBackground(getResources().getDrawable(R.drawable.background_screen));
            tv_email_one.setTextColor(getResources().getColor(R.color.white));
            iv_verify_one.setImageResource(R.drawable.white_add_a_profile_photo_128);
            seticon(iv_email_one);
        }
        if (Helper.getUserData(this).getDescriptionStatus().equalsIgnoreCase("1")) {
            status += 14.29;
            seticon(iv_email_two);
            desc_rl.setBackground(getResources().getDrawable(R.drawable.background_screen));
            tv_email_two.setTextColor(getResources().getColor(R.color.white));
            iv_verify_two.setImageResource(R.drawable.white_description_128);
        }
        if (Helper.getUserData(this).getSocialStatusComplete().equalsIgnoreCase("1")) {
            status += 14.29;
            seticon(iv_status_pp);
            ss_rl.setBackground(getResources().getDrawable(R.drawable.background_screen));
            tv_status_pp.setTextColor(getResources().getColor(R.color.white));
            iv_status.setImageResource(R.drawable.white_education_128);
        }
        if (Helper.getUserData(this).getAppearanceStatus().equalsIgnoreCase("1")) {
            status += 14.29;
            seticon(iv_status_Appearance);
            appearance_rl.setBackground(getResources().getDrawable(R.drawable.background_screen));
            tv_status_Appearance.setTextColor(getResources().getColor(R.color.white));
            iv_Appearance.setImageResource(R.drawable.white_eyes_appearance_128);
        }
        if (Helper.getUserData(this).getLifestyleStatus().equalsIgnoreCase("1")) {
            status += 14.29;
            seticon(iv_status_lifestyle);
            lifestyle_rl.setBackground(getResources().getDrawable(R.drawable.background_screen));
            tv_status_lifestyle.setTextColor(getResources().getColor(R.color.white));
            iv_lifestyle.setImageResource(R.drawable.white_lifestyle_128);
        }
        if (Helper.getUserData(this).getInterestStatus().equalsIgnoreCase("1")) {
            status += 14.29;
            seticon(iv_status_interest);
            interest_rl.setBackground(getResources().getDrawable(R.drawable.background_screen));
            tv_status_interest.setTextColor(getResources().getColor(R.color.white));
            iv_interest.setImageResource(R.drawable.white_share_your_interest_128);
        }
        if (Helper.getUserData(this).getPhotoAlbumStatus().equalsIgnoreCase("1")) {
            status += 14.29;
            seticon(iv_status_photos);
            album_rl.setBackground(getResources().getDrawable(R.drawable.background_screen));
            tv_status_photos.setTextColor(getResources().getColor(R.color.white));
            iv_photos.setImageResource(R.drawable.white_add_photos_128);
        }
        /*if (Helper.getUserData(this).getSubscriptionStatus().equalsIgnoreCase("1")) {
            status+=12.5;
            seticon(iv_status_plus);
            subscription_rl.setBackground(getResources().getDrawable(R.drawable.background_screen));
        }*/
        new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                while (pStatus < status) {
                    Log.d("tag", "run: ");
                    pStatus += 14.29;

                    handler.post(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            mProgress.setProgress((int) pStatus);
                            Log.d("tag", "run: "+pStatus);
                            tv.setText((int)pStatus + "%");
                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        // Just to display the progress slowly
                        Thread.sleep(1000); //thread will take approx 3 seconds to finish
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private void seticon(ImageView image) {
        image.setImageResource(R.drawable.ic_check);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.card_email:
                startActivity(new Intent(getApplicationContext(), UpdateEmailActivity.class));
                break;
            case R.id.card_profile_photo:
                startActivity(new Intent(getApplicationContext(), ProfilePicUpdate.class));
                break;
            case R.id.card_description:
                startActivity(new Intent(getApplicationContext(), UpdateDiscriptionActivity.class));
                break;
            case R.id.card_social_status:
                startActivity(new Intent(getApplicationContext(), UpdateSocialStatus.class));
                break;
            case R.id.card_appearance:
                startActivity(new Intent(getApplicationContext(), UpdateAppearanceActivity.class));
                break;
            case R.id.card_lifestyle:
                startActivity(new Intent(getApplicationContext(), UpdateLifestyle.class));
                break;
            case R.id.card_share_interest:
                startActivity(new Intent(getApplicationContext(), UpdateInterestActivity.class));
                break;
            case R.id.card_add_photo:
                startActivity(new Intent(getApplicationContext(), PhotoAlbumActivity.class));
                break;
           /* case R.id.card_pairme_plus:
                PairmePlusActivity plusActivity = new PairmePlusActivity();
                plusActivity.show(getSupportFragmentManager(), PairmePlusActivity.TAG);

                break;*/
        }
    }

    private void getUserdata() {
        // Helper.showLoader(this, "Please Wait....");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<UserInfo> call = service.getUserInfo("Bearer " + Helper.getToken(ProgressProfile.this), Helper.getId(ProgressProfile.this));
        call.enqueue(new Callback<UserInfo>() {

            @Override
            public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                if (response.isSuccessful()) {
                    status = 0;
                    pStatus=0;
                    //  Helper.hideLoader();
                    Helper.setUserData(new Gson().toJson(response.body().getData()), ProgressProfile.this);
                    initControl();
                } else {
                    //   Helper.hideLoader();
                    Log.d("error", "else: " + response.body());
                }
            }

            @Override
            public void onFailure(Call<UserInfo> call, Throwable t) {
                //  Helper.hideLoader();
                Log.d("error", "Failure; " + t);
                Toast.makeText(ProgressProfile.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
