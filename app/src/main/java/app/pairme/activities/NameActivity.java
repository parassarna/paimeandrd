package app.pairme.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import app.pairme.ApiServices.Helper;
import app.pairme.R;

import static app.pairme.Utils.flip;

public class NameActivity extends AppCompatActivity implements View.OnClickListener, TextWatcher {
    ImageView iv_back, iv_error, close_iv, iv_well, fab;
    TextView tv_name, tv_red_error;
    EditText et_name;
    LinearLayout error_ll;
    RelativeLayout main_bg;
    Window window;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name);
        initControl();
    }

    private void initControl() {
        iv_back = findViewById(R.id.iv_back);
        iv_well = findViewById(R.id.iv_well);
        tv_red_error = findViewById(R.id.tv_red_error);
        et_name = findViewById(R.id.et_name);
        tv_name = findViewById(R.id.tv_name);
        iv_error = findViewById(R.id.iv_error);
        main_bg = findViewById(R.id.main_bg);
        error_ll = findViewById(R.id.error_ll);
        close_iv = findViewById(R.id.close_iv);
        close_iv.setOnClickListener(this);
        fab = findViewById(R.id.fab);
        iv_back.setOnClickListener(this);
        fab.setOnClickListener(this);
        et_name.addTextChangedListener(this);
        if (Helper.getLanguage(this).equalsIgnoreCase("ar"))
            flip(fab);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.fab:
                if (et_name.getText().toString().trim().equals("")) {
                    tv_name.setText(getResources().getString(R.string.name_is_required));
                    slideUp(error_ll);
                } else if (et_name.getText().toString().length() < 3) {
                    tv_name.setText(getResources().getString(R.string.must_be_least_characters));
                    slideUp(error_ll);
                } else {
                    Helper.setFirstName(et_name.getText().toString(), this);
                    startActivity(new Intent(getApplicationContext(), EmailActivity.class));
                    break;
                }
                break;
            case R.id.close_iv:
                slidedown(error_ll);
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, LocationActivity.class));
        finish();
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        String txt1String = (charSequence.toString());
        if (txt1String.length() < 4) {
            tv_name.setText(" ");
            iv_well.setVisibility(View.GONE);
            iv_error.setVisibility(View.GONE);
        } else {
            tv_name.setText(txt1String + " " + getResources().getString(R.string.nice_ring_to_it));
            iv_well.setVisibility(View.VISIBLE);
            iv_error.setVisibility(View.GONE);
        }

    }


    @Override
    public void afterTextChanged(Editable editable) {

    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.error_color));
        }
    }

    public void slideUp(View view) {
        iv_well.setVisibility(View.GONE);
        iv_error.setVisibility(View.VISIBLE);
        fab.setVisibility(View.GONE);
        view.startAnimation(AnimationUtils.loadAnimation(this,
                R.anim.slide_up));
        view.setVisibility(View.VISIBLE);
        changeStatusBarColor();
        main_bg.setBackground(getResources().getDrawable(R.drawable.background_red));
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(500);
        }

    }

    public void slidedown(View view) {
        tv_name.setText(" ");
        iv_well.setVisibility(View.GONE);
        iv_error.setVisibility(View.GONE);
        fab.setVisibility(View.VISIBLE);
        view.startAnimation(AnimationUtils.loadAnimation(this,
                R.anim.slide_down));
        view.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                main_bg.setBackground(getResources().getDrawable(R.drawable.background_screen));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    window = getWindow();
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.setStatusBarColor(getResources().getColor(R.color.start_color));
                }
            }
        }, 1000);

    }
}
