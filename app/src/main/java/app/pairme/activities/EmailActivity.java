package app.pairme.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.regex.Pattern;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.EmailPojo;
import app.pairme.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.pairme.Utils.flip;

public class EmailActivity extends AppCompatActivity implements View.OnClickListener, TextWatcher {
    ImageView iv_back, iv_error, close_iv, iv_well;
    ImageView fab;
    TextView tv_error, tv_red_error;
    EditText et_email;
    LinearLayout error_ll;
    RelativeLayout main_bg;
    String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
            "[a-zA-Z0-9_+&*-]+)*@" +
            "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
            "A-Z]{2,7}$";
    Pattern pat = Pattern.compile(emailRegex);
    Window window;

    String[] domains = new String[]{
            /* Default domains included */
            "aol.com", "att.net", "comcast.net", "facebook.com", "gmail.com", "gmx.com", "googlemail.com",
            "google.com", "hotmail.com", "hotmail.co.uk", "mac.com", "me.com", "mail.com", "msn.com",
            "live.com", "sbcglobal.net", "verizon.net", "yahoo.com", "yahoo.co.uk",

            /* Other global domains */
            "email.com", "games.com" /* AOL */, "gmx.net", "hush.com", "hushmail.com", "inbox.com",
            "lavabit.com", "love.com" /* AOL */, "pobox.com", "rocketmail.com" /* Yahoo */,
            "safe-mail.net", "wow.com" /* AOL */, "ygm.com" /* AOL */, "ymail.com" /* Yahoo */, "zoho.com", "fastmail.fm",

            /* United States ISP domains */
            "bellsouth.net", "charter.net", "cox.net", "earthlink.net", "juno.com",

            /* British ISP domains */
            "btinternet.com", "virginmedia.com", "blueyonder.co.uk", "freeserve.co.uk", "live.co.uk",
            "ntlworld.com", "o2.co.uk", "orange.net", "sky.com", "talktalk.co.uk", "tiscali.co.uk",
            "virgin.net", "wanadoo.co.uk", "bt.com",

            /* Domains used in Asia */
            "sina.com", "qq.com", "naver.com", "hanmail.net", "daum.net", "nate.com", "yahoo.co.jp", "yahoo.co.kr", "yahoo.co.id", "yahoo.co.in", "yahoo.com.sg", "yahoo.com.ph",

            /* French ISP domains */
            "hotmail.fr", "live.fr", "laposte.net", "yahoo.fr", "wanadoo.fr", "orange.fr", "gmx.fr", "sfr.fr", "neuf.fr", "free.fr",

            /* German ISP domains */
            "gmx.de", "hotmail.de", "live.de", "online.de", "t-online.de" /* T-Mobile */, "web.de", "yahoo.de",

            /* Russian ISP domains */
            "mail.ru", "rambler.ru", "yandex.ru",

            /* Belgian ISP domains */
            "hotmail.be", "live.be", "skynet.be", "voo.be", "tvcablenet.be",

            /* Argentinian ISP domains */
            "hotmail.com.ar", "live.com.ar", "yahoo.com.ar", "fibertel.com.ar", "speedy.com.ar", "arnet.com.ar",

            /* Domains used in Mexico */
            "hotmail.com", "gmail.com", "yahoo.com.mx", "live.com.mx", "yahoo.com", "hotmail.es", "live.com", "hotmail.com.mx", "prodigy.net.mx", "msn.com"
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);
        initControl();

    }

    private void initControl() {
        iv_back = findViewById(R.id.iv_back);
        fab = findViewById(R.id.fab);
        iv_well = findViewById(R.id.iv_well);
        main_bg = findViewById(R.id.main_bg);
        error_ll = findViewById(R.id.error_ll);
        close_iv = findViewById(R.id.close_iv);
        iv_error = findViewById(R.id.iv_error);
        tv_red_error = findViewById(R.id.tv_red_error);
        close_iv.setOnClickListener(this);
        tv_error = findViewById(R.id.tv_error);
        et_email = findViewById(R.id.et_email);
        iv_back.setOnClickListener(this);
        fab.setOnClickListener(this);
        et_email.addTextChangedListener(this);
        if (Helper.getLanguage(this).equalsIgnoreCase("ar"))
            flip(fab);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.fab:
                if (!et_email.getText().toString().contains("@") || !et_email.getText().toString().contains(".com")) {
                    slideUp(error_ll);
                    tv_red_error.setText(getResources().getString(R.string.email_is_invalid));
                } else {
                    checkEmail();
                }
                break;
            case R.id.close_iv:
                slidedown(error_ll);
                break;
        }

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        String txt1String = (charSequence.toString());
        if (Patterns.EMAIL_ADDRESS.matcher(txt1String).matches()) {
            iv_well.setVisibility(View.VISIBLE);
            iv_error.setVisibility(View.GONE);
        } else {
            iv_well.setVisibility(View.GONE);

        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.error_color));
        }
    }

    public void slideUp(View view) {
        iv_well.setVisibility(View.GONE);
        iv_error.setVisibility(View.VISIBLE);
        fab.setVisibility(View.GONE);
        view.startAnimation(AnimationUtils.loadAnimation(this,
                R.anim.slide_up));
        view.setVisibility(View.VISIBLE);
        changeStatusBarColor();
        main_bg.setBackgroundColor(getResources().getColor(R.color.error_color));
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(500);
        }

    }

    public void slidedown(View view) {
        fab.setVisibility(View.VISIBLE);
        tv_error.setVisibility(View.GONE);
        iv_well.setVisibility(View.GONE);
        iv_error.setVisibility(View.GONE);
        view.startAnimation(AnimationUtils.loadAnimation(this,
                R.anim.slide_down));
        view.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                main_bg.setBackground(getResources().getDrawable(R.drawable.background_screen));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    window = getWindow();
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.setStatusBarColor(getResources().getColor(R.color.start_color));
                }
            }
        }, 1000);

    }

    public void checkEmail() {
        Helper.showLoader(EmailActivity.this, "Please Wait...");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<EmailPojo> call = service.checkEmail(et_email.getText().toString().trim());
        call.enqueue(new Callback<EmailPojo>() {
            @Override
            public void onResponse(Call<EmailPojo> call, Response<EmailPojo> response) {
                Helper.hideLoader();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().success) {
                            Helper.clearEmail(EmailActivity.this);
                            Helper.setEmail(et_email.getText().toString(), EmailActivity.this);
                            startActivity(new Intent(getApplicationContext(), PasswordActivity.class));
                        } else {
                            slideUp(error_ll);
                            tv_red_error.setText(getResources().getString(R.string.email_already));
                        }
                    }

                } else {
                    Log.i(">>>>", response.raw().toString());

                }
            }

            @Override
            public void onFailure(Call<EmailPojo> call, Throwable t) {
                Helper.hideLoader();

                t.printStackTrace();
            }
        });
    }
}
