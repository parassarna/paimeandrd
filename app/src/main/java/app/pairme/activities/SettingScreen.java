package app.pairme.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.JsonObject;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingScreen extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout relative_email, relative_Password, relative_Connections, relative_Location, relative_Membership, relative_Notification, relative_Blocklist, relative_Logout;
    Button declineButton;
    LinearLayout linear_four;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_screen);
        initScreen();
        changeStatusBarColor();
    }

    public void initScreen() {
        relative_email = findViewById(R.id.relative_email);
        relative_Password = findViewById(R.id.relative_Password);
        relative_Connections = findViewById(R.id.relative_Connections);
        relative_Location = findViewById(R.id.relative_Location);
        relative_Membership = findViewById(R.id.relative_Membership);
        relative_Notification = findViewById(R.id.relative_Notification);
        relative_Blocklist = findViewById(R.id.relative_Blocklist);
        relative_Logout = findViewById(R.id.relative_Logout);
        linear_four = findViewById(R.id.linear_four);
        relative_email.setOnClickListener(this);
        relative_Password.setOnClickListener(this);
        relative_Connections.setOnClickListener(this);
        relative_Location.setOnClickListener(this);
        relative_Membership.setOnClickListener(this);
        relative_Notification.setOnClickListener(this);
        relative_Blocklist.setOnClickListener(this);
        relative_Logout.setOnClickListener(this);
        if (Helper.getLanguage(this).equalsIgnoreCase("ar")) {
            for (int i = 0; i <= 7; i++) {
                String id = "icon" + (i);
                int resID = getResources().getIdentifier(id, "id", SettingScreen.this.getPackageName());
                ((ImageView) findViewById(resID)).setRotation(180);
            }
        }
        if (Helper.getUserData(this).getSubscriptionStatus().equalsIgnoreCase("1")) {
            relative_Membership.setVisibility(View.GONE);
        } else
            relative_Membership.setVisibility(Helper.getUserData(SettingScreen.this).getGender().equalsIgnoreCase("male") ? View.VISIBLE : View.GONE);
        linear_four.setVisibility(Helper.getUserData(SettingScreen.this).getGender().equalsIgnoreCase("male") ? View.VISIBLE : View.GONE);
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relative_email:
                startActivity(new Intent(getApplicationContext(), UpdateEmailActivity.class));
                break;
            case R.id.relative_Password:
                startActivity(new Intent(getApplicationContext(), UpdatePasswordActivity.class));
                break;
            case R.id.relative_Connections:
                startActivity(new Intent(getApplicationContext(), ConnectionActivity.class));
                break;
            case R.id.relative_Location:
                startActivity(new Intent(getApplicationContext(), LocationSettingActivity.class));

                break;
            case R.id.relative_Membership:
                startActivity(new Intent(getApplicationContext(), MembershipActivity.class));

                break;
            case R.id.relative_Notification:
                startActivity(new Intent(getApplicationContext(), NotificationActivity.class));

                break;
            case R.id.relative_Blocklist:
                startActivity(new Intent(getApplicationContext(), BlockListActivity.class));

                break;
            case R.id.relative_Logout:
                openDialog();
                break;


        }

    }

    private void openDialog() {
        dialog = new Dialog(SettingScreen.this);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.show();
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        declineButton = dialog.findViewById(R.id.btn_logout);
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                new Helper(getApplicationContext()).Logout();
                ApiInterface service = BaseUrl.createService(ApiInterface.class);
                Call<JsonObject> call = service.updatetoken("Bearer " + Helper.getToken(SettingScreen.this), "null");
                call.enqueue(new Callback<JsonObject>() {

                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if (!response.isSuccessful()) {
                            Log.d("error", "else: " + response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        Log.d("error", "Failure; " + t);
                    }
                });
                startActivity(new Intent(SettingScreen.this, LoginScreen.class));
                finishAffinity();
            }
        });
        TextView cancel = dialog.findViewById(R.id.tv_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


    }


}
