package app.pairme.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import app.pairme.ApiServices.Helper;
import app.pairme.R;

import static app.pairme.Utils.flip;
import static app.pairme.Utils.getNationality;

public class OriginalLocationActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView iv_back, close_iv, fab, iv_well;
    private LinearLayout error_ll;
    private RelativeLayout main_bg;
    private int size;
    private ProgressBar pb;
    private TextView tv_error;
    private Window window;
    private MaterialSpinner spinner_Location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_original_location);
        initControl();
        spinner_Location.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                Helper.setOrgLocation(getNationality(position), OriginalLocationActivity.this);
            }
        });
    }

    private void initControl() {
        iv_back = findViewById(R.id.iv_back);
        fab = findViewById(R.id.fab);
        pb = findViewById(R.id.pb);
        main_bg = findViewById(R.id.main_bg);
        tv_error = findViewById(R.id.tv_error);
        error_ll = findViewById(R.id.error_ll);
        close_iv = findViewById(R.id.close_iv);
        iv_well = findViewById(R.id.iv_well);
        spinner_Location = findViewById(R.id.spinner_location);
        close_iv.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        fab.setOnClickListener(this);
        if (Helper.getLanguage(this).equalsIgnoreCase("ar"))
            flip(fab);

        spinner_Location.setItems(Arrays.asList(getResources().getStringArray(R.array.nationality)));
    }

    public void slideUp(View view) {
        size = 0;
        fab.setVisibility(View.GONE);
        view.startAnimation(AnimationUtils.loadAnimation(this,
                R.anim.slide_up));
        view.setVisibility(View.VISIBLE);
        changeStatusBarColor();
        main_bg.setBackgroundColor(getResources().getColor(R.color.error_color));
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            v.vibrate(500);
        }

    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.error_color));
        }
    }


    public void slidedown(View view) {
        tv_error.setVisibility(View.GONE);
        fab.setVisibility(View.VISIBLE);
        view.startAnimation(AnimationUtils.loadAnimation(this,
                R.anim.slide_down));
        view.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                main_bg.setBackground(getResources().getDrawable(R.drawable.background_screen));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    window = getWindow();
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.setStatusBarColor(getResources().getColor(R.color.start_color));
                }
            }
        }, 1000);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.fab:
                if (spinner_Location.getItems().equals("Any")) {
                    spinner_Location.setError("Choose Nationality");
                } else {
                    //  Helper.setOrgLocation(spinner_Location.getItems().get(spinner_Location.getSelectedIndex()).toString(), this);
                    startActivity(new Intent(OriginalLocationActivity.this, NameActivity.class));
                }

                break;
            case R.id.close_iv:
                slidedown(error_ll);
                break;
        }
    }
}
