package app.pairme.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.CheckEmailPojo.CheckEmailPojo;
import app.pairme.ClassesPojo.LoginPojo.LoginPojo;
import app.pairme.MainActivity;
import app.pairme.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.pairme.Utils.flipview;

public class VerifyEmailActivity extends AppCompatActivity implements View.OnClickListener {

    EditText et_newEmail, et_confirm;
    Button btn_next, btn_login;
    String new_email, confirm_newEmail;
    TextView et_current, tv_add_description;
    RelativeLayout verify_rl;
    String android_id;
    Handler handler;
    Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_email);
        flipview(this, (LinearLayout) findViewById(R.id.linear));
        changeStatusBarColor();
        printKeyHash();
        initControl();
        et_current = findViewById(R.id.et_current_email);
        tv_add_description = findViewById(R.id.tv_add_description);
        verify_rl = findViewById(R.id.verify_rl);
        et_current.setText(Helper.getEmail(this));
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void initControl() {
        et_newEmail = findViewById(R.id.et_new_email);
        et_confirm = findViewById(R.id.et_confirm_email);
        btn_login = findViewById(R.id.btn_login);
        btn_next = findViewById(R.id.btn_next);
        btn_next.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        //setHandler();
    }

    private void setHandler() {
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                Log.d("tag", "run: ");
                loginApi();
                handler.postDelayed(this, 6000);
            }
        };
        handler.postDelayed(runnable, 6000);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                enterInfo();
                break;
            case R.id.btn_next:
                loginApi();


        }
    }

    public void printKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                android_id = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.i("keyhash", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public void loginApi() {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<LoginPojo> call = service.loginApi(Helper.getEmail(VerifyEmailActivity.this), Helper.getPassword(this), android_id, "android");
        call.enqueue(new Callback<LoginPojo>() {
            @Override
            public void onResponse(Call<LoginPojo> call, final Response<LoginPojo> response) {
                if (response.body().getData().confirmed.equalsIgnoreCase("1")) {
                    Helper.clearToken(getApplicationContext());
                    Helper.setToken(response.body().getData().getToken(), getApplicationContext());
                    Helper.setMyId(response.body().getData().getId(), getApplicationContext());
                    Helper.setLogin(false, getApplicationContext());
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    finishAffinity();
                } else {
                    Toast.makeText(VerifyEmailActivity.this, getResources().getString(R.string.verify_your_email), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginPojo> call, Throwable t) {
                Log.d("error", "Failure; " + t);
                Toast.makeText(VerifyEmailActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void enterInfo() {
        new_email = et_newEmail.getText().toString();
        confirm_newEmail = et_confirm.getText().toString();
        if (TextUtils.isEmpty(new_email) || TextUtils.isEmpty(confirm_newEmail)) {
            et_newEmail.setError(getResources().getString(R.string.enter_new_email));
        } else {
            if (new_email.equals(confirm_newEmail)) {
                verifyEmail();
            } else {
                Toast.makeText(this, "Email Not Matches", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void verifyEmail() {
        Helper.showLoader(VerifyEmailActivity.this, "Please wait...");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<CheckEmailPojo> call = service.updadeEmail("Bearer " +
                Helper.getToken(getApplicationContext()), new_email, confirm_newEmail);
        call.enqueue(new Callback<CheckEmailPojo>() {
            @Override
            public void onResponse(Call<CheckEmailPojo> call, Response<CheckEmailPojo> response) {
                Helper.hideLoader();
                if (response.body().getSuccess().equalsIgnoreCase("success")) {
                    Toast.makeText(VerifyEmailActivity.this, response.body().getData(), Toast.LENGTH_SHORT).show();
                    Helper.setEmail(new_email, VerifyEmailActivity.this);
                    //setHandler();
                } else {
                    et_newEmail.requestFocus();
                    et_newEmail.setError(getResources().getString(R.string.email_already));
                }
            }

            @Override
            public void onFailure(Call<CheckEmailPojo> call, Throwable t) {
                Helper.hideLoader();
                t.printStackTrace();
            }
        });
    }
}
