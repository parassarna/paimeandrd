package app.pairme.activities;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.DescriptionPojo.DescriptionPojo;
import app.pairme.ClassesPojo.Infodata;
import app.pairme.R;
import app.pairme.Utils;
import app.pairme.adapters.UpdatinfoAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.pairme.Utils.getArabic;

public class UpdateSocialStatus extends AppCompatActivity implements View.OnClickListener {
    Button btn_next, btn_skip;
    EditText et_education, et_occupation, et_sector, et_status, et_kids, et_want_kids, et_livingStation, et_relocate;
    List<String> educationName;
    Dialog dialog;
    ImageView iv_well, iv_well_occupation, iv_well_activity, iv_well_status, iv_well_kids, iv_well_want_kids, iv_well_living, iv_well_relocate;
    ProgressBar progressBar_cyclic;
    RecyclerView recyclerView;
    boolean updated;
    TextView tv_save;
    HashMap<String, String> name = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Utils.isArabic = Helper.getLanguage(this).equalsIgnoreCase("en");
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_social_status);
        changeStatusBarColor();
        initControl();
        initdata();
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void initControl() {
        et_education = findViewById(R.id.et_education);
        et_occupation = findViewById(R.id.et_occupation);
        et_sector = findViewById(R.id.et_sector);
        et_status = findViewById(R.id.et_status);
        et_kids = findViewById(R.id.et_kids);
        et_want_kids = findViewById(R.id.et_want_kids);
        et_livingStation = findViewById(R.id.et_living);
        et_relocate = findViewById(R.id.et_relocate);
        tv_save = findViewById(R.id.tv_save);
        iv_well = findViewById(R.id.iv_well);
        iv_well_occupation = findViewById(R.id.iv_well_occupation);
        iv_well_activity = findViewById(R.id.iv_well_activity);
        iv_well_status = findViewById(R.id.iv_well_status);
        iv_well_kids = findViewById(R.id.iv_well_kids);
        iv_well_want_kids = findViewById(R.id.iv_well_want_kids);
        iv_well_living = findViewById(R.id.iv_well_living);
        iv_well_relocate = findViewById(R.id.iv_well_relocate);
        et_education.setOnClickListener(this);
        et_occupation.setOnClickListener(this);
        et_sector.setOnClickListener(this);
        et_kids.setOnClickListener(this);
        et_status.setOnClickListener(this);
        et_want_kids.setOnClickListener(this);
        et_livingStation.setOnClickListener(this);
        et_relocate.setOnClickListener(this);
        tv_save.setOnClickListener(this);
        educationName = new ArrayList<String>();
    }

    private void initdata() {
        if (Helper.getUserData(this) != null) {
            et_education.setText(getArabic(this, Helper.getUserData(this).getEducation()));
            et_occupation.setText(getArabic(this, Helper.getUserData(this).getOccupation()));
            et_sector.setText(getArabic(this, Helper.getUserData(this).getJobSector()));
            et_status.setText(getArabic(this, Helper.getUserData(this).getRelationship()));
            et_kids.setText(getArabic(this, Helper.getUserData(this).getHaveKids()));
            et_want_kids.setText(getArabic(this, Helper.getUserData(this).getWantKids()));
            et_livingStation.setText(getArabic(this, Helper.getUserData(this).getLivingSituation()));
            et_relocate.setText(getArabic(this, Helper.getUserData(this).getWillingToRelocate()));
            name.put(getResources().getString(R.string.education), Helper.getUserData(this).getEducation());
            name.put(getResources().getString(R.string.occupation), Helper.getUserData(this).getOccupation());
            name.put(getResources().getString(R.string.sector), Helper.getUserData(this).getJobSector());
            name.put(getResources().getString(R.string.status), Helper.getUserData(this).getRelationship());
            name.put(getResources().getString(R.string.kids), Helper.getUserData(this).getHaveKids());
            name.put(getResources().getString(R.string.want_kids), Helper.getUserData(this).getWantKids());
            name.put(getResources().getString(R.string.living_situation), Helper.getUserData(this).getLivingSituation());
            name.put(getResources().getString(R.string.willing_to_relocate), Helper.getUserData(this).getWillingToRelocate());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_save:
                UpdateSocialStatusApi();
                break;
            case R.id.et_education:
                showDialog(getResources().getString(R.string.education), Helper.getinfodata(UpdateSocialStatus.this).socialStatus.educations, et_education, iv_well);
                break;
            case R.id.et_occupation:
                showDialog(getResources().getString(R.string.occupation), Helper.getinfodata(UpdateSocialStatus.this).socialStatus.occupation, et_occupation, iv_well_occupation);
                break;
            case R.id.et_sector:
                showDialog(getResources().getString(R.string.sector), Helper.getinfodata(UpdateSocialStatus.this).socialStatus.jobSector, et_sector, iv_well_activity);
                break;
            case R.id.et_kids:
                showDialog(getResources().getString(R.string.kids), Helper.getinfodata(UpdateSocialStatus.this).socialStatus.kidsList, et_kids, iv_well_kids);
                break;
            case R.id.et_status:
                showDialog(getResources().getString(R.string.status), Helper.getinfodata(UpdateSocialStatus.this).socialStatus.relStatus, et_status, iv_well_status);
                break;
            case R.id.et_want_kids:
                showDialog(getResources().getString(R.string.want_kids), Helper.getinfodata(UpdateSocialStatus.this).socialStatus.wantKidsList, et_want_kids, iv_well_want_kids);
                break;
            case R.id.et_living:
                showDialog(getResources().getString(R.string.living_situation), Helper.getinfodata(UpdateSocialStatus.this).socialStatus.wantLivingList, et_livingStation, iv_well_living);
                break;
            case R.id.et_relocate:
                showDialog(getResources().getString(R.string.willing_to_relocate), Helper.getinfodata(UpdateSocialStatus.this).socialStatus.relocateList, et_relocate, iv_well_relocate);
                break;
        }

    }

    private void UpdateSocialStatusApi() {
        Helper.showLoader(this, "Please wait..");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<DescriptionPojo> call = service.addSocialStatus("Bearer " + Helper.getToken(UpdateSocialStatus.this), name.get(getResources().getString(R.string.education)), name.get(getResources().getString(R.string.occupation)), name.get(getResources().getString(R.string.sector)), name.get(getResources().getString(R.string.status)), name.get(getResources().getString(R.string.kids)), name.get(getResources().getString(R.string.want_kids)), name.get(getResources().getString(R.string.living_situation)), name.get(getResources().getString(R.string.willing_to_relocate)));
        call.enqueue(new Callback<DescriptionPojo>() {
            @Override
            public void onResponse(Call<DescriptionPojo> call, Response<DescriptionPojo> response) {
                Helper.hideLoader();
                if (response.isSuccessful()) {
                    finish();

                } else {

                    Log.i(">>>>", response.raw().toString());
                }
            }

            @Override
            public void onFailure(Call<DescriptionPojo> call, Throwable t) {
                Helper.hideLoader();
                t.printStackTrace();
            }
        });

    }

    private void showDialog(final String header, final List<Infodata.Data> datas, final EditText editText, final ImageView iv_well) {
        Utils.selected = editText.getText().toString().trim();
        dialog = new Dialog(this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_listview);
        TextView tv_header = dialog.findViewById(R.id.tv_header);
        tv_header.setText(header);
        Button btndialog = dialog.findViewById(R.id.btndialog);
        progressBar_cyclic = dialog.findViewById(R.id.progressBar_cyclic);
        progressBar_cyclic.setVisibility(View.GONE);
        btndialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        recyclerView = dialog.findViewById(R.id.listview);
        UpdatinfoAdapter interestsAdapter = new UpdatinfoAdapter(datas, new UpdatinfoAdapter.OnStatusListner() {
            @Override
            public void OnStatusClick(String p, String trim) {
                UpdateSocialStatus.this.name.put(header, trim);
                updated = true;
                editText.setText(p);
                editText.setBackgroundResource(R.drawable.green_background);
                iv_well.setVisibility(View.VISIBLE);
                dialog.dismiss();
            }

            @Override
            public void OnStatusmultClick(List<String> p, List<String> names) {

            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(interestsAdapter);
        dialog.show();
    }


}
