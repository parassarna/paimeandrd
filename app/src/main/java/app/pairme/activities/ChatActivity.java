package app.pairme.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.Message;
import app.pairme.MyApplication;
import app.pairme.R;
import app.pairme.adapters.ChatAdapter;
import de.hdodenhof.circleimageview.CircleImageView;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int TYPING_TIMER_LENGTH = 600;
    static JSONObject data;
    private static Socket socket;
    public RecyclerView myRecylerView;
    public ChatAdapter chatBoxAdapter;
    public EditText messagetxt;
    public TextView send, typing_tv, match_tv, tv_chaterName;
    public String mUsername, otherusername, image_chat; /*Helper.getUserData(this).getId().toString()*/
    LinearLayoutManager mLayoutManager;
    Calendar c;
    NestedScrollView scrollView;
    String id;
    ImageView menu_iv;
    LinearLayout bottom_sheet, ll_cancel;
    JSONArray messagesArray = new JSONArray();
    String date, time;
    JSONObject jsonObject;
    CircleImageView image;
    private BottomSheetBehavior sheetBehavior;
    private List<Message> MessageList;
    private Handler mTypingHandler = new Handler();
    private Handler mGetHandler = new Handler();
    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Log.d("tag", "run:messages " + data);
                    jsonObject = new JSONObject();
                    try {
                        jsonObject.put("type", "text");
                        jsonObject.put("fromUserId", data.getString("fromUserId"));
                        jsonObject.put("toUserId", data.getString("toUserId"));
                        jsonObject.put("toSocketId", data.getString("toSocketId"));
                        jsonObject.put("message", data.getString("message"));
                        jsonObject.put("date", data.getString("date"));
                        jsonObject.put("time", data.getString("time"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    messagesArray.put(jsonObject);
                    addMessage();
                    scrollToBottom();
                }
            });
        }
    };


//    private Emitter.Listener onNotification = new Emitter.Listener() {
//        @Override
//        public void call(final Object... args) {
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    JSONObject data = (JSONObject) args[0];
//                    Log.d("tag", "run:notification " + data);
//                    jsonObject = new JSONObject();
//                    try {
//                        jsonObject.put("type", "text");
//                        jsonObject.put("user_id", "137");
//                        jsonObject.put("message", "hi pulkit rana");
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    messagesArray.put(jsonObject);
//                    scrollToBottom();
//                }
//            });
//        }
//    };

    private Emitter.Listener getMessages = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    JSONObject data = (JSONObject) args[0];
                    Log.d("tag", "run: " + data);
                    try {
                        messagesArray = data.getJSONArray("result");
                        addMessage();
                    } catch (Exception e) {
                        Log.d("TAG", "attemptSend: " + e.getMessage());
                        e.printStackTrace();
                    }
                }
            });
        }
    };
    private Emitter.Listener onTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    try {
                        if (data.getString("typing").equalsIgnoreCase("typing...") && data.getString("to_socket_id").equalsIgnoreCase(ChatActivity.this.data.getString("socket_id"))) {
                            typing_tv.setVisibility(View.VISIBLE);
                        } else {
                            typing_tv.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            });
        }
    };
    private Emitter.Listener onStopTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    try {
                        username = data.getString("from_user_id");
                    } catch (Exception e) {
                        Log.e("TAG", e.getMessage());
                        return;
                    }
                    removeTyping(username);
                }
            });
        }
    };
    private Runnable onTypingTimeout = new Runnable() {
        @Override
        public void run() {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("typing", false);
                jsonObject.put("socket_id", data.getString("socket_id"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            socket.emit("typing", jsonObject);
        }
    };


    public static Intent Start(Socket sockets, Activity activity, JSONObject id) {
        data = id;
        socket = sockets;
        return new Intent(activity, ChatActivity.class);
    }

    private void removeTyping(String username) {
        for (int i = MessageList.size() - 1; i >= 0; i--) {
            Message message = MessageList.get(i);
            if (message.getFrom_user_id().equals(username)) {
                MessageList.remove(i);
                chatBoxAdapter.notifyItemRemoved(i);
                scrollToBottom();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUsername = Helper.getUserData(this).getId().toString();
        date = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        time = new SimpleDateFormat("hh:mm aa", Locale.getDefault()).format(new Date());
        setContentView(R.layout.activity_chat);
        messagetxt = findViewById(R.id.message);
        image = findViewById(R.id.image);
        send = findViewById(R.id.send);
        match_tv = findViewById(R.id.match_tv);
        scrollView = findViewById(R.id.scrollView);
        typing_tv = findViewById(R.id.typing_tv);
        MessageList = new ArrayList<>();
        bottom_sheet = findViewById(R.id.bottom_sheet);
        sheetBehavior = BottomSheetBehavior.from(bottom_sheet);
        myRecylerView = findViewById(R.id.messagelist);
        tv_chaterName = findViewById(R.id.tv_chaterName);

        ll_cancel = findViewById(R.id.ll_cancel);
        menu_iv = findViewById(R.id.menu_iv);
        mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        myRecylerView.setLayoutManager(mLayoutManager);
        myRecylerView.setItemAnimator(new DefaultItemAnimator());
        chatBoxAdapter = new ChatAdapter(this, messagesArray);
        menu_iv.setOnClickListener(this);
        ll_cancel.setOnClickListener(this);
        send.setOnClickListener(this);
        tv_chaterName.setOnClickListener(this);
        myRecylerView.setHasFixedSize(true);
        myRecylerView.setAdapter(chatBoxAdapter);
        try {
            otherusername = data.getString("name");
            tv_chaterName.setText(otherusername);
            image_chat = data.getString("profile_image");
            match_tv.setText(otherusername);
            Glide.with(this).load("https://pairme.co/img/userProfile/" + data.getString("profile_image")).placeholder(R.drawable.place_holder_m).into(image);
        } catch (Exception e) {
            e.printStackTrace();
        }
        MyApplication app = (MyApplication) getApplication();
        socket = app.getSocket();

        final JSONObject jsonObject = new JSONObject();
        try {
            id = data.getString("id");
            jsonObject.put("fromUserId", mUsername);
            jsonObject.put("toUserId", data.getString("id"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        socket.emit("getMessages", jsonObject);
        socket.emit("addMessage", jsonObject);
        socket.on("getMessagesResponse", getMessages);
        socket.on("addMessageResponse", onNewMessage);

//        socket.on("getnotification", onNotification);
//        socket.on("sendios_notification", onNotification);
        socket.on("typing", onTyping);
        messagetxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("typing", "typing...");
                    jsonObject.put("socket_id", data.getString("socket_id"));
                    socket.emit("typing", jsonObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mTypingHandler.postDelayed(onTypingTimeout, TYPING_TIMER_LENGTH);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!messagetxt.getText().toString().isEmpty())
                    attemptSend();
            }
        });
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImage();
            }
        });


    }

    private void showImage() {
        Dialog dialog = new Dialog(ChatActivity.this);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog);
        ImageView tv_header = dialog.findViewById(R.id.imageView);
        try {
            Glide.with(this).load("https://pairme.co/img/userProfile/" + data.getString("profile_image")).into(tv_header);
        } catch (Exception e) {
            e.printStackTrace();
        }
        dialog.show();

    }

    private void addTyping(String from_user_id, String message, String toUserId, String toSocketId) {
        MessageList.add(new Message(from_user_id, message, toUserId, toSocketId));
        chatBoxAdapter.notifyItemInserted(MessageList.size() - 1);
        scrollToBottom();
    }

    private void attemptSend() {
        String message = messagetxt.getText().toString().trim();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "text");
            jsonObject.put("fileFormat", "");
            jsonObject.put("filePath", " ");
            jsonObject.put("fromUserId", mUsername);
            jsonObject.put("toUserId", data.getString("id"));
            jsonObject.put("message", message);
            jsonObject.put("date", date);
            jsonObject.put("time", time);
            jsonObject.put("toSocketId", data.getString("socket_id"));
        } catch (Exception e) {
            Log.d("tag", "attemptSend: " + e.getMessage());
            e.printStackTrace();
        }
        socket.emit("addMessage", jsonObject);
        socket.on("addMessageResponse", onNewMessage);
        messagesArray.put(jsonObject);
        addMessage();
        messagetxt.setText("");
        scrollToBottom();
    }


    private void addMessage() {
        chatBoxAdapter = new ChatAdapter(ChatActivity.this, messagesArray);
        myRecylerView.setAdapter(chatBoxAdapter);
        scrollToBottom();
    }

    private void scrollToBottom() {
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
                messagetxt.requestFocus();
            }
        });
        myRecylerView.scrollToPosition(chatBoxAdapter.getItemCount() - 1);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menu_iv:
                if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
                break;
            case R.id.ll_cancel:
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                break;
            case R.id.send:

                break;
            case R.id.tv_chaterName:
                //  UserInfoFragment userInfoFragment = new UserInfoFragment(data.getString("id"), 2);
                //  userInfoFragment.show(ChatActivity.this.getSupportFragmentManager(), UserInfoFragment.TAG);
                break;
        }

    }

}
