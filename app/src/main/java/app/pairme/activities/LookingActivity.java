package app.pairme.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import app.pairme.ApiServices.Helper;
import app.pairme.R;
import app.pairme.Utils;
import app.pairme.adapters.LookingAdapter;

import static app.pairme.Utils.isArabic;

public class LookingActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView iv_back;
    List<String> name;
    RecyclerView recyclerView;
    LookingAdapter lookingAdapter;
    ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.isArabic = Helper.getLanguage(this).equalsIgnoreCase("en");
        setContentView(R.layout.activity_looking);
        name = new ArrayList<String>();
        initControl();

        recyclerView = findViewById(R.id.recycler_lookingFor);

        for (int i = 0; i < Helper.getinfodata(this).basic.lookingFor.size(); i++) {
            if (isArabic) {
                name.add(Helper.getinfodata(this).basic.lookingFor.get(i).name);
            } else
                name.add(Utils.localisation.get(Helper.getinfodata(this).basic.lookingFor.get(i).name).getAsString());
        }
        lookingAdapter = new LookingAdapter(getApplicationContext(), name,Helper.getinfodata(this).basic.lookingFor);
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManagaer);
        recyclerView.setAdapter(lookingAdapter);


    }


    private void initControl() {
        iv_back = findViewById(R.id.iv_back);
        pb = findViewById(R.id.pb);
        iv_back.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.iv_back) {
            startActivity(new Intent(getApplicationContext(), GenderActivity.class));
            finish();
        }
    }

}
