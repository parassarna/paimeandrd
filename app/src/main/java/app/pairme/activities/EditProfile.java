package app.pairme.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.UserInfoPojo.UserInfo;
import app.pairme.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.pairme.Utils.getArabic;

public class EditProfile extends AppCompatActivity implements View.OnClickListener {
    RelativeLayout relative_profile,
            relative_photo_album, relative_description, relative_name, relative_basics, relative_Social_status, relative_appearance,
            relative_lifestyle, relative_interest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        changeStatusBarColor();
        initControl();
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void initControl() {
        relative_profile = findViewById(R.id.relative_profile);
        relative_photo_album = findViewById(R.id.relative_photo_album);
        relative_description = findViewById(R.id.relative_description);
        relative_name = findViewById(R.id.relative_name);
        relative_basics = findViewById(R.id.relative_basics);
        relative_Social_status = findViewById(R.id.relative_Social_status);
        relative_appearance = findViewById(R.id.relative_appearance);
        relative_lifestyle = findViewById(R.id.relative_lifestyle);
        relative_interest = findViewById(R.id.relative_interest);
        relative_profile.setOnClickListener(this);
        relative_photo_album.setOnClickListener(this);
        relative_description.setOnClickListener(this);
        relative_name.setOnClickListener(this);
        relative_basics.setOnClickListener(this);
        relative_Social_status.setOnClickListener(this);
        relative_appearance.setOnClickListener(this);
        relative_lifestyle.setOnClickListener(this);
        relative_interest.setOnClickListener(this);
        if (Helper.getLanguage(this).equalsIgnoreCase("ar")) {
            for (int i = 0; i <= 8; i++) {
                String id = "icon" + (i);
                int resID = getResources().getIdentifier(id, "id", EditProfile.this.getPackageName());
                ((ImageView) findViewById(resID)).setRotation(180);
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        getUserdata();
    }

    private void getUserdata() {
        // Helper.showLoader(this, "Please Wait....");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<UserInfo> call = service.getUserInfo("Bearer " + Helper.getToken(EditProfile.this), Helper.getId(EditProfile.this));
        call.enqueue(new Callback<UserInfo>() {

            @Override
            public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                if (response.isSuccessful()) {
                    //  Helper.hideLoader();
                    Helper.setUserData(new Gson().toJson(response.body().getData()), EditProfile.this);


                } else {
                    //   Helper.hideLoader();
                    Log.d("error", "else: " + response.body());
                }
            }

            @Override
            public void onFailure(Call<UserInfo> call, Throwable t) {
                //  Helper.hideLoader();
                Log.d("error", "Failure; " + t);
                Toast.makeText(EditProfile.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.relative_profile:
                startActivity(new Intent(getApplicationContext(), ProfilePicUpdate.class));
                break;
            case R.id.relative_photo_album:
                startActivity(new Intent(getApplicationContext(), PhotoAlbumActivity.class));
                break;
            case R.id.relative_description:
                startActivity(new Intent(getApplicationContext(), UpdateDiscriptionActivity.class));
                break;
            case R.id.relative_name:
                startActivity(new Intent(getApplicationContext(), UpdateNameActivity.class));
                break;
            case R.id.relative_basics:
                startActivity(new Intent(getApplicationContext(), UpdateBasicsActivity.class));
                break;
            case R.id.relative_Social_status:
                startActivity(new Intent(getApplicationContext(), UpdateSocialStatus.class));
                break;
            case R.id.relative_appearance:
                startActivity(new Intent(getApplicationContext(), UpdateAppearanceActivity.class));
                break;
            case R.id.relative_lifestyle:
                startActivity(new Intent(getApplicationContext(), UpdateLifestyle.class));
                break;
            case R.id.relative_interest:
                startActivity(new Intent(getApplicationContext(), UpdateInterestActivity.class));
                break;

        }

    }
}
