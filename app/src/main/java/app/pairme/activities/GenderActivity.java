package app.pairme.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import app.pairme.ApiServices.Helper;
import app.pairme.R;

public class GenderActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView iv_back;
    Button btn_man_inactive, btn_man_active, btn_woman_inactive, btn_woman_active;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gender);

        initControl();
    }

    private void initControl() {
        iv_back = findViewById(R.id.iv_back);
        btn_man_inactive = findViewById(R.id.btn_man_inactive);
        btn_man_active = findViewById(R.id.btn_man_active);
        btn_woman_inactive = findViewById(R.id.btn_woman_inactive);
        btn_woman_active = findViewById(R.id.btn_woman_active);
        iv_back.setOnClickListener(this);
        btn_man_inactive.setOnClickListener(this);
        btn_man_active.setOnClickListener(this);
        btn_woman_inactive.setOnClickListener(this);
        btn_woman_active.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                startActivity(new Intent(getApplicationContext(), LoginScreen.class));
                finish();
                break;
            case R.id.btn_man_inactive:

                btn_man_inactive.setVisibility(View.GONE);
                btn_man_active.setVisibility(View.VISIBLE);
                btn_woman_inactive.setVisibility(View.VISIBLE);
                btn_woman_active.setVisibility(View.GONE);
                Helper.clearGender(this);
                Helper.setGender("male", this);
                startActivity(new Intent(getApplicationContext(), LookingActivity.class));
                break;
            case R.id.btn_woman_inactive:
                btn_woman_active.setVisibility(View.VISIBLE);
                btn_woman_inactive.setVisibility(View.GONE);
                btn_man_active.setVisibility(View.GONE);
                btn_man_inactive.setVisibility(View.VISIBLE);
                Helper.clearGender(this);
                Helper.setGender("female", this);
                startActivity(new Intent(getApplicationContext(), LookingActivity.class));
                break;

        }
    }

}
