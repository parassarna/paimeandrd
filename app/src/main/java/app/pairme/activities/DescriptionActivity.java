package app.pairme.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.wang.avi.AVLoadingIndicatorView;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ClassesPojo.DescriptionPojo.DescriptionPojo;
import app.pairme.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.pairme.Utils.flipview;

public class DescriptionActivity extends AppCompatActivity implements View.OnClickListener {
    Button btn_skip, btn_next;
    EditText et_description, et_searching;
    TextView tv_error, tv_error_one;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);
        initControl();
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void initControl() {
        flipview(this,(LinearLayout)findViewById(R.id.linear));
        btn_next = findViewById(R.id.btn_next);
        btn_skip = findViewById(R.id.btn_skip);
        btn_skip.setOnClickListener(this);
        btn_next.setOnClickListener(this);
        et_description = findViewById(R.id.et_description);
        et_searching = findViewById(R.id.et_searching);
        tv_error = findViewById(R.id.tv_error);
        tv_error_one = findViewById(R.id.tv_error_one);
        changeStatusBarColor();
        et_description.setOnClickListener(this);
        et_description.addTextChangedListener(new DescriptionActivity.TextCustomTextWatcher(et_description));
        et_searching.setOnClickListener(this);
        et_searching.addTextChangedListener(new DescriptionActivity.TextCustomTextWatcher(et_searching));


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_skip:
                startActivity(new Intent(getApplicationContext(), SocialStatusActivity.class));
                break;
            case R.id.btn_next:
                addDescription();
                startActivity(new Intent(getApplicationContext(), SocialStatusActivity.class));
                break;
        }

    }

    public void addDescription() {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<DescriptionPojo> call = service.addDescription("Bearer " + getIntent().getStringExtra("token"), et_description.getText().toString(), et_searching.getText().toString());
        // Call<DescriptionPojo> call = service.addDescription("Bearer " + Helper.getToken(this), et_desp.getText().toString(), et_searching.getText().toString());

        call.enqueue(new Callback<DescriptionPojo>() {
            @Override
            public void onResponse(Call<DescriptionPojo> call, Response<DescriptionPojo> response) {
                if (response.isSuccessful()) {
                } else {
                    Log.i(">>>>", response.raw().toString());
                }
            }

            @Override
            public void onFailure(Call<DescriptionPojo> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public class TextCustomTextWatcher implements TextWatcher {
        View editText;

        public TextCustomTextWatcher(View editText) {
            this.editText = editText;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            switch (editText.getId()) {
                case R.id.et_description:
                    if (s.length() < 0) {
                        et_description.setBackgroundResource(R.drawable.black_background);
                    }
                    break;
                case R.id.et_searching:
                    if (s.length() < 0) {
                        et_searching.setBackgroundResource(R.drawable.black_background);
                    }
                    break;

            }

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {


        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (editText.getId()) {
                case R.id.et_description:
                    if (s.length() > 0) {
                        String txt1String = (s.toString());
                        if (txt1String.length() <= 14) {
                            tv_error.setVisibility(View.GONE);
                            et_description.setBackgroundResource(R.drawable.black_background);
                        } else if (txt1String.length() >= 14 && txt1String.length() <= 15) {
                            tv_error.setVisibility(View.VISIBLE);
                            et_description.setBackgroundResource(R.drawable.error_background);
                        } else {
                            tv_error.setVisibility(View.GONE);
                            et_description.setBackgroundResource(R.drawable.green_background);
                        }

                    }
                    break;
                case R.id.et_searching:
                    if (s.length() > 0) {
                        et_searching.setBackgroundResource(R.drawable.black_background);
                        String txt1String = (s.toString());
                        if (txt1String.length() <= 14) {
                            tv_error_one.setVisibility(View.GONE);
                            et_searching.setBackgroundResource(R.drawable.black_background);
                        } else if (txt1String.length() >= 14 && txt1String.length() <= 15) {
                            tv_error_one.setVisibility(View.VISIBLE);
                            et_searching.setBackgroundResource(R.drawable.error_background);
                        } else {
                            tv_error_one.setVisibility(View.GONE);
                            et_searching.setBackgroundResource(R.drawable.green_background);
                        }

                    }
                    break;
            }
        }
    }


}
