package app.pairme.activities;

import android.Manifest;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Calendar;
import java.util.Locale;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.BankTransferPojo;
import app.pairme.R;
import app.pairme.adapters.FileUtils;
import app.pairme.datePicker.DatePickerPopWin;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BankTransfer extends AppCompatActivity implements View.OnClickListener {

    EditText et_username, et_country, et_city, et_email, et_mobile, et_applicant_name, et_deposit_value, et_date, et_bank_name, et_bank_account, et_notes;
    LinearLayout ll_upload;
    ImageView iv_preview;
    Button btn_submit;
    CardView card_iv;
    Uri uri;
    File file;
    int mYear, mMonth, mDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_transfer);
        et_username = findViewById(R.id.et_username);
        et_country = findViewById(R.id.et_country);
        et_city = findViewById(R.id.et_city);
        et_email = findViewById(R.id.et_email);
        et_mobile = findViewById(R.id.et_mobile);
        et_applicant_name = findViewById(R.id.et_applicant_name);
        et_deposit_value = findViewById(R.id.et_deposit_value);
        et_date = findViewById(R.id.et_date);
        et_bank_name = findViewById(R.id.et_bank_name);
        et_bank_account = findViewById(R.id.et_bank_account);
        et_notes = findViewById(R.id.et_notes);
        ll_upload = findViewById(R.id.ll_upload);
        iv_preview = findViewById(R.id.iv_preview);
        btn_submit = findViewById(R.id.btn_submit);
        card_iv = findViewById(R.id.card_iv);
        btn_submit.setOnClickListener(this);
        ll_upload.setOnClickListener(this);
        et_date.setOnClickListener(this);
        et_username.setText(Helper.getUserData(BankTransfer.this).getName());
        et_country.setText(Helper.getUserData(BankTransfer.this).getLivesIn());
        et_city.setText(Helper.getUserData(BankTransfer.this).getLivesIn());
        et_email.setText(Helper.getUserData(BankTransfer.this).getEmail());
        et_mobile.setText(Helper.getUserData(BankTransfer.this).getContact());
        checkPerm();

    }

    private void openImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(intent, 123);
    }

    private void date() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        Log.d("tag", "date: " + Locale.getDefault());
        DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(BankTransfer.this, new DatePickerPopWin.OnDatePickedListener() {
            @Override
            public void onDatePickCompleted(int year, String month, int day, String dateDesc) {
                et_date.setText(day + " " + (month + 1) + " " + year);
            }
        }).textConfirm("CONFIRMAR") //text of confirm button
                .textCancel("CANCELAR") //text of cancel button
                .btnTextSize(16) // button text size
                .viewTextSize(25) // pick view text size
                .colorCancel(Color.parseColor("#999999")) //color of cancel button
                .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                .minYear(c.get(Calendar.YEAR))
                .minMonth(c.get(Calendar.MONTH))
                //min year in loop
                //min year in loop
                .maxYear(2550) // max year in loop
                .showDayMonthYear(true)
                .build();
        pickerPopWin.showPopWin(BankTransfer.this);
    }


    private void checkPerm() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (data != null) {
            if (requestCode == 123) {
                uri = data.getData();
                Helper.setImage(String.valueOf(uri), BankTransfer.this);
                file = FileUtils.getFile(this, uri);
//                try {
//                    compressedImageFile = new Compressor(this).compressToFile(file);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
                ll_upload.setVisibility(View.GONE);
                card_iv.setVisibility(View.VISIBLE);
                iv_preview.setImageURI(uri);
            } else {
                Toast.makeText(getApplicationContext(), "Failed to Load Image", Toast.LENGTH_SHORT).show();
            }
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            Toast.makeText(this, getResources().getString(R.string.select_image), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_upload:
                openImage();
                break;
            case R.id.et_date:
                date();
                break;
            case R.id.btn_submit:

                if (et_applicant_name.getText().toString().isEmpty()) {
                    et_applicant_name.setError(getResources().getString(R.string.please_enter_required_field));
                    return;
                } else if (et_deposit_value.getText().toString().isEmpty()) {
                    et_deposit_value.setError(getResources().getString(R.string.please_enter_required_field));
                    return;
                } else if (et_date.getText().toString().isEmpty()) {
                    et_date.setError(getResources().getString(R.string.please_enter_required_field));
                    return;
                } else if (et_bank_name.getText().toString().isEmpty()) {
                    et_bank_name.setError(getResources().getString(R.string.please_enter_required_field));
                    return;
                } else if (et_bank_account.getText().toString().isEmpty()) {
                    et_bank_account.setError(getResources().getString(R.string.please_enter_required_field));
                    return;
                } else if (et_notes.getText().toString().isEmpty()) {
                    et_notes.setError(getResources().getString(R.string.please_enter_required_field));
                    return;
                } else if (et_notes.getText().toString().isEmpty()) {
                    et_notes.setError(getResources().getString(R.string.please_enter_required_field));
                    return;
                } else {
                    Banketransfer();
                }
                break;
        }

    }

    private void Banketransfer() {
        Helper.showLoader(this, "Image Upload...");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        MultipartBody.Part part = null;
        if (file != null) {
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            part = MultipartBody.Part.createFormData("receipt", file.getName(), requestBody);
        }
        RequestBody userid = RequestBody.create(MediaType.parse("text/plain"), Helper.getUserData(getApplicationContext()).getId().toString());
        RequestBody packageid = RequestBody.create(MediaType.parse("text/plain"), Helper.getUserData(getApplicationContext()).getPackagesId().toString());
        RequestBody aplicantname = RequestBody.create(MediaType.parse("text/plain"), et_applicant_name.getText().toString());
        RequestBody depositvalue = RequestBody.create(MediaType.parse("text/plain"), et_deposit_value.getText().toString());
        RequestBody date = RequestBody.create(MediaType.parse("text/plain"), et_date.getText().toString());
        RequestBody bankname = RequestBody.create(MediaType.parse("text/plain"), et_bank_name.getText().toString());
        RequestBody bankaccount = RequestBody.create(MediaType.parse("text/plain"), et_bank_account.getText().toString());
        RequestBody note = RequestBody.create(MediaType.parse("text/plain"), et_notes.getText().toString());
        RequestBody transfer_method = RequestBody.create(MediaType.parse("text/plain"), "Bank");
        service.Banktransfer("Bearer " + Helper.getToken(BankTransfer.this), part, userid,
                packageid, transfer_method, aplicantname,
                depositvalue, date, bankname, bankaccount, note).enqueue(new Callback<BankTransferPojo>() {
            @Override
            public void onResponse(@NotNull Call<BankTransferPojo> call, @NotNull Response<BankTransferPojo> response) {
                Helper.hideLoader();
                if (response.isSuccessful()) {
                    Toast.makeText(BankTransfer.this, "Transaction SuccessFull", Toast.LENGTH_SHORT).show();
                    finish();

                } else {
                    Toast.makeText(BankTransfer.this, "Transaction Failed", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(@NotNull Call<BankTransferPojo> call, @NotNull Throwable t) {
                Helper.hideLoader();
                Toast.makeText(BankTransfer.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.d("TAG", "onFailure: " + t.getMessage());

            }
        });


    }
}
