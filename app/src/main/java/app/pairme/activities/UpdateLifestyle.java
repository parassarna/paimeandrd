package app.pairme.activities;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.HashMap;
import java.util.List;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.DescriptionPojo.DescriptionPojo;
import app.pairme.ClassesPojo.Infodata;
import app.pairme.R;
import app.pairme.Utils;
import app.pairme.adapters.UpdatinfoAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.pairme.Utils.getArabic;
import static app.pairme.Utils.language;
import static app.pairme.Utils.multiple;

public class UpdateLifestyle extends AppCompatActivity implements View.OnClickListener {
    EditText et_Smoking, et_eating_habits, et_exercise, et_Sleep_Habits, et_pets, et_family_values, et_Polygamy, et_Personality, et_eye_color;
    ImageView iv_well_smoking, iv_well_habits, iv_well_exercise, iv_well_sleep, iv_pets, iv_family, iv_Polygamy, iv_Personality, iv_Language;
    TextView btn_save;
    Dialog dialog;
    ProgressBar progressBar_cyclic;
    androidx.recyclerview.widget.RecyclerView RecyclerView;
    boolean updated;
    HashMap<String, String> named = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_lifestyle);
        changeStatusBarColor();
        initScreen();
    }

    private void initScreen() {
        et_Smoking = findViewById(R.id.et_Smoking);
        et_eating_habits = findViewById(R.id.et_eating_habits);
        et_exercise = findViewById(R.id.et_exercise);
        et_Sleep_Habits = findViewById(R.id.et_Sleep_Habits);
        et_pets = findViewById(R.id.et_pets);
        et_family_values = findViewById(R.id.et_family_values);
        et_Polygamy = findViewById(R.id.et_Polygamy);
        et_Personality = findViewById(R.id.et_Personality);
        et_eye_color = findViewById(R.id.et_eye_color);
        btn_save = findViewById(R.id.tv_save);
        iv_well_smoking = findViewById(R.id.iv_well_smoking);
        iv_well_habits = findViewById(R.id.iv_well_habits);
        iv_well_exercise = findViewById(R.id.iv_well_exercise);
        iv_well_sleep = findViewById(R.id.iv_well_sleep);
        iv_pets = findViewById(R.id.iv_pets);
        iv_family = findViewById(R.id.iv_family);
        iv_Polygamy = findViewById(R.id.iv_Polygamy);
        iv_Personality = findViewById(R.id.iv_Personality);
        iv_Language = findViewById(R.id.iv_Language);
        et_Smoking.setOnClickListener(this);
        et_eating_habits.setOnClickListener(this);
        et_exercise.setOnClickListener(this);
        et_Sleep_Habits.setOnClickListener(this);
        et_pets.setOnClickListener(this);
        et_family_values.setOnClickListener(this);
        et_Polygamy.setOnClickListener(this);
        et_Personality.setOnClickListener(this);
        et_eye_color.setOnClickListener(this);
        btn_save.setOnClickListener(this);
        initData();
    }

    private void initData() {
        if (Helper.getUserData(this) != null) {
            et_Smoking.setText(getArabic(this, Helper.getUserData(this).getSmoking()));
            et_eating_habits.setText(getArabic(this, Helper.getUserData(this).getEatingHabits()));
            et_exercise.setText(getArabic(this, Helper.getUserData(this).getExerciseHabits()));
            et_Sleep_Habits.setText(getArabic(this, Helper.getUserData(this).getSleepHabits()));
            et_pets.setText(getArabic(this, Helper.getUserData(this).getPets()));
            et_family_values.setText(getArabic(this, Helper.getUserData(this).getFamilyValues()));
            et_Polygamy.setText(getArabic(this, Helper.getUserData(this).getPolygamyOpinion()));
            et_Personality.setText(Utils.setData(this, Helper.getUserData(UpdateLifestyle.this).getPersonality()));
            et_eye_color.setText(Utils.setData(this, Helper.getUserData(UpdateLifestyle.this).getLanguages()));
            named.put(getResources().getString(R.string.smoking), Helper.getUserData(this).getSmoking());
            named.put(getResources().getString(R.string.eating_habits), Helper.getUserData(this).getEatingHabits());
            named.put(getResources().getString(R.string.execise_habits), Helper.getUserData(this).getExerciseHabits());
            named.put(getResources().getString(R.string.sleep_habits), Helper.getUserData(this).getSleepHabits());
            named.put(getResources().getString(R.string.pets), Helper.getUserData(this).getPets());
            named.put(getResources().getString(R.string.family_values), Helper.getUserData(this).getFamilyValues());
            named.put(getResources().getString(R.string.polygamy_opinion), Helper.getUserData(this).getPolygamyOpinion());
            named.put(getResources().getString(R.string.languages), Helper.getUserData(this).getLanguages());
            named.put(getResources().getString(R.string.personality), Helper.getUserData(this).getPersonality());
        }
    }

    private String getStringData(List<String> ids, int type) {
        StringBuilder convertedstring = new StringBuilder();
        for (int i = 0; i < ids.size(); i++) {
            if (i == 0)
                convertedstring = new StringBuilder(ids.get(i));
            else
                convertedstring.append(",").append(ids.get(i));
        }
        return convertedstring.toString();
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.et_Smoking:
                showDialog(getResources().getString(R.string.smoking), Helper.getinfodata(UpdateLifestyle.this).lifestyle.smoking, et_Smoking, iv_well_smoking);
                break;
            case R.id.et_eating_habits:
                showDialog(getResources().getString(R.string.eating_habits), Helper.getinfodata(UpdateLifestyle.this).lifestyle.eatingHabits, et_eating_habits, iv_well_habits);
                break;
            case R.id.et_exercise:
                showDialog(getResources().getString(R.string.execise_habits), Helper.getinfodata(UpdateLifestyle.this).lifestyle.exerciseHabits, et_exercise, iv_well_exercise);
                break;
            case R.id.et_Sleep_Habits:
                showDialog(getResources().getString(R.string.sleep_habits), Helper.getinfodata(UpdateLifestyle.this).lifestyle.sleepHabbits, et_Sleep_Habits, iv_well_sleep);
                break;
            case R.id.et_pets:
                showDialog(getResources().getString(R.string.pets), Helper.getinfodata(UpdateLifestyle.this).lifestyle.pets, et_pets, iv_pets);
                break;
            case R.id.et_family_values:
                showDialog(getResources().getString(R.string.family_values), Helper.getinfodata(UpdateLifestyle.this).lifestyle.familyValue, et_family_values, iv_family);
                break;
            case R.id.et_Polygamy:
                showDialog(getResources().getString(R.string.polygamy_opinion), Helper.getinfodata(UpdateLifestyle.this).lifestyle.polygamyOpinion, et_Polygamy, iv_Polygamy);
                break;
            case R.id.et_Personality:
                showDialog(getResources().getString(R.string.personality), Helper.getinfodata(UpdateLifestyle.this).lifestyle.personality, et_Personality, iv_Personality);
                break;
            case R.id.et_eye_color:
                showDialog(getResources().getString(R.string.languages), Helper.getinfodata(UpdateLifestyle.this).lifestyle.languages, et_eye_color, iv_Language);
                break;
            case R.id.tv_save:
                update();
                break;


        }

    }


    private void setData(EditText editText, String s, ImageView view) {
        editText.setText(s);
        editText.setBackgroundResource(R.drawable.green_background);
        view.setVisibility(View.VISIBLE);
    }

    private void showDialog(final String header, final List<Infodata.Data> data, final EditText editText, final ImageView view) {
        Utils.multiple = header.equalsIgnoreCase(getResources().getString(R.string.personality)) || header.equalsIgnoreCase(getResources().getString(R.string.languages));
        Utils.language = header.equalsIgnoreCase(getResources().getString(R.string.personality)) || header.equalsIgnoreCase(getResources().getString(R.string.languages));
        Utils.selected = editText.getText().toString().trim();
        dialog = new Dialog(UpdateLifestyle.this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_listview);
        TextView tv_header = dialog.findViewById(R.id.tv_header);
        tv_header.setText(header);
        final TextView tv_count = dialog.findViewById(R.id.tv_count);

        Button btndialog = (Button) dialog.findViewById(R.id.btndialog);
        Button btndok = (Button) dialog.findViewById(R.id.btndok);
        Log.d("tag", "showDialog: " + header);
        if (header.equalsIgnoreCase(getResources().getString(R.string.languages)) || header.equalsIgnoreCase(getResources().getString(R.string.personality))) {
            tv_count.setText(header.equalsIgnoreCase(getResources().getString(R.string.languages)) ? "(0/5)" : "(0/3)");
            tv_count.setVisibility(View.VISIBLE);
            btndok.setVisibility(View.VISIBLE);
        }
        progressBar_cyclic = dialog.findViewById(R.id.progressBar_cyclic);
        progressBar_cyclic.setVisibility(View.GONE);
        btndialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btndok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        RecyclerView = dialog.findViewById(R.id.listview);
        UpdatinfoAdapter interestsAdapter = new UpdatinfoAdapter(data, new UpdatinfoAdapter.OnStatusListner() {
            @Override
            public void OnStatusClick(String p, String trim) {
                UpdateLifestyle.this.named.put(header, trim);
                updated = true;
                setData(editText, p, view);
                dialog.dismiss();
            }

            @Override
            public void OnStatusmultClick(List<String> p, List<String> names) {
                updated = true;
                tv_count.setText(header.equalsIgnoreCase(getResources().getString(R.string.languages)) ? "(" + p.size() + "/5)" : "(" + p.size() + "/3)");
                setData(editText, getStringData(p, 1), view);
                named.put(header, getStringData(names, 1));
            }
        });
        RecyclerView.setLayoutManager(new LinearLayoutManager(UpdateLifestyle.this));
        RecyclerView.setAdapter(interestsAdapter);
        dialog.show();
    }

    public void update() {
        Helper.showLoader(this, "Please wait...");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<DescriptionPojo> call = service.updateLifestyle("Bearer " + Helper.getToken(UpdateLifestyle.this),
                named.get(getResources().getString(R.string.smoking)),
                named.get(getResources().getString(R.string.eating_habits)),
                named.get(getResources().getString(R.string.execise_habits)),
                named.get(getResources().getString(R.string.sleep_habits)),
                named.get(getResources().getString(R.string.pets)),
                named.get(getResources().getString(R.string.family_values)),
                named.get(getResources().getString(R.string.polygamy_opinion)),
                named.get(getResources().getString(R.string.personality)),
                named.get(getResources().getString(R.string.languages)));
        call.enqueue(new Callback<DescriptionPojo>() {
            @Override
            public void onResponse(Call<DescriptionPojo> call, Response<DescriptionPojo> response) {
                Helper.hideLoader();
                if (response.isSuccessful()) {
                    Toast.makeText(UpdateLifestyle.this, R.string.details_updated, Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Log.i(">>>>", response.raw().toString());
                }
            }

            @Override
            public void onFailure(Call<DescriptionPojo> call, Throwable t) {
                Helper.hideLoader();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (updated)
            openDialog();
        else super.onBackPressed();
    }

    private void openDialog() {
        dialog = new Dialog(UpdateLifestyle.this);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.show();
        dialog.setCancelable(false);
        Button declineButton = dialog.findViewById(R.id.btn_logout);
        declineButton.setText(getResources().getString(R.string.yes));
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                update();
            }
        });
        TextView cancel = dialog.findViewById(R.id.tv_cancel);
        cancel.setText(getResources().getString(R.string.no));
        TextView tv_all = dialog.findViewById(R.id.tv_all);
        TextView tv_logout = dialog.findViewById(R.id.tv_logout);
        tv_all.setVisibility(View.GONE);
        tv_logout.setText(getResources().getString(R.string.do_you_Wish_to_save_the_Changes));
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        multiple = false;
        language = false;
    }
}
