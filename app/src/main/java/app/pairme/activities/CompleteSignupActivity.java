package app.pairme.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.HashMap;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.RegisterPojo.RegisterPojo;
import app.pairme.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.pairme.Utils.flip;
import static app.pairme.Utils.valid;

public class CompleteSignupActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView iv_back;
    ImageView fab;
    HashMap<String, String> names = new HashMap<>();
    String name, age, location, org_location, gender, password, email, looking, subscription_status, androidId;
    ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_signup);
        initControl();
        TextView privacyTextView = findViewById(R.id.sign_up_privacy);
        privacyTextView.setMovementMethod(LinkMovementMethod.getInstance());
        name = Helper.getFirstName(this);
        looking = Helper.getLooking(this);
        age = Helper.getAge(this);
        location = Helper.getLocation(this);
        org_location = Helper.getOrgLocation(this);
        gender = Helper.getGender(this);
        if (gender.equalsIgnoreCase("male")) {
            subscription_status = "0";
        } else {
            subscription_status = "1";
        }
        password = Helper.getPassword(this);
        email = Helper.getEmail(this);

    }


    private void initControl() {
        iv_back = findViewById(R.id.iv_back);
        fab = findViewById(R.id.fab);
        iv_back.setOnClickListener(this);
        fab.setOnClickListener(this);
        if (Helper.getLanguage(this).equalsIgnoreCase("ar"))
            flip(fab);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.fab:
                registerApi();
                break;
        }


    }

    public void registerApi() {
        Helper.showLoader(CompleteSignupActivity.this, "Please wait...");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<RegisterPojo> call = service.registerApi(name, email, age, location, looking,
                password, org_location, gender, subscription_status, androidId, "android");
        call.enqueue(new Callback<RegisterPojo>() {
            @Override
            public void onResponse(Call<RegisterPojo> call, Response<RegisterPojo> response) {
                Helper.hideLoader();
                if (response.body().status) {
                    Helper.clearEmail(getApplicationContext());
                    Helper.setEmail(email, CompleteSignupActivity.this);
                    Helper.clearName(getApplicationContext());
                    valid = response.body().valid;
                    Helper.setFirstName(response.body().getName(), CompleteSignupActivity.this);
                    Intent intent = new Intent(getApplicationContext(), ImageSignup.class);
                    intent.putExtra("email", response.body().getEmail());
                    startActivity(intent);
                } else {
                }

            }

            @Override
            public void onFailure(Call<RegisterPojo> call, Throwable t) {
                Helper.hideLoader();
                Log.d("error", "Failure; " + t);
            }
        });
    }


}
