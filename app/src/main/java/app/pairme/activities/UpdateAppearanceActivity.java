package app.pairme.activities;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.DescriptionPojo.DescriptionPojo;
import app.pairme.ClassesPojo.Infodata;
import app.pairme.R;
import app.pairme.Utils;
import app.pairme.adapters.UpdatHeightAdapter;
import app.pairme.adapters.UpdatinfoAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.pairme.Utils.getArabic;

public class UpdateAppearanceActivity extends AppCompatActivity implements View.OnClickListener {

    EditText et_Appearance, et_body_type, et_Height, et_Complexion, et_health_status, et_hair_color, et_hair_length, et_type, et_eye_color, et_eye_wear, et_facial_hair, et_weapon, et_style, et_makeup;
    ImageView iv_well, iv_body_type, iv_Height, iv_Complexion, iv_HealthStatus, iv_hair_color, iv_hair_length, iv_hair_type, iv_eye_color, iv_facial_hair, iv_eye_wear, iv_weapon, iv_style, iv_makeup;
    TextView tv_save;
    Dialog dialog;
    boolean updated;
    ProgressBar progressBar_cyclic;
    androidx.recyclerview.widget.RecyclerView RecyclerView;
    List<Infodata.Data> height = new ArrayList<>();
    ArrayList<String> names = new ArrayList<>();
    HashMap<String, String> name = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_appearance);
        changeStatusBarColor();
        inflateheight();
        initScreen();
        initData();
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    public void initScreen() {
        et_Appearance = findViewById(R.id.et_Appearance);
        et_body_type = findViewById(R.id.et_body_type);
        et_Height = findViewById(R.id.et_Height);
        et_Complexion = findViewById(R.id.et_Complexion);
        et_health_status = findViewById(R.id.et_health_status);
        et_hair_color = findViewById(R.id.et_hair_color);
        et_hair_length = findViewById(R.id.et_hair_length);
        et_type = findViewById(R.id.et_type);
        et_eye_color = findViewById(R.id.et_eye_color);
        iv_well = findViewById(R.id.iv_well);
        iv_body_type = findViewById(R.id.iv_body_type);
        iv_Height = findViewById(R.id.iv_Height);
        iv_Complexion = findViewById(R.id.iv_Complexion);
        iv_HealthStatus = findViewById(R.id.iv_HealthStatus);
        iv_hair_color = findViewById(R.id.iv_hair_color);
        iv_hair_length = findViewById(R.id.iv_hair_length);
        iv_hair_type = findViewById(R.id.iv_hair_type);
        iv_eye_color = findViewById(R.id.iv_eye_color);
        et_eye_wear = findViewById(R.id.et_eye_wear);
        // et_facial_hair = findViewById(R.id.et_facial_hair);
        et_weapon = findViewById(R.id.et_weapon);
        //  et_style = findViewById(R.id.et_style);
        //  iv_facial_hair = findViewById(R.id.iv_facial_hair);
        iv_eye_wear = findViewById(R.id.iv_eye_wear);
        iv_weapon = findViewById(R.id.iv_weapon);
        // iv_style = findViewById(R.id.iv_style);
        tv_save = findViewById(R.id.tv_save);
        et_makeup = findViewById(R.id.et_makeup);
        iv_makeup = findViewById(R.id.iv_makeup);
        et_Appearance.setOnClickListener(this);
        et_body_type.setOnClickListener(this);
        et_Height.setOnClickListener(this);
        et_Complexion.setOnClickListener(this);
        et_health_status.setOnClickListener(this);
        et_hair_color.setOnClickListener(this);
        et_hair_length.setOnClickListener(this);
        et_type.setOnClickListener(this);
        et_eye_color.setOnClickListener(this);
        tv_save.setOnClickListener(this);
        et_eye_wear.setOnClickListener(this);
        // et_facial_hair.setOnClickListener(this);
        et_weapon.setOnClickListener(this);
        //  et_style.setOnClickListener(this);
        et_makeup.setOnClickListener(this);
    }

    private void initData() {
        if (Helper.getUserData(this) != null) {
            et_Appearance.setText(getArabic(this, Helper.getUserData(this).getAppearance()));
            et_body_type.setText(getArabic(this, Helper.getUserData(this).getBodyType()));
            et_Height.setText(Helper.getUserData(this).getHeight());
            et_Complexion.setText(getArabic(this, Helper.getUserData(this).getComplexion()));
            et_health_status.setText(getArabic(this, Helper.getUserData(this).getHealthStatus()));
            et_hair_color.setText(getArabic(this, Helper.getUserData(this).getHairColor()));
            et_hair_length.setText(getArabic(this, Helper.getUserData(this).getHairLength()));
            et_type.setText(getArabic(this, Helper.getUserData(this).getHairType()));
            et_eye_color.setText(getArabic(this, Helper.getUserData(this).getEyeColor()));
            et_eye_wear.setText(getArabic(this, Helper.getUserData(this).getEyeWear()));
            // et_facial_hair.setText(getArabic(this, Helper.getUserData(this).getFacialHair()));
            et_weapon.setText(getArabic(this, Helper.getUserData(this).getSecretWeapon()));
            //  et_style.setText(getArabic(this, Helper.getUserData(this).getStyle()));
            et_makeup.setText(getArabic(this, Helper.getUserData(this).getMakeup()));
            UpdateAppearanceActivity.this.name.put(getResources().getString(R.string.appearance), Helper.getUserData(this).getAppearance());
            UpdateAppearanceActivity.this.name.put(getResources().getString(R.string.body_type), Helper.getUserData(this).getBodyType());
            UpdateAppearanceActivity.this.name.put(getResources().getString(R.string.height), Helper.getUserData(this).getHeight());
            UpdateAppearanceActivity.this.name.put(getResources().getString(R.string.complexion), Helper.getUserData(this).getComplexion());
            UpdateAppearanceActivity.this.name.put(getResources().getString(R.string.health_status), Helper.getUserData(this).getHealthStatus());
            UpdateAppearanceActivity.this.name.put(getResources().getString(R.string.hair_color), Helper.getUserData(this).getHairColor());
            UpdateAppearanceActivity.this.name.put(getResources().getString(R.string.hair_length), Helper.getUserData(this).getHairLength());
            UpdateAppearanceActivity.this.name.put(getResources().getString(R.string.hair_type), Helper.getUserData(this).getHairType());
            UpdateAppearanceActivity.this.name.put(getResources().getString(R.string.eye_color), Helper.getUserData(this).getEyeColor());
            UpdateAppearanceActivity.this.name.put(getResources().getString(R.string.eye_wear), Helper.getUserData(this).getEyeWear());
            UpdateAppearanceActivity.this.name.put(getResources().getString(R.string.secret_weapon), Helper.getUserData(this).getSecretWeapon());
            UpdateAppearanceActivity.this.name.put(getResources().getString(R.string.makeup), Helper.getUserData(this).getMakeup());
        }
    }

    private void inflateheight() {
        Infodata.Data infodata;
        int j = 0;
        for (int i = 119; i <= 220; i++) {
            infodata = new Infodata.Data();
            if (i == 119) {
                infodata.name = getResources().getString(R.string.not_set);
            } else if (i == 120)
                infodata.name = getResources().getString(R.string.shorter);
            else if (i == 220)
                infodata.name = getResources().getString(R.string.taller);
            else
                infodata.name = String.valueOf(i);
            height.add(infodata);
            j++;
        }
    }


    @Override
    public void onClick(View v) {
        names.clear();
        switch (v.getId()) {
            case R.id.et_Appearance:
                showDialog(getResources().getString(R.string.appearance), Helper.getinfodata(UpdateAppearanceActivity.this).appearance.appearance, et_Appearance, iv_well);
                break;
            case R.id.et_body_type:
                showDialog(getResources().getString(R.string.body_type), Helper.getinfodata(UpdateAppearanceActivity.this).appearance.bodyType, et_body_type, iv_body_type);
                break;
            case R.id.et_Height:
                showHeightDialog(getResources().getString(R.string.height), height, et_Height, iv_Height);
                break;
            case R.id.et_Complexion:
                showDialog(getResources().getString(R.string.complexion), Helper.getinfodata(UpdateAppearanceActivity.this).appearance.complexion, et_Complexion, iv_Complexion);
                break;
            case R.id.et_health_status:
                showDialog(getResources().getString(R.string.health_status), Helper.getinfodata(UpdateAppearanceActivity.this).appearance.healthStatus, et_health_status, iv_HealthStatus);
                break;
            case R.id.et_hair_color:
                showDialog(getResources().getString(R.string.hair_color), Helper.getinfodata(UpdateAppearanceActivity.this).appearance.hairColor, et_hair_color, iv_hair_color);
                break;
            case R.id.et_hair_length:
                showDialog(getResources().getString(R.string.hair_length), Helper.getinfodata(UpdateAppearanceActivity.this).appearance.hairLength, et_hair_length, iv_hair_length);
                break;
            case R.id.et_type:
                showDialog(getResources().getString(R.string.hair_type), Helper.getinfodata(UpdateAppearanceActivity.this).appearance.hairType, et_type, iv_hair_type);
                break;
            case R.id.et_eye_color:
                showDialog(getResources().getString(R.string.eye_color), Helper.getinfodata(UpdateAppearanceActivity.this).appearance.eyeColor, et_eye_color, iv_eye_color);
                break;
            case R.id.et_eye_wear:
                showDialog(getResources().getString(R.string.eye_wear), Helper.getinfodata(UpdateAppearanceActivity.this).appearance.eyeWear, et_eye_wear, iv_eye_wear);
                break;
            case R.id.et_weapon:
                showDialog(getResources().getString(R.string.secret_weapon), Helper.getinfodata(UpdateAppearanceActivity.this).appearance.secretWeapon, et_weapon, iv_weapon);
                break;
            case R.id.et_makeup:
                showDialog(getResources().getString(R.string.makeup), Helper.getinfodata(UpdateAppearanceActivity.this).appearance.makeup, et_makeup, iv_makeup);
                break;
            case R.id.tv_save:
                update();
                break;


        }

    }


    private void showDialog(final String header, final List<Infodata.Data> datas, final EditText editText, final ImageView iv_well) {
        Utils.selected = editText.getText().toString().trim();
        dialog = new Dialog(UpdateAppearanceActivity.this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_listview);
        TextView tv_header = dialog.findViewById(R.id.tv_header);
        tv_header.setText(header);
        Button btndialog = dialog.findViewById(R.id.btndialog);
        progressBar_cyclic = dialog.findViewById(R.id.progressBar_cyclic);
        progressBar_cyclic.setVisibility(View.GONE);
        btndialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        RecyclerView = dialog.findViewById(R.id.listview);
        UpdatinfoAdapter interestsAdapter = new UpdatinfoAdapter(datas, new UpdatinfoAdapter.OnStatusListner() {
            @Override
            public void OnStatusClick(String p, String name) {
                UpdateAppearanceActivity.this.name.put(header, name);
                updated = true;
                editText.setText(p);
                editText.setBackgroundResource(R.drawable.green_background);
                iv_well.setVisibility(View.VISIBLE);
                dialog.dismiss();
            }

            @Override
            public void OnStatusmultClick(List<String> p, List<String> names) {

            }
        });
        RecyclerView.setLayoutManager(new LinearLayoutManager(UpdateAppearanceActivity.this));
        RecyclerView.setAdapter(interestsAdapter);
        dialog.show();
    }


    private void showHeightDialog(final String header, final List<Infodata.Data> datas, final EditText editText, final ImageView iv_well) {
        dialog = new Dialog(UpdateAppearanceActivity.this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_listview);
        TextView tv_header = dialog.findViewById(R.id.tv_header);
        tv_header.setText(header);
        Button btndialog = (Button) dialog.findViewById(R.id.btndialog);
        progressBar_cyclic = dialog.findViewById(R.id.progressBar_cyclic);
        progressBar_cyclic.setVisibility(View.GONE);
        btndialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        RecyclerView = dialog.findViewById(R.id.listview);
        UpdatHeightAdapter interestsAdapter = new UpdatHeightAdapter(datas, new UpdatHeightAdapter.OnStatusListner() {
            @Override
            public void OnStatusClick(String p, String name) {
                UpdateAppearanceActivity.this.name.put(header, name);
                updated = true;
                editText.setText(p);
                editText.setBackgroundResource(R.drawable.green_background);
                iv_well.setVisibility(View.VISIBLE);
                dialog.dismiss();
            }

            @Override
            public void OnStatusmultClick(List<String> p, List<String> name) {

            }

        });
        RecyclerView.setLayoutManager(new LinearLayoutManager(UpdateAppearanceActivity.this));
        RecyclerView.setAdapter(interestsAdapter);
        dialog.show();
    }

    public void update() {
        Helper.showLoader(this, "Please wait..");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<DescriptionPojo> call = service.updateAppearance("Bearer " + Helper.getToken(UpdateAppearanceActivity.this),
                name.get(getResources().getString(R.string.appearance)),
                name.get(getResources().getString(R.string.body_type)),
                name.get(getResources().getString(R.string.height)),
                name.get(getResources().getString(R.string.complexion)),
                name.get(getResources().getString(R.string.health_status)),
                name.get(getResources().getString(R.string.hair_color)),
                name.get(getResources().getString(R.string.hair_length)),
                name.get(getResources().getString(R.string.hair_type)),
                name.get(getResources().getString(R.string.eye_color)),
                name.get(getResources().getString(R.string.eye_wear)),
                name.get(getResources().getString(R.string.secret_weapon)),
                name.get(getResources().getString(R.string.makeup)));
        call.enqueue(new Callback<DescriptionPojo>() {
            @Override
            public void onResponse(Call<DescriptionPojo> call, Response<DescriptionPojo> response) {
                Helper.hideLoader();
                if (response.isSuccessful()) {
                    Toast.makeText(UpdateAppearanceActivity.this, R.string.details_updated, Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Log.i(">>>>", response.raw().toString());
                }
            }

            @Override
            public void onFailure(Call<DescriptionPojo> call, Throwable t) {
                Helper.hideLoader();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (updated)
            openDialog();
        else super.onBackPressed();
    }

    private void openDialog() {
        dialog = new Dialog(UpdateAppearanceActivity.this);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.show();
        dialog.setCancelable(false);
        Button declineButton = dialog.findViewById(R.id.btn_logout);
        declineButton.setText(getResources().getString(R.string.ok));
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                update();
            }
        });
        TextView cancel = dialog.findViewById(R.id.tv_cancel);
        cancel.setText(getResources().getString(R.string.no));
        TextView tv_all = dialog.findViewById(R.id.tv_all);
        TextView tv_logout = dialog.findViewById(R.id.tv_logout);
        tv_all.setVisibility(View.GONE);
        tv_logout.setText(getResources().getString(R.string.do_you_Wish_to_save_the_Changes));
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
}
