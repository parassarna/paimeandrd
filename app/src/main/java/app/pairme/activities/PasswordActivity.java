package app.pairme.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import app.pairme.ApiServices.Helper;
import app.pairme.R;

import static app.pairme.Utils.flip;

public class PasswordActivity extends AppCompatActivity implements View.OnClickListener, TextWatcher {
    EditText et_password, et_email;
    ImageView btn_fab;
    TextView tv_error;
    ImageView show_password, iv_back, iv_error, close_iv, hide_password, iv_well;
    LinearLayout linear_one_inactive, linear_one_active, linear_two_inactive, linear_two_active, linear_three_inactive, linear_three_active;
    LinearLayout error_ll;
    RelativeLayout main_bg;
    Window window;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        initControl();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.show_pass_btn:
                if (et_password.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())) {
                    ((ImageView) (view)).setImageResource(R.drawable.ic_show_password);
                    //Show Password
                    et_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    ((ImageView) (view)).setImageResource(R.drawable.ic_hide_password);
                    //Hide Password
                    et_password.setTransformationMethod(PasswordTransformationMethod.getInstance());

                }
                break;
            case R.id.fab:
                if (TextUtils.isEmpty(et_password.getText().toString()) || et_password.getText().toString().length() < 6) {
                    slideUp(error_ll);
                } else {
                    Helper.clearPassword(PasswordActivity.this);
                    Helper.setPassword(et_password.getText().toString(), PasswordActivity.this);
                    startActivity(new Intent(PasswordActivity.this, CompleteSignupActivity.class));
                    break;
                }
                break;
            case R.id.close_iv:
                slidedown(error_ll);
                break;
            case R.id.iv_back:
                finish();
                break;

        }
    }

    private void initControl() {
        et_password = findViewById(R.id.et_password);
        show_password = findViewById(R.id.show_pass_btn);
        hide_password = findViewById(R.id.hide_pass_btn);
        btn_fab = findViewById(R.id.fab);
        iv_well = findViewById(R.id.iv_well);
        iv_back = findViewById(R.id.iv_back);
        tv_error = findViewById(R.id.tv_error);
        linear_one_inactive = findViewById(R.id.linear_one_inactive);
        linear_one_active = findViewById(R.id.linear_one_active);
        linear_two_inactive = findViewById(R.id.linear_two_inactive);
        linear_two_active = findViewById(R.id.linear_two_active);
        linear_three_inactive = findViewById(R.id.linear_three_inactive);
        linear_three_active = findViewById(R.id.linear_three_active);
        iv_error = findViewById(R.id.iv_error);
        main_bg = findViewById(R.id.main_bg);
        error_ll = findViewById(R.id.error_ll);
        close_iv = findViewById(R.id.close_iv);
        close_iv.setOnClickListener(this);
        show_password.setOnClickListener(this);
        hide_password.setOnClickListener(this);
        btn_fab.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        et_password.addTextChangedListener(this);
        if (Helper.getLanguage(this).equalsIgnoreCase("ar"))
            flip(btn_fab);
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (charSequence.length() > 0) {
            String txt1String = (charSequence.toString());
            if (txt1String.length() < 6) {
                tv_error.setText(getResources().getString(R.string.too_short));
                iv_well.setVisibility(View.GONE);
                linear_one_inactive.setVisibility(View.VISIBLE);
                linear_one_active.setVisibility(View.GONE);
                linear_two_inactive.setVisibility(View.VISIBLE);
                linear_two_active.setVisibility(View.GONE);
                linear_three_inactive.setVisibility(View.VISIBLE);
                linear_three_active.setVisibility(View.GONE);
            } else {
                if (txt1String.length() > 6) {
                    iv_well.setVisibility(View.VISIBLE);
                    tv_error.setText(getResources().getString(R.string.weak));
                    linear_one_inactive.setVisibility(View.GONE);
                    linear_one_active.setVisibility(View.VISIBLE);
                    linear_two_inactive.setVisibility(View.VISIBLE);
                    linear_two_active.setVisibility(View.GONE);
                    linear_three_inactive.setVisibility(View.VISIBLE);
                    linear_three_active.setVisibility(View.GONE);
                }

                if (txt1String.length() > 7) {
                    tv_error.setText(getResources().getString(R.string.Good));
                    iv_well.setVisibility(View.VISIBLE);
                    linear_one_inactive.setVisibility(View.GONE);
                    linear_one_active.setVisibility(View.VISIBLE);
                    linear_two_inactive.setVisibility(View.GONE);
                    linear_two_active.setVisibility(View.VISIBLE);
                    linear_three_inactive.setVisibility(View.VISIBLE);
                    linear_three_active.setVisibility(View.GONE);
                }
                if (txt1String.length() > 10) {
                    tv_error.setText(getResources().getString(R.string.Strong));
                    iv_well.setVisibility(View.VISIBLE);
                    linear_one_inactive.setVisibility(View.GONE);
                    linear_one_active.setVisibility(View.VISIBLE);
                    linear_two_inactive.setVisibility(View.GONE);
                    linear_two_active.setVisibility(View.VISIBLE);
                    linear_three_inactive.setVisibility(View.GONE);
                    linear_three_active.setVisibility(View.VISIBLE);
                }

            }
        } else {
            iv_well.setVisibility(View.GONE);
            linear_one_inactive.setVisibility(View.VISIBLE);
            linear_one_active.setVisibility(View.GONE);
            linear_two_inactive.setVisibility(View.VISIBLE);
            linear_two_active.setVisibility(View.GONE);
            linear_three_inactive.setVisibility(View.VISIBLE);
            linear_three_active.setVisibility(View.GONE);
        }

    }

    @Override
    public void afterTextChanged(Editable editable) {
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.error_color));
        }
    }


    public void slideUp(View view) {
        tv_error.setVisibility(View.VISIBLE);
        iv_well.setVisibility(View.GONE);
        iv_error.setVisibility(View.VISIBLE);
        btn_fab.setVisibility(View.GONE);
        view.startAnimation(AnimationUtils.loadAnimation(this,
                R.anim.slide_up));
        view.setVisibility(View.VISIBLE);
        changeStatusBarColor();
        main_bg.setBackgroundColor(getResources().getColor(R.color.error_color));
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(500);
        }

    }

    public void slidedown(View view) {
        btn_fab.setVisibility(View.VISIBLE);
        iv_well.setVisibility(View.GONE);
        iv_error.setVisibility(View.GONE);
        view.startAnimation(AnimationUtils.loadAnimation(this,
                R.anim.slide_down));
        view.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                main_bg.setBackground(getResources().getDrawable(R.drawable.background_screen));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    window = getWindow();
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.setStatusBarColor(getResources().getColor(R.color.start_color));
                }
            }
        }, 1000);

    }
}
