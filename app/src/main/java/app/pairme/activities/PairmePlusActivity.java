package app.pairme.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.MyBounceInterpolator;
import app.pairme.ClassesPojo.Packages;
import app.pairme.R;
import app.pairme.payments.Payment_Gateway;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class
PairmePlusActivity extends BottomSheetDialogFragment {
    public static final String TAG = "PairmePlusActivity";
    private final long DELAY_MS = 1500;//delay in milliseconds before task is to be executed
    private final long PERIOD_MS = 1500;
    public List<Packages.Datum> data = new ArrayList<>();
    String amt;
    LinearLayout linear_one_month, linear_two_month, linear_three_month;
    TextView tv_popular_one, tv_continue, tv_popular_two, tv_popular_three, offer_one, offer_two, offer_three, currency_tv,
            currency1_tv,
            currency2_tv, time1, month1, time2, month2, time3, month3;
    Dialog dialog;
    View view;
    LinearLayout bottom_sheet;
    TextView close_tv;
    Context context;
    String package_id;
    String period = "";
    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private int[] layouts;
    private int currentPage = 0;
    private Timer timer;
    private ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {


        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
            if (position == layouts.length - 1) {

            } else {

            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    @Override
    public void onAttach(@NonNull Context context) {
        this.context = context;
        super.onAttach(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void setWhiteNavigationBar(@NonNull Dialog dialog) {
        Window window = dialog.getWindow();
        if (window != null) {
            DisplayMetrics metrics = new DisplayMetrics();
            window.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            GradientDrawable dimDrawable = new GradientDrawable();
            GradientDrawable navigationBarDrawable = new GradientDrawable();
            navigationBarDrawable.setShape(GradientDrawable.RECTANGLE);
            navigationBarDrawable.setColor(Color.WHITE);
            Drawable[] layers = {dimDrawable, navigationBarDrawable};
            LayerDrawable windowBackground = new LayerDrawable(layers);
            windowBackground.setLayerInsetTop(1, metrics.heightPixels);
            window.setBackgroundDrawable(windowBackground);
        }
    }

    public void packages() {
        Helper.showLoader(getActivity(), "");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<Packages> call = service.getPackages();
        call.enqueue(new Callback<Packages>() {
            @Override
            public void onResponse(Call<Packages> call, Response<Packages> response) {
                Helper.hideLoader();
                if (response.isSuccessful()) {
                    data = response.body().data;
                    time1.setText(data.get(0).name);
                    month1.setVisibility(View.GONE);
                    //  month1.setText(getResources().getString(R.string.months));
                    time2.setText(data.get(1).name);
                    month2.setVisibility(View.GONE);

                    //  month2.setText(getResources().getString(R.string.months));
                    time3.setText(data.get(2).name);
                    month3.setVisibility(View.GONE);

                    //  month3.setText(getResources().getString(R.string.months));

                    if (data.get(1).discount != null)
                        offer_two.setText(data.get(1).discount);

                    currency_tv.setText("SAR " + data.get(0).price);
                    currency1_tv.setText("SAR " + data.get(1).price);
                    currency2_tv.setText("SAR " + data.get(2).price);
                    amt = data.get(1).price;
                    package_id = (data.get(1).id);
                }
            }

            @Override
            public void onFailure(Call<Packages> call, Throwable t) {
                Log.d("error", "Failure; " + t);
                Helper.hideLoader();
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_action_bottom_dialog, container, false);
        bottom_sheet = view.findViewById(R.id.bottom_sheet);
        close_tv = view.findViewById(R.id.close_tv);
        viewPager = view.findViewById(R.id.view_pager);
        dotsLayout = view.findViewById(R.id.layoutDots);
        currency_tv = view.findViewById(R.id.currency_tv);
        currency1_tv = view.findViewById(R.id.currency1_tv);
        currency2_tv = view.findViewById(R.id.currency2_tv);
        tv_continue = view.findViewById(R.id.tv_continue);
        tv_popular_one = view.findViewById(R.id.tv_popular_one);
        tv_popular_two = view.findViewById(R.id.tv_popular_two);
        tv_popular_three = view.findViewById(R.id.tv_popular_three);
        linear_one_month = view.findViewById(R.id.linear_one_month);
        linear_two_month = view.findViewById(R.id.linear_two_month);
        linear_three_month = view.findViewById(R.id.linear_three_month);
        time1 = view.findViewById(R.id.time1);
        month1 = view.findViewById(R.id.month1);
        time2 = view.findViewById(R.id.time2);
        month2 = view.findViewById(R.id.month2);
        time3 = view.findViewById(R.id.time3);
        month3 = view.findViewById(R.id.month3);
        offer_one = view.findViewById(R.id.offer_one);
        offer_two = view.findViewById(R.id.offer_two);
        offer_three = view.findViewById(R.id.offer_three);

        layouts = new int[]{
                R.layout.pairme_slide1,
                R.layout.pairme_slide2,
                R.layout.pairme_slide3};
        addBottomDots(0);
        close_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slidedown(bottom_sheet);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        bottom_sheet.setVisibility(View.GONE);
                        dialog.dismiss();
                    }
                }, 1000);

            }
        });
        tv_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Payment_Gateway.class);
                intent.putExtra("package_id", package_id);
                intent.putExtra("amt", amt);
                intent.putExtra("period", period);
                startActivity(intent);
                dialog.dismiss();
            }
        });
        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == layouts.length) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer();
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);


        linear_one_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                period = time1.getText().toString();
                package_id = (data.get(0).packageType);
                amt = data.get(0).price;
                tv_popular_one.setVisibility(View.VISIBLE);
                final Animation myAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 30);
                myAnim.setInterpolator(interpolator);
                tv_popular_one.startAnimation(myAnim);
                linear_one_month.setBackgroundColor(Color.WHITE);
                tv_popular_two.setVisibility(View.GONE);
                linear_two_month.setBackgroundColor(getResources().getColor(R.color.graycolor_light));
                tv_popular_three.setVisibility(View.GONE);
                linear_three_month.setBackgroundColor(getResources().getColor(R.color.graycolor_light));
                if (data.get(0).discount != null)
                    offer_one.setText(data.get(0).discount);
                offer_two.setText(" ");
                offer_three.setText(" ");
            }
        });
        linear_two_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                period = time2.getText().toString();
                package_id = (data.get(1).packageType);
                amt = data.get(1).price;
                tv_popular_two.setVisibility(View.VISIBLE);
                final Animation myAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 30);
                myAnim.setInterpolator(interpolator);
                tv_popular_two.startAnimation(myAnim);
                linear_two_month.setBackgroundColor(Color.WHITE);
                tv_popular_one.setVisibility(View.GONE);
                linear_one_month.setBackgroundColor(getResources().getColor(R.color.graycolor_light));
                tv_popular_three.setVisibility(View.GONE);
                linear_three_month.setBackgroundColor(getResources().getColor(R.color.graycolor_light));

                if (data.get(1).discount != null)
                    offer_two.setText(data.get(1).discount);
                offer_one.setText(" ");
                offer_three.setText(" ");
            }
        });
        linear_three_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                period = time3.getText().toString();
                package_id = (data.get(2).packageType);
                amt = data.get(2).price;
                tv_popular_three.setVisibility(View.VISIBLE);
                final Animation myAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 30);
                myAnim.setInterpolator(interpolator);
                tv_popular_three.startAnimation(myAnim);
                linear_three_month.setBackgroundColor(getResources().getColor(R.color.white));
                tv_popular_one.setVisibility(View.GONE);
                linear_one_month.setBackgroundColor(getResources().getColor(R.color.graycolor_light));
                tv_popular_two.setVisibility(View.GONE);
                linear_two_month.setBackgroundColor(getResources().getColor(R.color.graycolor_light));
                offer_one.setText(" ");
                offer_two.setText("");
                if (data.get(2).discount != null)
                    offer_three.setText(data.get(2).discount);

            }
        });


        return view;

    }

    private void slidedown(LinearLayout bottom_sheet) {
        bottom_sheet.startAnimation(AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_out_down));
    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];
        int[] colorsActive = context.getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = context.getResources().getIntArray(R.array.array_dot_inactive);
        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(context);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        packages();
        dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) dialogInterface;
                setupFullHeight(bottomSheetDialog);
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setWhiteNavigationBar(dialog);
        }
        return dialog;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((View) getView().getParent()).setBackgroundColor(Color.TRANSPARENT);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        slideUp(bottom_sheet);
    }

    public void slideUp(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_up_dialog));
        view.setVisibility(View.VISIBLE);
    }

    private void setupFullHeight(BottomSheetDialog bottomSheetDialog) {
        FrameLayout bottomSheet = bottomSheetDialog.findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();

        int windowHeight = getWindowHeight();
        if (layoutParams != null) {
            layoutParams.height = windowHeight + 200;
        }
        bottomSheet.setLayoutParams(layoutParams);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    private int getWindowHeight() {
        // Calculate window height for fullscreen use
        DisplayMetrics displayMetrics = new DisplayMetrics();
        (getActivity()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        private MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);
            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            view = (View) object;
            container.removeView(view);
        }

    }

}



