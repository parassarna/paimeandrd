package app.pairme.activities;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.DescriptionPojo.DescriptionPojo;
import app.pairme.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateDiscriptionActivity extends AppCompatActivity implements View.OnClickListener {
    EditText et_description, et_searching;
    TextView tv_save, tv_error, tv_error_one;
    Dialog dialog;
    boolean updated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_discription);
        et_description = findViewById(R.id.et_description);
        et_searching = findViewById(R.id.et_searching);
        tv_save = findViewById(R.id.tv_save);
        tv_error = findViewById(R.id.tv_error);
        tv_error_one = findViewById(R.id.tv_error_one);
        et_description.setText(Helper.getUserData(this).getAboutMeContent());
        et_searching.setText(Helper.getUserData(this).getLookingForContent());
        changeStatusBarColor();
        et_description.setOnClickListener(this);
        tv_save.setOnClickListener(this);
        et_description.addTextChangedListener(new TextCustomTextWatcher(et_description));
        et_searching.setOnClickListener(this);
        et_searching.addTextChangedListener(new TextCustomTextWatcher(et_searching));

    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_save:
                addDescription();
        }
    }

    public void addDescription() {
        Helper.showLoader(this, "Please wait..");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<DescriptionPojo> call = service.addDescription("Bearer " + Helper.getToken(UpdateDiscriptionActivity.this), et_description.getText().toString(), et_searching.getText().toString());
        // Call<DescriptionPojo> call = service.addDescription("Bearer " + Helper.getToken(this), et_desp.getText().toString(), et_searching.getText().toString());

        call.enqueue(new Callback<DescriptionPojo>() {
            @Override
            public void onResponse(Call<DescriptionPojo> call, Response<DescriptionPojo> response) {
                Helper.hideLoader();
                if (response.isSuccessful()) {
                    Toast.makeText(UpdateDiscriptionActivity.this, R.string.details_updated, Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Log.i(">>>>", response.raw().toString());
                }
            }

            @Override
            public void onFailure(Call<DescriptionPojo> call, Throwable t) {
                Helper.hideLoader();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (updated)
            openDialog();
        else super.onBackPressed();
    }

    private void openDialog() {
        dialog = new Dialog(UpdateDiscriptionActivity.this);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.show();
        dialog.setCancelable(false);
        Button declineButton = dialog.findViewById(R.id.btn_logout);
        declineButton.setText(getResources().getString(R.string.ok));
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDescription();
                dialog.dismiss();
            }
        });
        TextView cancel = dialog.findViewById(R.id.tv_cancel);
        cancel.setText(getResources().getString(R.string.no));
        TextView tv_all = dialog.findViewById(R.id.tv_all);
        TextView tv_logout = dialog.findViewById(R.id.tv_logout);
        tv_all.setVisibility(View.GONE);
        tv_logout.setText(getResources().getString(R.string.do_you_Wish_to_save_the_Changes));
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    public class TextCustomTextWatcher implements TextWatcher {
        View editText;

        public TextCustomTextWatcher(View editText) {
            this.editText = editText;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            updated = true;
            switch (editText.getId()) {
                case R.id.et_description:
                    if (s.length() < 0) {
                        et_description.setBackgroundResource(R.drawable.black_background);
                    }
                    break;
                case R.id.et_searching:
                    if (s.length() < 0) {
                        et_searching.setBackgroundResource(R.drawable.black_background);
                    }
                    break;

            }

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {


        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (editText.getId()) {
                case R.id.et_description:
                    if (s.length() > 0) {
                        String txt1String = (s.toString());
                        if (txt1String.length() <= 14) {
                            tv_error.setVisibility(View.GONE);
                            et_description.setBackgroundResource(R.drawable.black_background);
                        } else if (txt1String.length() >= 14 && txt1String.length() <= 15) {
                            tv_error.setVisibility(View.VISIBLE);
                            et_description.setBackgroundResource(R.drawable.error_background);
                        } else {
                            tv_error.setVisibility(View.GONE);
                            et_description.setBackgroundResource(R.drawable.green_background);
                        }

                    }
                    break;
                case R.id.et_searching:
                    if (s.length() > 0) {
                        et_searching.setBackgroundResource(R.drawable.black_background);
                        String txt1String = (s.toString());
                        if (txt1String.length() <= 14) {
                            tv_error_one.setVisibility(View.GONE);
                            et_searching.setBackgroundResource(R.drawable.black_background);
                        } else if (txt1String.length() >= 14 && txt1String.length() <= 15) {
                            tv_error_one.setVisibility(View.VISIBLE);
                            et_searching.setBackgroundResource(R.drawable.error_background);
                        } else {
                            tv_error_one.setVisibility(View.GONE);
                            et_searching.setBackgroundResource(R.drawable.green_background);
                        }

                    }
                    break;
            }
        }
    }


}
