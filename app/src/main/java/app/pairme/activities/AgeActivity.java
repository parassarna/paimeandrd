package app.pairme.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import app.pairme.ApiServices.Helper;
import app.pairme.R;

import static app.pairme.Utils.flip;

public class AgeActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView iv_back, close_iv, iv_error, iv_well, fab_active;
    EditText et_age;
    TextView tv_error, tv_red_error;
    LinearLayout error_ll;
    RelativeLayout main_bg;
    Window window;
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() > 0) {
                int txt1String = Integer.parseInt(s.toString());
                if (txt1String < 18) {
                    tv_error.setText(getResources().getString(R.string.you_must_be_min_18_years_old_to_use_pairme));
                    iv_error.setVisibility(View.VISIBLE);
                    iv_well.setVisibility(View.GONE);
                } else {
                    tv_error.setText(et_age.getText().toString().trim() + " " + getResources().getString(R.string.wonderful_age));
                    iv_well.setVisibility(View.VISIBLE);
                    iv_error.setVisibility(View.GONE);
                }
            } else {
                tv_error.setText("");
                iv_error.setVisibility(View.GONE);
                iv_well.setVisibility(View.GONE);
            }

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_age);
        initControl();
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.error_color));
        }
    }

    private void initControl() {
        iv_back = findViewById(R.id.iv_back);
        et_age = findViewById(R.id.et_age);
        fab_active = findViewById(R.id.fab_active);
        main_bg = findViewById(R.id.main_bg);
        error_ll = findViewById(R.id.error_ll);
        close_iv = findViewById(R.id.close_iv);
        tv_error = findViewById(R.id.tv_error);
        tv_red_error = findViewById(R.id.tv_red_error);
        iv_error = findViewById(R.id.iv_error);
        iv_well = findViewById(R.id.iv_well);
        iv_back.setOnClickListener(this);
        fab_active.setOnClickListener(this);
        close_iv.setOnClickListener(this);
        et_age.addTextChangedListener(textWatcher);
        if (Helper.getLanguage(this).equalsIgnoreCase("ar"))
            flip(fab_active);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.fab_active:
                if (et_age.getText().toString().equals("")) {
                    iv_error.setVisibility(View.VISIBLE);
                    slideUp(error_ll);
                    tv_red_error.setText(getResources().getString(R.string.age_is_required));

                } else if (Integer.parseInt(et_age.getText().toString()) < 18) {
                    iv_error.setVisibility(View.VISIBLE);
                    slideUp(error_ll);
                    tv_error.setText(getResources().getString(R.string.you_must_be_min_18_years_old_to_use_pairme));
                    tv_red_error.setText(getResources().getString(R.string.you_must_be_min_18_years_old_to_use_pairme));

                } else {
                    Helper.setAge(et_age.getText().toString(), AgeActivity.this);
                    startActivity(new Intent(this, LocationActivity.class));
                }
                break;
            case R.id.close_iv:
                tv_error.setText("");
                slidedown(error_ll);
                break;
        }

    }

    public void slideUp(View view) {
        fab_active.setVisibility(View.GONE);
        view.startAnimation(AnimationUtils.loadAnimation(this,
                R.anim.slide_up));
        view.setVisibility(View.VISIBLE);
        changeStatusBarColor();
        main_bg.setBackgroundColor(getResources().getColor(R.color.error_color));
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            v.vibrate(500);
        }

    }

    public void slidedown(View view) {
        fab_active.setVisibility(View.VISIBLE);
        view.startAnimation(AnimationUtils.loadAnimation(this,
                R.anim.slide_down));
        view.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                main_bg.setBackground(getResources().getDrawable(R.drawable.background_screen));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    window = getWindow();
                    window.setStatusBarColor(getResources().getColor(R.color.start_color));
                }
            }
        }, 1000);

    }

}
