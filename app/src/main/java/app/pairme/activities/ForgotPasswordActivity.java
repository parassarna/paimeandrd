package app.pairme.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.EmailPojo;
import app.pairme.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView iv_back, close_iv;
    Button btn_login_inactive,
            btn_login_active;
    EditText et_email;
    RelativeLayout main_bg;
    Window window;
    LinearLayout error_ll;
    TextView tv_error;
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            String regex = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(et_email.getText().toString());
            System.out.println(et_email.getText().toString() + " : " + matcher.matches());
            String username = et_email.getText().toString().trim();
            if (!username.isEmpty() && matcher.matches()) {
                btn_login_inactive.setVisibility(View.GONE);
                btn_login_active.setVisibility(View.VISIBLE);
            } else {
                btn_login_inactive.setVisibility(View.VISIBLE);
                btn_login_active.setVisibility(View.GONE);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        initScreen();
        changeStatusBarColor();
    }

    private void initScreen() {
        iv_back = findViewById(R.id.iv_back);
        btn_login_inactive = findViewById(R.id.btn_login_inactive);
        btn_login_active = findViewById(R.id.btn_login_active);
        et_email = findViewById(R.id.et_email);
        main_bg = findViewById(R.id.main_bg);
        error_ll = findViewById(R.id.error_ll);
        close_iv = findViewById(R.id.close_iv);
        tv_error = findViewById(R.id.tv_error);
        iv_back.setOnClickListener(this);
        close_iv.setOnClickListener(this);
        btn_login_active.setOnClickListener(this);
        et_email.addTextChangedListener(textWatcher);
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                login();
                break;
            case R.id.close_iv:
                slidedown(error_ll);
                break;
            case R.id.btn_login_active:
                if (TextUtils.isEmpty(et_email.getText().toString())) {
                    slideUp(error_ll);
                    tv_error.setText(getResources().getString(R.string.oops_login_is_required));
                } else if (TextUtils.isEmpty(et_email.getText().toString())) {
                    slideUp(error_ll);
                    tv_error.setText(getResources().getString(R.string.Enter_Username));
                } else {
                    checkEmail();

                }
                break;
        }
    }

    private void login() {
        startActivity(new Intent(getApplicationContext(), LoginScreenMain.class));
        finish();
    }


    public void slideUp(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this,
                R.anim.slide_up));
        view.setVisibility(View.VISIBLE);
        changeStatusBarColor();
        main_bg.setBackgroundColor(getResources().getColor(R.color.error_color));
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(500);
        }

    }

    public void slidedown(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this,
                R.anim.slide_down));
        view.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                main_bg.setBackground(getResources().getDrawable(R.drawable.background_screen));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    window = getWindow();
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.setStatusBarColor(getResources().getColor(R.color.start_color));
                }
            }
        }, 1000);

    }

    public void checkEmail() {
        Helper.showLoader(ForgotPasswordActivity.this, "Please wait...");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<EmailPojo> call = service.forgetPassword(et_email.getText().toString().trim());
        call.enqueue(new Callback<EmailPojo>() {
            @Override
            public void onResponse(Call<EmailPojo> call, Response<EmailPojo> response) {
                Helper.hideLoader();
                if (response.isSuccessful() && response.body().success) {
                    login();
                } else {
                    Toast.makeText(ForgotPasswordActivity.this, response.body().message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<EmailPojo> call, Throwable t) {
                Helper.hideLoader();
                t.printStackTrace();
            }
        });
    }

}
