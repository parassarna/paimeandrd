package app.pairme.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.provider.Settings;
import android.se.omapi.Session;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import app.pairme.ApiServices.Helper;
import app.pairme.LocationUtils;
import app.pairme.R;
import app.pairme.adapters.PlaceAutocompleteAdapter;
import app.pairme.locationInterface;

import static app.pairme.Utils.flip;

public class LocationActivity extends AppCompatActivity implements View.OnClickListener, locationInterface, LocationUtils.MyLocation {
    ImageView iv_back, show_pass_btn, close_iv, fab, iv_well;
    LinearLayout error_ll;
    RelativeLayout main_bg;
    AutoCompleteTextView mSearchEdittext;
    int size;
    ProgressBar pb;
    TextView tv_error;
    LocationUtils locationUtils;
    Geocoder geocoder;
    Window window;
    List<Address> addresses;
    private FusedLocationProviderClient mFusedLocationClient;
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() > 10) {
                tv_error.setText(getResources().getString(R.string.lovely_city));
                iv_well.setVisibility(View.VISIBLE);
            } else {
                tv_error.setText("");
                iv_well.setVisibility(View.GONE);
            }

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        initControl();
        mSearchEdittext.setAdapter(new PlaceAutocompleteAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, this));


    }

    private void initControl() {
        iv_back = findViewById(R.id.iv_back);
        fab = findViewById(R.id.fab);
        pb = findViewById(R.id.pb);
        show_pass_btn = findViewById(R.id.show_pass_btn);
        mSearchEdittext = findViewById(R.id.btn_marriage_inactive);
        main_bg = findViewById(R.id.main_bg);
        tv_error = findViewById(R.id.tv_error);
        error_ll = findViewById(R.id.error_ll);
        close_iv = findViewById(R.id.close_iv);
        iv_well = findViewById(R.id.iv_well);
        close_iv.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        show_pass_btn.setOnClickListener(this);
        fab.setOnClickListener(this);
        mSearchEdittext.addTextChangedListener(textWatcher);
        if (Helper.getLanguage(this).equalsIgnoreCase("ar"))
        flip(fab);
    }

    public void slideUp(View view) {
        size = 0;
        fab.setVisibility(View.GONE);
        view.startAnimation(AnimationUtils.loadAnimation(this,
                R.anim.slide_up));
        view.setVisibility(View.VISIBLE);
        changeStatusBarColor();
        main_bg.setBackgroundColor(getResources().getColor(R.color.error_color));
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(500);
        }

    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.error_color));
        }
    }


    public void slidedown(View view) {
        tv_error.setVisibility(View.GONE);
        fab.setVisibility(View.VISIBLE);
        view.startAnimation(AnimationUtils.loadAnimation(this,
                R.anim.slide_down));
        view.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                main_bg.setBackground(getResources().getDrawable(R.drawable.background_screen));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    window = getWindow();
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.setStatusBarColor(getResources().getColor(R.color.start_color));
                }
            }
        }, 1000);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.fab:
                if (mSearchEdittext.getText().toString().isEmpty()) {
                    tv_error.setVisibility(View.VISIBLE);
                    slideUp(error_ll);
                } else {
                    Helper.setLocation(mSearchEdittext.getText().toString(), this);
                    startActivity(new Intent(getApplicationContext(), OriginalLocationActivity.class));
                }
                break;
            case R.id.close_iv:
                slidedown(error_ll);
                break;
            case R.id.show_pass_btn:
                if (ContextCompat.checkSelfPermission(LocationActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(LocationActivity.this,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    GPSStatus();
                } else {
                    GetCurrentlocation();
                }
                break;
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void getLocationPermission() {
        requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                123);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void GPSStatus() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!GpsStatus) {
            Toast.makeText(LocationActivity.this, "On Location in High Accuracy", Toast.LENGTH_SHORT).show();
            startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 2);
        } else {
            GetCurrentlocation();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            GPSStatus();
        }
    }

    // main method used for get the location of user
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void GetCurrentlocation() {
        locationUtils = new LocationUtils(LocationActivity.this, this, false);
        if (ActivityCompat.checkSelfPermission(LocationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(LocationActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            getLocationPermission();
            return;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void getSize(int size) {
        LocationActivity.this.size = size;
    }

    @Override
    public void locationUpdates(Location location) {
        if (location != null) {
            geocoder = new Geocoder(LocationActivity.this, Locale.ENGLISH);
        }

        try {
            assert location != null;
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            Double[] latlng = new Double[2];
            latlng[0] = location.getLongitude();
            latlng[1] = location.getLatitude();
        } catch (IOException e) {
            Log.e("test", "location problem");
        }
        if (addresses != null) {
            //String address = addresses.get(0).getAddressLine(0);
            String address = addresses.get(0).getLocality() + ", " + addresses.get(0).getAdminArea();
            mSearchEdittext.setText(address);
        }
    }


}
