package app.pairme.activities;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.DescriptionPojo.DescriptionPojo;
import app.pairme.ClassesPojo.EmailPojo;
import app.pairme.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateEmailActivity extends AppCompatActivity implements View.OnClickListener {
    EditText et_new_email, et_confirm_email;
    TextView et_current_email, tv_confirmed, tv_save;
    String currunt_email;
    Dialog dialog;
    boolean updated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_email);
        checkEmail();
        changeStatusBarColor();
        initScreen();
        et_current_email.setText(currunt_email);
    }

    private void initScreen() {
        et_new_email = findViewById(R.id.et_new_email);
        tv_confirmed = findViewById(R.id.tv_confirmed);
        et_confirm_email = findViewById(R.id.et_confirm_email);
        et_current_email = findViewById(R.id.et_current_email);
        tv_save = findViewById(R.id.tv_save);
        et_new_email.setOnClickListener(this);
        et_confirm_email.setOnClickListener(this);
        et_confirm_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                updated = true;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        tv_save.setOnClickListener(this);
        currunt_email = Helper.getUserData(UpdateEmailActivity.this).getEmail();
    }

    public void update() {
        Helper.showLoader(this, "Please wait..");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<DescriptionPojo> call = service.updateEmail("Bearer " + Helper.getToken(UpdateEmailActivity.this),
                et_current_email.getText().toString(), et_confirm_email.getText().toString());
        call.enqueue(new Callback<DescriptionPojo>() {
            @Override
            public void onResponse(Call<DescriptionPojo> call, Response<DescriptionPojo> response) {
                Helper.hideLoader();
                if (response.isSuccessful()) {
                    Toast.makeText(UpdateEmailActivity.this, R.string.details_updated, Toast.LENGTH_SHORT).show();                    finish();
                } else {
                    Log.i(">>>>", response.raw().toString());
                }
            }

            @Override
            public void onFailure(Call<DescriptionPojo> call, Throwable t) {
                Helper.hideLoader();
                t.printStackTrace();
            }
        });
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.et_new_email:
                break;
            case R.id.et_confirm_email:
                break;
            case R.id.et_current_email:
                break;
            case R.id.tv_save:
                update();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (updated)
            openDialog();
        else super.onBackPressed();
    }

    private void openDialog() {
        dialog = new Dialog(UpdateEmailActivity.this);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.show();
        dialog.setCancelable(false);
        Button declineButton = dialog.findViewById(R.id.btn_logout);
        declineButton.setText(getResources().getString(R.string.yes));
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                update();
            }
        });
        TextView cancel = dialog.findViewById(R.id.tv_cancel);
        cancel.setText(getResources().getString(R.string.no));
        TextView tv_all = dialog.findViewById(R.id.tv_all);
        TextView tv_logout = dialog.findViewById(R.id.tv_logout);
        tv_all.setVisibility(View.GONE);
        tv_logout.setText(getResources().getString(R.string.do_you_Wish_to_save_the_Changes));
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    public void checkEmail() {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<EmailPojo> call = service.checkEmail(Helper.getUserData(this).getEmail());
        call.enqueue(new Callback<EmailPojo>() {
            @Override
            public void onResponse(Call<EmailPojo> call, Response<EmailPojo> response) {

                if (response.isSuccessful()) {
                    tv_confirmed.setVisibility(View.VISIBLE);
                    Toast.makeText(UpdateEmailActivity.this, "Email is verified", Toast.LENGTH_SHORT).show();
                } else {
                    Log.i(">>>>", response.raw().toString());
                }
            }

            @Override
            public void onFailure(Call<EmailPojo> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

}
