package app.pairme.activities;

import android.Manifest;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.Infodata;
import app.pairme.ClassesPojo.LoginPojo.LoginPojo;
import app.pairme.ClassesPojo.UploadImagePojo.UploadImagePojo;
import app.pairme.ClassesPojo.UserInfoPojo.UserInfo;
import app.pairme.R;
import app.pairme.adapters.FileUtils;
import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.pairme.Utils.confirmed;
import static app.pairme.Utils.flipview;
import static app.pairme.Utils.getImageUri;
import static app.pairme.Utils.getRealPathFromURI;

public class ImageSignup extends AppCompatActivity implements View.OnClickListener {
    CircleImageView iv_profile;
    ImageView img_choose;
    Button btn_skip, btn_next;
    int RESULT_LOAD_IMAGE = 4;
    Uri uri;
    String token, myId;
    File file;
    boolean isImageAdded = false;
    String android_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkPerm();
        setContentView(R.layout.activity_image_signup);
        flipview(this, (LinearLayout) findViewById(R.id.linear));
        loginApi();
        changeStatusBarColor();
        initControl();
    }


    private void initControl() {
        iv_profile = findViewById(R.id.iv_profile);
        img_choose = findViewById(R.id.iv_choose_image);
        btn_skip = findViewById(R.id.btn_skip);
        btn_next = findViewById(R.id.btn_next);
        btn_skip.setOnClickListener(this);
        btn_next.setOnClickListener(this);
        img_choose.setOnClickListener(this);
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_skip:
                startActivity(new Intent(getApplicationContext(), DescriptionActivity.class));
                break;
            case R.id.btn_next:
                if (isImageAdded) {
                    makeApiCall();

                } else {
                    Toast.makeText(this, getResources().getString(R.string.select_image), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.iv_choose_image:
                isImageAdded = true;
                chooseImage();
                break;
        }
    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(intent, RESULT_LOAD_IMAGE);
    }


    private void checkPerm() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.i(">>>>>>>>>>", " " + requestCode + "  " + resultCode + "  ");
        if (data != null) {
            if (requestCode == RESULT_LOAD_IMAGE) {
                uri = data.getData();
                file = FileUtils.getFile(this, uri);
                try {
                    file = new Compressor(this).compressToFile(file);
                } catch (IOException e) {
                    Log.d("tag", "onActivityResult: "+e.getMessage());
                    e.printStackTrace();
                }
                ((CircleImageView) findViewById(R.id.iv_profile)).setImageURI(uri);
                //  Toast.makeText(getApplicationContext(), "Image Loaded" + uri, Toast.LENGTH_SHORT).show();

            } else {
            }
            super.onActivityResult(requestCode, resultCode, data);
        } else {
        }
    }


    public void makeApiCall() {
        Helper.showLoader(this, null);
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        MultipartBody.Part part = null;
        try {
            if (file != null) {
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                part = MultipartBody.Part.createFormData("profile_image", URLEncoder.encode(file.getName(), "utf-8"), requestBody);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Call<UploadImagePojo> call = service.uploadProfileImage(part, "Bearer " + Helper.getToken(ImageSignup.this));
        call.enqueue(new Callback<UploadImagePojo>() {
            @Override
            public void onResponse(Call<UploadImagePojo> call, Response<UploadImagePojo> response) {
                Helper.hideLoader();
                if (response.isSuccessful()) {
                    Intent intent = new Intent(ImageSignup.this, DescriptionActivity.class);
                    intent.putExtra("token", token);
                    startActivity(intent);
                    Log.i("Upload", String.valueOf(response.body().getSuccess()));
                } else {
                    Log.i("Upload else", response.raw().toString());
                }
            }

            @Override
            public void onFailure(Call<UploadImagePojo> call, Throwable t) {
                Helper.hideLoader();
                Log.e("Upload error:", t.getMessage());
            }

        });
    }

    public void loginApi() {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<LoginPojo> call = service.loginApi(getIntent().getStringExtra("email"), Helper.getPassword(this), android_id, "android");
        call.enqueue(new Callback<LoginPojo>() {
            @Override
            public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                if (response.isSuccessful()) {
                    token = response.body().getData().getToken();
                    Helper.setToken(token, ImageSignup.this);
                    myId = response.body().getData().getId();
                    Helper.setMyId(myId, ImageSignup.this);
                    confirmed = response.body().getData().confirmed.equalsIgnoreCase("1");
                    getUserdata();
                } else {
                    Log.d("error", "else: " + response.body().getStatus());
                }

            }

            @Override
            public void onFailure(Call<LoginPojo> call, Throwable t) {
                Log.d("error", "Failure; " + t);
            }
        });
    }

    private void getUserdata() {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<UserInfo> call = service.getUserInfo("Bearer " + token, myId);
        call.enqueue(new Callback<UserInfo>() {

            @Override
            public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                if (response.isSuccessful()) {
                    Helper.setUserData(new Gson().toJson(response.body().getData()), ImageSignup.this);
                    searchlist();
                } else {
                    Log.d("error", "else: " + response.body());
                }
            }

            @Override
            public void onFailure(Call<UserInfo> call, Throwable t) {
                //  Helper.hideLoader();
                Log.d("error", "Failure; " + t);
                Toast.makeText(ImageSignup.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void searchlist() {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<Infodata> call = service.getSearchlist(Helper.getLanguage(ImageSignup.this));
        call.enqueue(new Callback<Infodata>() {
            @Override
            public void onResponse(Call<Infodata> call, Response<Infodata> response) {
                if (response.isSuccessful()) {
                    Helper.setinfodata(new Gson().toJson(response.body().data), ImageSignup.this);
                } else {
                    Log.i(">>>>", response.raw().toString());
                }
            }

            @Override
            public void onFailure(Call<Infodata> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

}
