package app.pairme.activities;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import app.pairme.ApiServices.Helper;
import app.pairme.R;

public class SupportActivity extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout relative_contact, relative_faq, relative_safety, relative_terms, relative_privacy, relative_rate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);
        changeStatusBarColor();
        initScreen();
    }

    private void initScreen() {
        relative_contact = findViewById(R.id.relative_contact);
        relative_faq = findViewById(R.id.relative_faq);
        relative_safety = findViewById(R.id.relative_safety);
        relative_terms = findViewById(R.id.relative_terms);
        relative_privacy = findViewById(R.id.relative_privacy);
        relative_rate = findViewById(R.id.relative_rate);
        relative_contact.setOnClickListener(this);
        relative_faq.setOnClickListener(this);
        relative_safety.setOnClickListener(this);
        relative_terms.setOnClickListener(this);
        relative_privacy.setOnClickListener(this);
        relative_rate.setOnClickListener(this);
        if (Helper.getLanguage(this).equalsIgnoreCase("ar")) {
            for (int i = 0; i <= 5; i++) {
                String id = "icon" + (i);
                int resID = getResources().getIdentifier(id, "id", this.getPackageName());
                ((ImageView) findViewById(resID)).setRotation(180);
            }
        }
    }


    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relative_contact:
                startActivity(new Intent(getApplicationContext(), ContactUs.class));
                break;
            case R.id.relative_faq:
                startActivity(new Intent(getApplicationContext(), FaqScreen.class));
                break;
            case R.id.relative_safety:
                startActivity(new Intent(getApplicationContext(), SafetyAdviceActivity.class));
                break;
            case R.id.relative_terms:
                startActivity(new Intent(getApplicationContext(), TermsActivity.class));
                break;
            case R.id.relative_privacy:
                startActivity(new Intent(getApplicationContext(), PrivacyPolicy.class));
                break;
            case R.id.relative_rate:
                String APP_PNAME = "app.pairme";
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + APP_PNAME)));
                break;


        }

    }
}
