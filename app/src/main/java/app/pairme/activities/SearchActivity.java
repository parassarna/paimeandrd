package app.pairme.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.FilterPojo;
import app.pairme.ClassesPojo.Infodata;
import app.pairme.LocationUtils;
import app.pairme.R;
import app.pairme.Utils;
import app.pairme.adapters.PlaceAutocompleteAdapter;
import app.pairme.adapters.UpdatHeightAdapter;
import app.pairme.adapters.UpdatinfoAdapter;
import app.pairme.locationInterface;
import io.apptik.widget.MultiSlider;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.pairme.Utils.getNationality;

public class SearchActivity extends AppCompatActivity implements View.OnClickListener, locationInterface, LocationUtils.MyLocation {
    Number preMin = 18, minheight, maxheight, preMax = 80;
    MultiSlider seekBar, heightSeekbar;
    Dialog dialog;
    TextView age, height;
    ImageView iv_down, show_pass_btn, iv_up, iv_down_social, iv_up_social, iv_down_appearance, iv_up_appearance, iv_down_lifestyle, iv_up_lifestyle, iv_down_interest, iv_up_interest, tv_advance_search, iv_well_signup, iv_well_looking, iv_well_location, iv_well_origin, iv_well_maxDistance, iv_well_name, iv_well_education, iv_well_occupation, iv_well_activity, iv_well_status, iv_well_kids, iv_well_want_kids, iv_well_living, iv_well_relocate, iv_well_Appearance, iv_well_body_type, iv_well_Complexion, iv_well_health_status, iv_well_hair_color, iv_well_hair_length, iv_well_type, iv_well_eye_color,
            iv_well_eye_wear, iv_well_facial_hair, iv_well_weapon, iv_well_style, iv_well_Smoking, iv_well_eating_habits, iv_well_exercise, iv_well_Sleep_Habits, iv_well_pets, iv_well_family_values, iv_well_Polygamy, iv_well_Personality, iv_well_Language, iv_well_Hobbies, iv_well_Cuisine, iv_well_Music, iv_well_Movies, iv_well_Sports;
    RelativeLayout relative_basic, relative_stat, relative_appearanc, relative_life, relative_int;
    EditText et_new_email, et_looking, et_maxDistance, et_name, et_education, et_occupation, et_sector, et_status, et_kids, et_want_kids, et_living, et_relocate, et_Appearance, et_body_type, et_Complexion, et_health_status, et_hair_color, et_hair_length, et_type, et_eye_color, et_eye_wear, et_facial_hair, et_weapon, et_style, et_Smoking, et_eating_habits, et_exercise, et_Sleep_Habits, et_pets, et_family_values, et_Polygamy, et_Personality, et_eye_language, et_Hobbies, et_Cuisine, et_Music, et_Movies, et_Sports;
    Switch switch_online, switch_photos;
    RecyclerView recyclerView;
    ArrayList<Infodata.Data> names = new ArrayList<>();
    Button btn_clear, btn_search;
    AutoCompleteTextView et_location;
    LocationUtils locationUtils;
    Geocoder geocoder;
    Window window;
    List<Address> addresses;
    HashMap<String, String> named = new HashMap<>();
    boolean updated;
    MaterialSpinner spinner_Location;
    String origin;
    FragmentManager manager = getSupportFragmentManager();
    private ProgressBar progressBar_cyclic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        initScreen();
        seekBar.setMin(18);
        seekBar.setMax(80);
        heightSeekbar.setMax(220);
        heightSeekbar.setMin(120);
        seekBar.getThumb(0).setValue(18);
        seekBar.getThumb(1).setValue(80);
        heightSeekbar.getThumb(0).setValue(120);
        heightSeekbar.getThumb(1).setValue(220);
        seekBar.setOnThumbValueChangeListener(new MultiSlider.OnThumbValueChangeListener() {
            @Override
            public void onValueChanged(MultiSlider multiSlider, MultiSlider.Thumb thumb, int thumbIndex, int value) {

                if (thumbIndex == 0) {
                    preMin = value;
                } else {
                    preMax = value;
                }
                if (preMax.equals(preMin))
                    preMax = 80;
                age.setText(preMin + " - " + preMax);
            }


        });
        heightSeekbar.setOnThumbValueChangeListener(new MultiSlider.OnThumbValueChangeListener() {
            @Override
            public void onValueChanged(MultiSlider multiSlider, MultiSlider.Thumb thumb, int thumbIndex, int value) {
                if (thumbIndex == 0) {
                    minheight = value;
                } else {
                    maxheight = value;
                }
                height.setText(minheight + " - " + maxheight);
            }


        });

        spinner_Location.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                origin = getNationality(position);
            }
        });
    }

    private void initScreen() {
        et_new_email = findViewById(R.id.et_new_email);
        switch_online = findViewById(R.id.switch_online);
        age = findViewById(R.id.age);
        height = findViewById(R.id.height);
        tv_advance_search = findViewById(R.id.tv_advance_search);
        btn_clear = findViewById(R.id.btn_clear);
        seekBar = findViewById(R.id.rangeseekbar);
        switch_photos = findViewById(R.id.switch_photos);
        et_looking = findViewById(R.id.et_looking);
        et_maxDistance = findViewById(R.id.et_maxDistance);
        et_name = findViewById(R.id.et_name);
        et_education = findViewById(R.id.et_education);
        et_occupation = findViewById(R.id.et_occupation);
        et_sector = findViewById(R.id.et_sector);
        et_status = findViewById(R.id.et_status);
        et_kids = findViewById(R.id.et_kids);
        et_want_kids = findViewById(R.id.et_want_kids);
        et_living = findViewById(R.id.et_living);
        et_relocate = findViewById(R.id.et_relocate);
        et_Appearance = findViewById(R.id.et_Appearance);
        et_body_type = findViewById(R.id.et_body_type);
        heightSeekbar = findViewById(R.id.heightSeekbar);
        et_Complexion = findViewById(R.id.et_Complexion);
        et_health_status = findViewById(R.id.et_health_status);
        et_hair_color = findViewById(R.id.et_hair_color);
        et_hair_length = findViewById(R.id.et_hair_length);
        et_type = findViewById(R.id.et_type);
        et_eye_color = findViewById(R.id.et_eye_color);
        et_eye_wear = findViewById(R.id.et_eye_wear);
        et_facial_hair = findViewById(R.id.et_facial_hair);
        et_weapon = findViewById(R.id.et_weapon);
        et_style = findViewById(R.id.et_style);
        et_Smoking = findViewById(R.id.et_Smoking);
        et_eating_habits = findViewById(R.id.et_eating_habits);
        et_exercise = findViewById(R.id.et_exercise);
        et_Sleep_Habits = findViewById(R.id.et_Sleep_Habits);
        et_pets = findViewById(R.id.et_pets);
        et_family_values = findViewById(R.id.et_family_values);
        et_Polygamy = findViewById(R.id.et_Polygamy);
        et_Personality = findViewById(R.id.et_Personality);
        et_eye_language = findViewById(R.id.et_eye_language);
        et_Hobbies = findViewById(R.id.et_Hobbies);
        et_Cuisine = findViewById(R.id.et_Cuisine);
        et_Music = findViewById(R.id.et_Music);
        et_Movies = findViewById(R.id.et_Movies);
        et_Sports = findViewById(R.id.et_Sports);
        et_looking = findViewById(R.id.et_looking);
        et_location = findViewById(R.id.et_location);
        show_pass_btn = findViewById(R.id.show_pass_btn);
        spinner_Location = findViewById(R.id.spinner_location);
        spinner_Location.setItems(Arrays.asList(getResources().getStringArray(R.array.nationality)));
        et_looking.setOnClickListener(this);
        show_pass_btn.setOnClickListener(this);
        et_maxDistance.setOnClickListener(this);
        iv_down = findViewById(R.id.iv_down);
        iv_up = findViewById(R.id.iv_up);
        iv_down_social = findViewById(R.id.iv_down_social);
        iv_up_social = findViewById(R.id.iv_up_social);
        iv_down_appearance = findViewById(R.id.iv_down_appearance);
        iv_up_appearance = findViewById(R.id.iv_up_appearance);
        iv_down_lifestyle = findViewById(R.id.iv_down_lifestyle);
        iv_up_lifestyle = findViewById(R.id.iv_up_lifestyle);
        iv_down_interest = findViewById(R.id.iv_down_interest);
        iv_up_interest = findViewById(R.id.iv_up_interest);
        relative_basic = findViewById(R.id.relative_basics);
        relative_stat = findViewById(R.id.relative_stat);
        relative_appearanc = findViewById(R.id.relative_appearanc);
        relative_life = findViewById(R.id.relative_life);
        relative_int = findViewById(R.id.relative_int);
        btn_search = findViewById(R.id.btn_search);
        iv_well_signup = findViewById(R.id.iv_well_signup);
        iv_well_looking = findViewById(R.id.iv_well_looking);
        iv_well_location = findViewById(R.id.iv_well_location);
        iv_well_maxDistance = findViewById(R.id.iv_well_maxDistance);
        iv_well_name = findViewById(R.id.iv_well_name);
        iv_well_education = findViewById(R.id.iv_well_education);
        iv_well_occupation = findViewById(R.id.iv_well_occupation);
        iv_well_activity = findViewById(R.id.iv_well_activity);
        iv_well_status = findViewById(R.id.iv_well_status);
        iv_well_kids = findViewById(R.id.iv_well_kids);
        iv_well_want_kids = findViewById(R.id.iv_well_want_kids);
        iv_well_living = findViewById(R.id.iv_well_living);
        iv_well_relocate = findViewById(R.id.iv_well_relocate);
        iv_well_Appearance = findViewById(R.id.iv_well_Appearance);
        iv_well_body_type = findViewById(R.id.iv_well_body_type);
        iv_well_Complexion = findViewById(R.id.iv_well_Complexion);
        iv_well_health_status = findViewById(R.id.iv_well_health_status);
        iv_well_hair_color = findViewById(R.id.iv_well_hair_color);
        iv_well_hair_length = findViewById(R.id.iv_well_hair_length);
        iv_well_type = findViewById(R.id.iv_well_type);
        iv_well_eye_color = findViewById(R.id.iv_well_eye_color);
        iv_well_eye_wear = findViewById(R.id.iv_well_eye_wear);
        iv_well_facial_hair = findViewById(R.id.iv_well_facial_hair);
        iv_well_weapon = findViewById(R.id.iv_well_weapon);
        iv_well_style = findViewById(R.id.iv_well_style);
        iv_well_Smoking = findViewById(R.id.iv_well_Smoking);
        iv_well_eating_habits = findViewById(R.id.iv_well_eating_habits);
        iv_well_exercise = findViewById(R.id.iv_well_exercise);
        iv_well_Sleep_Habits = findViewById(R.id.iv_well_Sleep_Habits);
        iv_well_pets = findViewById(R.id.iv_well_pets);
        iv_well_family_values = findViewById(R.id.iv_well_family_values);
        iv_well_Polygamy = findViewById(R.id.iv_well_Polygamy);
        iv_well_Personality = findViewById(R.id.iv_well_Personality);
        iv_well_Language = findViewById(R.id.iv_well_Language);
        iv_well_Hobbies = findViewById(R.id.iv_well_Hobbies);
        iv_well_Cuisine = findViewById(R.id.iv_well_Cuisine);
        iv_well_Music = findViewById(R.id.iv_well_Music);
        iv_well_Movies = findViewById(R.id.iv_well_Movies);
        iv_well_Sports = findViewById(R.id.iv_well_Sports);
        iv_down.setOnClickListener(this);
        iv_up.setOnClickListener(this);
        iv_down_social.setOnClickListener(this);
        iv_up_social.setOnClickListener(this);
        iv_down_appearance.setOnClickListener(this);
        iv_up_appearance.setOnClickListener(this);
        iv_down_lifestyle.setOnClickListener(this);
        iv_up_lifestyle.setOnClickListener(this);
        iv_down_interest.setOnClickListener(this);
        iv_up_interest.setOnClickListener(this);
        et_Hobbies.setOnClickListener(this);
        et_Cuisine.setOnClickListener(this);
        et_Music.setOnClickListener(this);
        et_Movies.setOnClickListener(this);
        et_Sports.setOnClickListener(this);
        btn_clear.setOnClickListener(this);
        et_new_email.setOnClickListener(this);
        et_education.setOnClickListener(this);
        et_occupation.setOnClickListener(this);
        et_sector.setOnClickListener(this);
        et_status.setOnClickListener(this);
        et_kids.setOnClickListener(this);
        et_want_kids.setOnClickListener(this);
        et_living.setOnClickListener(this);
        et_relocate.setOnClickListener(this);
        et_Personality.setOnClickListener(this);
        et_eye_language.setOnClickListener(this);
        et_Appearance.setOnClickListener(this);
        et_body_type.setOnClickListener(this);
        et_Complexion.setOnClickListener(this);
        et_health_status.setOnClickListener(this);
        et_hair_color.setOnClickListener(this);
        et_hair_length.setOnClickListener(this);
        et_type.setOnClickListener(this);
        et_eye_color.setOnClickListener(this);
        et_eye_wear.setOnClickListener(this);
        et_facial_hair.setOnClickListener(this);
        et_weapon.setOnClickListener(this);
        et_style.setOnClickListener(this);
        et_Smoking.setOnClickListener(this);
        et_eating_habits.setOnClickListener(this);
        et_exercise.setOnClickListener(this);
        et_Sleep_Habits.setOnClickListener(this);
        et_pets.setOnClickListener(this);
        et_family_values.setOnClickListener(this);
        et_Polygamy.setOnClickListener(this);
        btn_search.setOnClickListener(this);
        tv_advance_search.setOnClickListener(this);
        et_location.setAdapter(new PlaceAutocompleteAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, SearchActivity.this));
        //  et_origin.setAdapter(new PlaceAutocompleteAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, SearchActivity.this));
        if (Helper.getUserData(SearchActivity.this).getGender().equalsIgnoreCase("male"))
            if (Helper.getUserData(SearchActivity.this).getSubscriptionStatus().equals("0")) {
                et_maxDistance.setEnabled(false);
                et_education.setEnabled(false);
                et_sector.setEnabled(false);
                et_status.setEnabled(false);
                et_kids.setEnabled(false);
                et_want_kids.setEnabled(false);
                et_living.setEnabled(false);
                et_relocate.setEnabled(false);
                et_Appearance.setEnabled(false);
                et_body_type.setEnabled(false);
                et_Complexion.setEnabled(false);
                et_health_status.setEnabled(false);
                et_hair_color.setEnabled(false);
                et_hair_length.setEnabled(false);
                et_type.setEnabled(false);
                et_eye_color.setEnabled(false);
                et_eye_wear.setEnabled(false);
                et_facial_hair.setEnabled(false);
                et_weapon.setEnabled(false);
                et_style.setEnabled(false);
                et_Smoking.setEnabled(false);
                et_eating_habits.setEnabled(false);
                et_exercise.setEnabled(false);
                et_Sleep_Habits.setEnabled(false);
                et_pets.setEnabled(false);
                et_family_values.setEnabled(false);
                et_Polygamy.setEnabled(false);
                et_Personality.setEnabled(false);
                et_eye_language.setEnabled(false);
                et_Hobbies.setEnabled(false);
                et_Cuisine.setEnabled(false);
                et_Movies.setEnabled(false);
                et_Music.setEnabled(false);
                et_Sports.setEnabled(false);
                heightSeekbar.setEnabled(false);
                et_maxDistance.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_education.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_sector.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_status.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_kids.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_want_kids.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_living.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_relocate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_Appearance.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_body_type.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_Complexion.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_health_status.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_hair_color.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_hair_length.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_type.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_eye_color.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_eye_wear.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_facial_hair.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_weapon.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_style.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_Smoking.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_eating_habits.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_exercise.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_Sleep_Habits.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_pets.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_family_values.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_Polygamy.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_Personality.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_eye_language.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_Hobbies.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_Cuisine.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_Movies.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_Music.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                et_Sports.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
            }
        getData();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        Utils.multiple = false;
        names.clear();
        switch (v.getId()) {
            case R.id.iv_down:
                relative_basic.setVisibility(relative_basic.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
                iv_down.setImageResource(relative_basic.getVisibility() == View.GONE ? R.drawable.down : R.drawable.up);
                break;
            case R.id.iv_down_social:
                relative_stat.setVisibility(relative_stat.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
                iv_down_social.setImageResource(relative_stat.getVisibility() == View.GONE ? R.drawable.down : R.drawable.up);
                break;
            case R.id.iv_down_appearance:
                relative_appearanc.setVisibility(relative_appearanc.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
                iv_down_appearance.setImageResource(relative_appearanc.getVisibility() == View.GONE ? R.drawable.down : R.drawable.up);
                break;
            case R.id.iv_down_lifestyle:
                relative_life.setVisibility(relative_life.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
                iv_down_lifestyle.setImageResource(relative_life.getVisibility() == View.GONE ? R.drawable.down : R.drawable.up);
                break;
            case R.id.iv_down_interest:
                relative_int.setVisibility(relative_int.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
                iv_down_interest.setImageResource(relative_int.getVisibility() == View.GONE ? R.drawable.down : R.drawable.up);
                break;
            case R.id.btn_clear:
                deleteFilter();
                break;
            case R.id.btn_search:
                searchFilter();
                break;
            case R.id.et_new_email:
                Infodata.Data infodata = new Infodata.Data();
                infodata.name = getResources().getString(R.string.Best);
                names.add(infodata);
                infodata = new Infodata.Data();
                infodata.name = getResources().getString(R.string.Newest);
                names.add(infodata);
                infodata = new Infodata.Data();
                infodata.name = getResources().getString(R.string.Most);
                names.add(infodata);
                showHeightDialog(getResources().getString(R.string.Sort_By), names, et_new_email, iv_well_signup);
                break;
            case R.id.et_looking:
                show_Dialog(getResources().getString(R.string.looking_for), Helper.getinfodata(SearchActivity.this).basic.lookingFor, et_looking, iv_well_looking);
                break;
            case R.id.et_location:
                et_location.setAdapter(new PlaceAutocompleteAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, this));
                break;
            case R.id.show_pass_btn:
                if (ContextCompat.checkSelfPermission(SearchActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(SearchActivity.this,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    GPSStatus();
                } else {
                    GetCurrentlocation();
                }
                break;
            case R.id.et_maxDistance:
                inflatedistance();
                showHeightDialog(getResources().getString(R.string.max_distance), names, et_maxDistance, iv_well_maxDistance);
                break;
            case R.id.et_Smoking:
                show_Dialog(getResources().getString(R.string.smoking), Helper.getinfodata(SearchActivity.this).lifestyle.smoking, et_Smoking, iv_well_Smoking);
                break;
            case R.id.et_eating_habits:
                show_Dialog(getResources().getString(R.string.eating_habits), Helper.getinfodata(SearchActivity.this).lifestyle.eatingHabits, et_eating_habits, iv_well_eating_habits);
                break;
            case R.id.et_exercise:
                show_Dialog(getResources().getString(R.string.execise_habits), Helper.getinfodata(SearchActivity.this).lifestyle.exerciseHabits, et_exercise, iv_well_exercise);
                break;

            case R.id.et_Sleep_Habits:
                show_Dialog(getResources().getString(R.string.sleep_habits), Helper.getinfodata(SearchActivity.this).lifestyle.sleepHabbits, et_Sleep_Habits, iv_well_Sleep_Habits);
                break;
            case R.id.et_pets:
                show_Dialog(getResources().getString(R.string.pets), Helper.getinfodata(SearchActivity.this).lifestyle.pets, et_pets, iv_well_pets);
                break;
            case R.id.et_family_values:
                show_Dialog(getResources().getString(R.string.family_values), Helper.getinfodata(SearchActivity.this).lifestyle.familyValue, et_family_values, iv_well_family_values);
                break;
            case R.id.et_Polygamy:
                show_Dialog(getResources().getString(R.string.polygamy_opinion), Helper.getinfodata(SearchActivity.this).lifestyle.polygamyOpinion, et_Polygamy, iv_well_Polygamy);
                break;
            case R.id.et_Appearance:
                show_Dialog(getResources().getString(R.string.appearance), Helper.getinfodata(SearchActivity.this).appearance.appearance, et_Appearance, iv_well_Appearance);
                break;
            case R.id.et_body_type:
                show_Dialog(getResources().getString(R.string.body_type), Helper.getinfodata(SearchActivity.this).appearance.bodyType, et_body_type, iv_well_body_type);
                break;
            case R.id.et_Complexion:
                show_Dialog(getResources().getString(R.string.complexion), Helper.getinfodata(SearchActivity.this).appearance.complexion, et_Complexion, iv_well_Complexion);
                break;

            case R.id.et_health_status:
                show_Dialog(getResources().getString(R.string.health_status), Helper.getinfodata(SearchActivity.this).appearance.healthStatus, et_health_status, iv_well_health_status);
                break;
            case R.id.et_hair_color:
                show_Dialog(getResources().getString(R.string.hair_color), Helper.getinfodata(SearchActivity.this).appearance.hairColor, et_hair_color, iv_well_hair_color);
                break;
            case R.id.et_hair_length:
                show_Dialog(getResources().getString(R.string.hair_length), Helper.getinfodata(SearchActivity.this).appearance.hairLength, et_hair_length, iv_well_hair_length);
                break;
            case R.id.et_type:
                show_Dialog(getResources().getString(R.string.hair_type), Helper.getinfodata(SearchActivity.this).appearance.hairType, et_type, iv_well_type);
                break;
            case R.id.et_eye_color:
                show_Dialog(getResources().getString(R.string.eye_color), Helper.getinfodata(SearchActivity.this).appearance.eyeColor, et_eye_color, iv_well_eye_color);
                break;
            case R.id.et_eye_wear:
                show_Dialog(getResources().getString(R.string.eye_wear), Helper.getinfodata(SearchActivity.this).appearance.eyeWear, et_eye_wear, iv_well_eye_wear);
                break;
            case R.id.et_facial_hair:
                show_Dialog(getResources().getString(R.string.facial_hair), Helper.getinfodata(SearchActivity.this).appearance.facials, et_facial_hair, iv_well_facial_hair);
                break;
            case R.id.et_weapon:
                show_Dialog(getResources().getString(R.string.secret_weapon), Helper.getinfodata(SearchActivity.this).appearance.secretWeapon, et_weapon, iv_well_weapon);
                break;
            case R.id.et_style:
                show_Dialog(getResources().getString(R.string.style), Helper.getinfodata(SearchActivity.this).appearance.style, et_style, iv_well_style);
                break;
            case R.id.et_Hobbies:
                Utils.multiple = true;
                show_Dialog(getResources().getString(R.string.hobbies), Helper.getinfodata(SearchActivity.this).interests.hobbies, et_Hobbies, iv_well_Hobbies);
                break;
            case R.id.et_Cuisine:
                Utils.multiple = true;
                show_Dialog(getResources().getString(R.string.hobbies), Helper.getinfodata(SearchActivity.this).interests.cuisines, et_Cuisine, iv_well_Cuisine);
                break;
            case R.id.et_Music:
                Utils.multiple = true;
                show_Dialog(getResources().getString(R.string.music), Helper.getinfodata(SearchActivity.this).interests.musics, et_Music, iv_well_Music);

                break;
            case R.id.et_Movies:
                Utils.multiple = true;
                show_Dialog(getResources().getString(R.string.movies), Helper.getinfodata(SearchActivity.this).interests.movies, et_Movies, iv_well_Movies);
                break;
            case R.id.et_Sports:
                Utils.multiple = true;
                show_Dialog(getResources().getString(R.string.sports), Helper.getinfodata(SearchActivity.this).interests.sports, et_Sports, iv_well_Sports);
                break;
            case R.id.et_education:
                show_Dialog(getResources().getString(R.string.education), Helper.getinfodata(SearchActivity.this).socialStatus.educations, et_education, iv_well_education);
                break;
            case R.id.et_occupation:
                show_Dialog(getResources().getString(R.string.occupation), Helper.getinfodata(SearchActivity.this).socialStatus.occupation, et_occupation, iv_well_occupation);
                break;
            case R.id.et_sector:
                show_Dialog(getResources().getString(R.string.sector), Helper.getinfodata(SearchActivity.this).socialStatus.jobSector, et_sector, iv_well_activity);
                break;
            case R.id.et_status:
                show_Dialog(getResources().getString(R.string.status), Helper.getinfodata(SearchActivity.this).socialStatus.relStatus, et_status, iv_well_status);
                break;
            case R.id.et_kids:
                show_Dialog(getResources().getString(R.string.kids), Helper.getinfodata(SearchActivity.this).socialStatus.kidsList, et_kids, iv_well_location);
                break;
            case R.id.et_want_kids:
                show_Dialog(getResources().getString(R.string.want_kids), Helper.getinfodata(SearchActivity.this).socialStatus.wantKidsList, et_want_kids, iv_well_want_kids);
                break;
            case R.id.et_living:
                show_Dialog(getResources().getString(R.string.living_situation), Helper.getinfodata(SearchActivity.this).socialStatus.wantLivingList, et_living, iv_well_living);
                break;
            case R.id.et_relocate:
                show_Dialog(getResources().getString(R.string.sports), Helper.getinfodata(SearchActivity.this).socialStatus.relocateList, et_relocate, iv_well_Sports);
                break;
            case R.id.et_Personality:
                Utils.multiple = true;
                show_Dialog(getResources().getString(R.string.personality), Helper.getinfodata(SearchActivity.this).lifestyle.personality, et_Personality, iv_well_Personality);
                break;
            case R.id.et_eye_language:
                Utils.multiple = true;
                show_Dialog(getResources().getString(R.string.languages), Helper.getinfodata(SearchActivity.this).lifestyle.languages, et_eye_language, iv_well_Language);
                break;
            case R.id.tv_advance_search:
                PairmePlusActivity plusActivity = new PairmePlusActivity();
                plusActivity.show(getSupportFragmentManager(), PairmePlusActivity.TAG);
                break;
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void GetCurrentlocation() {
        locationUtils = new LocationUtils(SearchActivity.this, this, false);
        if (ActivityCompat.checkSelfPermission(SearchActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(SearchActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            getLocationPermission();
            return;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void getLocationPermission() {
        requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                123);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void GPSStatus() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!GpsStatus) {
            Toast.makeText(SearchActivity.this, "On Location in High Accuracy", Toast.LENGTH_SHORT).show();
            startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 2);
        } else {
            GetCurrentlocation();
        }
    }

    private void inflatedistance() {
        Infodata.Data infodata = new Infodata.Data();
        infodata.name = "Anywhere";
        names.add(infodata);
        infodata = new Infodata.Data();
        infodata.name = "10 km";
        names.add(infodata);
        infodata = new Infodata.Data();
        infodata.name = "25 km";
        names.add(infodata);
        infodata = new Infodata.Data();
        infodata.name = "50 km";
        names.add(infodata);
        infodata = new Infodata.Data();
        infodata.name = "100 km";
        names.add(infodata);
        infodata = new Infodata.Data();
        infodata.name = "250 km";
        names.add(infodata);
        infodata = new Infodata.Data();
        infodata.name = "500 km";
        names.add(infodata);
    }


    private void show_Dialog(final String header, final List<Infodata.Data> data, final TextView editText, final ImageView iv) {
        dialog = new Dialog(this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_listview);
        TextView tv_header = dialog.findViewById(R.id.tv_header);
        tv_header.setText(header);
        final TextView tv_count = dialog.findViewById(R.id.tv_count);
        Button btndialog = dialog.findViewById(R.id.btndialog);
        final Button btndok = dialog.findViewById(R.id.btndok);

        progressBar_cyclic = dialog.findViewById(R.id.progressBar_cyclic);
        progressBar_cyclic.setVisibility(View.GONE);
        btndialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                editText.setText(null);
            }
        });
        btndok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        recyclerView = dialog.findViewById(R.id.listview);
        UpdatinfoAdapter interestsAdapter = new UpdatinfoAdapter(SearchActivity.this, data, new UpdatinfoAdapter.OnStatusListner() {
            @Override
            public void OnStatusClick(String p, String trim) {
                SearchActivity.this.named.put(header, trim);
                setData(editText, p, iv);
                dialog.dismiss();
            }

            @Override
            public void OnStatusmultClick(List<String> p, List<String> names) {
                tv_count.setVisibility(View.VISIBLE);
                btndok.setVisibility(View.VISIBLE);
                tv_count.setText("(" + p.size() + (header.equalsIgnoreCase("language") ? "/5" : "/3)"));
                if (p.size() > 0)
                    setData(editText, getStringData(p), iv);
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(SearchActivity.this));
        recyclerView.setAdapter(interestsAdapter);
        dialog.show();
    }

    private void showHeightDialog(final String header, final List<Infodata.Data> datas, final EditText editText, final ImageView iv_well) {
        dialog = new Dialog(SearchActivity.this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_listview);
        TextView tv_header = dialog.findViewById(R.id.tv_header);
        tv_header.setText(header);
        Button btndialog = dialog.findViewById(R.id.btndialog);
        progressBar_cyclic = dialog.findViewById(R.id.progressBar_cyclic);
        progressBar_cyclic.setVisibility(View.GONE);
        btndialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        recyclerView = dialog.findViewById(R.id.listview);
        UpdatHeightAdapter interestsAdapter = new UpdatHeightAdapter(datas, new UpdatHeightAdapter.OnStatusListner() {
            @Override
            public void OnStatusClick(String p, String name) {
                named.put(header, p);
                updated = true;
                editText.setText(p);
                editText.setBackgroundResource(R.drawable.green_background);
                iv_well.setVisibility(View.VISIBLE);
                dialog.dismiss();
            }


            @Override
            public void OnStatusmultClick(List<String> p, List<String> name) {

            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(SearchActivity.this));
        recyclerView.setAdapter(interestsAdapter);
        dialog.show();
    }


    public void getData() {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<FilterPojo> call = service.getFilter("Bearer " + Helper.getToken(this));
        call.enqueue(new Callback<FilterPojo>() {
            @Override
            public void onResponse(Call<FilterPojo> call, final Response<FilterPojo> response) {
                if (response.body().success == 1) {
                    try {
                        if (response.body().data.ageMax != null && response.body().data.ageMin != null) {
                            seekBar.getThumb(0).setValue(Integer.parseInt(response.body().data.ageMin));
                            seekBar.getThumb(1).setValue(Integer.parseInt(response.body().data.ageMax));
                            age.setText(response.body().data.ageMin + "-" + response.body().data.ageMax);
                        } else {
                            preMax = Float.parseFloat(response.body().data.ageMax);
                            preMin = Float.parseFloat(response.body().data.ageMin);
                        }
                        if (response.body().data.height != null) {
                            heightSeekbar.getThumb(0).setValue(Integer.parseInt(response.body().data.height));
                            heightSeekbar.getThumb(1).setValue(120);
                            height.setText(120 + "-" + response.body().data.height);
                        } else {
                            minheight = 120;
                            maxheight = Float.parseFloat(response.body().data.height);
                        }

                        if (response.body().data.searchSort != null)
                            et_new_email.setText(response.body().data.searchSort);
                        named.put(getResources().getString(R.string.new_email), response.body().data.searchSort);

                        if (response.body().data.lookingFor != null)
                            et_looking.setText(Utils.getArabic(SearchActivity.this, response.body().data.lookingFor));
                        named.put(getResources().getString(R.string.looking_for), response.body().data.lookingFor);

                        if (response.body().data.livesIn != null)
                            et_location.setText(response.body().data.livesIn);
                        named.put(getResources().getString(R.string.location), response.body().data.livesIn);

                        if (response.body().data.originallyFrom != null)
                            spinner_Location.setText(response.body().data.originallyFrom);
                        named.put(getResources().getString(R.string.nationality), response.body().data.originallyFrom);

                        if (response.body().data.distance != null)
                            et_maxDistance.setText(response.body().data.distance);
                        named.put(getResources().getString(R.string.max_distance), response.body().data.distance);

                        if (response.body().data.name != null)
                            et_name.setText(response.body().data.name);
                        named.put(getResources().getString(R.string.name), response.body().data.name);

                        if (response.body().data.education != null)
                            et_education.setText(Utils.getArabic(SearchActivity.this, response.body().data.education));
                        named.put(getResources().getString(R.string.name), response.body().data.education);

                        if (response.body().data.occupation != null)
                            et_occupation.setText(Utils.getArabic(SearchActivity.this, response.body().data.occupation));
                        named.put(getResources().getString(R.string.name), response.body().data.occupation);

                        if (response.body().data.sector != null)
                            et_sector.setText(Utils.getArabic(SearchActivity.this, response.body().data.sector));
                        named.put(getResources().getString(R.string.name), response.body().data.sector);

                        if (response.body().data.status != null)
                            et_status.setText(Utils.getArabic(SearchActivity.this, response.body().data.status));
                        named.put(getResources().getString(R.string.name), response.body().data.status);

                        if (response.body().data.haveKids != null)
                            et_kids.setText(Utils.getArabic(SearchActivity.this, response.body().data.haveKids));
                        named.put(getResources().getString(R.string.name), response.body().data.haveKids);

                        if (response.body().data.wantKids != null)
                            et_want_kids.setText(Utils.getArabic(SearchActivity.this, response.body().data.wantKids));
                        named.put(getResources().getString(R.string.name), response.body().data.wantKids);

                        if (response.body().data.livingSituation != null)
                            et_living.setText(Utils.getArabic(SearchActivity.this, response.body().data.livingSituation));
                        named.put(getResources().getString(R.string.name), response.body().data.livingSituation);

                        if (response.body().data.searchRelocate != null)
                            et_relocate.setText(Utils.getArabic(SearchActivity.this, response.body().data.searchRelocate));
                        named.put(getResources().getString(R.string.name), response.body().data.searchRelocate);

                        if (response.body().data.searchAppearance != null)
                            et_Appearance.setText(Utils.getArabic(SearchActivity.this, response.body().data.searchAppearance));
                        named.put(getResources().getString(R.string.name), response.body().data.searchAppearance);

                        if (response.body().data.searchBodyType != null)
                            et_body_type.setText(Utils.getArabic(SearchActivity.this, response.body().data.searchBodyType));
                        named.put(getResources().getString(R.string.name), response.body().data.searchBodyType);

                        if (response.body().data.searchComplexion != null)
                            et_Complexion.setText(Utils.getArabic(SearchActivity.this, response.body().data.searchComplexion));
                        named.put(getResources().getString(R.string.name), response.body().data.searchComplexion);

                        if (response.body().data.healthStatus != null)
                            et_health_status.setText(Utils.getArabic(SearchActivity.this, response.body().data.healthStatus));
                        named.put(getResources().getString(R.string.name), response.body().data.healthStatus);

                        if (response.body().data.searchHairColor != null)
                            et_hair_color.setText(Utils.getArabic(SearchActivity.this, response.body().data.searchHairColor));
                        named.put(getResources().getString(R.string.name), response.body().data.searchHairColor);

                        if (response.body().data.searchHairLength != null)
                            et_hair_length.setText(Utils.getArabic(SearchActivity.this, response.body().data.searchHairLength));
                        named.put(getResources().getString(R.string.name), response.body().data.searchHairLength);

                        if (response.body().data.hairType != null)
                            et_type.setText(Utils.getArabic(SearchActivity.this, response.body().data.hairType));
                        named.put(getResources().getString(R.string.name), response.body().data.hairType);

                        if (response.body().data.searchEyeColor != null)
                            et_eye_color.setText(Utils.getArabic(SearchActivity.this, response.body().data.searchEyeColor));
                        named.put(getResources().getString(R.string.name), response.body().data.searchEyeColor);

                        if (response.body().data.searchEyeWear != null)
                            et_eye_wear.setText(Utils.getArabic(SearchActivity.this, response.body().data.searchEyeWear));
                        named.put(getResources().getString(R.string.name), response.body().data.searchEyeWear);

                        if (response.body().data.facialHair != null)
                            et_facial_hair.setText(Utils.getArabic(SearchActivity.this, response.body().data.facialHair));
                        named.put(getResources().getString(R.string.name), response.body().data.facialHair);

                        if (response.body().data.searchSecretWeapon != null)
                            et_weapon.setText(Utils.getArabic(SearchActivity.this, response.body().data.searchSecretWeapon));
                        named.put(getResources().getString(R.string.name), response.body().data.searchSecretWeapon);

                        if (response.body().data.searchStyle != null)
                            et_style.setText(Utils.getArabic(SearchActivity.this, response.body().data.searchStyle));
                        named.put(getResources().getString(R.string.name), response.body().data.searchStyle);

                        if (response.body().data.smoking != null)
                            et_Smoking.setText(Utils.getArabic(SearchActivity.this, response.body().data.smoking));
                        named.put(getResources().getString(R.string.name), response.body().data.smoking);

                        if (response.body().data.eatingHabits != null)
                            et_eating_habits.setText(Utils.getArabic(SearchActivity.this, response.body().data.eatingHabits));
                        named.put(getResources().getString(R.string.name), response.body().data.eatingHabits);

                        if (response.body().data.exerciseHabits != null)
                            et_exercise.setText(Utils.getArabic(SearchActivity.this, response.body().data.exerciseHabits));
                        named.put(getResources().getString(R.string.name), response.body().data.exerciseHabits);

                        if (response.body().data.sleepHabits != null)
                            et_Sleep_Habits.setText(Utils.getArabic(SearchActivity.this, response.body().data.sleepHabits));
                        named.put(getResources().getString(R.string.name), response.body().data.sleepHabits);

                        if (response.body().data.pets != null)
                            et_pets.setText(Utils.getArabic(SearchActivity.this, response.body().data.pets));
                        named.put(getResources().getString(R.string.name), response.body().data.pets);

                        if (response.body().data.familyValues != null)
                            et_family_values.setText(Utils.getArabic(SearchActivity.this, response.body().data.familyValues));
                        named.put(getResources().getString(R.string.name), response.body().data.familyValues);

                        if (response.body().data.polygamy != null)
                            et_Polygamy.setText(Utils.getArabic(SearchActivity.this, response.body().data.polygamy));
                        named.put(getResources().getString(R.string.name), response.body().data.polygamy);

                        if (response.body().data.personality != null)
                            et_Personality.setText(Utils.getArabic(SearchActivity.this, response.body().data.personality));
                        named.put(getResources().getString(R.string.name), response.body().data.personality);

                        if (response.body().data.language != null)
                            et_eye_language.setText(Utils.getArabic(SearchActivity.this, response.body().data.language));
                        named.put(getResources().getString(R.string.name), response.body().data.language);

                        if (response.body().data.hobbies != null)
                            et_Hobbies.setText(Utils.getArabic(SearchActivity.this, response.body().data.hobbies));
                        named.put(getResources().getString(R.string.name), response.body().data.hobbies);

                        if (response.body().data.cuisine != null)
                            et_Cuisine.setText(Utils.getArabic(SearchActivity.this, response.body().data.cuisine));
                        named.put(getResources().getString(R.string.name), response.body().data.cuisine);

                        if (response.body().data.music != null)
                            et_Music.setText(Utils.getArabic(SearchActivity.this, response.body().data.music));
                        named.put(getResources().getString(R.string.name), response.body().data.music);

                        if (response.body().data.movies != null)
                            et_Movies.setText(Utils.getArabic(SearchActivity.this, response.body().data.movies));
                        named.put(getResources().getString(R.string.name), response.body().data.movies);

                        if (response.body().data.sports != null)
                            et_Sports.setText(Utils.getArabic(SearchActivity.this, response.body().data.sports));
                        named.put(getResources().getString(R.string.name), response.body().data.sports);

                        if (response.body().data.online != null)
                            switch_online.setChecked(response.body().data.online.equalsIgnoreCase("1"));
                        if (response.body().data.filterPhotos != null)
                            switch_photos.setChecked(response.body().data.filterPhotos.equalsIgnoreCase("1"));
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                } else {

                    Log.i(">>>>", response.raw().toString());
                }

            }

            @Override
            public void onFailure(Call<FilterPojo> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }

    public void deleteFilter() {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<JsonObject> call = service.deleteFilter("Bearer " + Helper.getToken(this));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    et_new_email.setText(null);
                    et_looking.setText(null);
                    et_location.setText(null);
                    spinner_Location.setText(null);
                    et_maxDistance.setText(null);
                    et_name.setText(null);
                    et_education.setText(null);
                    et_occupation.setText(null);
                    et_sector.setText(null);
                    et_status.setText(null);
                    et_kids.setText(null);
                    et_want_kids.setText(null);
                    et_living.setText(null);
                    et_relocate.setText(null);
                    et_Appearance.setText(null);
                    et_body_type.setText(null);
                    et_Complexion.setText(null);
                    et_health_status.setText(null);
                    et_hair_color.setText(null);
                    et_hair_length.setText(null);
                    et_type.setText(null);
                    et_eye_color.setText(null);
                    et_eye_wear.setText(null);
                    et_facial_hair.setText(null);
                    et_weapon.setText(null);
                    et_style.setText(null);
                    et_Smoking.setText(null);
                    et_eating_habits.setText(null);
                    et_exercise.setText(null);
                    et_Sleep_Habits.setText(null);
                    et_pets.setText(null);
                    et_family_values.setText(null);
                    et_Polygamy.setText(null);
                    et_Personality.setText(null);
                    et_eye_language.setText(null);
                    et_Hobbies.setText(null);
                    et_Cuisine.setText(null);
                    et_Music.setText(null);
                    et_Movies.setText(null);
                    et_Sports.setText(null);
                    switch_online.setChecked(false);
                    switch_photos.setChecked(false);
                    finish();
                } else {
                    Log.i(">>>>", response.raw().toString());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void searchFilter() {
        String sort = null;
        if (et_new_email.getText().toString().equalsIgnoreCase("Most active") || et_new_email.getText().toString().equalsIgnoreCase("أنشط المشتركين"))
            sort = "Most active";
        else if (et_new_email.getText().toString().equalsIgnoreCase("Best matches") || et_new_email.getText().toString().equalsIgnoreCase("أنسب المشتركين"))
            sort = "Best matches";
        else if (et_new_email.getText().toString().equalsIgnoreCase("Newest Users") || et_new_email.getText().toString().equalsIgnoreCase("أحدث المشتركين"))
            sort = "Newest Users";
        Helper.showLoader(SearchActivity.this, "Please Wait...");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<JsonObject> call = service.searchfilter("Bearer " + Helper.getToken(this),
                switch_online.isChecked() ? "1" : "0",
                sort,
                switch_photos.isChecked() ? "1" : "0",
                String.valueOf(preMin),
                String.valueOf(preMax),
                named.get(getResources().getString(R.string.looking_for)),
                named.get(getResources().getString(R.string.education)),
                named.get(getResources().getString(R.string.occupation)),
                named.get(getResources().getString(R.string.sector)),
                named.get(getResources().getString(R.string.status)),
                named.get(getResources().getString(R.string.status)),
                named.get(getResources().getString(R.string.kids)),
                named.get(getResources().getString(R.string.living_situation)),
                named.get(getResources().getString(R.string.want_kids)),
                named.get(getResources().getString(R.string.location)),
                named.get(getResources().getString(R.string.willing_to_relocate)),
                named.get(getResources().getString(R.string.appearance)),
                named.get(getResources().getString(R.string.body_type)),
                named.get(getResources().getString(R.string.health_status)),
                named.get(getResources().getString(R.string.hair_color)),
                named.get(getResources().getString(R.string.hair_length)),
                named.get(getResources().getString(R.string.eye_color)),
                named.get(getResources().getString(R.string.eye_wear)),
                named.get(getResources().getString(R.string.makeup)),
                named.get(getResources().getString(R.string.secret_weapon)),
                named.get(getResources().getString(R.string.style)),
                named.get(getResources().getString(R.string.complexion)),
                named.get(getResources().getString(R.string.origin)),
                named.get(et_name.getText().toString()),
                String.valueOf(maxheight),
                named.get(getResources().getString(R.string.max_distance)),
                named.get(getResources().getString(R.string.health_status)),
                named.get(getResources().getString(R.string.hair_type)),
                named.get(getResources().getString(R.string.facial_hair)),
                named.get(getResources().getString(R.string.smoking)),
                named.get(getResources().getString(R.string.eating_habits)),
                named.get(getResources().getString(R.string.execise_habits)),
                named.get(getResources().getString(R.string.sleep_habits)),
                named.get(getResources().getString(R.string.pets)),
                named.get(getResources().getString(R.string.family_values)),
                named.get(getResources().getString(R.string.polygamy_opinion)),
                named.get(getResources().getString(R.string.personality)),
                named.get(getResources().getString(R.string.languages)),
                named.get(getResources().getString(R.string.hobbies)),
                named.get(getResources().getString(R.string.cuisine)),
                named.get(getResources().getString(R.string.music)),
                named.get(getResources().getString(R.string.movies)),
                named.get(getResources().getString(R.string.sports)));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Helper.hideLoader();
                if (response.isSuccessful()) {
                    finish();
                } else {
                    Log.i(">>>>", response.raw().toString());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Helper.hideLoader();
                t.printStackTrace();
            }
        });
    }


    private void setData(TextView editText, String s, ImageView iv) {
        editText.setText(s);
        editText.setBackgroundResource(R.drawable.green_background);
        iv.setVisibility(View.VISIBLE);
    }

    private String getStringData(List<String> ids) {
        StringBuilder convertedstring = new StringBuilder();
        for (int i = 0; i < ids.size(); i++) {
            if (i == 0)
                convertedstring = new StringBuilder(ids.get(i));
            else
                convertedstring.append(",").append(ids.get(i));
        }
        return convertedstring.toString();
    }


    @Override
    public void getSize(int size) {

    }

    @Override
    public void locationUpdates(Location location) {
        if (location != null) {
            geocoder = new Geocoder(SearchActivity.this, Locale.ENGLISH);
        }

        try {
            assert location != null;
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            Double[] latlng = new Double[2];
            latlng[0] = location.getLongitude();
            latlng[1] = location.getLatitude();
        } catch (IOException e) {
            Log.e("test", "location problem");
        }
        if (addresses != null) {
            //String address = addresses.get(0).getAddressLine(0);
            String address = addresses.get(0).getLocality() + ", " + addresses.get(0).getAdminArea();
            et_location.setText(address);
        }
    }

}




