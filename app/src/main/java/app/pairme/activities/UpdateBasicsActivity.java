package app.pairme.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.DescriptionPojo.DescriptionPojo;
import app.pairme.ClassesPojo.Infodata;
import app.pairme.ClassesPojo.UserInfoPojo.Data;
import app.pairme.LocationUtils;
import app.pairme.R;
import app.pairme.Utils;
import app.pairme.adapters.PlaceAutocompleteAdapter;
import app.pairme.adapters.UpdatinfoAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.pairme.Utils.getArabic;
import static app.pairme.Utils.isArabic;

public class UpdateBasicsActivity extends AppCompatActivity implements View.OnClickListener, LocationUtils.MyLocation {
    EditText et_age, et_looking_for;
    AutoCompleteTextView et_location;
    TextView tv_save, nationality, et_i_am;
    ImageView iv_age, iv_looking_for, iv_location, iv_origin, show_pass_btn;
    Dialog dialog;
    ProgressBar progressBar_cyclic;
    ArrayList<String> looking_name, looking_id, name;
    boolean updated;
    Data userInfo;
    LocationUtils locationUtils;
    List<Address> addresses;
    Geocoder geocoder;
    RecyclerView recyclerView;
    HashMap<String, String> named = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_basics);
        changeStatusBarColor();
        userInfo = Helper.getUserData(this);
        looking_name = new ArrayList<>();
        looking_id = new ArrayList<>();
        initScreen();

    }

    private void initScreen() {
        et_i_am = findViewById(R.id.et_i_am);
        et_age = findViewById(R.id.et_age);
        nationality = findViewById(R.id.nationality);
        et_looking_for = findViewById(R.id.et_looking_for);
        et_location = findViewById(R.id.et_location);
        // et_origin = findViewById(R.id.et_origin);
        iv_age = findViewById(R.id.iv_age);
        iv_location = findViewById(R.id.iv_location);
        iv_origin = findViewById(R.id.iv_origin);
        iv_looking_for = findViewById(R.id.iv_looking_for);
        show_pass_btn = findViewById(R.id.show_pass_btn);
        tv_save = findViewById(R.id.tv_save);
        et_age.setOnClickListener(this);
        et_looking_for.setOnClickListener(this);
        et_location.setAdapter(new PlaceAutocompleteAdapter(getApplicationContext(), android.R.layout.simple_list_item_1));
        tv_save.setOnClickListener(this);
        show_pass_btn.setOnClickListener(this);
        initData();


    }

    private void initData() {
        if (userInfo.getGender() != null) {
            // et_i_am.setText(userInfo.getGender());
            et_i_am.setText(isArabic ? userInfo.getGender() : Utils.localisation.get(userInfo.getGender()).getAsString());
        }
        if (userInfo.getAge() != null)
            et_age.setText(userInfo.getAge());
        if (userInfo.getLookingFor() != null) {
            et_looking_for.setText(getArabic(this, Helper.getUserData(this).getLookingFor()));
            named.put(getResources().getString(R.string.looking_for), Helper.getUserData(this).getLookingFor());
        }
        if (userInfo.getLivesIn() != null)
            et_location.setText(userInfo.getLivesIn());
        if (userInfo.getOrginallyFrom() != null)
            nationality.setText(getArabic(UpdateBasicsActivity.this, userInfo.getOrginallyFrom().trim()));
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.et_i_am:
                et_i_am.setText(getResources().getString(R.string.man));
                break;
            case R.id.show_pass_btn:
                getlocation();
                break;
            case R.id.et_age:
                et_age.getText().toString().trim();
                et_age.setBackgroundResource(R.drawable.green_background);
                iv_age.setVisibility(View.VISIBLE);
                break;
            case R.id.et_looking_for:
                showDialog(getResources().getString(R.string.looking_for), Helper.getinfodata(UpdateBasicsActivity.this).basic.lookingFor, et_looking_for, iv_looking_for);
                break;
            case R.id.et_location:
                et_location.setAdapter(new PlaceAutocompleteAdapter(getApplicationContext(), android.R.layout.simple_list_item_1));
                et_location.setBackgroundResource(R.drawable.green_background);
                iv_location.setVisibility(View.VISIBLE);
                break;
            case R.id.tv_save:
                updateBasicsApi();
                break;


        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void getlocation() {
        if (ContextCompat.checkSelfPermission(UpdateBasicsActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(UpdateBasicsActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            GPSStatus();
        } else {
            GetCurrentlocation();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void getLocationPermission() {
        requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                123);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void GPSStatus() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!GpsStatus) {
            Toast.makeText(UpdateBasicsActivity.this, "On Location in High Accuracy", Toast.LENGTH_SHORT).show();
            startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 2);
        } else {
            GetCurrentlocation();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            GPSStatus();
        }
    }

    // main method used for get the location of user
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void GetCurrentlocation() {
        locationUtils = new LocationUtils(UpdateBasicsActivity.this, this, false);
        if (ActivityCompat.checkSelfPermission(UpdateBasicsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(UpdateBasicsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            getLocationPermission();
            return;
        }
    }

    private void showDialog(final String header, final List<Infodata.Data> datas, final EditText editText, final ImageView iv_well) {
        Utils.selected = editText.getText().toString().trim();
        dialog = new Dialog(this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_listview);
        TextView tv_header = dialog.findViewById(R.id.tv_header);
        tv_header.setText(header);
        Button btndialog = dialog.findViewById(R.id.btndialog);
        progressBar_cyclic = dialog.findViewById(R.id.progressBar_cyclic);
        progressBar_cyclic.setVisibility(View.GONE);
        btndialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        recyclerView = dialog.findViewById(R.id.listview);
        UpdatinfoAdapter interestsAdapter = new UpdatinfoAdapter(datas, new UpdatinfoAdapter.OnStatusListner() {
            @Override
            public void OnStatusClick(String p, String trim) {
                UpdateBasicsActivity.this.named.put(header, trim);
                updated = true;
                editText.setText(p);
                editText.setBackgroundResource(R.drawable.green_background);
                iv_well.setVisibility(View.VISIBLE);
                dialog.dismiss();
            }

            @Override
            public void OnStatusmultClick(List<String> p, List<String> names) {

            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(interestsAdapter);
        dialog.show();
    }


    private void updateBasicsApi() {
        Helper.showLoader(this, "Please wait..");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<DescriptionPojo> call = service.updateBasics("Bearer " + Helper.getToken(UpdateBasicsActivity.this),
                et_age.getText().toString(), named.get(getResources().getString(R.string.looking_for)), et_location.getText().toString(), nationality.getText().toString());
        call.enqueue(new Callback<DescriptionPojo>() {
            @Override
            public void onResponse(Call<DescriptionPojo> call, Response<DescriptionPojo> response) {
                Helper.hideLoader();
                if (response.isSuccessful()) {
                    Toast.makeText(UpdateBasicsActivity.this, R.string.details_updated, Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<DescriptionPojo> call, Throwable t) {
                Helper.hideLoader();
                Log.d("error", "Failure; " + t);
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (updated)
            openDialog();
        else super.onBackPressed();
    }

    private void openDialog() {
        final Dialog dialog = new Dialog(UpdateBasicsActivity.this);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.show();
        dialog.setCancelable(false);
        Button declineButton = dialog.findViewById(R.id.btn_logout);
        declineButton.setText(getResources().getString(R.string.yes));
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                updateBasicsApi();
            }
        });
        TextView cancel = dialog.findViewById(R.id.tv_cancel);
        cancel.setText(getResources().getString(R.string.no));
        TextView tv_all = dialog.findViewById(R.id.tv_all);
        TextView tv_logout = dialog.findViewById(R.id.tv_logout);
        tv_all.setVisibility(View.GONE);
        tv_logout.setText(getResources().getString(R.string.do_you_Wish_to_save_the_Changes));
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    @Override
    public void locationUpdates(Location location) {
        if (location != null) {
            geocoder = new Geocoder(UpdateBasicsActivity.this, Locale.ENGLISH);
        }
        try {
            assert location != null;
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            Double[] latlng = new Double[2];
            latlng[0] = location.getLongitude();
            latlng[1] = location.getLatitude();
        } catch (IOException e) {
            Log.e("test", "location problem");
        }
        if (addresses != null) {
            String address = addresses.get(0).getLocality() + ", " + addresses.get(0).getAdminArea();
            setData(et_location, iv_location, show_pass_btn, address);
        }

    }

    private void setData(AutoCompleteTextView et, ImageView iv, ImageView btn, String address) {
        et.setText(address);
        iv.setVisibility(View.VISIBLE);
        et.setBackgroundResource(R.drawable.green_background);
        btn.setVisibility(View.GONE);
    }
}
