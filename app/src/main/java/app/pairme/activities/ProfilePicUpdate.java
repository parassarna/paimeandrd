package app.pairme.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.UploadImagePojo.UploadImagePojo;
import app.pairme.R;
import app.pairme.Utils;
import app.pairme.adapters.FileUtils;
import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.pairme.Utils.getImageUri;
import static app.pairme.Utils.getRealPathFromURI;
import static app.pairme.Utils.imageloader;

public class ProfilePicUpdate extends AppCompatActivity implements View.OnClickListener {
    ImageView iv_signup;
    CircleImageView iv_profile;
    ImageView img_choose;
    int RESULT_LOAD_IMAGE = 4;
    Uri uri;
    String imageUrl;
    File compressedImageFile, file;
    boolean isImageAdded = false;
    TextView tv_save;
    boolean updated;
    Dialog dialog;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_pic_update);
        changeStatusBarColor();
        initScreen();
        imageUrl = Helper.getUserData(ProfilePicUpdate.this).getProfileImage();
        if (imageUrl != null && !imageUrl.equalsIgnoreCase("https://pairme.co/img/userProfile/")) {
            if (Helper.getUserData(this).getGender().equalsIgnoreCase("male")) {
                Glide.with(ProfilePicUpdate.this).load("https://pairme.co/img/userProfile/" + imageUrl).placeholder(imageloader(this)).error(R.drawable.place_holder_m).into(iv_profile);
            } else {
                Glide.with(ProfilePicUpdate.this).load("https://pairme.co/img/userProfile/" + imageUrl).placeholder(imageloader(this)).error(R.drawable.place_holder_f).into(iv_profile);
            }
        }
        else iv_profile.setImageResource(Helper.getUserData(this).getGender().equalsIgnoreCase("male")?R.drawable.place_holder_m:R.drawable.place_holder_f);

    }

    private void initScreen() {
        iv_signup = findViewById(R.id.iv_signup);
        iv_profile = findViewById(R.id.iv_profile);
        img_choose = findViewById(R.id.iv_choose_image);
        tv_save = findViewById(R.id.tv_save);

        img_choose.setOnClickListener(this);
        iv_signup.setOnClickListener(this);
        tv_save.setOnClickListener(this);


    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_signup:
                startActivity(new Intent(ProfilePicUpdate.this, InfoProfile.class));
                break;
            case R.id.iv_choose_image:
                isImageAdded = true;
                checkPermission();

                break;
            case R.id.tv_save:
                makeApiCall();
        }
    }


    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(intent, RESULT_LOAD_IMAGE);
    }


    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(ProfilePicUpdate.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) + ContextCompat
                .checkSelfPermission(ProfilePicUpdate.this,
                        Manifest.permission.CAMERA)+ContextCompat
                .checkSelfPermission(ProfilePicUpdate.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (ProfilePicUpdate.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (ProfilePicUpdate.this, Manifest.permission.CAMERA)) {
                Snackbar.make(ProfilePicUpdate.this.findViewById(android.R.id.content),
                        "Please Grant Permissions to upload profile photo",
                        Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, 101);
                            }
                        }).show();
            } else {
                requestPermissions(
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 101);
            }
        } else {
            chooseImage();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (data != null) {
            if (requestCode == RESULT_LOAD_IMAGE) {
                uri = data.getData();
                Helper.setImage(String.valueOf(uri), ProfilePicUpdate.this);
                file = FileUtils.getFile(this, uri);
                try {
                    file = new Compressor(this).compressToFile(file);
                } catch (IOException e) {
                    Log.d("tag", "onActivityResult: "+e.getMessage());
                    e.printStackTrace();
                }
                ((CircleImageView) findViewById(R.id.iv_profile)).setImageURI(uri);

            } else {
            }
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    public void makeApiCall() {
        Helper.showLoader(ProfilePicUpdate.this, "Please wait...");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        MultipartBody.Part part = null;
        if (file != null) {
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            try {
                part = MultipartBody.Part.createFormData("profile_image", URLEncoder.encode(file.getName(), "utf-8"), requestBody);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        Log.d("tag", "makeApiCall: " + "Bearer " + Helper.getToken(ProfilePicUpdate.this));
        Call<UploadImagePojo> call = service.uploadProfileImage(part, "Bearer " + Helper.getToken(ProfilePicUpdate.this));
        call.enqueue(new Callback<UploadImagePojo>() {
            @Override
            public void onResponse(Call<UploadImagePojo> call, Response<UploadImagePojo> response) {
                Helper.hideLoader();
                if (response.isSuccessful()) {
                    Toast.makeText(ProfilePicUpdate.this, R.string.details_updated, Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Log.i("Upload else", response.raw().toString());
                }
            }

            @Override
            public void onFailure(Call<UploadImagePojo> call, Throwable t) {
                Helper.hideLoader();
                Log.e("Upload error:", t.getMessage());
            }

        });
    }


    @Override
    public void onBackPressed() {
        if (updated)
            openDialog();
        else super.onBackPressed();
    }

    private void openDialog() {
        dialog = new Dialog(ProfilePicUpdate.this);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.show();
        dialog.setCancelable(false);
        Button declineButton = dialog.findViewById(R.id.btn_logout);
        declineButton.setText(getResources().getString(R.string.yes));
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                makeApiCall();
            }
        });
        TextView cancel = dialog.findViewById(R.id.tv_cancel);
        cancel.setText(getResources().getString(R.string.no));
        TextView tv_all = dialog.findViewById(R.id.tv_all);
        TextView tv_logout = dialog.findViewById(R.id.tv_logout);
        tv_all.setVisibility(View.GONE);
        tv_logout.setText(getResources().getString(R.string.do_you_Wish_to_save_the_Changes));
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
}
