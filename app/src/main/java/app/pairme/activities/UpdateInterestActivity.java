package app.pairme.activities;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.List;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.DescriptionPojo.DescriptionPojo;
import app.pairme.ClassesPojo.Infodata;
import app.pairme.R;
import app.pairme.Utils;
import app.pairme.adapters.UpdatinfoAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.pairme.Utils.multiple;

public class UpdateInterestActivity extends AppCompatActivity implements View.OnClickListener {

    EditText et_Hobbies, et_Cuisine, et_Music, et_Movies, et_Sports;
    TextView tv_save;
    ImageView iv_Hobbies, iv_Cuisine, iv_Music, iv_Movies, iv_Sports;
    Dialog dialog;
    ProgressBar progressBar_cyclic;
    RecyclerView recyclerView;
    boolean updated;
    HashMap<String, String> named = new HashMap<>();

    private String getStringData(List<String> ids, int type) {
        StringBuilder convertedstring = new StringBuilder();
        for (int i = 0; i < ids.size(); i++) {
            if (i == 0)
                convertedstring = new StringBuilder(ids.get(i));
            else
                convertedstring.append(",").append(ids.get(i));
        }
        return convertedstring.toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_interest);
        changeStatusBarColor();
        initScreen();
    }

    private void initScreen() {
        et_Hobbies = findViewById(R.id.et_Hobbies);
        et_Cuisine = findViewById(R.id.et_Cuisine);
        et_Music = findViewById(R.id.et_Music);
        et_Movies = findViewById(R.id.et_Movies);
        et_Sports = findViewById(R.id.et_Sports);
        tv_save = findViewById(R.id.tv_save);
        iv_Hobbies = findViewById(R.id.iv_Hobbies);
        iv_Cuisine = findViewById(R.id.iv_Cuisine);
        iv_Music = findViewById(R.id.iv_Music);
        iv_Movies = findViewById(R.id.iv_Movies);
        iv_Sports = findViewById(R.id.iv_Sports);
        et_Hobbies.setOnClickListener(this);
        et_Cuisine.setOnClickListener(this);
        et_Music.setOnClickListener(this);
        et_Movies.setOnClickListener(this);
        et_Sports.setOnClickListener(this);
        tv_save.setOnClickListener(this);
        initData();
    }

    private void initData() {
        if (Helper.getUserData(UpdateInterestActivity.this) != null) {
            et_Hobbies.setText(Utils.setData(this, Helper.getUserData(this).getHobbies()));
            et_Cuisine.setText(Utils.setData(this, Helper.getUserData(this).getCuisine()));
            et_Music.setText(Utils.setData(this, Helper.getUserData(this).getMusic()));
            et_Movies.setText(Utils.setData(this, Helper.getUserData(this).getMovies()));
            et_Sports.setText(Utils.setData(this, Helper.getUserData(this).getSports()));
            named.put(getResources().getString(R.string.hobbies), Helper.getUserData(this).getHobbies());
            named.put(getResources().getString(R.string.cuisine), Helper.getUserData(this).getCuisine());
            named.put(getResources().getString(R.string.music), Helper.getUserData(this).getMusic());
            named.put(getResources().getString(R.string.movies), Helper.getUserData(this).getMovies());
            named.put(getResources().getString(R.string.sports), Helper.getUserData(this).getSports());
        }
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.et_Hobbies:
                showDialog(getResources().getString(R.string.hobbies), Helper.getinfodata(UpdateInterestActivity.this).interests.hobbies, et_Hobbies, iv_Hobbies);
                break;
            case R.id.et_Cuisine:
                showDialog(getResources().getString(R.string.cuisine), Helper.getinfodata(UpdateInterestActivity.this).interests.cuisines, et_Cuisine, iv_Cuisine);
                break;
            case R.id.et_Music:
                showDialog(getResources().getString(R.string.music), Helper.getinfodata(UpdateInterestActivity.this).interests.musics, et_Music, iv_Music);
                break;
            case R.id.et_Movies:
                showDialog(getResources().getString(R.string.movies), Helper.getinfodata(UpdateInterestActivity.this).interests.movies, et_Movies, iv_Movies);
                break;
            case R.id.et_Sports:
                showDialog(getResources().getString(R.string.sports), Helper.getinfodata(UpdateInterestActivity.this).interests.sports, et_Sports, iv_Sports);
                break;
            case R.id.tv_save:
                update();
                break;

        }
    }

    private void showDialog(final String header, final List<Infodata.Data> data, final EditText editText, final ImageView iv) {
        Utils.multiple = true;
        Utils.selected = editText.getText().toString().trim();
        dialog = new Dialog(UpdateInterestActivity.this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_listview);
        TextView tv_header = dialog.findViewById(R.id.tv_header);
        tv_header.setText(header);
        final TextView tv_count = dialog.findViewById(R.id.tv_count);
        tv_count.setVisibility(View.VISIBLE);
        Button btndialog = dialog.findViewById(R.id.btndialog);
        Button btndok = dialog.findViewById(R.id.btndok);
        btndok.setVisibility(View.VISIBLE);
        progressBar_cyclic = dialog.findViewById(R.id.progressBar_cyclic);
        progressBar_cyclic.setVisibility(View.GONE);
        btndialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btndok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        recyclerView = dialog.findViewById(R.id.listview);
        UpdatinfoAdapter interestsAdapter = new UpdatinfoAdapter(data, new UpdatinfoAdapter.OnStatusListner() {
            @Override
            public void OnStatusClick(String p, String trim) {
                UpdateInterestActivity.this.named.put(header, trim);
                updated = true;
                setData(editText, p, iv);
                dialog.dismiss();
            }

            @Override
            public void OnStatusmultClick(List<String> p, List<String> names) {
                Log.d("tag", "OnStatusmultClick: "+new Gson().toJson(p));
                tv_count.setText("(" + p.size() + "/3)");
                setData(editText, getStringData(p, 1), iv);
                UpdateInterestActivity.this.named.put(header, getStringData(names, 1));

            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(UpdateInterestActivity.this));
        recyclerView.setAdapter(interestsAdapter);
        dialog.show();
    }

    private void setData(EditText editText, String s, ImageView imageView) {
        editText.setText(s);
        editText.setBackgroundResource(R.drawable.green_background);
        imageView.setVisibility(View.VISIBLE);
    }


    public void update() {
        Helper.showLoader(this, "Please wait...");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<DescriptionPojo> call = service.updateInterest("Bearer " + Helper.getToken(UpdateInterestActivity.this),
                named.get(getResources().getString(R.string.hobbies)),
                named.get(getResources().getString(R.string.cuisine)),
                named.get(getResources().getString(R.string.music)),
                named.get(getResources().getString(R.string.movies)),
                named.get(getResources().getString(R.string.sports)));
        call.enqueue(new Callback<DescriptionPojo>() {
            @Override
            public void onResponse(Call<DescriptionPojo> call, Response<DescriptionPojo> response) {
                Helper.hideLoader();
                if (response.isSuccessful()) {
                    Toast.makeText(UpdateInterestActivity.this, R.string.details_updated, Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Log.i(">>>>", response.raw().toString());
                }
            }

            @Override
            public void onFailure(Call<DescriptionPojo> call, Throwable t) {
                Helper.hideLoader();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (updated)
            openDialog();
        else super.onBackPressed();
    }

    private void openDialog() {
        dialog = new Dialog(UpdateInterestActivity.this);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.show();
        dialog.setCancelable(false);
        Button declineButton = dialog.findViewById(R.id.btn_logout);
        declineButton.setText(getResources().getString(R.string.yes));
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                update();
            }
        });
        TextView cancel = dialog.findViewById(R.id.tv_cancel);
        cancel.setText(getResources().getString(R.string.no));
        TextView tv_all = dialog.findViewById(R.id.tv_all);
        TextView tv_logout = dialog.findViewById(R.id.tv_logout);
        tv_all.setVisibility(View.GONE);
        tv_logout.setText(getResources().getString(R.string.do_you_Wish_to_save_the_Changes));
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    @Override
    protected void onPause() {
        super.onPause();
        multiple = false;
    }
}
