package app.pairme.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import com.google.gson.Gson;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.UserInfoPojo.UserInfo;
import app.pairme.MainActivity;
import app.pairme.R;
import app.pairme.Transformer.ZoomOutTranformer;
import app.pairme.UI.ViewPagerCustomDuration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginScreen extends AppCompatActivity implements View.OnClickListener {
    final Handler handler = new Handler();
    RelativeLayout btn_fb;
    int currentPage = 0;

    RelativeLayout bottom_sheet;
    Switch switch_language;
    int time = 3000;
    String android_id;
    View view;
    private ViewPagerCustomDuration viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private List<Integer> layouts = new ArrayList<>();
    private TextView slide_title, slide_desc;
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
            if (position == 0) {
                setDescriptions(R.string.slide_1_title, R.string.slide_1_desc);
            } else if (position == 1) {
                setDescriptions(R.string.slide_2_title, R.string.slide_2_desc);

            } else if (position == 2)
                setDescriptions(R.string.slide_3_title, R.string.slide_3_desc);
            else {
                setDescriptions(R.string.slide_4_title, R.string.slide_4_desc);
            }
        }

        private void setDescriptions(int title, int desc) {
            slide_title.setText(getString(title));
            slide_desc.setText(getString(desc));

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };
    private Button btn_joinus, btn_twitter, btn_login;
    private TwitterAuthClient client;
    private CallbackManager mCallbackManager;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        super.onCreate(savedInstanceState);

        adddata();
        setContentView(R.layout.activity_login_screen);
        FacebookSdk.sdkInitialize(getApplicationContext());
        client = new TwitterAuthClient();
        initViews();
        changeStatusBarColor();
        //  enable_location();


    }

    private void adddata() {
        layouts.add(R.layout.welcome_slide1);
        layouts.add(R.layout.welcome_slide2);
        layouts.add(R.layout.welcome_slide3);
        layouts.add(R.layout.welcome_slide4);
    }

    private void initViews() {
        viewPager = findViewById(R.id.view_pager);
        dotsLayout = findViewById(R.id.layoutDots);
        btn_fb = findViewById(R.id.relative_fb);
        btn_joinus = findViewById(R.id.btn_join_us);
        btn_twitter = findViewById(R.id.btn_twitter);
        btn_login = findViewById(R.id.btn_login);
        slide_title = findViewById(R.id.slide_title);
        slide_desc = findViewById(R.id.slide_desc);
        bottom_sheet = findViewById(R.id.bottom_sheet);
        switch_language = findViewById(R.id.switch_language);
        btn_joinus.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        btn_twitter.setOnClickListener(this);
        btn_fb.setOnClickListener(this);
        switch_language.setChecked(!Helper.getLanguage(LoginScreen.this).equals("ar"));
        slideUp(bottom_sheet);
        addBottomDots(0);
        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
//        viewPager.setPageTransformer(true, new ZoomOutTranformer());
        viewPager.setScrollDurationFactor(4);
        LoginManager.getInstance().logOut();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (currentPage == layouts.size()) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
                handler.postDelayed(this, time);
            }
        }, time);
        switch_language.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Configuration configuration = getResources().getConfiguration();
                Locale locale;
                if (isChecked) {
                    locale = new Locale("en");
                    getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
                } else {
                    locale = new Locale("ar");
                    getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                }
                configuration.setLocale(locale);
                configuration.setLayoutDirection(locale);
                Helper.setLanguage(LoginScreen.this, isChecked ? "en" : "ar");
                getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());
                onConfigurationChanged(configuration);
            }
        });
    }


    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.activity_login_screen);
        initViews();
    }


    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.size()];
        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);
        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(getApplicationContext());
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_join_us:
                startActivity(new Intent(getApplicationContext(), GenderActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                slidedown(bottom_sheet);
                break;
            case R.id.btn_login:
                startActivity(new Intent(getApplicationContext(), LoginScreenMain.class));
                break;
            case R.id.btn_twitter:
                customLoginTwitter();
                break;
        }
    }

    public void slideUp(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up));
        view.setVisibility(View.VISIBLE);
        bottom_sheet.setVisibility(View.VISIBLE);
    }

    public void Login(View view) {
        LoginManager.getInstance()
                .logInWithReadPermissions(LoginScreen.this,
                        Arrays.asList("public_profile", "email"));
        Loginwith_FB();
    }


    private void slidedown(RelativeLayout bottom_sheet) {
        bottom_sheet.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down));
    }

    public void Loginwith_FB() {
        // initialze the facebook sdk and request to facebook for login
        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("token", String.valueOf(loginResult.getAccessToken()));
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                // App code
                Toast.makeText(LoginScreen.this, "Login Cancel", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("tag", "onError: ");
                Toast.makeText(LoginScreen.this, "Login Error" + error.toString(), Toast.LENGTH_SHORT).show();
            }

        });


    }

    private void handleFacebookAccessToken(final AccessToken token) {
        GraphRequest request = GraphRequest.newMeRequest(token, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject user, GraphResponse graphResponse) {
                Log.d("tag", "onCompleted: " + new Gson().toJson(user));
                String id = Profile.getCurrentProfile().getId();
                ApiInterface service = BaseUrl.createService(ApiInterface.class);
                Call<UserInfo> call = service.socialLogin(user.optString("first_name"), user.optString("age_range"), id, token.toString(), "https://graph.facebook.com/" + id + "/picture?width=500&width=500", 0, user.optString("gender"));
                call.enqueue(new Callback<UserInfo>() {

                    @Override
                    public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                        if (response.body().getSuccess()) {
                            Helper.setUserData(new Gson().toJson(response.body().getData()), LoginScreen.this);
                            Helper.clearToken(LoginScreen.this);
                            Helper.setToken(response.body().getToken(), LoginScreen.this);
                            Helper.setMyId(response.body().getData().getId().toString(), LoginScreen.this);
                            Helper.setLogin(true, LoginScreen.this);
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            finish();
                        } else {
                            Log.d("error", "else: " + response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<UserInfo> call, Throwable t) {
                        //  Helper.hideLoader();
                        Log.d("error", "Failure; " + t);
                        Toast.makeText(LoginScreen.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
                //after get the info of user we will pass to function which will store the info in our server
//                Call_Api_For_Signup(+ user.optString("first_name")
//                        , "" + user.optString("last_name"), "" + user.optString("birthday")
//                        , "" + user.optString("gender"),
//                        "https://graph.facebook.com/" + id + "/picture?width=500&width=500");

            }
        });

        // here is the request to facebook sdk for which type of info we have required
        Bundle parameters = new Bundle();
        parameters.putString("fields", "last_name,first_name,email,birthday,gender,age_range");
        request.setParameters(parameters);
        request.executeAsync();
    }


    public void customLoginTwitter() {
        TwitterCore.getInstance().getSessionManager().clearActiveSession();
        //check if user is already authenticated or not
        client.authorize(this, new com.twitter.sdk.android.core.Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                // Do something with result, which provides a TwitterSession for making API calls
                TwitterSession twitterSession = result.data;
                //call fetch email only when permission is granted
                fetchTwitterEmail(twitterSession);
            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(LoginScreen.this, "Failed to authenticate. Please try again.", Toast.LENGTH_SHORT).show();
            }
        });

        //if user is not authenticated start authenticating
    }

    public void fetchTwitterEmail(final TwitterSession twitterSession) {
        client.requestEmail(twitterSession, new com.twitter.sdk.android.core.Callback<String>() {
            @Override
            public void success(Result<String> result) {
                Log.d("tag", "success: " + "User Id : " + twitterSession.getUserId() + "\nScreen Name : " + twitterSession.getUserName() + "\nEmail Id : " + result.data);
            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(LoginScreen.this, "Failed to authenticate. Please try again.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mCallbackManager != null)
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(layouts.get(position), container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }

    }

}
