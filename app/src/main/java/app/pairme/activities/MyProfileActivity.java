package app.pairme.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.UserInfoPojo.PhotoAlbum;
import app.pairme.ClassesPojo.UserInfoPojo.UserInfo;
import app.pairme.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyProfileActivity extends BottomSheetDialogFragment {
    public static final String TAG = "MyProfileActivity";
    public List<String> getPhotoAlbum = new ArrayList<>();
    String id;
    View reverse, skip;
    CardView card_name, card_basics, lifestyle_cv, interests_cv, appearance_cv, card_details;
    ImageView iv_profile, settings;
    TextView tv_name, tv_age, tv_smiling, tv_liven_in, tv_contact, tv_email, tv_gender, tv_looking_for, tv_originally_from, tv_smoking, tv_eating_habits,
            tv_exercise_habits, tv_sleep_habits, tv_pets, tv_family_values, tv_polygamy_opinion, tv_personality1, tv_personality2, tv_personality3, tv_languages, tv_languages2, tv_languages3, tv_languages4, tv_languages5, tv_education,
            tv_occupation, tv_job_sector, tv_relationship, tv_have_kids, tv_want_kids, tv_living_situation, tv_willing_to_relocate, tv_appearance,
            tv_body_type, tv_height, tv_complexion, tv_health_status, tv_hair_color, tv_hair_length, tv_hair_type,
            tv_eye_color, tv_eye_wear, tv_makeup, tv_facial_hair, tv_secret_weapon, tv_tv_style, tv_about_me_content, tv_looking_for_content, status,
            tv_complete_profile, tv_hobbies, tv_hobbies2, tv_hobbies3, tv_cuisine, tv_cuisine2, tv_cuisine3, tv_music, tv_music2, tv_music3, tv_movies, tv_movies2, tv_movies3, tv_sports, tv_sports2, tv_sports3;
    Dialog dialog;
    List<PhotoAlbum> userdata = new ArrayList<>();
    List<String> img = new ArrayList<>();
    LinearLayout layoutDots, bottom_sheet;
    RelativeLayout smoking_rl, rl, eating_rl, appearance_rl, exc_rl, sleep_rl, pets_rl, family_rl, polygamy_rl, person_rl, lang_rl, hobies_rl, cuisine_rl, music_rl, movies_rl, sports_rl, body_rl, complextion_rl, hstatus_rl, hair_rl, hairlength_rl, type_rl, eye_rl, eyewear_rl, makeup_rl, weapon_rl,
            style_rl;
    NestedScrollView scollview;
    boolean name, request, lifestyle, interests, appearance, about, basics, ss;
    private int counter = 0;
    private View view;
    private LinearLayout age_rl, look_rl, city_rl, origin_rl, education_rl, occupation_rl, sector_rl, status_rl, kids_rl, hvkids_rl, ls_rl, relocate_rl;

    public MyProfileActivity(String id) {
        this.id = id;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_my_profile, container, false);

        changeStatusBarColor();
        initControl();
        checkedProfile();
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slideUp(bottom_sheet);
            }
        });
        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: ");
                if (bottom_sheet.getVisibility() == View.VISIBLE)
                    bottom_sheet.setVisibility(View.GONE);
            }
        });
        return view;

    }

    private void addbottomdots(int pos) {
        Glide.with(getContext()).load(getPhotoAlbum.get(counter))
                .error(R.drawable.place_holder).into(iv_profile);
        layoutDots.removeAllViews();
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1.0f);
        param.setMargins(5, 0, 5, 0);
        View[] dots = new View[getPhotoAlbum.size()];
        for (int i = 0; i < getPhotoAlbum.size(); i++) {
            dots[i] = new View(getActivity());
            dots[i].setBackgroundColor(Color.GRAY);
            dots[i].setLayoutParams(param);
            layoutDots.addView(dots[i]);
        }
        dots[pos].setBackgroundColor(Color.WHITE);

    }

    private void checkedProfile() {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<JsonObject> call = service.checkedProfile("Bearer " + Helper.getToken(getContext()), id);
        call.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {

                } else {
                    Log.d("error", "else: " + response.body());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d("error", "Failure; " + t);
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) dialogInterface;
                setupFullHeight(bottomSheetDialog);
            }
        });
        return dialog;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;
        slideUp(card_name);
        slideUp(card_basics);
    }

    public void slideUp(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_uphalf));
        view.setVisibility(View.VISIBLE);
    }

    private void setupFullHeight(BottomSheetDialog bottomSheetDialog) {
        FrameLayout bottomSheet = bottomSheetDialog.findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();

        int windowHeight = getWindowHeight();
        if (layoutParams != null) {
            layoutParams.height = windowHeight + 200;
        }
        bottomSheet.setLayoutParams(layoutParams);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    private int getWindowHeight() {
        // Calculate window height for fullscreen use
        DisplayMetrics displayMetrics = new DisplayMetrics();
        (getActivity()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    private void initControl() {
        iv_profile = view.findViewById(R.id.user_iv);
        bottom_sheet = view.findViewById(R.id.bottom_sheet);
        scollview = view.findViewById(R.id.scollview);
        settings = view.findViewById(R.id.settings);
        layoutDots = view.findViewById(R.id.layoutDots);
        tv_smiling = view.findViewById(R.id.tv_smiling);
        appearance_cv = view.findViewById(R.id.appearance_cv);
        lifestyle_cv = view.findViewById(R.id.lifestyle_cv);
        interests_cv = view.findViewById(R.id.interests_cv);
        skip = view.findViewById(R.id.skip);
        rl = view.findViewById(R.id.rl);
        reverse = view.findViewById(R.id.reverse);
        card_name = view.findViewById(R.id.card_name);
        card_basics = view.findViewById(R.id.card_basics);
        card_details = view.findViewById(R.id.card_details);
        smoking_rl = view.findViewById(R.id.smoking_rl);
        eating_rl = view.findViewById(R.id.eating_rl);
        exc_rl = view.findViewById(R.id.exc_rl);
        sleep_rl = view.findViewById(R.id.sleep_rl);
        pets_rl = view.findViewById(R.id.pets_rl);
        family_rl = view.findViewById(R.id.family_rl);
        polygamy_rl = view.findViewById(R.id.polygamy_rl);
        person_rl = view.findViewById(R.id.person_rl);
        lang_rl = view.findViewById(R.id.lang_rl);
        hobies_rl = view.findViewById(R.id.hobies_rl);
        cuisine_rl = view.findViewById(R.id.cuisine_rl);
        music_rl = view.findViewById(R.id.music_rl);
        movies_rl = view.findViewById(R.id.movies_rl);
        sports_rl = view.findViewById(R.id.sports_rl);
        tv_name = view.findViewById(R.id.tv_name);
        tv_age = view.findViewById(R.id.tv_age);
        tv_smiling = view.findViewById(R.id.tv_smiling);
        tv_liven_in = view.findViewById(R.id.tv_location);
        tv_looking_for = view.findViewById(R.id.tv_looking);
        tv_smoking = view.findViewById(R.id.tv_smoking);
        tv_eating_habits = view.findViewById(R.id.tv_eating_habits);
        tv_exercise_habits = view.findViewById(R.id.tv_exercise_habits);
        tv_sleep_habits = view.findViewById(R.id.tv_sleep_habits);
        tv_pets = view.findViewById(R.id.tv_pets);
        tv_family_values = view.findViewById(R.id.tv_family_values);
        tv_polygamy_opinion = view.findViewById(R.id.tv_polygamy_opinion);
        tv_personality1 = view.findViewById(R.id.tv_personality1);
        tv_personality2 = view.findViewById(R.id.tv_personality2);
        tv_personality3 = view.findViewById(R.id.tv_personality3);
        tv_languages = view.findViewById(R.id.tv_languages1);
        tv_languages2 = view.findViewById(R.id.tv_languages2);
        tv_languages3 = view.findViewById(R.id.tv_languages3);
        tv_languages4 = view.findViewById(R.id.tv_languages4);
        tv_languages5 = view.findViewById(R.id.tv_languages5);
        tv_education = view.findViewById(R.id.tv_education);
        tv_occupation = view.findViewById(R.id.tv_occupation);
        tv_job_sector = view.findViewById(R.id.tv_sector);
        tv_relationship = view.findViewById(R.id.tv_status);
        tv_have_kids = view.findViewById(R.id.tv_kids);
        tv_want_kids = view.findViewById(R.id.tv_want_kids);
        tv_living_situation = view.findViewById(R.id.tv_living_situation);
        tv_willing_to_relocate = view.findViewById(R.id.tv_willing_relocate);
        tv_appearance = view.findViewById(R.id.tv_appearance);
        tv_body_type = view.findViewById(R.id.tv_body_type);
        tv_complexion = view.findViewById(R.id.tv_complexion);
        tv_health_status = view.findViewById(R.id.tv_health_status);
        tv_hair_color = view.findViewById(R.id.tv_hair_color);
        tv_hair_length = view.findViewById(R.id.tv_hair_length);
        tv_hair_type = view.findViewById(R.id.tv_hair_type);
        tv_eye_color = view.findViewById(R.id.tv_eye_color);
        tv_eye_wear = view.findViewById(R.id.tv_eye_wear);
        tv_makeup = view.findViewById(R.id.tv_makeup);
        tv_facial_hair = view.findViewById(R.id.tv_facial_hair);
        tv_secret_weapon = view.findViewById(R.id.tv_secret_weapon);
        tv_tv_style = view.findViewById(R.id.tv_tv_style);
        tv_originally_from = view.findViewById(R.id.tv_origin);
        tv_hobbies = view.findViewById(R.id.tv_hobbies1);
        tv_hobbies2 = view.findViewById(R.id.tv_hobbies2);
        tv_hobbies3 = view.findViewById(R.id.tv_hobbies3);
        tv_cuisine = view.findViewById(R.id.tv_cuisine1);
        tv_cuisine2 = view.findViewById(R.id.tv_cuisine2);
        tv_cuisine3 = view.findViewById(R.id.tv_cuisine3);
        tv_music = view.findViewById(R.id.tv_music1);
        tv_music2 = view.findViewById(R.id.tv_music2);
        tv_music3 = view.findViewById(R.id.tv_music3);
        tv_movies = view.findViewById(R.id.tv_movies1);
        tv_movies2 = view.findViewById(R.id.tv_movies2);
        tv_movies3 = view.findViewById(R.id.tv_movies3);
        tv_sports = view.findViewById(R.id.tv_sports1);
        tv_sports2 = view.findViewById(R.id.tv_sports2);
        tv_sports3 = view.findViewById(R.id.tv_sports3);
        smoking_rl = view.findViewById(R.id.smoking_rl);
        eating_rl = view.findViewById(R.id.eating_rl);
        exc_rl = view.findViewById(R.id.exc_rl);
        sleep_rl = view.findViewById(R.id.sleep_rl);
        pets_rl = view.findViewById(R.id.pets_rl);
        family_rl = view.findViewById(R.id.family_rl);
        polygamy_rl = view.findViewById(R.id.polygamy_rl);
        person_rl = view.findViewById(R.id.person_rl);
        lang_rl = view.findViewById(R.id.lang_rl);
        hobies_rl = view.findViewById(R.id.hobies_rl);
        cuisine_rl = view.findViewById(R.id.cuisine_rl);
        music_rl = view.findViewById(R.id.music_rl);
        movies_rl = view.findViewById(R.id.movies_rl);
        sports_rl = view.findViewById(R.id.sports_rl);
        age_rl = view.findViewById(R.id.age_rl);
        look_rl = view.findViewById(R.id.look_rl);
        city_rl = view.findViewById(R.id.city_rl);
        origin_rl = view.findViewById(R.id.origin_rl);
        education_rl = view.findViewById(R.id.education_rl);
        occupation_rl = view.findViewById(R.id.occupation_rl);
        sector_rl = view.findViewById(R.id.sector_rl);
        status_rl = view.findViewById(R.id.status_rl);
        kids_rl = view.findViewById(R.id.kids_rl);
        hvkids_rl = view.findViewById(R.id.hvkids_rl);
        ls_rl = view.findViewById(R.id.ls_rl);
        relocate_rl = view.findViewById(R.id.relocate_rl);
        appearance_rl = view.findViewById(R.id.appearance_rl);
        body_rl = view.findViewById(R.id.body_rl);
        complextion_rl = view.findViewById(R.id.complextion_rl);
        hstatus_rl = view.findViewById(R.id.hstatus_rl);
        hair_rl = view.findViewById(R.id.hair_rl);
        hairlength_rl = view.findViewById(R.id.hairlength_rl);
        type_rl = view.findViewById(R.id.type_rl);
        eye_rl = view.findViewById(R.id.eye_rl);
        eyewear_rl = view.findViewById(R.id.eyewear_rl);
        makeup_rl = view.findViewById(R.id.makeup_rl);
        weapon_rl = view.findViewById(R.id.weapon_rl);
        style_rl = view.findViewById(R.id.style_rl);
        LinearLayout layout_report = view.findViewById(R.id.ll_report);
        layout_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), getResources().getString(R.string.report), Toast.LENGTH_SHORT).show();
            }
        });
        LinearLayout layout_flag = view.findViewById(R.id.ll_block);
        layout_flag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), getResources().getString(R.string.block), Toast.LENGTH_SHORT).show();
            }
        });
        LinearLayout layout_cancel = view.findViewById(R.id.ll_cancel);
        layout_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), getResources().getString(R.string.cancel), Toast.LENGTH_SHORT).show();
            }
        });
        reverse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                --counter;
                if (counter >= 0)
                    addbottomdots(counter);
                else counter = 0;
            }
        });
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter++;
                if (counter < getPhotoAlbum.size())
                    addbottomdots(counter);
                else counter = getPhotoAlbum.size();
            }
        });
        getUserdata();
        scollview.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                if (scollview != null) {
                    Rect mReact = new Rect();
                    scollview.getHitRect(mReact);
                    if (!card_details.getLocalVisibleRect(mReact) && !ss) {
                        slideUp(card_details);
                    } else if (!appearance_cv.getLocalVisibleRect(mReact) && !appearance) {
                        slideUp(appearance_cv);
                    } else if (!lifestyle_cv.getLocalVisibleRect(mReact) && !lifestyle) {
                        slideUp(lifestyle_cv);
                    } else if (!interests_cv.getLocalVisibleRect(mReact) && !interests) {
                        slideUp(interests_cv);
                    }
                }
            }
        });
    }


    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void getUserdata() {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<UserInfo> call = service.getUserInfo("Bearer " + Helper.getToken(getContext()), id);
        call.enqueue(new Callback<UserInfo>() {

            @Override
            public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                if (response.isSuccessful()) {
                    if (!response.body().getData().getProfileImage().equalsIgnoreCase(""))
                        getPhotoAlbum.add("https://pairme.co/img/userProfile/" + response.body().getData().getProfileImage());
                    if (response.body().getData().getPhotoAlbum() != null)
                        for (PhotoAlbum getPhotoAlbums : response.body().getData().getPhotoAlbum()) {
                            getPhotoAlbum.add("https://pairme.co/img/userAlbum/" + getPhotoAlbums.getImage());
                        }
                    addbottomdots(counter);
                    tv_name.setText(response.body().getData().getName());
                    if (response.body().getData().getAge().equalsIgnoreCase("") && response.body().getData().getLookingFor().equalsIgnoreCase("") && response.body().getData().getLivesIn().equalsIgnoreCase("") && response.body().getData().getOrginallyFrom().equalsIgnoreCase("")) {
                        card_basics.setVisibility(View.GONE);
                        basics = true;
                    } else {
                        if (response.body().getData().getAge().equalsIgnoreCase(""))
                            age_rl.setVisibility(View.GONE);
                        else tv_age.setText(response.body().getData().getAge());
                        if (response.body().getData().getLookingFor().equalsIgnoreCase(""))
                            look_rl.setVisibility(View.GONE);
                        else
                            tv_looking_for.setText(response.body().getData().getLookingFor().toString());
                        if (response.body().getData().getLivesIn().equalsIgnoreCase(""))
                            city_rl.setVisibility(View.GONE);
                        else tv_liven_in.setText(response.body().getData().getLivesIn());
                        if (response.body().getData().getOrginallyFrom().equalsIgnoreCase(""))
                            origin_rl.setVisibility(View.GONE);
                        else
                            tv_originally_from.setText(response.body().getData().getOrginallyFrom());
                    }
                    if (response.body().getData().getEducation().equalsIgnoreCase("") && response.body().getData().getOccupation().equalsIgnoreCase("") && response.body().getData().getJobSector().equalsIgnoreCase("") && response.body().getData().getRelationship().equalsIgnoreCase("") && response.body().getData().getHaveKids().equalsIgnoreCase("") && response.body().getData().getWantKids().equalsIgnoreCase("") && response.body().getData().getLivingSituation().equalsIgnoreCase("") && response.body().getData().getWillingToRelocate().equalsIgnoreCase("")) {
                        card_details.setVisibility(View.GONE);
                        ss = true;
                    } else {
                        if (response.body().getData().getEducation().equalsIgnoreCase(""))
                            education_rl.setVisibility(View.GONE);
                        else
                            tv_education.setText(response.body().getData().getEducation().toString());
                        if (response.body().getData().getOccupation().equalsIgnoreCase(""))
                            occupation_rl.setVisibility(View.GONE);
                        else
                            tv_occupation.setText(response.body().getData().getOccupation().toString());
                        if (response.body().getData().getJobSector().equalsIgnoreCase(""))
                            sector_rl.setVisibility(View.GONE);
                        else
                            tv_job_sector.setText(response.body().getData().getJobSector().toString());
                        if (response.body().getData().getRelationship().equalsIgnoreCase(""))
                            status_rl.setVisibility(View.GONE);
                        else {
                            Log.d(TAG, "onResponse: "+(status_rl.getVisibility()==View.VISIBLE));
                            tv_relationship.setText(response.body().getData().getRelationship().toString());
                        }
                        if (response.body().getData().getHaveKids().equalsIgnoreCase(""))
                            kids_rl.setVisibility(View.GONE);
                        else
                            tv_have_kids.setText(response.body().getData().getHaveKids().toString());
                        if (response.body().getData().getWantKids().equalsIgnoreCase(""))
                            hvkids_rl.setVisibility(View.GONE);
                        else
                            tv_want_kids.setText(response.body().getData().getWantKids().toString());
                        if (response.body().getData().getLivingSituation().equalsIgnoreCase(""))
                            ls_rl.setVisibility(View.GONE);
                        else
                            tv_living_situation.setText(response.body().getData().getLivingSituation().toString());
                        if (response.body().getData().getWillingToRelocate().equalsIgnoreCase(""))
                            relocate_rl.setVisibility(View.GONE);
                        else
                            tv_willing_to_relocate.setText(response.body().getData().getWillingToRelocate().toString());
                    }

                    if (response.body().getData().getAppearance().equalsIgnoreCase("") && response.body().getData().getBodyType().equalsIgnoreCase("") && response.body().getData().getComplexion().equalsIgnoreCase("") && response.body().getData().getHealthStatus().equalsIgnoreCase("") && response.body().getData().getHairColor().equalsIgnoreCase("") && response.body().getData().getHairLength().equalsIgnoreCase("") && response.body().getData().getHairType().equalsIgnoreCase("") && response.body().getData().getEyeColor().equalsIgnoreCase("") && response.body().getData().getEyeWear().equalsIgnoreCase("") && response.body().getData().getMakeup().equalsIgnoreCase("") && response.body().getData().getSecretWeapon().equalsIgnoreCase("") && response.body().getData().getStyle().equalsIgnoreCase("")) {
                        appearance_cv.setVisibility(View.GONE);
                        appearance = true;
                    } else {
                        if (!response.body().getData().getAppearance().equalsIgnoreCase(""))
                            tv_appearance.setText(response.body().getData().getAppearance().toString());
                        else appearance_rl.setVisibility(View.GONE);
                        if (!response.body().getData().getBodyType().equalsIgnoreCase(""))
                            tv_body_type.setText(response.body().getData().getBodyType().toString());
                        else body_rl.setVisibility(View.GONE);
                        if (!response.body().getData().getComplexion().equalsIgnoreCase(""))
                            tv_complexion.setText(response.body().getData().getComplexion().toString());
                        else complextion_rl.setVisibility(View.GONE);
                        if (!response.body().getData().getHealthStatus().equalsIgnoreCase(""))
                            tv_health_status.setText(response.body().getData().getHealthStatus().toString());
                        else hstatus_rl.setVisibility(View.GONE);
                        if (!response.body().getData().getHairColor().equalsIgnoreCase(""))
                            tv_hair_color.setText(response.body().getData().getHairColor().toString());
                        else hair_rl.setVisibility(View.GONE);
                        if (!response.body().getData().getHairLength().equalsIgnoreCase(""))
                            tv_hair_length.setText(response.body().getData().getHairLength().toString());
                        else hairlength_rl.setVisibility(View.GONE);
                        if (!response.body().getData().getHairType().equalsIgnoreCase(""))
                            tv_hair_type.setText(response.body().getData().getHairType().toString());
                        else type_rl.setVisibility(View.GONE);
                        if (!response.body().getData().getEyeColor().equalsIgnoreCase(""))
                            tv_eye_color.setText(response.body().getData().getEyeColor().toString());
                        else eye_rl.setVisibility(View.GONE);
                        if (!response.body().getData().getEyeWear().equalsIgnoreCase(""))
                            tv_eye_wear.setText(response.body().getData().getEyeWear().toString());
                        else eyewear_rl.setVisibility(View.GONE);
                        if (!response.body().getData().getMakeup().equalsIgnoreCase(""))
                            tv_makeup.setText(response.body().getData().getMakeup());
                        else makeup_rl.setVisibility(View.GONE);
                        if (!response.body().getData().getSecretWeapon().equalsIgnoreCase(""))
                            tv_secret_weapon.setText(response.body().getData().getSecretWeapon().toString());
                        else weapon_rl.setVisibility(View.GONE);
                        if (!response.body().getData().getStyle().equalsIgnoreCase(""))
                            tv_tv_style.setText(response.body().getData().getStyle().toString());
                        else style_rl.setVisibility(View.GONE);

                    }
                    if (response.body().getData().getSmoking().equalsIgnoreCase("") && response.body().getData().getEatingHabits().equalsIgnoreCase("") && response.body().getData().getExerciseHabits().equalsIgnoreCase("") && response.body().getData().getSleepHabits().equalsIgnoreCase("") && response.body().getData().getPets().equalsIgnoreCase("") && response.body().getData().getFamilyValues().equalsIgnoreCase("") && response.body().getData().getPolygamyOpinion().equalsIgnoreCase("") && response.body().getData().getPersonality().equalsIgnoreCase("") && response.body().getData().getLanguages().equalsIgnoreCase("")) {
                        lifestyle_cv.setVisibility(View.GONE);
                        lifestyle = true;
                    } else {
                        if (response.body().getData().getSmoking().equalsIgnoreCase(""))
                            smoking_rl.setVisibility(View.GONE);
                        else tv_smoking.setText(response.body().getData().getSmoking());
                        if (response.body().getData().getEatingHabits().equalsIgnoreCase(""))
                            eating_rl.setVisibility(View.GONE);
                        else
                            tv_eating_habits.setText(response.body().getData().getEatingHabits());
                        if (response.body().getData().getExerciseHabits().equalsIgnoreCase(""))
                            exc_rl.setVisibility(View.GONE);
                        else
                            tv_exercise_habits.setText(response.body().getData().getExerciseHabits());
                        if (response.body().getData().getSleepHabits().equalsIgnoreCase(""))
                            sleep_rl.setVisibility(View.GONE);
                        else tv_sleep_habits.setText(response.body().getData().getSleepHabits());
                        if (response.body().getData().getPets().equalsIgnoreCase(""))
                            pets_rl.setVisibility(View.GONE);
                        else tv_pets.setText(response.body().getData().getPets());
                        if (response.body().getData().getFamilyValues().equalsIgnoreCase(""))
                            family_rl.setVisibility(View.GONE);
                        else tv_family_values.setText(response.body().getData().getFamilyValues());
                        if (response.body().getData().getPolygamyOpinion().equalsIgnoreCase(""))
                            polygamy_rl.setVisibility(View.GONE);
                        else
                            tv_polygamy_opinion.setText(response.body().getData().getPolygamyOpinion());
                        if (response.body().getData().getPersonality().equalsIgnoreCase(""))
                            person_rl.setVisibility(View.GONE);
                        else setData("tv_personality", response.body().getData().getPersonality());
                        if (response.body().getData().getLanguages().equalsIgnoreCase(""))
                            lang_rl.setVisibility(View.GONE);
                        else setData("tv_languages", response.body().getData().getLanguages());
                    }
                    if (response.body().getData().getHobbies().equalsIgnoreCase("") && response.body().getData().getCuisine().equalsIgnoreCase("") && response.body().getData().getMovies().equalsIgnoreCase("") && response.body().getData().getMusic().equalsIgnoreCase("") && response.body().getData().getSports().equalsIgnoreCase("")) {
                        interests_cv.setVisibility(View.GONE);
                        interests = true;
                    } else {
                        if (response.body().getData().getLanguages().equalsIgnoreCase(""))
                            hobies_rl.setVisibility(View.GONE);
                        else setData("tv_hobbies", response.body().getData().getLanguages());
                        if (response.body().getData().getLanguages().equalsIgnoreCase(""))
                            cuisine_rl.setVisibility(View.GONE);
                        else setData("tv_cuisine", response.body().getData().getLanguages());
                        if (response.body().getData().getLanguages().equalsIgnoreCase(""))
                            music_rl.setVisibility(View.GONE);
                        else setData("tv_music", response.body().getData().getLanguages());
                        if (response.body().getData().getLanguages().equalsIgnoreCase(""))
                            movies_rl.setVisibility(View.GONE);
                        else setData("tv_movies", response.body().getData().getLanguages());
                        if (response.body().getData().getLanguages().equalsIgnoreCase(""))
                            sports_rl.setVisibility(View.GONE);
                        else setData("tv_sports", response.body().getData().getLanguages());
                    }


                } else {
                    Log.d("error", "else: " + response.body());
                }
            }

            @Override
            public void onFailure(Call<UserInfo> call, Throwable t) {
                Log.d("error", "Failure; " + t);
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setData(String tv_personality, String personality) {
        String[] data = personality.split(",");
        Log.d(TAG, "setData: " + data.length);
        for (int i = 0; i < data.length; i++) {
            String id = tv_personality + (i + 1);
            int resID = getResources().getIdentifier(id, "id", getActivity().getPackageName());
            ((TextView) view.findViewById(resID)).setText(data[i]);
            ((TextView) view.findViewById(resID)).setVisibility(View.VISIBLE);
        }
    }
}

