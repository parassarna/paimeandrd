package app.pairme.activities;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.BlockListPojo.BlockListPojo;
import app.pairme.R;
import app.pairme.adapters.BlockListAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BlockListActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    RelativeLayout layout;
    ArrayList<String> images, name, location, age, ids;
    String id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blocklist);
        changeStatusBarColor();
        initScreen();
        getBlockList();
    }

    private void initScreen() {
        recyclerView = findViewById(R.id.rv_block_list);
        layout = findViewById(R.id.relative_no_user);
        recyclerView.setLayoutManager(new LinearLayoutManager(BlockListActivity.this));
        images = new ArrayList<>();
        name = new ArrayList<>();
        location = new ArrayList<>();
        age = new ArrayList<>();
        ids = new ArrayList<>();
        id = Helper.getUserData(getApplicationContext()).getId().toString();
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
        }
    }

    private void getBlockList() {
        Helper.showLoader(this, "Please wait..");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<BlockListPojo> call = service.getBlockedUser("Bearer " + Helper.getToken(getApplicationContext()), id);
        call.enqueue(new Callback<BlockListPojo>() {
            @Override
            public void onResponse(Call<BlockListPojo> call, Response<BlockListPojo> response) {
                Helper.hideLoader();
                if (response.isSuccessful()) {
                    recyclerView.setAdapter(new BlockListAdapter(BlockListActivity.this, response.body().getData()));
                } else {
                    Log.d("error", "else: " + response.body());
                }

            }

            @Override
            public void onFailure(Call<BlockListPojo> call, Throwable t) {
                Helper.hideLoader();
                Log.d("error", "Failure; " + t);
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


}
