package app.pairme.activities;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.R;
import app.pairme.adapters.ExploreAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Activity_Explore extends AppCompatActivity {
    RecyclerView explore_rv;
    TextView title_tv;
    JsonArray jsonArray = new JsonArray();
    boolean isLoading;
    ExploreAdapter exploreAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_);
        changeStatusBarColor();
        initView();
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void initView() {
        explore_rv = findViewById(R.id.explore_rv);
        title_tv = findViewById(R.id.title_tv);
        explore_rv.setLayoutManager(new LinearLayoutManager(this));
        explore_rv.setHasFixedSize(true);
        searchlist();
        pagination();
    }

    private void pagination() {
        explore_rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == jsonArray.size() - 1) {
                        //bottom of list!
                        searchlist();
                        isLoading = true;
                    }
                }
            }
        });
    }

    public void searchlist() {
        Helper.showProgress(Activity_Explore.this);
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<JsonObject> call = service.getActivity("Bearer " + Helper.getToken(this));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Helper.hideProgress();
                if (response.isSuccessful()) {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("type", "userdata");
                    jsonObject.add("userData", response.body().get("data").getAsJsonObject().get("userData").getAsJsonObject());
                    jsonArray.add(jsonObject);
                    if (response.body().get("data").getAsJsonObject().get("matching_data").getAsJsonArray().size() > 0) {
                        jsonObject = new JsonObject();
                        jsonObject.addProperty("type", "matching_data");
                        jsonObject.add("matching_data", response.body().get("data").getAsJsonObject().get("matching_data").getAsJsonArray());
                        jsonArray.add(jsonObject);
                    }
                    if (response.body().get("data").getAsJsonObject().get("checkdata").getAsJsonArray().size() > 0) {
                        jsonObject = new JsonObject();
                        jsonObject.addProperty("type", "checkdata");
                        jsonObject.add("checkdata", response.body().get("data").getAsJsonObject().get("checkdata").getAsJsonArray());
                        jsonArray.add(jsonObject);
                    }
                    if (response.body().get("data").getAsJsonObject().get("meetData").getAsJsonArray().size() > 0) {
                        jsonObject = new JsonObject();
                        jsonObject.addProperty("type", "meetData");
                        jsonObject.add("meetData", response.body().get("data").getAsJsonObject().get("meetData").getAsJsonArray());
                        jsonArray.add(jsonObject);
                    }
                    if (response.body().get("data").getAsJsonObject().get("featured_members").getAsJsonArray().size() > 0) {
                        jsonObject = new JsonObject();
                        jsonObject.addProperty("type", "featured_members");
                        jsonObject.add("featured_members", response.body().get("data").getAsJsonObject().get("featured_members").getAsJsonArray());
                        jsonArray.add(jsonObject);
                    }
                    if (response.body().get("data").getAsJsonObject().get("newest_member").getAsJsonArray().size() > 0) {
                        jsonObject = new JsonObject();
                        jsonObject.addProperty("type", "newest_member");
                        jsonObject.add("newest_member", response.body().get("data").getAsJsonObject().get("newest_member").getAsJsonArray());
                        jsonArray.add(jsonObject);
                    }
                    if (response.body().get("data").getAsJsonObject().get("likedata").getAsJsonArray().size() > 0) {
                        jsonObject = new JsonObject();
                        jsonObject.addProperty("type", "likedata");
                        jsonObject.add("likedata", response.body().get("data").getAsJsonObject().get("likedata").getAsJsonArray());
                        jsonArray.add(jsonObject);
                    }
                    if (response.body().get("data").getAsJsonObject().get("news").getAsJsonArray().size() > 0) {
                        jsonObject = new JsonObject();
                        jsonObject.addProperty("type", "news");
                        jsonObject.add("news", response.body().get("data").getAsJsonObject().get("news").getAsJsonArray());
                        jsonArray.add(jsonObject);
                    }
                    if (response.body().get("data").getAsJsonObject().get("blog").getAsJsonArray().size() > 0) {
                        jsonObject = new JsonObject();
                        jsonObject.addProperty("type", "blog");
                        jsonObject.add("blog", response.body().get("data").getAsJsonObject().get("blog").getAsJsonArray());
                        jsonArray.add(jsonObject);
                    }
                    if (response.body().get("data").getAsJsonObject().get("story").getAsJsonArray().size() > 0) {
                        jsonObject = new JsonObject();
                        jsonObject.addProperty("type", "story");
                        jsonObject.add("story", response.body().get("data").getAsJsonObject().get("story").getAsJsonArray());
                        jsonArray.add(jsonObject);
                    }
                    isLoading = false;
                    if (exploreAdapter == null) {
                        exploreAdapter = new ExploreAdapter(Activity_Explore.this, title_tv, jsonArray);
                        explore_rv.setAdapter(exploreAdapter);
                    } else exploreAdapter.notifyDataSetChanged();
                } else {
                    Log.i(">>>>", response.raw().toString());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            title_tv.setVisibility(View.VISIBLE);
            explore_rv.setVisibility(View.VISIBLE);
        } else super.onBackPressed();
    }
}
