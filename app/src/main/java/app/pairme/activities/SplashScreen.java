package app.pairme.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.Infodata;
import app.pairme.ClassesPojo.UserInfoPojo.UserInfo;
import app.pairme.MainActivity;
import app.pairme.MyService;
import app.pairme.R;
import app.pairme.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreen extends AppCompatActivity {
    Handler handler;
    Animation animation;
    ImageView iv_splash;
    Locale myLocale;
//    FirebaseAnalytics mFirebaseAnalytics;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
//        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        checkPermission();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        // Toast.makeText(this, refreshedToken, Toast.LENGTH_SHORT).show();
        Log.d("TAG", "onCreate: " + refreshedToken);
//        if (!isMyServiceRunning()) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
//                startForegroundService(new Intent(getApplicationContext(), MyService.class));
//            else startService(new Intent(getApplicationContext(), MyService.class));
//        }
        Configuration configuration = getResources().getConfiguration();
        Locale.setDefault(Helper.getLanguage(this).equals("ar") ? new Locale("ar") : new Locale("en"));
        configuration.setLayoutDirection(Helper.getLanguage(this).equals("ar") ? new Locale("ar") : new Locale("en"));
        handler = new Handler();
        printKeyHash();
        iv_splash = findViewById(R.id.iv_splash);
        Glide.with(SplashScreen.this).load(R.drawable.splash)
                .fitCenter()
                .into(iv_splash);
        changeStatusBarColor();
        if (Helper.getToken(SplashScreen.this) != null)
            getUserdata();
        else getLocalisation();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.FOREGROUND_SERVICE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (SplashScreen.this, Manifest.permission.FOREGROUND_SERVICE)) {
                Snackbar.make(SplashScreen.this.findViewById(android.R.id.content),
                        "Please Grant Permissions to get Notifications",
                        Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                requestPermissions(new String[]{Manifest.permission.FOREGROUND_SERVICE}, 101);
                            }
                        }).show();
            } else {
                requestPermissions(
                        new String[]{Manifest.permission.FOREGROUND_SERVICE}, 101);
            }
        }
    }

    private void getLocalisation() {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<JsonObject> call = service.localisation();
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    Utils.localisation = response.body().get("data").getAsJsonObject();
                    searchlist();
                } else {
                    Log.i(">>>>", response.raw().toString());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    private void switchsplash() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                setLocale(Helper.getLanguage(SplashScreen.this));
                startActivity(new Intent(SplashScreen.this, Helper.getLogin(SplashScreen.this) ? MainActivity.class : LoginScreen.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        }, 5000);

    }

    public void getUserdata() {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<UserInfo> call = service.getUserInfo("Bearer " + Helper.getToken(SplashScreen.this), Helper.getId(SplashScreen.this));
        call.enqueue(new Callback<UserInfo>() {

            @Override
            public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                if (response.isSuccessful()) {
                    Helper.setUserData(new Gson().toJson(response.body().getData()), SplashScreen.this);
                    getLocalisation();
                    // searchlist();
                } else {
                    Log.d("error", "else: " + response.body());
                }
            }

            @Override
            public void onFailure(Call<UserInfo> call, Throwable t) {
                getLocalisation();
                Log.d("error", "Failure; " + t);
            }
        });
    }

    public void searchlist() {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<Infodata> call = service.getSearchlist(Helper.getLanguage(SplashScreen.this));
        call.enqueue(new Callback<Infodata>() {
            @Override
            public void onResponse(Call<Infodata> call, Response<Infodata> response) {
                if (response.isSuccessful()) {
                    Helper.setinfodata(new Gson().toJson(response.body().data), SplashScreen.this);
                    switchsplash();
                } else {
                    Log.i(">>>>", response.raw().toString());
                }
            }

            @Override
            public void onFailure(Call<Infodata> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    public void printKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i("keyhash", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public void setLocale(String localeName) {
        myLocale = new Locale(localeName);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    private boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);

        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (MyService.class.getName().equals(service.service.getClassName())) {
                Log.i("tag", "isMyServiceRunning? " + true + "");
                return true;
            }
        }

        Log.i("tag", "isMyServiceRunning? " + false + "");
        return false;
    }

}
