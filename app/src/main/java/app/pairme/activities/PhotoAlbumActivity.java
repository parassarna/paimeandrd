package app.pairme.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.UploadAlbumPojos;
import app.pairme.ClassesPojo.UserInfoPojo.PhotoAlbum;
import app.pairme.R;
import app.pairme.adapters.AlbumAdapter;
import app.pairme.adapters.FileUtils;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.pairme.Utils.getImageUri;
import static app.pairme.Utils.getRealPathFromURI;

public class PhotoAlbumActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    List<String> album;
    TextView tv_save;
    ImageView iv_add_photo;
    int RESULT_LOAD_IMAGE = 1;
    Uri uri;
    File compressedImageFile, file;
    Bitmap photo;
    List<MultipartBody.Part> ImagesParts = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_album);
        changeStatusBarColor();
        recyclerView = findViewById(R.id.album_rv);
        tv_save = findViewById(R.id.tv_save);
        iv_add_photo = findViewById(R.id.iv_add_photo);
        album = new ArrayList<>();
        iv_add_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermission();
            }
        });
        if (Helper.getUserData(PhotoAlbumActivity.this).getPhotoAlbum() != null)
            setData(Helper.getUserData(PhotoAlbumActivity.this).getPhotoAlbum());
    }

    private void setData(List<PhotoAlbum> photoAlbums) {
        album.clear();
        for (PhotoAlbum photoAlbum : photoAlbums) {
            album.add("https://pairme.co/img/userAlbum/" + photoAlbum.getImage());
        }
        recyclerView.setLayoutManager(new GridLayoutManager(PhotoAlbumActivity.this, 2));
        recyclerView.setAdapter(new AlbumAdapter(PhotoAlbumActivity.this, album, 0, photoAlbums));
    }

    private void openDialog() {
        final Dialog dialog = new Dialog(PhotoAlbumActivity.this);
        dialog.setContentView(R.layout.dialog_photo);
        dialog.show();
        dialog.setCancelable(false);
        TextView choose_camera = dialog.findViewById(R.id.choose_camera);
        TextView choose_file = dialog.findViewById(R.id.choose_file);
        Button btndialog = dialog.findViewById(R.id.btndialog);

        choose_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePictureIntent, 2);
            }
        });
        choose_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
                dialog.dismiss();
            }
        });
        btndialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }


    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(intent, 1);
    }


    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(PhotoAlbumActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) + ContextCompat
                .checkSelfPermission(PhotoAlbumActivity.this,
                        Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (PhotoAlbumActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (PhotoAlbumActivity.this, Manifest.permission.CAMERA)) {
                Snackbar.make(PhotoAlbumActivity.this.findViewById(android.R.id.content),
                        "Please Grant Permissions to upload profile photo",
                        Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 101);
                            }
                        }).show();
            } else {
                requestPermissions(
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 101);
            }
        } else {
            openDialog();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean camera_Permission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
        boolean storage_Permission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
        if (camera_Permission && storage_Permission) {
            openDialog();
        }
    }

    public void makeApiCall() {
        Helper.showLoader(PhotoAlbumActivity.this, "Please wait....");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<UploadAlbumPojos> call = service.uploadAlbum(ImagesParts, "Bearer " + Helper.getToken(PhotoAlbumActivity.this));
        call.enqueue(new Callback<UploadAlbumPojos>() {
            @Override
            public void onResponse(Call<UploadAlbumPojos> call, Response<UploadAlbumPojos> response) {
                Helper.hideLoader();
                ImagesParts.clear();
                setData(response.body().photoAlbum);

            }

            @Override
            public void onFailure(Call<UploadAlbumPojos> call, Throwable t) {
                Helper.hideLoader();
                Log.e("Upload error:", t.getMessage());
            }

        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 2) {
                photo = (Bitmap) data.getExtras().get("data");
                file = new File(getRealPathFromURI(PhotoAlbumActivity.this, getImageUri(PhotoAlbumActivity.this, photo)));
                try {
                    file = new Compressor(this).compressToFile(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (file != null) {
                    RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                    try {
                        ImagesParts.add(MultipartBody.Part.createFormData("images[]", URLEncoder.encode(file.getName(), "utf-8"), requestBody));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            } else if (requestCode == 1) {
                if (data.getData() != null) {
                    uri = data.getData();
                    file = getFile();

                    if (file != null) {
                        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        try {
                            ImagesParts.add(MultipartBody.Part.createFormData("images[]", URLEncoder.encode(file.getName(), "utf-8"), requestBody));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();
                        for (int i = 0; i < mClipData.getItemCount(); i++) {
                            uri = mClipData.getItemAt(i).getUri();
                            file = getFile();
                            if (file != null) {
                                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                                try {
                                    ImagesParts.add(MultipartBody.Part.createFormData("images[]", URLEncoder.encode(file.getName(), "utf-8"), requestBody));
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
            }
        }
        makeApiCall();
    }

    public File getFile() {
        file = FileUtils.getFile(this, uri);
        try {
            file = new Compressor(this).compressToFile(file);
        } catch (Exception e) {
            Log.d("tag", "onActivityResult: " + e.getMessage());
            e.printStackTrace();
        }
        return file;
    }
}
