package app.pairme.activities;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.UpdateName;
import app.pairme.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateNameActivity extends AppCompatActivity implements View.OnClickListener {

    EditText et_current_name, et_new_name;
    TextView tv_save;
    String currentDate;
    Dialog dialog;

    boolean updated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_name);
        changeStatusBarColor();
        initScreen();
    }

    private void initScreen() {
        et_current_name = findViewById(R.id.et_current_name);
        et_new_name = findViewById(R.id.et_new_name);
        tv_save = findViewById(R.id.tv_save);
        et_current_name.setText(Helper.getUserData(this).getName());
        tv_save.setOnClickListener(this);
        et_new_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                updated = true;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        currentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_save) {
            updateName();
        }
    }

    private void updateName() {
        Helper.showLoader(UpdateNameActivity.this, "Please wait...");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<UpdateName> call = service.updateName("Bearer " + Helper.getToken(getApplicationContext()), et_new_name.getText().toString(), currentDate);
        call.enqueue(new Callback<UpdateName>() {
            @Override
            public void onResponse(Call<UpdateName> call, Response<UpdateName> response) {
                Helper.hideLoader();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Toast.makeText(UpdateNameActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<UpdateName> call, Throwable t) {
                Helper.hideLoader();
                Log.d("error", "Failure; " + t);
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
