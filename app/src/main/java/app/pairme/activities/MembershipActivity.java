package app.pairme.activities;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import app.pairme.ApiServices.Helper;
import app.pairme.R;

public class MembershipActivity extends AppCompatActivity {
    TextView tv_purchase;
    RelativeLayout relative_email;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_membership);
        tv_purchase = findViewById(R.id.tv_purchase);
        relative_email = findViewById(R.id.relative_email);
        tv_purchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PairmePlusActivity plusActivity = new PairmePlusActivity();
                plusActivity.show(getSupportFragmentManager(), PairmePlusActivity.TAG);
            }
        });
        if (Helper.getLanguage(this).equalsIgnoreCase("ar")) {
            String id = "iv_one";
            int resID = getResources().getIdentifier(id, "id", MembershipActivity.this.getPackageName());
            ((ImageView) findViewById(resID)).setRotation(180);

        }
        relative_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });
        changeStatusBarColor();
    }

    private void openDialog() {
        dialog = new Dialog(MembershipActivity.this);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.show();
        dialog.setCancelable(false);
        Button declineButton = dialog.findViewById(R.id.btn_logout);
        declineButton.setText(getResources().getString(R.string.ok));
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        TextView cancel = dialog.findViewById(R.id.tv_cancel);
        TextView tv_all = dialog.findViewById(R.id.tv_all);
        TextView tv_logout = dialog.findViewById(R.id.tv_logout);
        tv_all.setText(getResources().getString(R.string.purchase_are_restored));
        tv_logout.setText(getResources().getString(R.string.Purchase));
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
        }
    }

}
