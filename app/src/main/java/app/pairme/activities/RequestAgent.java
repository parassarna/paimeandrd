package app.pairme.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.AgentData;
import app.pairme.ClassesPojo.Infodata;
import app.pairme.R;
import app.pairme.Utils;
import app.pairme.adapters.UpdatHeightAdapter;
import app.pairme.adapters.UpdatinfoAdapter;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.pairme.Utils.getArabic;
import static app.pairme.Utils.getImageUri;
import static app.pairme.Utils.getNationality;
import static app.pairme.Utils.getRealPathFromURI;

public class RequestAgent extends AppCompatActivity implements View.OnClickListener {

    CircleImageView iv_profile;
    Button btn_submit, btn_later;
    EditText et_username, et_city, et_zip, et_email, et_age, et_marriage_type, et_marital_status, et_have_children, et_education, et_Work,
            et_mobile, et_additional_information, et_R_marriage_type, et_R_age, et_R_country, et_R_city, et_R_marital_status, et_R_have_children, et_R_education, et_R_Work,
            et_color, et_weight, et_tall, et_R_weight, et_R_Tribed, et_notes, et_R_suburb;

    Uri uri;
    MaterialSpinner et_nationality, et_R_Nationality;
    File file, compressedImageFile;
    ImageView iv_choose_image;
    List<String> data = new ArrayList<>();
    RecyclerView recycler_view;
    ProgressBar progressBar_cyclic;
    Dialog dialog;
    String nationalities = "Choose from the list", r_nationality = "Choose from the list";
    boolean updated;
    HashMap<String, String> name = new HashMap<>();
    List<Infodata.Data> taller = new ArrayList<>();
    List<Infodata.Data> weight = new ArrayList<>();
    MultipartBody.Part part;
    List<Infodata.Data> lookingFor = new ArrayList<>();

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        nationalities = getString(R.string.chooselist);
        r_nationality = getString(R.string.chooselist);
        setContentView(R.layout.activity_request_agent);
        et_username = findViewById(R.id.et_username);
        et_nationality = findViewById(R.id.et_nationality);
        et_R_Nationality = findViewById(R.id.et_R_Nationality);
        et_city = findViewById(R.id.et_city);
        et_email = findViewById(R.id.et_email);
        et_R_suburb = findViewById(R.id.et_R_suburb);
        et_age = findViewById(R.id.et_age);
        et_marriage_type = findViewById(R.id.et_marriage_type);
        et_marital_status = findViewById(R.id.et_marital_status);
        et_have_children = findViewById(R.id.et_have_children);
        et_education = findViewById(R.id.et_education);
        et_Work = findViewById(R.id.et_Work);
        et_mobile = findViewById(R.id.et_mobile);
        et_additional_information = findViewById(R.id.et_additional_information);
        et_R_marriage_type = findViewById(R.id.et_R_marriage_type);
        et_R_age = findViewById(R.id.et_R_age);
        btn_later = findViewById(R.id.btn_later);
        et_R_country = findViewById(R.id.et_R_country);
        et_R_city = findViewById(R.id.et_R_city);
        et_R_marital_status = findViewById(R.id.et_R_marital_status);
        et_R_have_children = findViewById(R.id.et_R_have_children);
        et_R_education = findViewById(R.id.et_R_education);
        et_R_Work = findViewById(R.id.et_R_Work);
        et_color = findViewById(R.id.et_color);
        et_tall = findViewById(R.id.et_tall);
        et_R_weight = findViewById(R.id.et_R_weight);
        et_R_Tribed = findViewById(R.id.et_R_Tribed);
        et_notes = findViewById(R.id.et_notes);
        iv_profile = findViewById(R.id.iv_profile);
        btn_submit = findViewById(R.id.btn_submit);
        iv_choose_image = findViewById(R.id.iv_choose_image);
        btn_submit.setOnClickListener(this);
        iv_choose_image.setOnClickListener(this);
        btn_later.setOnClickListener(this);
        et_marriage_type.setOnClickListener(this);
        et_marital_status.setOnClickListener(this);
        et_have_children.setOnClickListener(this);
        et_education.setOnClickListener(this);
        et_Work.setOnClickListener(this);
        et_R_marriage_type.setOnClickListener(this);
        et_R_marital_status.setOnClickListener(this);
        et_R_have_children.setOnClickListener(this);
        et_R_education.setOnClickListener(this);
        et_R_Work.setOnClickListener(this);
        et_color.setOnClickListener(this);
        et_tall.setOnClickListener(this);
        et_R_weight.setOnClickListener(this);
        et_R_Tribed.setOnClickListener(this);
        Glide.with(getApplicationContext()).load("https://pairme.co/img/userProfile/" + Helper.getUserData(RequestAgent.this).getProfileImage()).error(Helper.getUserData(RequestAgent.this).getGender().equalsIgnoreCase("male") ? R.drawable.place_holder_m : R.drawable.place_holder_f).into(iv_profile);
        et_username.setText(Helper.getUserData(RequestAgent.this).getName());
        et_city.setText(Helper.getUserData(RequestAgent.this).getLivesIn());
        et_email.setText(Helper.getUserData(RequestAgent.this).getEmail());
        et_age.setText(Helper.getUserData(RequestAgent.this).getAge());
        et_marriage_type.setText(getArabic(RequestAgent.this, Helper.getUserData(RequestAgent.this).getLookingFor()));
        et_marital_status.setText(getArabic(RequestAgent.this, Helper.getUserData(RequestAgent.this).getRelationship()));
        et_have_children.setText(getArabic(RequestAgent.this, Helper.getUserData(RequestAgent.this).getHaveKids()));
        et_education.setText(getArabic(RequestAgent.this, Helper.getUserData(RequestAgent.this).getEducation()));
        et_Work.setText(getArabic(RequestAgent.this, Helper.getUserData(RequestAgent.this).getJobSector()));
        et_mobile.setText(Helper.getUserData(RequestAgent.this).getContact());
        et_additional_information.setText(Helper.getUserData(RequestAgent.this).getAboutMeContent());
        et_nationality.setItems(Arrays.asList(getResources().getStringArray(R.array.nationality)));
        et_nationality.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                nationalities = getNationality(position);
            }
        });
        et_R_Nationality.setItems(Arrays.asList(getResources().getStringArray(R.array.nationality)));
        et_R_Nationality.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                r_nationality = getNationality(position);

            }
        });
        setagentData();
        checkPerm();
        inflateTall();
        inflateWeight();
        for (int i = 0; i < Helper.getinfodata(RequestAgent.this).basic.lookingFor.size(); i++) {
            if (!Helper.getinfodata(RequestAgent.this).basic.lookingFor.get(i).name.equalsIgnoreCase("Socialising") && !Helper.getinfodata(RequestAgent.this).basic.lookingFor.get(i).name.equalsIgnoreCase("Online Friends"))
                lookingFor.add(Helper.getinfodata(RequestAgent.this).basic.lookingFor.get(i));
        }
    }

    private void inflateTall() {
        Infodata.Data infodata;
        int j = 0;
        for (int i = 120; i <= 220; i++) {
            infodata = new Infodata.Data();
            if (i == 120) {
                infodata.name = getResources().getString(R.string.not_set);
            } else
                infodata.name = String.valueOf(i + " cm");
            taller.add(infodata);
            j++;
        }
    }

    private void inflateWeight() {
        Infodata.Data infodata;
        int j = 0;
        for (int i = 19; i <= 200; i++) {
            infodata = new Infodata.Data();
            if (i == 19) {
                infodata.name = getResources().getString(R.string.not_set);
            } else
                infodata.name = String.valueOf(i + " kg");
            weight.add(infodata);
            j++;
        }
    }


    private void openImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(intent, 123);
    }

    public File getFile() {
        String[] filePath = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, filePath, null, null, null);
        cursor.moveToFirst();
        String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);
        file = new File(getRealPathFromURI(this, getImageUri(this, bitmap)));
        return file;
    }

    private void checkPerm() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (data != null) {
            if (requestCode == 123) {
                uri = data.getData();
                Helper.setImage(String.valueOf(uri), RequestAgent.this);
                file = getFile();
//                try {
//                    compressedImageFile = new Compressor(this).compressToFile(file);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
                iv_profile.setImageURI(uri);
            } else {
                Toast.makeText(getApplicationContext(), "Failed to Load Image", Toast.LENGTH_SHORT).show();
            }
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            Toast.makeText(this, "Select Image", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.iv_choose_image:
                openImage();
                break;
            case R.id.btn_later:
                savelater();
                Toast.makeText(this, R.string.agentdetails, Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_submit:
                if (et_username.getText().toString().isEmpty()) {
                    et_username.setError(getResources().getString(R.string.please_enter_required_field));
                    return;
                } else if (et_notes.getText().toString().isEmpty()) {
                    et_notes.setError(getResources().getString(R.string.please_enter_required_field));
                    return;
                } else if (et_notes.getText().toString().isEmpty()) {
                    et_notes.setError(getResources().getString(R.string.please_enter_required_field));
                    return;
                } else {
                    request();
                }
                break;
            case R.id.et_marriage_type:
                showDialog(getResources().getString(R.string.marriage_type), lookingFor, et_marriage_type);
                break;
            case R.id.et_marital_status:
                showDialog(getResources().getString(R.string.marital_status), Helper.getinfodata(RequestAgent.this).socialStatus.relStatus, et_marital_status);
                break;
            case R.id.et_have_children:
                showDialog(getResources().getString(R.string.have_children), Helper.getinfodata(RequestAgent.this).socialStatus.kidsList, et_have_children);
                break;
            case R.id.et_education:
                showDialog(getResources().getString(R.string.education), Helper.getinfodata(RequestAgent.this).socialStatus.educations, et_education);
                break;
            case R.id.et_Work:
                showDialog(getResources().getString(R.string.work), Helper.getinfodata(RequestAgent.this).socialStatus.jobSector, et_Work);
                break;
            case R.id.et_R_marriage_type:

                showDialog(getResources().getString(R.string.marriage_type), lookingFor, et_R_marriage_type);
                break;
            case R.id.et_R_marital_status:
                showDialog(getResources().getString(R.string.marital_status), Helper.getinfodata(RequestAgent.this).socialStatus.relStatus, et_R_marital_status);
                break;
            case R.id.et_R_have_children:
                showDialog(getResources().getString(R.string.have_children), Helper.getinfodata(RequestAgent.this).socialStatus.kidsList, et_R_have_children);
                break;
            case R.id.et_R_education:
                showDialog(getResources().getString(R.string.education), Helper.getinfodata(RequestAgent.this).socialStatus.educations, et_R_education);
                break;
            case R.id.et_R_Work:
                showDialog(getResources().getString(R.string.work), Helper.getinfodata(RequestAgent.this).socialStatus.jobSector, et_R_Work);
                break;
            case R.id.et_color:
                showDialog(getResources().getString(R.string.color), Helper.getinfodata(RequestAgent.this).appearance.hairColor, et_color);
                break;
            case R.id.et_tall:
                showHeightDialog(getResources().getString(R.string.tall), taller, et_tall);
                break;
            case R.id.et_R_weight:
                showHeightDialog(getResources().getString(R.string.weight), weight, et_R_weight);
                break;
            case R.id.et_R_Tribed:
                showDialog(getResources().getString(R.string.tribed_or_non_tribed), Helper.getinfodata(RequestAgent.this).socialStatus.kidsList, et_R_Tribed);
                break;

        }

    }

    private void setagentData() {
        nationalities = Helper.getUserData(RequestAgent.this).getOrginallyFrom();
        if (Helper.getagentdata(RequestAgent.this) != null) {
            et_R_marriage_type.setText(getArabic(RequestAgent.this, Helper.getagentdata(RequestAgent.this).marriage));
            et_R_age.setText(Helper.getagentdata(RequestAgent.this).age);
            et_R_country.setText(Helper.getagentdata(RequestAgent.this).country);
            et_R_city.setText(Helper.getagentdata(RequestAgent.this).city);
            et_R_suburb.setText(getArabic(RequestAgent.this, Helper.getagentdata(RequestAgent.this).suburb));
            et_R_marital_status.setText(getArabic(RequestAgent.this, Helper.getagentdata(RequestAgent.this).status));
            et_R_have_children.setText(getArabic(RequestAgent.this, Helper.getagentdata(RequestAgent.this).wantkids));
            et_R_education.setText(getArabic(RequestAgent.this, Helper.getagentdata(RequestAgent.this).education));
            et_R_Work.setText(getArabic(RequestAgent.this, Helper.getagentdata(RequestAgent.this).work));
            et_color.setText(getArabic(RequestAgent.this, Helper.getagentdata(RequestAgent.this).color));
            et_tall.setText(getArabic(RequestAgent.this, Helper.getagentdata(RequestAgent.this).tall));
            et_R_weight.setText(getArabic(RequestAgent.this, Helper.getagentdata(RequestAgent.this).weight));
            et_R_Tribed.setText(getArabic(RequestAgent.this, Helper.getagentdata(RequestAgent.this).tribed));
            et_notes.setText(Helper.getagentdata(RequestAgent.this).requirements);
            et_R_Nationality.setText(getArabic(RequestAgent.this, Helper.getagentdata(RequestAgent.this).nationality));
            r_nationality = Helper.getagentdata(RequestAgent.this).nationality;
        }
    }

    private void savelater() {
        AgentData agentData = new AgentData();
        agentData.marriage = et_R_marriage_type.getText().toString();
        agentData.age = et_R_age.getText().toString();
        agentData.country = et_R_country.getText().toString();
        agentData.nationality = r_nationality;
        agentData.city = et_R_city.getText().toString();
        agentData.suburb = et_R_suburb.getText().toString();
        agentData.status = et_R_marital_status.getText().toString();
        agentData.wantkids = et_R_have_children.getText().toString();
        agentData.education = et_R_education.getText().toString();
        agentData.work = et_R_Work.getText().toString();
        agentData.color = et_color.getText().toString();
        agentData.tall = et_tall.getText().toString();
        agentData.weight = et_R_weight.getText().toString();
        agentData.tribed = et_R_Tribed.getText().toString();
        agentData.requirements = et_notes.getText().toString();
        Helper.setagentData(new Gson().toJson(agentData), RequestAgent.this);
    }

    private void showDialog(final String header, final List<Infodata.Data> datas, final EditText editText) {
        Utils.selected = editText.getText().toString().trim();
        dialog = new Dialog(RequestAgent.this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_listview);
        TextView tv_header = dialog.findViewById(R.id.tv_header);
        tv_header.setText(header);
        Button btndialog = dialog.findViewById(R.id.btndialog);
        progressBar_cyclic = dialog.findViewById(R.id.progressBar_cyclic);
        progressBar_cyclic.setVisibility(View.GONE);
        btndialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        recycler_view = dialog.findViewById(R.id.listview);
        UpdatinfoAdapter interestsAdapter = new UpdatinfoAdapter(datas, new UpdatinfoAdapter.OnStatusListner() {
            @Override
            public void OnStatusClick(String p, String name) {
                RequestAgent.this.name.put(header, name);
                updated = true;
                editText.setText(p);
                dialog.dismiss();
            }

            @Override
            public void OnStatusmultClick(List<String> p, List<String> names) {

            }
        });
        recycler_view.setLayoutManager(new LinearLayoutManager(RequestAgent.this));
        recycler_view.setAdapter(interestsAdapter);
        dialog.show();
    }

    private void showHeightDialog(final String header, final List<Infodata.Data> datas, final EditText editText) {

        dialog = new Dialog(RequestAgent.this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_listview);
        TextView tv_header = dialog.findViewById(R.id.tv_header);
        tv_header.setText(header);
        Button btndialog = (Button) dialog.findViewById(R.id.btndialog);
        progressBar_cyclic = dialog.findViewById(R.id.progressBar_cyclic);
        progressBar_cyclic.setVisibility(View.GONE);
        btndialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        recycler_view = dialog.findViewById(R.id.listview);
        UpdatHeightAdapter interestsAdapter = new UpdatHeightAdapter(datas, new UpdatHeightAdapter.OnStatusListner() {
            @Override
            public void OnStatusClick(String p, String name) {
                RequestAgent.this.name.put(header, name);
                updated = true;
                editText.setText(p);
                dialog.dismiss();
            }

            @Override
            public void OnStatusmultClick(List<String> p, List<String> name) {

            }

        });
        recycler_view.setLayoutManager(new LinearLayoutManager(RequestAgent.this));
        recycler_view.setAdapter(interestsAdapter);
        dialog.show();
    }


    private void request() {
        Helper.showLoader(this, "Please wait...");
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        if (file != null) {
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            part = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
        } else {
            part = null;
        }
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), et_username.getText().toString().trim());
        RequestBody nationality = RequestBody.create(MediaType.parse("text/plain"), nationalities);
        RequestBody city = RequestBody.create(MediaType.parse("text/plain"), et_city.getText().toString());
        RequestBody age = RequestBody.create(MediaType.parse("text/plain"), et_age.getText().toString());
        RequestBody marriage_type = RequestBody.create(MediaType.parse("text/plain"), et_marriage_type.getText().toString());
        RequestBody marital_status = RequestBody.create(MediaType.parse("text/plain"), et_marital_status.getText().toString());
        RequestBody have_children = RequestBody.create(MediaType.parse("text/plain"), et_have_children.getText().toString());
        RequestBody education = RequestBody.create(MediaType.parse("text/plain"), et_education.getText().toString());
        RequestBody work = RequestBody.create(MediaType.parse("text/plain"), et_Work.getText().toString());
        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), et_email.getText().toString());
        RequestBody mobile = RequestBody.create(MediaType.parse("text/plain"), et_mobile.getText().toString().trim());
        RequestBody dditional_info = RequestBody.create(MediaType.parse("text/plain"), et_additional_information.getText().toString());
        RequestBody req_marriage_typ = RequestBody.create(MediaType.parse("text/plain"), et_marriage_type.getText().toString());
        RequestBody req_age = RequestBody.create(MediaType.parse("text/plain"), et_R_age.getText().toString());
        RequestBody req_nationality = RequestBody.create(MediaType.parse("text/plain"), r_nationality);
        RequestBody req_country = RequestBody.create(MediaType.parse("text/plain"), et_R_country.getText().toString());
        RequestBody req_city = RequestBody.create(MediaType.parse("text/plain"), et_R_city.getText().toString());
        RequestBody req_marital_stat = RequestBody.create(MediaType.parse("text/plain"), et_R_marital_status.getText().toString());
        RequestBody req_have_childre = RequestBody.create(MediaType.parse("text/plain"), et_R_have_children.getText().toString());
        RequestBody req_education = RequestBody.create(MediaType.parse("text/plain"), et_R_education.getText().toString());
        RequestBody req_work = RequestBody.create(MediaType.parse("text/plain"), et_R_Work.getText().toString());
        RequestBody suburb = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody color = RequestBody.create(MediaType.parse("text/plain"), et_color.getText().toString());
        RequestBody tall = RequestBody.create(MediaType.parse("text/plain"), et_tall.getText().toString());
        RequestBody weight = RequestBody.create(MediaType.parse("text/plain"), et_R_weight.getText().toString());
        RequestBody tribed_non_tribe = RequestBody.create(MediaType.parse("text/plain"), et_R_Tribed.getText().toString());
        RequestBody required_additional_info = RequestBody.create(MediaType.parse("text/plain"), et_additional_information.getText().toString());
        service.requestagent(part, name, nationality, city, age, marriage_type, marital_status, have_children, education, work, email, mobile, dditional_info, req_marriage_typ, req_age, req_nationality, req_country, req_city, req_marital_stat, req_have_childre, req_education, req_work, suburb, color, tall, weight, tribed_non_tribe, required_additional_info, "Bearer " + Helper.getToken(RequestAgent.this)).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Helper.hideLoader();
                if (response.isSuccessful()) {
                    if (response.body().get("success").getAsBoolean()) {
                        savelater();
                        Toast.makeText(RequestAgent.this, "Transaction SuccessFull", Toast.LENGTH_SHORT).show();
                        finish();

                    } else {
                        Toast.makeText(RequestAgent.this, "Transaction Failed", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(RequestAgent.this, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Helper.hideLoader();
                Toast.makeText(RequestAgent.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

}
