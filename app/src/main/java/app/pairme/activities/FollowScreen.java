package app.pairme.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;

import app.pairme.ApiServices.Helper;
import app.pairme.R;

public class FollowScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follow_screen);
        if (Helper.getLanguage(this).equalsIgnoreCase("ar")) {
            for (int i = 0; i <5; i++) {
                String id = "icon" + (i);
                int resID = getResources().getIdentifier(id, "id",this.getPackageName());
                ((ImageView) findViewById(resID)).setRotation(180);
            }
        }
    }
}
