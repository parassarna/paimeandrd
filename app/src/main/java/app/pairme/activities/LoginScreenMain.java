package app.pairme.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.iid.FirebaseInstanceId;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import net.steamcrafted.loadtoast.LoadToast;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.LoginPojo.LoginPojo;
import app.pairme.MainActivity;
import app.pairme.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.pairme.Utils.flip;

public class LoginScreenMain extends AppCompatActivity implements View.OnClickListener {
    EditText et_password, et_email;
    TextView tv_forgot_password, tv_error;
    Button btn_facebook, btn_login_inactive, btn_login_active, btn_twitter;
    ImageView show_password, iv_back, hide_password, close_iv;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    boolean enable;
    LinearLayout error_ll;
    RelativeLayout main_bg;
    LoadToast lt;
    ViewGroup root;
    String android_id;
    Window window;

    ProgressBar progressBar;
    private TwitterAuthClient client;
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String username = et_email.getText().toString().trim();
            String password = et_password.getText().toString().trim();
            if (!username.isEmpty() && !password.isEmpty()) {
                btn_login_inactive.setVisibility(View.GONE);
                btn_login_active.setVisibility(View.VISIBLE);
            } else {
                btn_login_inactive.setVisibility(View.VISIBLE);
                btn_login_active.setVisibility(View.GONE);
            }
            // btn_login_inactive.setEnabled(!username.isEmpty() && !password.isEmpty());

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen_main);
        TwitterCore.getInstance().getSessionManager().clearActiveSession();
        client = new TwitterAuthClient();
        android_id = FirebaseInstanceId.getInstance().getToken();
        initControl();
    }



    private void initControl() {
        et_email = findViewById(R.id.et_email);
        iv_back = findViewById(R.id.iv_back);
        et_password = findViewById(R.id.et_password);
        tv_forgot_password = findViewById(R.id.tv_forgot_password);
        show_password = findViewById(R.id.show_pass_btn);
        hide_password = findViewById(R.id.hide_pass_btn);
        btn_login_inactive = findViewById(R.id.btn_login_inactive);
        progressBar = findViewById(R.id.progress_bar);
        btn_login_active = findViewById(R.id.btn_login_active);
        btn_facebook = findViewById(R.id.btn_facebook);
        btn_twitter = findViewById(R.id.btn_twitter);
        main_bg = findViewById(R.id.main_bg);
        error_ll = findViewById(R.id.error_ll);
        close_iv = findViewById(R.id.close_iv);
        tv_error = findViewById(R.id.tv_error);
        tv_forgot_password.setOnClickListener(this);
        show_password.setOnClickListener(this);
        hide_password.setOnClickListener(this);
        btn_login_inactive.setOnClickListener(this);
        btn_login_active.setOnClickListener(this);
        btn_facebook.setOnClickListener(this);
        btn_twitter.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        close_iv.setOnClickListener(this);
        et_email.addTextChangedListener(textWatcher);
        et_password.addTextChangedListener(textWatcher);
        if (Helper.getLanguage(this).equalsIgnoreCase("ar"))
            flip(iv_back);

    }

    public void loginApi() {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<LoginPojo> call = service.loginApi(et_email.getText().toString(), et_password.getText().toString(), android_id, "android");
        call.enqueue(new Callback<LoginPojo>() {
            @Override
            public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    if (response.body().getData().confirmed.equalsIgnoreCase("1")) {
                        progressBar.setVisibility(View.GONE);
                        btn_login_active.setVisibility(View.GONE);
                        Helper.clearToken(LoginScreenMain.this);
                        Helper.setToken(response.body().getData().getToken(), LoginScreenMain.this);
                        Helper.setMyId(response.body().getData().getId(), LoginScreenMain.this);
                        Helper.setLogin(true, LoginScreenMain.this);
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        finishAffinity();
                    } else {
                        tv_error.setText(getResources().getString(R.string.verify_your_email));
                    }

                } else {
                    progressBar.setVisibility(View.GONE);
                    btn_login_active.setVisibility(View.VISIBLE);
                    tv_error.setText(response.body().getMessage());
                    slideUp(error_ll);
                    // Log.d("error", "else: " + response.body().getStatus());
                }

            }

            @Override
            public void onFailure(Call<LoginPojo> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                btn_login_active.setVisibility(View.VISIBLE);
                Log.d("error", "Failure; " + t);
                slideUp(error_ll);
                tv_error.setText(t.getMessage());
                Toast.makeText(LoginScreenMain.this, "Password mismatch", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.show_pass_btn:
                if (et_password.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())) {
                    ((ImageView) (view)).setImageResource(R.drawable.ic_hide_password);
                    //Show Password
                    et_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    ((ImageView) (view)).setImageResource(R.drawable.ic_show_password);
                    //Hide Password
                    et_password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
                break;
            case R.id.btn_login_active:
                if (TextUtils.isEmpty(et_email.getText().toString()) && TextUtils.isEmpty(et_password.getText().toString())) {
                    slideUp(error_ll);
                    tv_error.setText(getResources().getString(R.string.oops_login_is_required));
                } else if (TextUtils.isEmpty(et_email.getText().toString())) {
                    slideUp(error_ll);
                    tv_error.setText(getResources().getString(R.string.Enter_Username));
                } else if (TextUtils.isEmpty(et_password.getText().toString())) {
                    slideUp(error_ll);
                    tv_error.setText(getResources().getString(R.string.Enter_Password));
                } else {
                    btn_login_active.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                    loginApi();
                }
                break;
            case R.id.tv_forgot_password:
                startActivity(new Intent(getApplicationContext(), ForgotPasswordActivity.class));
                break;

            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_twitter:
                customLoginTwitter();
                break;
            case R.id.close_iv:
                slidedown(error_ll);
                break;

        }


    }

    public void customLoginTwitter() {
        //check if user is already authenticated or not
        client.authorize(this, new com.twitter.sdk.android.core.Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                // Do something with result, which provides a TwitterSession for making API calls
                TwitterSession twitterSession = result.data;

                //call fetch email only when permission is granted
                fetchTwitterEmail(twitterSession);
            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(LoginScreenMain.this, "Failed to authenticate. Please try again.", Toast.LENGTH_SHORT).show();
            }
        });

        //if user is not authenticated start authenticating
    }

    public void fetchTwitterEmail(final TwitterSession twitterSession) {
        client.requestEmail(twitterSession, new com.twitter.sdk.android.core.Callback<String>() {
            @Override
            public void success(Result<String> result) {
                Log.d("tag", "success: " + "User Id : " + twitterSession.getUserId() + "\nScreen Name : " + twitterSession.getUserName() + "\nEmail Id : " + result.data);
            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(LoginScreenMain.this, "Failed to authenticate. Please try again.", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void slideUp(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this,
                R.anim.slide_up));
        view.setVisibility(View.VISIBLE);
        changeStatusBarColor();
        main_bg.setBackgroundColor(getResources().getColor(R.color.error_color));
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(500);
        }

    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.error_color));
        }
    }

    public void slidedown(View view) {

        view.startAnimation(AnimationUtils.loadAnimation(this,
                R.anim.slide_down));
        view.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                main_bg.setBackground(getResources().getDrawable(R.drawable.background_screen));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    window = getWindow();
                    window.setStatusBarColor(getResources().getColor(R.color.start_color));
                }
            }
        }, 1000);

    }


}
