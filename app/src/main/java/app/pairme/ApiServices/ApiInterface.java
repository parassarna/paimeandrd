package app.pairme.ApiServices;


import com.google.gson.JsonObject;

import java.util.List;

import app.pairme.ClassesPojo.AllUserPojo.AllUserPojo;
import app.pairme.ClassesPojo.BankAccountsPojo.BankAccountsPojo;
import app.pairme.ClassesPojo.BankTransferPojo;
import app.pairme.ClassesPojo.BlockListPojo.BlockListPojo;
import app.pairme.ClassesPojo.BlockPojo;
import app.pairme.ClassesPojo.ChatListPojo.ChatListPojo;
import app.pairme.ClassesPojo.CheckEmailPojo.CheckEmailPojo;
import app.pairme.ClassesPojo.DescriptionPojo.DescriptionPojo;
import app.pairme.ClassesPojo.EmailPojo;
import app.pairme.ClassesPojo.FilterPojo;
import app.pairme.ClassesPojo.Infodata;
import app.pairme.ClassesPojo.LikedislikePojo;
import app.pairme.ClassesPojo.LoginPojo.LoginPojo;
import app.pairme.ClassesPojo.Matches;
import app.pairme.ClassesPojo.Packages;
import app.pairme.ClassesPojo.RegisterPojo.RegisterPojo;
import app.pairme.ClassesPojo.RequestPojo;
import app.pairme.ClassesPojo.SendMessagePojo.SendMessagePojo;
import app.pairme.ClassesPojo.UpdateName;
import app.pairme.ClassesPojo.UploadAlbumPojos;
import app.pairme.ClassesPojo.UploadImagePojo.UploadImagePojo;
import app.pairme.ClassesPojo.UserInfoPojo.UserInfo;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {


    @FormUrlEncoded
    @POST("login")
    Call<LoginPojo> loginApi(@Field("email") String email, @Field("password") String password, @Field("device_token") String device_token, @Field("device_type") String device_type);

    @FormUrlEncoded
    @POST("register")
    Call<RegisterPojo> registerApi(@Field("name") String name, @Field("email") String email, @Field("age") String age, @Field("lives_in") String lives_in,
                                   @Field("looking_for") String looking_for, @Field("password") String password, @Field("nationality") String orginally_from,
                                   @Field("gender") String gender, @Field("subscription_status") String subscription_status, @Field("device_token") String device_token,
                                   @Field("device_type") String device_type);

    @FormUrlEncoded
    @POST("socialLogin")
    Call<UserInfo> socialLogin(@Field("name") String name, @Field("age_range") String age_range, @Field("email") String email,
                               @Field("password") String password,
                               @Field("profile_image") String profile_image,
                               @Field("type") int type,
                               @Field("gender") String gender);

    @FormUrlEncoded
    @POST("addDescription")
    Call<DescriptionPojo> addDescription(@Header("Authorization") String auth, @Field("about_content") String about_content,
                                         @Field("looking_for_content") String looking_for_content);

    @FormUrlEncoded
    @POST("updateEmail")
    Call<CheckEmailPojo> updadeEmail(@Header("Authorization") String auth, @Field("email") String email,
                                     @Field("email2") String email2);

    @FormUrlEncoded
    @POST("checkEmail")
    Call<EmailPojo> checkEmail(@Field("email") String email);

    @FormUrlEncoded
    @POST("forgetPassword")
    Call<EmailPojo> forgetPassword(@Field("email") String email);

    @POST("allUsersList")
    Call<AllUserPojo> getAllList(@Header("Authorization") String auth);

    @GET("users")
    Call<ChatListPojo> getChatUsers(@Path("Authorization") String auth);

    @GET("localisation")
    Call<JsonObject> localisation();

    @POST("searchuserList")
    Call<AllUserPojo> searchList(@Header("Authorization") String auth);


    @FormUrlEncoded
    @POST("likeDislike")
    Call<LikedislikePojo> likedislike(@Header("Authorization") String auth, @Field("id") String id, @Field("status") int status);

    @FormUrlEncoded
    @POST("updateName")
    Call<UpdateName> updateName(@Header("Authorization") String auth, @Field("name") String name, @Field("date") String date);

    @FormUrlEncoded
    @POST("contactUs")
    Call<DescriptionPojo> contactUS(@Header("Authorization") String auth, @Field("id") String id, @Field("name") String name, @Field("email") String email, @Field("topic") String topic, @Field("message") String message);

    @FormUrlEncoded
    @POST("details")
    Call<UserInfo> getUserInfo(@Header("Authorization") String auth, @Field("id") String id);

    @FormUrlEncoded
    @POST("allblockedUsers")
    Call<BlockListPojo> getBlockedUser(@Header("Authorization") String auth, @Field("id") String id);

    @FormUrlEncoded
    @POST("blockUser")
    Call<BlockPojo> blockUser(@Header("Authorization") String auth, @Field("id") String id);


    @FormUrlEncoded
    @POST("unblockUser")
    Call<BlockPojo> unblockUser(@Header("Authorization") String auth, @Field("id") String id);

    @FormUrlEncoded
    @POST("checkedProfile")
    Call<JsonObject> checkedProfile(@Header("Authorization") String auth, @Field("id") String id);


    @POST("requestuser")
    Call<RequestPojo> requestsData(@Header("Authorization") String auth);

    @GET("getDropdown")
    Call<Infodata> getSearchlist(@Query("lang") String latLng);

    @POST("feed")
    Call<JsonObject> getActivity(@Header("Authorization") String auth);

    @POST("matchesList")
    Call<Matches> getmatchesList(@Header("Authorization") String auth);

    @FormUrlEncoded
    @POST("deleteChat")
    Call<JsonObject> deletechat(@Header("Authorization") String auth, @Field("id") String id);

    @POST("getFilter")
    Call<FilterPojo> getFilter(@Header("Authorization") String auth);

    @POST("deletefilter")
    Call<JsonObject> deleteFilter(@Header("Authorization") String auth);

    @GET("/maps/api/geocode/json?sensor=true&result_type=country|locality")
    Call<JsonObject> getLocation(@Query("latlng") String latLng,
                                 @Query("key") String key);

    @Multipart
    @POST("uploadAlbum")
    Call<UploadAlbumPojos> uploadAlbum(@Part List<MultipartBody.Part> Image, @Header("Authorization") String auth);

    @Multipart
    @POST("uploadDp")
    Call<UploadImagePojo> uploadProfileImage(@Part MultipartBody.Part file, @Header("Authorization") String auth);

    @FormUrlEncoded
    @POST("updateSocialStatus")
    Call<DescriptionPojo> addSocialStatus(@Header("Authorization") String auth, @Field("education") String education,
                                          @Field("occupation") String occupation, @Field("sector") String sector,
                                          @Field("relationship") String relationship, @Field("have_kids") String have_kids,
                                          @Field("want_kids") String want_kids, @Field("living_situation") String living_situation, @Field("relocate") String relocate);


    @FormUrlEncoded
    @POST("updateAppearance")
    Call<DescriptionPojo> updateAppearance(@Header("Authorization") String auth, @Field("appearance") String appearance,
                                           @Field("body_type") String body_type, @Field("height") String height,
                                           @Field("complexion") String complexion, @Field("health_status") String health_status,
                                           @Field("hair_color") String hair_color, @Field("hair_length") String hair_length,
                                           @Field("hair_type") String hair_type, @Field("eye_color") String eye_color,
                                           @Field("eye_wear") String eye_wear, @Field("secret_weapon") String secret_weapon, @Field("makeup") String makeup);

    @FormUrlEncoded
    @POST("updateLifestyle")
    Call<DescriptionPojo> updateLifestyle(@Header("Authorization") String auth, @Field("smoking") String smoking,
                                          @Field("eating_habits") String Eating_habits, @Field("exercise_habits") String Exercise_habits,
                                          @Field("sleep_habits") String Sleep_habits, @Field("pets") String Pets,
                                          @Field("family_values") String Family_values, @Field("polygamy_opinion") String Polygamy_opinion,
                                          @Field("personality") String personality, @Field("languages") String languages);

    @FormUrlEncoded
    @POST("updateInterest")
    Call<DescriptionPojo> updateInterest(@Header("Authorization") String auth, @Field("hobbies") String hobbies,
                                         @Field("cuisine") String cuisine, @Field("music") String music,
                                         @Field("movies") String movies, @Field("sports") String sports);

    @FormUrlEncoded
    @POST("changePassword")
    Call<DescriptionPojo> changePassword(@Header("Authorization") String auth, @Field("password") String password,
                                         @Field("change_password") String change_password);

    @FormUrlEncoded
    @POST("updateEmail")
    Call<DescriptionPojo> updateEmail(@Header("Authorization") String auth, @Field("email") String password,
                                      @Field("newemail") String change_password);

    @FormUrlEncoded
    @POST("updateToken")
    Call<JsonObject> updatetoken(@Header("Authorization") String auth, @Field("token") String password);

    @FormUrlEncoded
    @POST("updateBasics")
    Call<DescriptionPojo> updateBasics(@Header("Authorization") String auth, @Field("age") String age,
                                       @Field("looking_for") String looking_for, @Field("location") String location,
                                       @Field("origin") String origin);

    @FormUrlEncoded
    @POST("searchFilter")
    Call<JsonObject> searchfilter(@Header("Authorization") String auth, @Field("checkboxOnline") String checkboxOnline,
                                  @Field("search_sort") String search_sort, @Field("filterPhotos") String filterPhotos,
                                      @Field("ageMin") String ageMin, @Field("ageMax") String ageMax,
                                  @Field("lookingFor") String lookingFor, @Field("education") String education,
                                  @Field("occupation") String occupation, @Field("sector") String sector, @Field("status") String status,
                                  @Field("relationship") String relationship, @Field("have_kids") String have_kids,
                                  @Field("living_situation") String living_situation, @Field("want_kids") String want_kids, @Field("lives_in") String lives_in,
                                  @Field("search-relocate") String search_relocate, @Field("search-appearance") String search_appearance,
                                  @Field("search-body_type") String search_body_type,
                                  @Field("search-disabilities") String search_disabilities,
                                  @Field("search-hair_color") String search_hair_color, @Field("search-hair_length") String search_hair_length,
                                  @Field("search-eye_color") String search_eye_color, @Field("search-eye_wear") String search_eye_wear,
                                  @Field("search-makeup") String search_makeup, @Field(" search-secret_weapon") String search_secret_weapon,
                                  @Field("search-style") String search_style,
                                  @Field("search-complexion") String search_complexion, @Field("originally_from") String originally_from,
                                  @Field("name") String name, @Field("height") String height, @Field("distance") String distance,
                                  @Field("health_status") String health_status, @Field("hair_type") String hair_type, @Field("facial_hair") String facial_hair,
                                  @Field("smoking") String smoking, @Field("eating_habits") String eating_habits, @Field("exercise_habits") String exercise_habits,
                                  @Field("sleep_habits") String sleep_habits, @Field("pets") String pets, @Field("family_values") String family_values,
                                  @Field("polygamy") String polygamy, @Field("personality") String personality, @Field("language") String language,
                                  @Field("hobbies") String hobbies, @Field("cuisine") String cuisine, @Field("music") String music, @Field("movies") String movies,
                                  @Field("sports") String sports);

    @FormUrlEncoded
    @POST("send")
    Call<SendMessagePojo> sendMessage(@Header("Authorization") String auth, @Field("sender_id") String sender_id, @Field("receiver_id") String receiver_id, @Field("message") String message);


    @POST("bankTransfer")
    @Multipart
    Call<BankTransferPojo> Banktransfer(@Header("Authorization") String auth, @Part MultipartBody.Part file, @Part("user_id") RequestBody userid,
                                        @Part("package_id") RequestBody package_id, @Part("transfer_method") RequestBody transfer_method,
                                        @Part("applicant_name") RequestBody applicant_name,
                                        @Part("deposit_value") RequestBody deposit_value, @Part("deposit_date") RequestBody deposit_date,
                                        @Part("bank_name") RequestBody bank_name, @Part("account_no_transfer_from") RequestBody account_no_transfer_from,
                                        @Part("notes") RequestBody notes);


    @FormUrlEncoded
    @POST("onlinetransfer")
    Call<JsonObject> OnlineTransfer(@Field("user_id") String userid, @Field("package_id") String package_id, @Field("transfer_method") String transfer_method, @Field("deposit_value") String deposit_value, @Field("transaction_id") String transactionId, @Header("Authorization") String auth);

    @FormUrlEncoded
    @POST("getStripeClient")
    Call<JsonObject> getStripeClient(@Field("amount") String amount, @Header("Authorization") String auth);

    @FormUrlEncoded
    @POST("delAlbumImage")
    Call<JsonObject> delAlbumImage(@Field("image_id") String image_id, @Header("Authorization") String auth);

    @GET("getPackages")
    Call<Packages> getPackages();

    @GET("bankAccountDetails")
    Call<BankAccountsPojo> getBankAccounts(@Header("Authorization") String auth);

    @POST("requestagent")
    @Multipart
    Call<JsonObject> requestagent(
            @Part MultipartBody.Part file,
            @Part("name") RequestBody name,
            @Part("nationality") RequestBody nationality,
            @Part("city") RequestBody city,
            @Part("age") RequestBody age,
            @Part("marriage_type") RequestBody marriage_type,
            @Part("marital_status") RequestBody marital_status,
            @Part("have_children") RequestBody have_children,
            @Part("education") RequestBody education,
            @Part("work") RequestBody work,
            @Part("email") RequestBody email,
            @Part("mobile") RequestBody mobile,
            @Part("additional_info") RequestBody dditional_info,
            @Part("req_marriage_typ") RequestBody req_marriage_typ,
            @Part("req_age") RequestBody req_age,
            @Part("req_nationality") RequestBody req_nationality,
            @Part("req_country") RequestBody req_country,
            @Part("req_city") RequestBody req_city,
            @Part("req_marital_stat") RequestBody req_marital_stat,
            @Part("req_have_childre") RequestBody req_have_childre,
            @Part("req_education") RequestBody req_education,
            @Part("req_work") RequestBody req_work,
            @Part("suburb") RequestBody suburb,
            @Part("color") RequestBody color,
            @Part("tall") RequestBody tall,
            @Part("weight") RequestBody weight,
            @Part("tribed_non_tribe") RequestBody tribed_non_tribe,
            @Part("required_additional_info") RequestBody required_additional_info,
            @Header("Authorization") String auth);


}



