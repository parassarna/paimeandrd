package app.pairme.ApiServices;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;

import app.pairme.ClassesPojo.AgentData;
import app.pairme.ClassesPojo.Infodata;
import app.pairme.ClassesPojo.UserInfoPojo.Data;


public class Helper {
    public static final String LookingFor = "LookingFor";
    private static final String PREF_NAME = "GarPref";
    private static final String IS_LOGIN = "IsLoggedIn";
    private static final String IS_Data = "IS_Data";
    private static final String KEY_NAME = "username";
    private static final String KEY_Email = "phone";
    private static final String KEY_UID = "uid";
    private static final String USER = "user";
    private static final String AGENT = "AGENT";
    public static KProgressHUD kProgressHUD;
    public static ProgressBar progressBar;
    private static Context scontext;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    String user = " ";
    int PRIVATE_MODE = 0;

    public Helper(Context context) {
        this.scontext = context;
        pref = scontext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public static void showLoader(Context context, String message) {
        kProgressHUD = new KProgressHUD(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .show();
    }

    public static void hideLoader() {
        if (kProgressHUD != null)
            kProgressHUD.dismiss();

    }

    public static void showProgress(Activity context) {
        progressBar = new ProgressBar(context);
        progressBar.setVisibility(View.VISIBLE);
    }

    public static void hideProgress() {
        progressBar.setVisibility(View.GONE);

    }


    public static void toast(Context mContext, String Message) {
        Toast.makeText(mContext, Message, Toast.LENGTH_SHORT).show();
    }

    public static void setLogin(boolean login, Context sActivity) {
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(IS_LOGIN, login);
        editor.apply();
    }

    public static void setinfodata(String filterPojo, Context sActivity) {
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(IS_Data, filterPojo);
        editor.apply();
    }

    public static Infodata.Datum getinfodata(Context sActivity) {
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        return new Gson().fromJson(pref.getString(IS_Data, ""), Infodata.Datum.class);
    }

    public static AgentData getagentdata(Context sActivity) {
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        return new Gson().fromJson(pref.getString(AGENT, ""),AgentData.class);
    }

    public static boolean getLogin(Context context) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
        return pref.getBoolean(IS_LOGIN, false);
    }

    public static void setUserData(String data, Context sActivity) {
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(USER, data);
        editor.apply();
    }

    public static void setagentData(String data, Context sActivity) {
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(AGENT, data);
        editor.apply();
    }


    public static void setFirstName(String FirstName, Context sActivity) {
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("FirstName", FirstName);
        editor.apply();
    }


    public static String getFirstName(Context context) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
        return pref.getString("FirstName", "");
    }

    public static void clearName(Context context) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.remove("FirstName");
        editor.apply();
    }

    public static void clearLanguage(Context context) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.remove("language");
        editor.apply();
    }

    public static String setGender(String Gender, Context sActivity) {
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("Gender", Gender);
        editor.apply();
        return Gender;
    }

    public static String getGender(Context context) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
        return pref.getString("Gender", "");
    }

    public static void clearGender(Context context) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.remove("Gender");
        editor.apply();
    }

    public static void setImage(String Image, Context sActivity) {
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("Image", Image);
        editor.apply();

    }

    public static String getEmail(Context context) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
        return pref.getString("Email", "");
    }

    public static void setEmail(String Email, Context sActivity) {
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("Email", Email);
        editor.apply();
    }

    public static void clearEmail(Context context) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.remove("Email");
        editor.apply();
    }

    public static String getAge(Context context) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
        return pref.getString("Age", "");
    }

    public static void setAge(String Age, Context sActivity) {
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("Age", Age);
        editor.apply();
    }


    public static String getLocation(Context context) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
        return pref.getString("Location", "");
    }

    public static void setLocation(String Location, Context sActivity) {
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("Location", Location);
        editor.apply();
    }


    public static String getOrgLocation(Context context) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
        return pref.getString("OrgLocation", "");
    }

    public static void setOrgLocation(String Location, Context sActivity) {
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("OrgLocation", Location);
        editor.apply();
    }


    public static String getPassword(Context context) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
        return pref.getString("Password", "");
    }

    public static void setPassword(String Password, Context sActivity) {
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("Password", Password);
        editor.apply();
    }

    public static void clearPassword(Context context) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.remove("Password");
        editor.apply();
    }

    public static String getLooking(Context context) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
        return pref.getString("Looking", "");
    }

    public static void setLooking(String Looking, Context sActivity) {
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("Looking", Looking);
        editor.apply();
    }

    public static void clearLooking(Context context) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.remove("Marriage").remove("Marriage Misyar").remove("Marriage Polygamy").remove("Socialising").remove("Online Friends");
        editor.apply();
    }


    public static void setId(String Id, Context sActivity) {
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("Id", Id);
        editor.apply();

    }

    public static String getId(Context context) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
        return pref.getString("MyId", "");
    }


    public static void setToken(String token, Context sActivity) {
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("token", token);
        editor.apply();
    }

    public static String getToken(Context context) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
        return pref.getString("token", null);
    }

    public static void setMyId(String MyId, Context sActivity) {
        Log.d("tag", "setMyId: " + MyId);
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("MyId", MyId);
        editor.apply();
    }

    public static String getMyId(Context context) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
        return pref.getString("MyId", "");
    }

    public static void clearToken(Context context) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.remove("token");
        editor.apply();
    }

    public static Data getUserData(Context sActivity) {
        SharedPreferences pref = sActivity.getSharedPreferences("MyPref", 0);
        return new Gson().fromJson(pref.getString(USER, ""), Data.class);
    }

    public static void Logout() {
        SharedPreferences pref = scontext.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        clearToken(scontext);
        setLogin(false, scontext);
        editor.apply();

    }


    public static void setLanguage(Context context, String language) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("language", language);
        editor.apply();
    }

    public static String getLanguage(Context context) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
        return pref.getString("language", "ar");
    }
}
