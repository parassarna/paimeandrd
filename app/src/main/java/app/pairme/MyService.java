package app.pairme;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class MyService extends Service {

    private static final int NOTIFICATION_ID = 1;
    String channelname = "Pairme";
    MyApplication app;
    JSONObject jsonObject;
    NotificationCompat.Builder notification;
    private boolean mRunning = false;
    private Thread mThread;
    private Socket mSocket;
    private InputStream mInputStream;
    private Emitter.Listener onNotification = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            JSONObject data = (JSONObject) args[0];
            Log.d("tag", "run:notification " + data);
            jsonObject = new JSONObject();
            try {
                jsonObject.put("type", "text");
                jsonObject.put("user_id", "137");
                jsonObject.put("message", "hi pulkit rana");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Notification notification = new NotificationCompat.Builder(this, channelname)
                .setContentTitle("")
                .setContentText("").build();
        startForeground(NOTIFICATION_ID, notification);
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        app = (MyApplication) getApplication();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            makeNotificationChannel(channelname, channelname, NotificationManager.IMPORTANCE_DEFAULT);
        }
        mSocket = app.getSocket();
        mSocket.connect();
        mSocket.on("connect", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                setNotificationMessage("Connected", "Pairme");
            }

        });
        mSocket.on("getnotification", onNotification);
       // mSocket.on("getnotification", onNotification);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    void makeNotificationChannel(String id, String name, int importance) {
        NotificationChannel channel = new NotificationChannel(id, name, importance);
        channel.setShowBadge(true); // set false to disable badges, Oreo exclusive

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        assert notificationManager != null;
        notificationManager.createNotificationChannel(channel);
    }

    void setNotificationMessage(String message, String title) {

        // make the channel. The method has been discussed before.

        // the check ensures that the channel will only be made
        // if the device is running Android 8+

        notification =
                new NotificationCompat.Builder(this, channelname);
        // the second parameter is the channel id.
        // it should be the same as passed to the makeNotificationChannel() method

        notification
                .setSmallIcon(R.drawable.app_icon) // can use any other icon
                .setContentTitle(title)
                .setContentText(message); // this shows a number in the notification dots

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        assert notificationManager != null;
        notificationManager.notify((int) System.currentTimeMillis(), notification.build());
        // it is better to not use 0 as notification id, so used 1.
    }


    @Override
    public void onDestroy() {
        Toast.makeText(app, "destroy", Toast.LENGTH_SHORT).show();
        super.onDestroy();
        Intent broadcastIntent = new Intent("Restart");
        sendBroadcast(broadcastIntent);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.i("tag", "serviceonTaskRemoved()");


        // workaround for kitkat: set an alarm service to trigger service again
        Intent intent = new Intent(getApplicationContext(), MyService.class);

        Intent broadcastIntent = new Intent("Restart");
        sendBroadcast(broadcastIntent);
        super.onTaskRemoved(rootIntent);

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.i("tag", "onLowMemory()");
    }
}
