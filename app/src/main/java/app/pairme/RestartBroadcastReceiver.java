package app.pairme;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import static com.facebook.FacebookSdk.getApplicationContext;

public class RestartBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("tag", "Service Stops, let's restart again.");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            context.startForegroundService(new Intent(getApplicationContext(), MyService.class));
        else context.startService(new Intent(getApplicationContext(), MyService.class));
    }
}