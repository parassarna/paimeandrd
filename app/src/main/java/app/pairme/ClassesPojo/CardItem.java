package app.pairme.ClassesPojo;

import java.io.Serializable;
import java.util.List;

public class CardItem implements Serializable {
    public int counter = 0;
    List<String> photoAlbums;
    private String image, name, age, location, fb_id, job_title, company, school, birthday, gender, about, swipe;
    public CardItem(List<String> photoAlbums, String image, String name, String age, String location, String fb_id, String job_title, String company, String school, String birthday, String gender, String about, String swipe) {
        this.image = image;
        this.name = name;
        this.age = age;
        this.location = location;
        this.fb_id = fb_id;
        this.job_title = job_title;
        this.company = company;
        this.school = school;
        this.birthday = birthday;
        this.gender = gender;
        this.about = about;
        this.swipe = swipe;
        this.photoAlbums = photoAlbums;
    }

    public List<String> getPhotoAlbums() {
        return photoAlbums;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getFb_id() {
        return fb_id;
    }

    public void setFb_id(String fb_id) {
        this.fb_id = fb_id;
    }

    public String getJob_title() {
        return job_title;
    }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getSwipe() {
        return swipe;
    }

    public void setSwipe(String swipe) {
        this.swipe = swipe;
    }


    /* public CardItem(String image, String name, String age, String location) {
        this.image = image;
        this.name = name;
        this.age = age;
        this.location = location;
    }*/

  /*  public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }*/
}
