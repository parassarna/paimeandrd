package app.pairme.ClassesPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FilterPojo implements Serializable {

    private final static long serialVersionUID = 7046708218347142501L;
    @SerializedName("success")
    @Expose
    public Integer success;
    @SerializedName("subscription_status")
    @Expose
    public Integer subscription_status;
    @SerializedName("data")
    @Expose
    public Data data;

    public class Data implements Serializable {

        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("user_id")
        @Expose
        public String userId;
        @SerializedName("filterPhotos")
        @Expose
        public String filterPhotos;
        @SerializedName("search_sort")
        @Expose
        public String searchSort;
        @SerializedName("ageMin")
        @Expose
        public String ageMin;
        @SerializedName("ageMax")
        @Expose
        public String ageMax;
        @SerializedName("lookingFor")
        @Expose
        public String lookingFor;
        @SerializedName("education")
        @Expose
        public String education;
        @SerializedName("occupation")
        @Expose
        public String occupation;
        @SerializedName("created_at")
        @Expose
        public String createdAt;
        @SerializedName("updated_at")
        @Expose
        public String updatedAt;
        @SerializedName("sector")
        @Expose
        public String sector;
        @SerializedName("status")
        @Expose
        public String status;
        @SerializedName("relationship")
        @Expose
        public String relationship;
        @SerializedName("have_kids")
        @Expose
        public String haveKids;
        @SerializedName("want_kids")
        @Expose
        public String wantKids;
        @SerializedName("living_situation")
        @Expose
        public String livingSituation;
        @SerializedName("search-relocate")
        @Expose
        public String searchRelocate;
        @SerializedName("search-appearance")
        @Expose
        public String searchAppearance;
        @SerializedName("search-body_type")
        @Expose
        public String searchBodyType;
        @SerializedName("search-complexion")
        @Expose
        public String searchComplexion;
        @SerializedName("search-disabilities")
        @Expose
        public String searchDisabilities;
        @SerializedName("search-hair_color")
        @Expose
        public String searchHairColor;
        @SerializedName("search-hair_length")
        @Expose
        public String searchHairLength;
        @SerializedName("search-eye_color")
        @Expose
        public String searchEyeColor;
        @SerializedName("search-eye_wear")
        @Expose
        public String searchEyeWear;
        @SerializedName("search-makeup")
        @Expose
        public String searchMakeup;
        @SerializedName("search-secret_weapon")
        @Expose
        public String searchSecretWeapon;
        @SerializedName("search-style")
        @Expose
        public String searchStyle;
        @SerializedName("lives_in")
        @Expose
        public String livesIn;
        @SerializedName("originally_from")
        @Expose
        public String originallyFrom;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("height")
        @Expose
        public String height;
        @SerializedName("distance")
        @Expose
        public String distance;
        @SerializedName("health_status")
        @Expose
        public String healthStatus;
        @SerializedName("hair_type")
        @Expose
        public String hairType;
        @SerializedName("facial_hair")
        @Expose
        public String facialHair;
        @SerializedName("smoking")
        @Expose
        public String smoking;
        @SerializedName("online")
        @Expose
        public String online;
        @SerializedName("eating_habits")
        @Expose
        public String eatingHabits;
        @SerializedName("exercise_habits")
        @Expose
        public String exerciseHabits;
        @SerializedName("sleep_habits")
        @Expose
        public String sleepHabits;
        @SerializedName("pets")
        @Expose
        public String pets;
        @SerializedName("family_values")
        @Expose
        public String familyValues;
        @SerializedName("polygamy")
        @Expose
        public String polygamy;
        @SerializedName("personality")
        @Expose
        public String personality;
        @SerializedName("language")
        @Expose
        public String language;
        @SerializedName("hobbies")
        @Expose
        public String hobbies;
        @SerializedName("cuisine")
        @Expose
        public String cuisine;
        @SerializedName("music")
        @Expose
        public String music;
        @SerializedName("movies")
        @Expose
        public String movies;
        @SerializedName("sports")
        @Expose
        public String sports;

    }
}
