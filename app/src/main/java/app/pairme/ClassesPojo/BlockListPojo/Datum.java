package app.pairme.ClassesPojo.BlockListPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Datum implements Serializable {

    private final static long serialVersionUID = -3820663500707194498L;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("userage")
    @Expose
    private String userage;
    @SerializedName("userlivein")
    @Expose
    private String userlivein;
    @SerializedName("userimage")
    @Expose
    private String userimage;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("who_blocked")
    @Expose
    private String whoBlocked;
    @SerializedName("whom_blocked")
    @Expose
    private String whomBlocked;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("update_at")
    @Expose
    private String updateAt;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserage() {
        return userage;
    }

    public void setUserage(String userage) {
        this.userage = userage;
    }

    public String getUserlivein() {
        return userlivein;
    }

    public void setUserlivein(String userlivein) {
        this.userlivein = userlivein;
    }

    public String getUserimage() {
        return userimage;
    }

    public void setUserimage(String userimage) {
        this.userimage = userimage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWhoBlocked() {
        return whoBlocked;
    }

    public void setWhoBlocked(String whoBlocked) {
        this.whoBlocked = whoBlocked;
    }

    public String getWhomBlocked() {
        return whomBlocked;
    }

    public void setWhomBlocked(String whomBlocked) {
        this.whomBlocked = whomBlocked;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }
}
