package app.pairme.ClassesPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Infodata {
    @SerializedName("success")
    @Expose
    public Integer success;
    @SerializedName("data")
    @Expose
    public Datum data;

    public static class Data implements Serializable {

        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("create_date")
        @Expose
        public String createDate;
        @SerializedName("status")
        @Expose
        public String status;
    }

    public class Interests {

        @SerializedName("hobbies")
        @Expose
        public List<Data> hobbies = null;
        @SerializedName("cuisines")
        @Expose
        public List<Data> cuisines = null;
        @SerializedName("musics")
        @Expose
        public List<Data> musics = null;
        @SerializedName("movies")
        @Expose
        public List<Data> movies = null;
        @SerializedName("sports")
        @Expose
        public List<Data> sports = null;

    }

    public class Lifestyle {
        @SerializedName("smoking")
        @Expose
        public List<Data> smoking = null;
        @SerializedName("eating_habits")
        @Expose
        public List<Data> eatingHabits = null;
        @SerializedName("exercise_habits")
        @Expose
        public List<Data> exerciseHabits = null;
        @SerializedName("pets")
        @Expose
        public List<Data> pets = null;
        @SerializedName("family_value")
        @Expose
        public List<Data> familyValue = null;
        @SerializedName("polygamy_opinion")
        @Expose
        public List<Data> polygamyOpinion = null;
        @SerializedName("languages")
        @Expose
        public List<Data> languages = null;
        @SerializedName("personality")
        @Expose
        public List<Data> personality = null;
        @SerializedName("sleep_habits")
        @Expose
        public List<Data> sleepHabbits = null;


    }

    public class Appearance {
        public final static long serialVersionUID = 1951408130139610994L;
        @SerializedName("appearance")
        @Expose
        public List<Data> appearance = null;
        @SerializedName("facial_hair")
        @Expose
        public List<Data> facials = null;
        @SerializedName("body_type")
        @Expose
        public List<Data> bodyType = null;
        @SerializedName("complexion")
        @Expose
        public List<Data> complexion = null;
        @SerializedName("health_status")
        @Expose
        public List<Data> healthStatus = null;
        @SerializedName("hair_color")
        @Expose
        public List<Data> hairColor = null;
        @SerializedName("hair_length")
        @Expose
        public List<Data> hairLength = null;
        @SerializedName("hair_type")
        @Expose
        public List<Data> hairType = null;
        @SerializedName("eye_color")
        @Expose
        public List<Data> eyeColor = null;
        @SerializedName("eye_wear")
        @Expose
        public List<Data> eyeWear = null;
        @SerializedName("makeup")
        @Expose
        public List<Data> makeup = null;
        @SerializedName("secret_weapon")
        @Expose
        public List<Data> secretWeapon = null;
        @SerializedName("style")
        @Expose
        public List<Data> style = null;

    }

    public class SocialStatus {

        @SerializedName("educations")
        @Expose
        public List<Data> educations = null;
        @SerializedName("occupation")
        @Expose
        public List<Data> occupation = null;
        @SerializedName("job_sector")
        @Expose
        public List<Data> jobSector = null;
        @SerializedName("rel_status")
        @Expose
        public List<Data> relStatus = null;
        @SerializedName("kids_list")
        @Expose
        public List<Data> kidsList = null;
        @SerializedName("want_kids_list")
        @Expose
        public List<Data> wantKidsList = null;
        @SerializedName("want_living_list")
        @Expose
        public List<Data> wantLivingList = null;
        @SerializedName("relocateList")
        @Expose
        public List<Data> relocateList = null;

    }

    public class Basic {

        @SerializedName("looking_for")
        @Expose
        public List<Data> lookingFor = null;
    }


    public class Datum {

        @SerializedName("lifestyle")
        @Expose
        public Lifestyle lifestyle;
        @SerializedName("basic")
        @Expose
        public Basic basic;
        @SerializedName("social-status")
        @Expose
        public SocialStatus socialStatus;
        @SerializedName("interests")
        @Expose
        public Interests interests;
        @SerializedName("appearance")
        @Expose
        public Appearance appearance;
    }
}
