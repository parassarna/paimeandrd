package app.pairme.ClassesPojo.LoginPojo;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class LoginPojo implements Serializable {

    @SerializedName("status")
    @Expose
    private String status;

    public String getMessage() {
        return message;
    }

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private Data data;
    private final static long serialVersionUID = 1944452742288347629L;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
