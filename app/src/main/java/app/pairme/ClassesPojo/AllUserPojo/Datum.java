package app.pairme.ClassesPojo.AllUserPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import app.pairme.ClassesPojo.UserInfoPojo.PhotoAlbum;

public class Datum implements Serializable {

    private final static long serialVersionUID = 4282102657872894606L;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("lname")
    @Expose
    private String lname;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("contact")
    @Expose
    private String contact;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("age")
    @Expose
    private String age;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("looking_for")
    @Expose
    private String lookingFor;
    @SerializedName("smoking")
    @Expose
    private String smoking;
    @SerializedName("eating_habits")
    @Expose
    private String eatingHabits;
    @SerializedName("exercise_habits")
    @Expose
    private String exerciseHabits;
    @SerializedName("sleep_habits")
    @Expose
    private String sleepHabits;
    @SerializedName("pets")
    @Expose
    private String pets;
    @SerializedName("family_values")
    @Expose
    private String familyValues;
    @SerializedName("polygamy_opinion")
    @Expose
    private String polygamyOpinion;
    @SerializedName("personality")
    @Expose
    private String personality;
    @SerializedName("languages")
    @Expose
    private String languages;
    @SerializedName("education")
    @Expose
    private String education;
    @SerializedName("occupation")
    @Expose
    private String occupation;
    @SerializedName("job_sector")
    @Expose
    private String jobSector;
    @SerializedName("relationship")
    @Expose
    private String relationship;
    @SerializedName("have_kids")
    @Expose
    private String haveKids;
    @SerializedName("want_kids")
    @Expose
    private String wantKids;
    @SerializedName("living_situation")
    @Expose
    private String livingSituation;
    @SerializedName("willing_to_relocate")
    @Expose
    private String willingToRelocate;
    @SerializedName("appearance")
    @Expose
    private String appearance;
    @SerializedName("photo_album")
    @Expose
    private List<PhotoAlbum> photoAlbum = null;
    @SerializedName("body_type")
    @Expose
    private String bodyType;
    @SerializedName("height")
    @Expose
    private String height;
    @SerializedName("complexion")
    @Expose
    private String complexion;
    @SerializedName("health_status")
    @Expose
    private String healthStatus;
    @SerializedName("hair_color")
    @Expose
    private String hairColor;
    @SerializedName("hair_length")
    @Expose
    private String hairLength;
    @SerializedName("hair_type")
    @Expose
    private String hairType;
    @SerializedName("eye_color")
    @Expose
    private String eyeColor;
    @SerializedName("eye_wear")
    @Expose
    private String eyeWear;
    @SerializedName("makeup")
    @Expose
    private String makeup;
    @SerializedName("facial_hair")
    @Expose
    private String facialHair;
    @SerializedName("secret_weapon")
    @Expose
    private String secretWeapon;
    @SerializedName("style")
    @Expose
    private String style;
    @SerializedName("lives_in")
    @Expose
    private String livesIn;
    @SerializedName("orginally_from")
    @Expose
    private String orginallyFrom;
    @SerializedName("about_me_content")
    @Expose
    private String aboutMeContent;
    @SerializedName("looking_for_content")
    @Expose
    private String lookingForContent;
    @SerializedName("remember_token")
    @Expose
    private String rememberToken;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("complete_profile")
    @Expose
    private String completeProfile;
    @SerializedName("hobbies")
    @Expose
    private String hobbies;
    @SerializedName("cuisine")
    @Expose
    private String cuisine;
    @SerializedName("music")
    @Expose
    private String music;
    @SerializedName("movies")
    @Expose
    private String movies;
    @SerializedName("sports")
    @Expose
    private String sports;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLookingFor() {
        return lookingFor;
    }

    public void setLookingFor(String lookingFor) {
        this.lookingFor = lookingFor;
    }

    public String getSmoking() {
        return smoking;
    }

    public void setSmoking(String smoking) {
        this.smoking = smoking;
    }

    public String getEatingHabits() {
        return eatingHabits;
    }

    public void setEatingHabits(String eatingHabits) {
        this.eatingHabits = eatingHabits;
    }

    public String getExerciseHabits() {
        return exerciseHabits;
    }

    public void setExerciseHabits(String exerciseHabits) {
        this.exerciseHabits = exerciseHabits;
    }

    public String getSleepHabits() {
        return sleepHabits;
    }

    public void setSleepHabits(String sleepHabits) {
        this.sleepHabits = sleepHabits;
    }

    public String getPets() {
        return pets;
    }

    public void setPets(String pets) {
        this.pets = pets;
    }

    public String getFamilyValues() {
        return familyValues;
    }

    public void setFamilyValues(String familyValues) {
        this.familyValues = familyValues;
    }

    public String getPolygamyOpinion() {
        return polygamyOpinion;
    }

    public void setPolygamyOpinion(String polygamyOpinion) {
        this.polygamyOpinion = polygamyOpinion;
    }

    public String getPersonality() {
        return personality;
    }

    public void setPersonality(String personality) {
        this.personality = personality;
    }

    public String getLanguages() {
        return languages;
    }

    public void setLanguages(String languages) {
        this.languages = languages;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getJobSector() {
        return jobSector;
    }

    public void setJobSector(String jobSector) {
        this.jobSector = jobSector;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getHaveKids() {
        return haveKids;
    }

    public void setHaveKids(String haveKids) {
        this.haveKids = haveKids;
    }

    public String getWantKids() {
        return wantKids;
    }

    public void setWantKids(String wantKids) {
        this.wantKids = wantKids;
    }

    public String getLivingSituation() {
        return livingSituation;
    }

    public void setLivingSituation(String livingSituation) {
        this.livingSituation = livingSituation;
    }

    public String getWillingToRelocate() {
        return willingToRelocate;
    }

    public void setWillingToRelocate(String willingToRelocate) {
        this.willingToRelocate = willingToRelocate;
    }

    public String getAppearance() {
        return appearance;
    }

    public void setAppearance(String appearance) {
        this.appearance = appearance;
    }

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getComplexion() {
        return complexion;
    }

    public void setComplexion(String complexion) {
        this.complexion = complexion;
    }

    public String getHealthStatus() {
        return healthStatus;
    }

    public void setHealthStatus(String healthStatus) {
        this.healthStatus = healthStatus;
    }

    public String getHairColor() {
        return hairColor;
    }

    public void setHairColor(String hairColor) {
        this.hairColor = hairColor;
    }

    public String getHairLength() {
        return hairLength;
    }

    public void setHairLength(String hairLength) {
        this.hairLength = hairLength;
    }

    public String getHairType() {
        return hairType;
    }

    public void setHairType(String hairType) {
        this.hairType = hairType;
    }

    public String getEyeColor() {
        return eyeColor;
    }

    public void setEyeColor(String eyeColor) {
        this.eyeColor = eyeColor;
    }

    public String getEyeWear() {
        return eyeWear;
    }

    public void setEyeWear(String eyeWear) {
        this.eyeWear = eyeWear;
    }

    public String getMakeup() {
        return makeup;
    }

    public void setMakeup(String makeup) {
        this.makeup = makeup;
    }

    public String getFacialHair() {
        return facialHair;
    }

    public void setFacialHair(String facialHair) {
        this.facialHair = facialHair;
    }

    public String getSecretWeapon() {
        return secretWeapon;
    }

    public void setSecretWeapon(String secretWeapon) {
        this.secretWeapon = secretWeapon;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getLivesIn() {
        return livesIn;
    }

    public void setLivesIn(String livesIn) {
        this.livesIn = livesIn;
    }

    public String getOrginallyFrom() {
        return orginallyFrom;
    }

    public void setOrginallyFrom(String orginallyFrom) {
        this.orginallyFrom = orginallyFrom;
    }

    public String getAboutMeContent() {
        return aboutMeContent;
    }

    public void setAboutMeContent(String aboutMeContent) {
        this.aboutMeContent = aboutMeContent;
    }

    public String getLookingForContent() {
        return lookingForContent;
    }

    public void setLookingForContent(String lookingForContent) {
        this.lookingForContent = lookingForContent;
    }

    public String getRememberToken() {
        return rememberToken;
    }

    public void setRememberToken(String rememberToken) {
        this.rememberToken = rememberToken;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCompleteProfile() {
        return completeProfile;
    }

    public void setCompleteProfile(String completeProfile) {
        this.completeProfile = completeProfile;
    }

    public String getHobbies() {
        return hobbies;
    }

    public void setHobbies(String hobbies) {
        this.hobbies = hobbies;
    }

    public String getCuisine() {
        return cuisine;
    }

    public void setCuisine(String cuisine) {
        this.cuisine = cuisine;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }

    public String getMovies() {
        return movies;
    }

    public void setMovies(String movies) {
        this.movies = movies;
    }

    public String getSports() {
        return sports;
    }

    public void setSports(String sports) {
        this.sports = sports;
    }

    public List<PhotoAlbum> getPhotoAlbum() {

        return photoAlbum;
    }

}
