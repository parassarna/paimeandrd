package app.pairme.ClassesPojo.AllUserPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import app.pairme.ClassesPojo.UserInfoPojo.Data;

public class AllUserPojo implements Serializable {

    private final static long serialVersionUID = 5041951184571060801L;
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("requestcount")
    @Expose
    public int requestcount; @SerializedName("chatcount")
    @Expose
    public int chatcount;
    @SerializedName("data")
    @Expose
    private List<Data> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

}
