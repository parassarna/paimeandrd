package app.pairme.ClassesPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Packages {
    @SerializedName("success")
    @Expose
    public Boolean success;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;

    public class Datum {

        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("price")
        @Expose
        public String price;
        @SerializedName("discount")
        @Expose
        public String discount;
        @SerializedName("package_type")
        @Expose
        public String packageType;
        @SerializedName("description")
        @Expose
        public String description;
        @SerializedName("status")
        @Expose
        public Integer status;
        @SerializedName("create_date")
        @Expose
        public String createDate;

    }
}