package app.pairme.ClassesPojo.RegisterPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RegisterPojo implements Serializable {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("valid")
    @Expose
    public boolean valid;
    @SerializedName("status")
    @Expose
    public boolean status;
    @SerializedName("email")
    @Expose
    private String email;
    private final static long serialVersionUID = 2715872197870273809L;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
