package app.pairme.ClassesPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class EmailPojo implements Serializable {

    private final static long serialVersionUID = -3045898071518224884L;
    @SerializedName("success")
    @Expose
    public Boolean success;
    @SerializedName("message")
    @Expose
    public String message;

}
