package app.pairme.ClassesPojo.UploadImagePojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Data implements Serializable {

    private final static long serialVersionUID = 6620471026231454347L;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("lname")
    @Expose
    private Object lname;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("contact")
    @Expose
    private Object contact;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("age")
    @Expose
    private Integer age;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("looking_for")
    @Expose
    private String lookingFor;
    @SerializedName("smoking")
    @Expose
    private String smoking;
    @SerializedName("eating_habits")
    @Expose
    private String eatingHabits;
    @SerializedName("exercise_habits")
    @Expose
    private String exerciseHabits;
    @SerializedName("sleep_habits")
    @Expose
    private String sleepHabits;
    @SerializedName("pets")
    @Expose
    private String pets;
    @SerializedName("family_values")
    @Expose
    private String familyValues;
    @SerializedName("polygamy_opinion")
    @Expose
    private String polygamyOpinion;
    @SerializedName("personality")
    @Expose
    private String personality;
    @SerializedName("languages")
    @Expose
    private String languages;
    @SerializedName("education")
    @Expose
    private String education;
    @SerializedName("occupation")
    @Expose
    private String occupation;
    @SerializedName("job_sector")
    @Expose
    private String jobSector;
    @SerializedName("relationship")
    @Expose
    private String relationship;
    @SerializedName("have_kids")
    @Expose
    private String haveKids;
    @SerializedName("want_kids")
    @Expose
    private String wantKids;
    @SerializedName("living_situation")
    @Expose
    private String livingSituation;
    @SerializedName("willing_to_relocate")
    @Expose
    private String willingToRelocate;
    @SerializedName("appearance")
    @Expose
    private String appearance;
    @SerializedName("body_type")
    @Expose
    private Object bodyType;
    @SerializedName("height")
    @Expose
    private Object height;
    @SerializedName("complexion")
    @Expose
    private Object complexion;
    @SerializedName("health_status")
    @Expose
    private Object healthStatus;
    @SerializedName("hair_color")
    @Expose
    private Object hairColor;
    @SerializedName("hair_length")
    @Expose
    private Object hairLength;
    @SerializedName("hair_type")
    @Expose
    private Object hairType;
    @SerializedName("eye_color")
    @Expose
    private Object eyeColor;
    @SerializedName("eye_wear")
    @Expose
    private Object eyeWear;
    @SerializedName("makeup")
    @Expose
    private Object makeup;
    @SerializedName("facial_hair")
    @Expose
    private Object facialHair;
    @SerializedName("secret_weapon")
    @Expose
    private Object secretWeapon;
    @SerializedName("style")
    @Expose
    private Object style;
    @SerializedName("lives_in")
    @Expose
    private String livesIn;
    @SerializedName("orginally_from")
    @Expose
    private String orginallyFrom;
    @SerializedName("about_me_content")
    @Expose
    private String aboutMeContent;
    @SerializedName("looking_for_content")
    @Expose
    private String lookingForContent;
    @SerializedName("remember_token")
    @Expose
    private Object rememberToken;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("complete_profile")
    @Expose
    private Integer completeProfile;
    @SerializedName("hobbies")
    @Expose
    private String hobbies;
    @SerializedName("cuisine")
    @Expose
    private String cuisine;
    @SerializedName("music")
    @Expose
    private String music;
    @SerializedName("movies")
    @Expose
    private String movies;
    @SerializedName("sports")
    @Expose
    private String sports;
    @SerializedName("basic_info_complete_status")
    @Expose
    private Integer basicInfoCompleteStatus;
    @SerializedName("profile_status")
    @Expose
    private Integer profileStatus;
    @SerializedName("description_status")
    @Expose
    private Integer descriptionStatus;
    @SerializedName("social_status_complete")
    @Expose
    private Integer socialStatusComplete;
    @SerializedName("appearance_status")
    @Expose
    private Integer appearanceStatus;
    @SerializedName("lifestyle_status")
    @Expose
    private Integer lifestyleStatus;
    @SerializedName("dp_image_status")
    @Expose
    private Integer dpImageStatus;
    @SerializedName("photo_album_status")
    @Expose
    private Integer photoAlbumStatus;
    @SerializedName("interest_status")
    @Expose
    private Integer interestStatus;
    @SerializedName("subscription_status")
    @Expose
    private Integer subscriptionStatus;
    @SerializedName("packages_id")
    @Expose
    private Integer packagesId;
    @SerializedName("nameUpdatedat")
    @Expose
    private String nameUpdatedat;
    @SerializedName("type")
    @Expose
    private Object type;
    @SerializedName("socket_id")
    @Expose
    private String socketId;
    @SerializedName("facebook_id")
    @Expose
    private Object facebookId;
    @SerializedName("online")
    @Expose
    private String online;
    @SerializedName("user_status")
    @Expose
    private String userStatus;
    @SerializedName("social_login")
    @Expose
    private Integer socialLogin;
    @SerializedName("device_token")
    @Expose
    private String deviceToken;
    @SerializedName("user_create_date")
    @Expose
    private String userCreateDate;
    @SerializedName("nationality")
    @Expose
    private Object nationality;
    @SerializedName("device_type")
    @Expose
    private String deviceType;
    @SerializedName("userscol")
    @Expose
    private Object userscol;
    @SerializedName("confirmation_token")
    @Expose
    private Object confirmationToken;
    @SerializedName("photo_album")
    @Expose
    private List<Object> photoAlbum = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getLname() {
        return lname;
    }

    public void setLname(Object lname) {
        this.lname = lname;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public Object getContact() {
        return contact;
    }

    public void setContact(Object contact) {
        this.contact = contact;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLookingFor() {
        return lookingFor;
    }

    public void setLookingFor(String lookingFor) {
        this.lookingFor = lookingFor;
    }

    public String getSmoking() {
        return smoking;
    }

    public void setSmoking(String smoking) {
        this.smoking = smoking;
    }

    public String getEatingHabits() {
        return eatingHabits;
    }

    public void setEatingHabits(String eatingHabits) {
        this.eatingHabits = eatingHabits;
    }

    public String getExerciseHabits() {
        return exerciseHabits;
    }

    public void setExerciseHabits(String exerciseHabits) {
        this.exerciseHabits = exerciseHabits;
    }

    public String getSleepHabits() {
        return sleepHabits;
    }

    public void setSleepHabits(String sleepHabits) {
        this.sleepHabits = sleepHabits;
    }

    public String getPets() {
        return pets;
    }

    public void setPets(String pets) {
        this.pets = pets;
    }

    public String getFamilyValues() {
        return familyValues;
    }

    public void setFamilyValues(String familyValues) {
        this.familyValues = familyValues;
    }

    public String getPolygamyOpinion() {
        return polygamyOpinion;
    }

    public void setPolygamyOpinion(String polygamyOpinion) {
        this.polygamyOpinion = polygamyOpinion;
    }

    public String getPersonality() {
        return personality;
    }

    public void setPersonality(String personality) {
        this.personality = personality;
    }

    public String getLanguages() {
        return languages;
    }

    public void setLanguages(String languages) {
        this.languages = languages;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getJobSector() {
        return jobSector;
    }

    public void setJobSector(String jobSector) {
        this.jobSector = jobSector;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getHaveKids() {
        return haveKids;
    }

    public void setHaveKids(String haveKids) {
        this.haveKids = haveKids;
    }

    public String getWantKids() {
        return wantKids;
    }

    public void setWantKids(String wantKids) {
        this.wantKids = wantKids;
    }

    public String getLivingSituation() {
        return livingSituation;
    }

    public void setLivingSituation(String livingSituation) {
        this.livingSituation = livingSituation;
    }

    public String getWillingToRelocate() {
        return willingToRelocate;
    }

    public void setWillingToRelocate(String willingToRelocate) {
        this.willingToRelocate = willingToRelocate;
    }

    public String getAppearance() {
        return appearance;
    }

    public void setAppearance(String appearance) {
        this.appearance = appearance;
    }

    public Object getBodyType() {
        return bodyType;
    }

    public void setBodyType(Object bodyType) {
        this.bodyType = bodyType;
    }

    public Object getHeight() {
        return height;
    }

    public void setHeight(Object height) {
        this.height = height;
    }

    public Object getComplexion() {
        return complexion;
    }

    public void setComplexion(Object complexion) {
        this.complexion = complexion;
    }

    public Object getHealthStatus() {
        return healthStatus;
    }

    public void setHealthStatus(Object healthStatus) {
        this.healthStatus = healthStatus;
    }

    public Object getHairColor() {
        return hairColor;
    }

    public void setHairColor(Object hairColor) {
        this.hairColor = hairColor;
    }

    public Object getHairLength() {
        return hairLength;
    }

    public void setHairLength(Object hairLength) {
        this.hairLength = hairLength;
    }

    public Object getHairType() {
        return hairType;
    }

    public void setHairType(Object hairType) {
        this.hairType = hairType;
    }

    public Object getEyeColor() {
        return eyeColor;
    }

    public void setEyeColor(Object eyeColor) {
        this.eyeColor = eyeColor;
    }

    public Object getEyeWear() {
        return eyeWear;
    }

    public void setEyeWear(Object eyeWear) {
        this.eyeWear = eyeWear;
    }

    public Object getMakeup() {
        return makeup;
    }

    public void setMakeup(Object makeup) {
        this.makeup = makeup;
    }

    public Object getFacialHair() {
        return facialHair;
    }

    public void setFacialHair(Object facialHair) {
        this.facialHair = facialHair;
    }

    public Object getSecretWeapon() {
        return secretWeapon;
    }

    public void setSecretWeapon(Object secretWeapon) {
        this.secretWeapon = secretWeapon;
    }

    public Object getStyle() {
        return style;
    }

    public void setStyle(Object style) {
        this.style = style;
    }

    public String getLivesIn() {
        return livesIn;
    }

    public void setLivesIn(String livesIn) {
        this.livesIn = livesIn;
    }

    public String getOrginallyFrom() {
        return orginallyFrom;
    }

    public void setOrginallyFrom(String orginallyFrom) {
        this.orginallyFrom = orginallyFrom;
    }

    public String getAboutMeContent() {
        return aboutMeContent;
    }

    public void setAboutMeContent(String aboutMeContent) {
        this.aboutMeContent = aboutMeContent;
    }

    public String getLookingForContent() {
        return lookingForContent;
    }

    public void setLookingForContent(String lookingForContent) {
        this.lookingForContent = lookingForContent;
    }

    public Object getRememberToken() {
        return rememberToken;
    }

    public void setRememberToken(Object rememberToken) {
        this.rememberToken = rememberToken;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCompleteProfile() {
        return completeProfile;
    }

    public void setCompleteProfile(Integer completeProfile) {
        this.completeProfile = completeProfile;
    }

    public String getHobbies() {
        return hobbies;
    }

    public void setHobbies(String hobbies) {
        this.hobbies = hobbies;
    }

    public String getCuisine() {
        return cuisine;
    }

    public void setCuisine(String cuisine) {
        this.cuisine = cuisine;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }

    public String getMovies() {
        return movies;
    }

    public void setMovies(String movies) {
        this.movies = movies;
    }

    public String getSports() {
        return sports;
    }

    public void setSports(String sports) {
        this.sports = sports;
    }

    public Integer getBasicInfoCompleteStatus() {
        return basicInfoCompleteStatus;
    }

    public void setBasicInfoCompleteStatus(Integer basicInfoCompleteStatus) {
        this.basicInfoCompleteStatus = basicInfoCompleteStatus;
    }

    public Integer getProfileStatus() {
        return profileStatus;
    }

    public void setProfileStatus(Integer profileStatus) {
        this.profileStatus = profileStatus;
    }

    public Integer getDescriptionStatus() {
        return descriptionStatus;
    }

    public void setDescriptionStatus(Integer descriptionStatus) {
        this.descriptionStatus = descriptionStatus;
    }

    public Integer getSocialStatusComplete() {
        return socialStatusComplete;
    }

    public void setSocialStatusComplete(Integer socialStatusComplete) {
        this.socialStatusComplete = socialStatusComplete;
    }

    public Integer getAppearanceStatus() {
        return appearanceStatus;
    }

    public void setAppearanceStatus(Integer appearanceStatus) {
        this.appearanceStatus = appearanceStatus;
    }

    public Integer getLifestyleStatus() {
        return lifestyleStatus;
    }

    public void setLifestyleStatus(Integer lifestyleStatus) {
        this.lifestyleStatus = lifestyleStatus;
    }

    public Integer getDpImageStatus() {
        return dpImageStatus;
    }

    public void setDpImageStatus(Integer dpImageStatus) {
        this.dpImageStatus = dpImageStatus;
    }

    public Integer getPhotoAlbumStatus() {
        return photoAlbumStatus;
    }

    public void setPhotoAlbumStatus(Integer photoAlbumStatus) {
        this.photoAlbumStatus = photoAlbumStatus;
    }

    public Integer getInterestStatus() {
        return interestStatus;
    }

    public void setInterestStatus(Integer interestStatus) {
        this.interestStatus = interestStatus;
    }

    public Integer getSubscriptionStatus() {
        return subscriptionStatus;
    }

    public void setSubscriptionStatus(Integer subscriptionStatus) {
        this.subscriptionStatus = subscriptionStatus;
    }

    public Integer getPackagesId() {
        return packagesId;
    }

    public void setPackagesId(Integer packagesId) {
        this.packagesId = packagesId;
    }

    public String getNameUpdatedat() {
        return nameUpdatedat;
    }

    public void setNameUpdatedat(String nameUpdatedat) {
        this.nameUpdatedat = nameUpdatedat;
    }

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

    public String getSocketId() {
        return socketId;
    }

    public void setSocketId(String socketId) {
        this.socketId = socketId;
    }

    public Object getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(Object facebookId) {
        this.facebookId = facebookId;
    }

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public Integer getSocialLogin() {
        return socialLogin;
    }

    public void setSocialLogin(Integer socialLogin) {
        this.socialLogin = socialLogin;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getUserCreateDate() {
        return userCreateDate;
    }

    public void setUserCreateDate(String userCreateDate) {
        this.userCreateDate = userCreateDate;
    }

    public Object getNationality() {
        return nationality;
    }

    public void setNationality(Object nationality) {
        this.nationality = nationality;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public Object getUserscol() {
        return userscol;
    }

    public void setUserscol(Object userscol) {
        this.userscol = userscol;
    }

    public Object getConfirmationToken() {
        return confirmationToken;
    }

    public void setConfirmationToken(Object confirmationToken) {
        this.confirmationToken = confirmationToken;
    }

    public List<Object> getPhotoAlbum() {
        return photoAlbum;
    }

    public void setPhotoAlbum(List<Object> photoAlbum) {
        this.photoAlbum = photoAlbum;
    }
}
