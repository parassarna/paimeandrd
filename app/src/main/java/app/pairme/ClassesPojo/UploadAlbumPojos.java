package app.pairme.ClassesPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import app.pairme.ClassesPojo.UserInfoPojo.PhotoAlbum;

public class UploadAlbumPojos {
    @SerializedName("success")
    @Expose
    public String status;
    @SerializedName("photo_album")
    @Expose
    public List<PhotoAlbum> photoAlbum = null;
}
