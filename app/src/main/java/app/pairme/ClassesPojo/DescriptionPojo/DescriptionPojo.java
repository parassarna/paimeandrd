package app.pairme.ClassesPojo.DescriptionPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import app.pairme.ClassesPojo.UserInfoPojo.Data;

public class DescriptionPojo implements Serializable {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private Data data;
    private final static long serialVersionUID = 6128669199590127195L;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

}
