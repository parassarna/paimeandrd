package app.pairme.ClassesPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import app.pairme.ClassesPojo.UserInfoPojo.Data;

public class RequestPojo {


    @SerializedName("success")
    @Expose
    public Integer success;
    @SerializedName("liked_you")
    @Expose
    public List<Data> likedYou = null;
    @SerializedName("checked")
    @Expose
    public List<Data> checked = null;
    @SerializedName("messaged_you")
    @Expose
    public List<Data> messaged_you = null;

    public class data {

        private final static long serialVersionUID = -7789379718751247315L;
        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("age")
        @Expose
        public String age;
        @SerializedName("lives_in")
        @Expose
        public String livesIn;
        @SerializedName("profile_image")
        @Expose
        public String pic;

    }
}
