
package app.pairme.ClassesPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import app.pairme.ClassesPojo.UserInfoPojo.Data;
import app.pairme.ClassesPojo.UserInfoPojo.UserInfo;

public class Matches implements Serializable
{

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    public List<Data> data = null;
    private final static long serialVersionUID = 1911411627338321467L;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
