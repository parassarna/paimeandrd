package app.pairme.ClassesPojo;

public class Message {
    private String from_user_id, message, to_user_id, ip;

    public Message(String from_user_id, String message, String to_user_id, String ip) {
        this.from_user_id = from_user_id;
        this.message = message;
        this.to_user_id = to_user_id;
        this.ip = ip;
    }

    public String getFrom_user_id() {
        return from_user_id;
    }

    public void setFrom_user_id(String from_user_id) {
        this.from_user_id = from_user_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTo_user_id() {
        return to_user_id;
    }

    public void setTo_user_id(String to_user_id) {
        this.to_user_id = to_user_id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
