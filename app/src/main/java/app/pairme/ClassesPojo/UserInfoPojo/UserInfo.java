
package app.pairme.ClassesPojo.UserInfoPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserInfo implements Serializable
{

    @SerializedName("success")
    @Expose
    private Boolean success;

    public String getToken() {
        return token;
    }

    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("data")
    @Expose
    private Data data;
    private final static long serialVersionUID = 1911411627338321467L;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
