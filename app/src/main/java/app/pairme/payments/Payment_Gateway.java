package app.pairme.payments;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.PaymentIntentResult;
import com.stripe.android.Stripe;
import com.stripe.android.model.ConfirmPaymentIntentParams;
import com.stripe.android.model.PaymentIntent;
import com.stripe.android.model.PaymentMethodCreateParams;
import com.stripe.android.view.CardInputWidget;

import org.json.JSONException;

import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.BankAccountsPojo.BankAccountsPojo;
import app.pairme.R;
import app.pairme.adapters.BankAccountsAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Payment_Gateway extends AppCompatActivity implements View.OnClickListener {
    public static final int PAYPAL_REQUEST_CODE = 0;
    CardView card_cv,
            paypal_cv,
            bank_cv;
    String amt;
    EditText card_number,
            security_et;
    int Cardmonth,
            CardYear;
    CardInputWidget cardInputWidget;
    TextView pay, ed_et, amt_tv;
    LinearLayout card_ll, card_bank;
    String PAYPAL_CLIENT_ID = "AbNUSYQ_UBYhwqfGf8G47CTK-M_EgojVCKwN5HSBT2hHICUO30Ry5oGsS3Ejz9fgbF_8hnpnfpekiW3V";
    RecyclerView rv_accounts;
    List<String> name = new ArrayList<>();
    List<String> image = new ArrayList<>();
    List<String> account = new ArrayList<>();
    List<String> iban = new ArrayList<>();
    TextView period_tv;
    private PayPalConfiguration config = new PayPalConfiguration().environment(PayPalConfiguration.ENVIRONMENT_SANDBOX).clientId(PAYPAL_CLIENT_ID);
    private String paymentIntentClientSecret;
    private Stripe stripe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment__gateway);
        amt = getIntent().getStringExtra("amt");
        Toast.makeText(this, amt, Toast.LENGTH_SHORT).show();
        Toast.makeText(this, getIntent().getStringExtra("period"), Toast.LENGTH_SHORT).show();
        Log.d("TAG", "onCreate: " + amt);
        Log.d("tag", "onCreate_peroid: " + getIntent().getStringExtra("period"));
        getStriprclient();
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
        amt_tv = findViewById(R.id.amt_tv);
        rv_accounts = findViewById(R.id.rv_accounts);
        amt_tv.setText("SAR" + amt);
        card_cv = findViewById(R.id.card_cv);
        period_tv = findViewById(R.id.period_tv);
        paypal_cv = findViewById(R.id.paypal_cv);
        cardInputWidget = findViewById(R.id.cardInputWidget);
        bank_cv = findViewById(R.id.bank_cv);
        pay = findViewById(R.id.pay_tv);
        card_ll = findViewById(R.id.card_ll);
        card_bank = findViewById(R.id.card_bank);
        pay.setOnClickListener(this);
        card_cv.setOnClickListener(this);
        paypal_cv.setOnClickListener(this);
        bank_cv.setOnClickListener(this);
        period_tv.setText(getIntent().getStringExtra("period") + " " + getString(R.string.months));

    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    public void getBankAccounts() {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<BankAccountsPojo> call = service.getBankAccounts("Bearer " + Helper.getToken(Payment_Gateway.this));
        call.enqueue(new Callback<BankAccountsPojo>() {
            @Override
            public void onResponse(Call<BankAccountsPojo> call, Response<BankAccountsPojo> response) {
                name.clear();
                iban.clear();
                image.clear();
                account.clear();
                if (response.isSuccessful()) {
                    for (int i = 0; i < response.body().getData().size(); i++) {
                        name.add(response.body().getData().get(i).getName());
                        iban.add(response.body().getData().get(i).getIban());
                        image.add(response.body().getData().get(i).getImage());
                        account.add(response.body().getData().get(i).getAccountNo());
                    }
                    LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(Payment_Gateway.this, LinearLayoutManager.HORIZONTAL, false);
                    rv_accounts.setLayoutManager(horizontalLayoutManagaer);
                    rv_accounts.setAdapter(new BankAccountsAdapter(Payment_Gateway.this, name, iban, image, account));
                } else {
                    Toast.makeText(Payment_Gateway.this, "Failure", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BankAccountsPojo> call, Throwable t) {
                Log.d("error", "Failure; " + t);
                Toast.makeText(Payment_Gateway.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void onBuyPressed() {
        PayPalPayment payment = new PayPalPayment(new BigDecimal(amt), "USD", "Payable Amount",
                PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);
        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
            stripe.onPaymentResult(requestCode, data, new PaymentResultCallback(this));
        if (requestCode == 0)
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        OnlineTransfer(confirm.getProofOfPayment().getTransactionId(), "Paypal");
                        AlertDialog.Builder builder = new AlertDialog.Builder(Payment_Gateway.this);
                        builder.setMessage("Your amount has been paid successfully.")
                                .setCancelable(false).setNegativeButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                        Log.i("paymentExample", confirm.toJSONObject().toString(4));

                        // TODO: send 'confirm' to your server for verification.
                        // see https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                        // for more details.

                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pay_tv:
                if (paypal_cv.getBackground() != null)
                    onBuyPressed();
                else if (card_cv.getBackground() != null) {
                    startstripe();
                }
                break;
            case R.id.card_cv:
                card_cv.setBackground(getResources().getDrawable(R.drawable.background_gradiant));
                paypal_cv.setBackground(null);
                bank_cv.setBackground(null);
                card_ll.setVisibility(View.VISIBLE);
                card_bank.setVisibility(View.GONE);
                pay.setVisibility(View.VISIBLE);
                break;
            case R.id.paypal_cv:
                card_cv.setBackground(null);
                paypal_cv.setBackground(getResources().getDrawable(R.drawable.background_gradiant));
                bank_cv.setBackground(null);
                card_ll.setVisibility(View.GONE);
                card_bank.setVisibility(View.GONE);
                pay.setVisibility(View.VISIBLE);
                break;
            case R.id.bank_cv:
                getBankAccounts();
                card_cv.setBackground(null);
                paypal_cv.setBackground(null);
                bank_cv.setBackground(getResources().getDrawable(R.drawable.background_gradiant));
                card_bank.setVisibility(View.VISIBLE);
                card_ll.setVisibility(View.GONE);
                pay.setVisibility(View.GONE);
                break;
        }
    }

    private void startstripe() {
        InputMethodManager inputMethodManager =
                (InputMethodManager) getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                getCurrentFocus().getWindowToken(), 0);
        PaymentMethodCreateParams params = cardInputWidget.getPaymentMethodCreateParams();
        if (params != null) {
            ConfirmPaymentIntentParams confirmParams = ConfirmPaymentIntentParams
                    .createWithPaymentMethodCreateParams(params, paymentIntentClientSecret);
            final Context context = getApplicationContext();
            stripe = new Stripe(context, PaymentConfiguration.getInstance(context).getPublishableKey()
            );
            stripe.confirmPayment(this, confirmParams);
        }
    }

/*
    private void date() {
        final Calendar c = Calendar.getInstance();
        CardYear = c.get(Calendar.YEAR);
        Cardmonth = c.get(Calendar.MONTH);

        DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(getApplicationContext(), new DatePickerPopWin.OnDatePickedListener() {
            @Override
            public void onDatePickCompleted(int year, String month, String dateDesc) {
                CardYear = year;
                Cardmonth = month;
                if (isExpiryDate())
                    ed_et.setText(month + "/" + year);
                else
                    Toast.makeText(Payment_Gateway.this, "Please enter valid expiry date", Toast.LENGTH_SHORT).show();
            }


        }).textConfirm("Confirm") //text of confirm button
                .textCancel("Cancel") //text of cancel button
                .btnTextSize(16) // button text size
                .viewTextSize(25) // pick view text size
                .colorCancel(Color.parseColor("#999999")) //color of cancel button
                .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                .minYear(c.get(Calendar.YEAR)) //min year in loop
                //min year in loop
                .maxYear(2550) // max year in loop
                // date chose when init popwindow
                .showDayMonthYear(true)
                .build();
        pickerPopWin.showPopWin(Payment_Gateway.this);


    }
*/

    public void OnlineTransfer(String transactionId, String method) {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);

        service.OnlineTransfer(Helper.getUserData(getApplicationContext()).getId().toString(), getIntent().getStringExtra("package_id"), method, amt, transactionId, "Bearer " + Helper.getToken(Payment_Gateway.this)).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body().get("success").getAsBoolean()) {
                        Log.d("transactionResponse", "onResponse: " + "Success");
                        finish();
                    } else {
                        Log.d("transactionResponse", "onResponse: " + "Failed");
                    }

                } else {
                    Toast.makeText(Payment_Gateway.this, response.errorBody().toString(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                Toast.makeText(Payment_Gateway.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void getStriprclient() {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        service.getStripeClient(amt, "Bearer " + Helper.getToken(Payment_Gateway.this)).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {

                    if (response.body().get("success").getAsBoolean()) {
                        paymentIntentClientSecret = response.body().get("data").getAsString();
                    } else {
                        Log.d("transactionResponse", "onResponse: " + "Failed");

                    }

                } else {
                    Toast.makeText(Payment_Gateway.this, response.errorBody().toString(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                Toast.makeText(Payment_Gateway.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private boolean isExpiryDate() {
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);

        int e_month = Cardmonth;
        int e_year = CardYear;


        if (year < e_year) {
            return true;
        } else if (year == e_year) {
            if (month < e_month) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }


    }

    private boolean isValidate() {
        if (card_number.getText().toString().trim().isEmpty()) {
            card_number.setError("Enter Card number");
            card_number.requestFocus();
            return false;
        }
      /*  if (etCardNumber.getText().length() != 16) {
            etCardNumber.setError( "Card can't be less than 16 digits" );
            etCardNumber.requestFocus();
            return false;
        }*/

        if (security_et.getText().toString().trim().isEmpty()) {
            security_et.setError("Enter Card CVC");
            security_et.requestFocus();
            return false;
        }

        if (security_et.getText().toString().length() != 3) {
            security_et.setError("Card can't be less than 3 digits");
            security_et.requestFocus();
            return false;
        }


        return true;
    }

    private final class PaymentResultCallback
            implements ApiResultCallback<PaymentIntentResult> {
        @NonNull
        private final WeakReference<Payment_Gateway> activityRef;

        PaymentResultCallback(@NonNull Payment_Gateway activity) {
            activityRef = new WeakReference<>(activity);
        }

        @Override
        public void onSuccess(@NonNull PaymentIntentResult result) {
            final Payment_Gateway activity = activityRef.get();
            if (activity == null) {
                return;
            }

            PaymentIntent paymentIntent = result.getIntent();
            PaymentIntent.Status status = paymentIntent.getStatus();
            if (status == PaymentIntent.Status.Succeeded) {
                OnlineTransfer(paymentIntent.getId(), "stripe");
                Toast.makeText(activity, "Cogratulation you have subscribed with us", Toast.LENGTH_SHORT).show();

            } else if (status == PaymentIntent.Status.RequiresPaymentMethod) {
                // Payment failed
                Toast.makeText(activity, Objects.requireNonNull(paymentIntent.getLastPaymentError()).getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onError(@NonNull Exception e) {
            final Payment_Gateway activity = activityRef.get();
            if (activity == null) {
                return;
            }

            Toast.makeText(activity, e.toString(), Toast.LENGTH_SHORT).show();
            // Payment request failed – allow retrying using the same payment method
        }
    }
}
