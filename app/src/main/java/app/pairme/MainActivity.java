package app.pairme;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import java.util.Locale;

import app.pairme.ApiServices.ApiInterface;
import app.pairme.ApiServices.BaseUrl;
import app.pairme.ApiServices.Helper;
import app.pairme.ClassesPojo.UserInfoPojo.UserInfo;
import app.pairme.fragments.ChatFragment;
import app.pairme.fragments.DiscoverFragment;
import app.pairme.fragments.MenuFragment;
import app.pairme.fragments.RequestFragment;
import app.pairme.fragments.SearchFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static SharedPreferences sharedPreferences;
    public static String user_id;
    public static String user_name;
    public static String user_pic;
    public static String birthday;
    public static String token;
    public LinearLayout linear_discover_inactive, linear_discover_active, linear_search_inactive, linear_search_active,
            linear_request_inactive, linear_request_active, linear_chat_inactive, linear_chat_active,
            linear_menu_inactive, linear_menu_active;
    public TextView badge, badge_active, badgechat, badgechat_active;
    String currentLanguage = "en";
    Locale myLocale;
    FragmentManager manager = getSupportFragmentManager();
    FirebaseAnalytics mFirebaseAnalytics;
    RelativeLayout badge_layout1, badge_layout2, badge_layout3, badge_layout4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.isArabic = Helper.getLanguage(this).equalsIgnoreCase("en");
        Log.d("TAG", "onCreate: " + Utils.isArabic);
        setContentView(R.layout.activity_main);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        setLocale(Helper.getLanguage(MainActivity.this));
        initScreen();
        getUserdata();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private void initScreen() {
        badge = findViewById(R.id.badge);
        badge_layout3 = findViewById(R.id.badge_layout3);
        badgechat = findViewById(R.id.badgechat);
        badge_layout4 = findViewById(R.id.badge_layout4);
        badgechat_active = findViewById(R.id.badgechat_active);
        badge_active = findViewById(R.id.badge_active);
        badge_layout1 = findViewById(R.id.badge_layout1);
        badge_layout2 = findViewById(R.id.badge_layout2);
        linear_discover_inactive = findViewById(R.id.linear_discover_inactive);
        linear_discover_active = findViewById(R.id.linear_discover_active);
        linear_search_inactive = findViewById(R.id.linear_search_inactive);
        linear_search_active = findViewById(R.id.linear_search_active);
        linear_request_inactive = findViewById(R.id.linear_request_inactive);
        linear_request_active = findViewById(R.id.linear_request_active);
        linear_chat_inactive = findViewById(R.id.linear_chat_inactive);
        linear_chat_active = findViewById(R.id.linear_chat_active);
        linear_menu_inactive = findViewById(R.id.linear_menu_inactive);
        linear_menu_active = findViewById(R.id.linear_menu_active);

        linear_discover_inactive.setOnClickListener(this);
        linear_discover_active.setOnClickListener(this);
        linear_search_inactive.setOnClickListener(this);
        linear_search_active.setOnClickListener(this);
        linear_request_inactive.setOnClickListener(this);
        linear_request_active.setOnClickListener(this);
        linear_chat_inactive.setOnClickListener(this);
        linear_chat_active.setOnClickListener(this);
        linear_menu_inactive.setOnClickListener(this);
        linear_menu_active.setOnClickListener(this);

        linear_discover_active.setVisibility(View.VISIBLE);
        linear_discover_inactive.setVisibility(View.GONE);
        manager.beginTransaction().replace(R.id.frame, new DiscoverFragment()).commit();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linear_discover_inactive:
                linear_discover_inactive.setVisibility(View.GONE);
                linear_discover_active.setVisibility(View.VISIBLE);
                linear_search_inactive.setVisibility(View.VISIBLE);
                linear_search_active.setVisibility(View.GONE);
                badge_layout1.setVisibility(View.VISIBLE);
                badge_layout2.setVisibility(View.GONE);
                badge_layout3.setVisibility(View.VISIBLE);
                badge_layout4.setVisibility(View.GONE);
                linear_menu_inactive.setVisibility(View.VISIBLE);
                linear_menu_active.setVisibility(View.GONE);
                manager.popBackStack();
                manager.beginTransaction().replace(R.id.frame, new DiscoverFragment()).commit();
                break;
            case R.id.linear_search_inactive:
                linear_discover_inactive.setVisibility(View.VISIBLE);
                linear_discover_active.setVisibility(View.GONE);
                linear_search_inactive.setVisibility(View.GONE);
                linear_search_active.setVisibility(View.VISIBLE);
                badge_layout1.setVisibility(View.VISIBLE);
                badge_layout2.setVisibility(View.GONE);
                badge_layout3.setVisibility(View.VISIBLE);
                badge_layout4.setVisibility(View.GONE);
                linear_chat_inactive.setVisibility(View.VISIBLE);
                linear_chat_active.setVisibility(View.GONE);
                linear_menu_inactive.setVisibility(View.VISIBLE);
                linear_menu_active.setVisibility(View.GONE);
                manager.popBackStack();
                manager.beginTransaction().replace(R.id.frame, new SearchFragment()).commit();
                break;
            case R.id.linear_request_inactive:
                linear_discover_inactive.setVisibility(View.VISIBLE);
                linear_discover_active.setVisibility(View.GONE);
                linear_search_inactive.setVisibility(View.VISIBLE);
                linear_search_active.setVisibility(View.GONE);
                badge_layout1.setVisibility(View.GONE);
                badge_layout2.setVisibility(View.VISIBLE);
                badge_layout3.setVisibility(View.VISIBLE);
                badge_layout4.setVisibility(View.GONE);
                linear_menu_inactive.setVisibility(View.VISIBLE);
                linear_menu_active.setVisibility(View.GONE);
                manager.beginTransaction().replace(R.id.frame, new RequestFragment()).commit();
                break;
            case R.id.linear_chat_inactive:
                linear_discover_inactive.setVisibility(View.VISIBLE);
                linear_discover_active.setVisibility(View.GONE);
                linear_search_inactive.setVisibility(View.VISIBLE);
                linear_search_active.setVisibility(View.GONE);
                badge_layout1.setVisibility(View.VISIBLE);
                badge_layout2.setVisibility(View.GONE);
                badge_layout3.setVisibility(View.GONE);
                badge_layout4.setVisibility(View.VISIBLE);
                linear_menu_inactive.setVisibility(View.VISIBLE);
                linear_menu_active.setVisibility(View.GONE);
                manager.beginTransaction().replace(R.id.frame, new ChatFragment()).commit();
                break;
            case R.id.linear_menu_inactive:
                linear_discover_inactive.setVisibility(View.VISIBLE);
                linear_discover_active.setVisibility(View.GONE);
                linear_search_inactive.setVisibility(View.VISIBLE);
                linear_search_active.setVisibility(View.GONE);
                badge_layout1.setVisibility(View.VISIBLE);
                badge_layout2.setVisibility(View.GONE);
                badge_layout3.setVisibility(View.VISIBLE);
                badge_layout4.setVisibility(View.GONE);
                linear_menu_inactive.setVisibility(View.GONE);
                linear_menu_active.setVisibility(View.VISIBLE);
                manager.beginTransaction().replace(R.id.frame, new MenuFragment()).commit();
                break;

        }
    }

    private void getUserdata() {
        ApiInterface service = BaseUrl.createService(ApiInterface.class);
        Call<UserInfo> call = service.getUserInfo("Bearer " + Helper.getToken(MainActivity.this), Helper.getMyId(MainActivity.this));
        call.enqueue(new Callback<UserInfo>() {

            @Override
            public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                if (response.isSuccessful()) {
                    Helper.setUserData(new Gson().toJson(response.body().getData()), MainActivity.this);
                } else {
                    Log.d("error", "else: " + response.body());
                }
            }

            @Override
            public void onFailure(Call<UserInfo> call, Throwable t) {
                Log.d("error", "Failure; " + t);
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void setLocale(String localeName) {
        if (!localeName.equals(currentLanguage)) {
            myLocale = new Locale(localeName);
            Resources res = getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.locale = myLocale;
            res.updateConfiguration(conf, dm);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getIntent().hasExtra("type")) {
            String type = getIntent().getStringExtra("type");
            if (type.equalsIgnoreCase("matched")) {
                linear_chat_inactive.performClick();
                linear_chat_active.performClick();
            } else {
                linear_request_inactive.performClick();
                linear_request_active.performClick();
            }
        }
    }

}
