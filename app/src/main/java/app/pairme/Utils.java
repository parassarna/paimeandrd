package app.pairme;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.google.gson.JsonObject;

import app.pairme.ApiServices.Helper;

public final class Utils {

    public static JsonObject localisation = new JsonObject();
    public static String selected = null;
    public static boolean isArabic = false;
    public static boolean valid = false;
    public static boolean confirmed = false;
    public static boolean multiple = false;
    public static boolean language = false;

    static String[] nationalities = {"Choose from menu", "Moroccan", "Palestinian", "Bahraini", "Egyptian", "Emirati", "Indonesian", "Iranian", "Iraqi", "Jordanian", "Kuwaiti", "Lebanese", "Libyan", "Mauritanian", "Omani", "diagonal", "Saudi", "Sudanese", "Syrian", "Tunisian", "Turkish", "Yemeni", "algerian"};

    private Utils() {
    }

    public static float dp2px(Resources resources, float dp) {
        final float scale = resources.getDisplayMetrics().density;
        return dp * scale + 0.5f;
    }

    public static float sp2px(Resources resources, float sp) {
        final float scale = resources.getDisplayMetrics().scaledDensity;
        return sp * scale;
    }

    public static String setData(Activity activity, String datas) {
        StringBuilder final_key = null;
        if (datas != null) {
            final_key = new StringBuilder(datas);
            if (Helper.getLanguage(activity).equalsIgnoreCase("ar")) {
                String[] data = datas.split(",");
                for (int i = 0; i < data.length; i++) {
                    if (i == 0)
                        final_key = new StringBuilder(Utils.localisation.get(data[i]).getAsString().trim());
                    else
                        final_key.append(",").append(Utils.localisation.get(data[i]).getAsString().trim());
                }
            }
        }
        return final_key == null ? "" : final_key.toString();
    }

    public static String getArabic(Activity activity, String val) {
        String key = val;
        try {
            if (val != null)
                if (Helper.getLanguage(activity).equalsIgnoreCase("ar") && Utils.localisation.has(val.trim())) {
                    key = Utils.localisation.get(val.trim()).getAsString();
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return key;
    }

    public static void flip(ImageView imageView) {
        imageView.setRotation(180);
    }

    public static void flipview(Activity activity, LinearLayout linearLayout) {
        if (Helper.getLanguage(activity).equalsIgnoreCase("ar"))
            linearLayout.setScaleX(-1);
    }

    public static String getNationality(int pos) {

        return nationalities[pos];

    }

    public static CircularProgressDrawable imageloader(Context context) {
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();

        return circularProgressDrawable;
    }

    public static Uri getImageUri(Activity activity, Bitmap inImage) {
        String path = MediaStore.Images.Media.insertImage(activity.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static String getRealPathFromURI(Activity activity, Uri uri) {
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = activity.getContentResolver().query(uri, filePathColumn, null, null, null);
        if (cursor == null) {
            return uri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }

    }
}